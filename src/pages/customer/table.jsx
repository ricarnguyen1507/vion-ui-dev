import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import api from 'services/api_cms'

function getFilterStr (query) {
  const { filters } = query
  var arrfilter = []
  filters.map(({value, column: {field}}) => {
    let reSearch = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
    return arrfilter.push(`regexp(${field},/${reSearch}/)`)
  })
  return { strFilter: arrfilter.join(' AND ')}
}


export default function TableCustomer ({ ctx, genders, mapTypes, controlEditTable, setControlEditTable }) {
  const classes = useStyles()
  // const [dataTable, setDataTable] = useState(null)
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0
  }))
  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const { strFilter } = getFilterStr(query)
    return api.get("/list/customer", {params: {
      pageSize: query.pageSize,
      page: query.page,
      ...(strFilter && {filter: ' AND ' + strFilter}),
    }})
      .then(response => {
        const { summary: [{totalCount}], result } = response.data
        // setDataTable(result);
        return {
          data: result,
          page: query.page,
          pageSize: query.pageSize,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  function getAddressTypeName (type_value) {
    const type = mapTypes ? mapTypes[type_value] : ""
    return type ? type.type_name : ""
  }
  const [selectedRow, setSelectedRow] = useState(null)
  const [column] = useState(() => {
    const column = [
      {
        title: "Customer Name",
        field: "customer_name",
        editable: "onUpdate",
        cellStyle: {
          width: 20,
          maxWidth: 20
        },
        headerStyle: {
          width: 20,
          maxWidth: 20
        }
      },
      {
        title: "Gender",
        field: "gender",
        editable: "onUpdate",
        filtering: false,
        render: (rowData) => <div>{genders[rowData.gender]}</div>
      },
      { title: "Phone Number", field: "phone_number", editable: "onUpdate" },
      { title: "Email", field: "email", editable: "onUpdate" },
      {
        title: "Address",
        field: "address",
        filtering: false,
        render: rowData => getAddressTypeName(rowData.address ? rowData.address.address_type : '')
      },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Customer</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Customer</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.editData() }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { dataTable &&
                                            <ExportExcel data={dataTable} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),

        }}
        actions={[
          // {
          //     icon: AddBox,
          //     tooltip: 'Add Customer',
          //     isFreeAction: true,
          //     onClick: () => {
          //         functionEditData('Add')
          //     }
          // },
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Customer",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.editData(rowData)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}