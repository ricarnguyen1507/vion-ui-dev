import React, { useState } from 'react'
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import RoleButton from 'components/Role/Button';
import Button from "@material-ui/core/Button"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { clone, diff_customer, diffAddress } from 'services/diff'
import useGeneral from 'pages/style_general'
import api from 'services/api_cms'
// import ExportExcel from "./export_excel"
import ShippingList from './shipping_list'

export default function ({ originData, actionType, genders, addresstypes, mapTypes, functionBack, handleSubmitData, listProvinces }) {
  const classge = useGeneral()
  const [dataEdit, setDataEdit] = useState(() => {
    if (actionType === 'Add') {
      return {
        uid: '_:new_customer',
        address: {
          address_des: '',
          address_type: null
        }/* ,
                alt_name: [],
                alt_email: [],
                alt_phone: [],
                alt_info: [] */
      }
    }
    if (actionType === 'Edit') {
      /* listAltFields.forEach(x => {
                if(!originData[x]) {
                    originData[x] = []
                }
            }) */
      return clone(originData)
    }
  })

  /* const onAddItem = (e, field) => {
    e.preventDefault()
    e.stopPropagation()

    if(field.includes('_address') === true) {
      dataEdit[field].push({
        address_type: null,
        address_des: ''
      })
      setDataEdit({...dataEdit})
    } else {
      dataEdit[field].push('')
      setDataEdit({...dataEdit})
    }
  }

  const onDeleteItem = (e, field, idx) => {
    e.preventDefault()
    e.stopPropagation()
    dataEdit[field].splice(idx, 1)
    setDataEdit({...dataEdit})
  }
  const updateAltItem = (field, childField, data, index) => {
    if(field.includes('_address') === true && childField) {
      dataEdit[field][index][childField] = data
      setDataEdit({...dataEdit})
    }
    else{
      dataEdit[field][index] = data
      setDataEdit({...dataEdit})
    }
  } */


  // -------------------------------------------------------------------------
  // Handle Customer
  // -------------------------------------------------------------------------
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setDataEdit({ ...dataEdit, [name]: value })
  }
  const handleChangeCustomerGender = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'gender': genders.indexOf(item) })
  }

  const handleSubmit = function () {
    let submitData = actionType === 'Edit' ? diff_customer(originData, dataEdit, dataEdit.uid) : { set: dataEdit }
    let delAddr = {}

    if (dataEdit.address && dataEdit.address.uid) {
      api.get(`/address/${dataEdit.address && dataEdit.address.uid}`)
        .then(({data}) => {
          let oldAddress = {}
          if (data.result) {
            oldAddress = data.result[0] || {}
          }
          const diffAddrData = diffAddress(oldAddress, dataEdit.address, address.uid)
          if (diffAddrData.set && diffAddrData.set.province) {
            dataEdit.address.province = diffAddrData.set.province
          } else {
            delete dataEdit.address.province
          }
          if (diffAddrData.set && diffAddrData.set.district) {
            dataEdit.address.district = diffAddrData.set.district
          } else {
            delete dataEdit.address.district
          }

          if (diffAddrData.del) {
            delAddr = {
              uid: address.uid,
              province: diffAddrData.del?.province || {},
              district: diffAddrData.del?.district || {}
            }
          }

          submitData = {
            ...submitData,
            set: {
              ...submitData.set,
              uid: dataEdit.uid,
              address: {
                ...dataEdit.address
              }
            },
            del: {
              ...delAddr
            }
          }
          console.log("submitData", submitData)
          handleSubmitData(actionType, submitData)
        })
    } else {
      handleSubmitData(actionType, submitData)
    }

    // submitData.set = {
    //     ...submitData.set,
    //     address: address
    // }

    // let delAddress = {uid: dataEdit.address.uid, province: {uid: dataEdit.address.province && dataEdit.address.province.uid}, district: {uid: dataEdit.address.district && dataEdit.address.district.uid} }
    // submitData.delAddress = delAddress


  }

  const handleChangeAddressType = (e, type) => {
    e.preventDefault()
    const val = (type && type.type_value) || ""
    setDataEdit({
      ...dataEdit, address:
            {
              ...dataEdit.address,
              address_type: val,
            }
    })
  }


  const [listDistricts, setListDistricts] = useState(() => {
    const province_uid = dataEdit?.address?.province?.uid
    if(province_uid) {
      return listProvinces?.find(p => p.uid === province_uid)?.areas || []
    }
    return []
  })
  const [address, setAddress] = useState(() => ({
    ...dataEdit?.address,
    province: {uid: dataEdit?.address?.province?.uid},
    district: {uid: dataEdit?.address?.district?.uid}
  }))

  const handleChangeAddressDes = (e) => {
    e.preventDefault()
    setDataEdit({
      ...dataEdit, address:
            {
              ...dataEdit.address,
              address_des: e.target.value
            }
    })
    setAddress({
      ...address,
      address_des: e.target.value
    })
  }

  const handleChangeAddressProvince = (e, item) => {
    e.preventDefault()
    setAddress({
      ...address,
      province: {
        uid: item?.uid || null
      }
    })
    setDataEdit({
      ...dataEdit, address: {
        ...dataEdit.address,
        province: {
          uid: item?.uid || null
        }
      }
    })
    if (item) {
      setListDistricts(listProvinces.find(p => p.uid === item.uid).areas || [])
    }
  }
  const handleChangeAddressDistrict = (e, item) => {
    e.preventDefault()
    setAddress({
      ...address,
      district: {
        uid: item?.uid || null
      }
    })
    setDataEdit({
      ...dataEdit, address: {
        ...dataEdit.address,
        district: {
          uid: item?.uid || null
        }
      }
    })
  }


  //uupdate thoong tin giao hang
  const updateShippingData = (e, index) => {
    if(dataEdit.alt_info) {
      const temp = [...dataEdit.alt_info].map((item, idx) => {
        if(index === idx) {
          return e;
        }
        return item;
      });
      setDataEdit({
        ...dataEdit,
        alt_info: temp
      })
    }
  }

  const addShippingData = () => {
    setDataEdit({
      ...dataEdit,
      alt_info: [...(dataEdit.alt_info || []), {
        uid: "_:new_alt_info",
        address: {}
      }]
    })
  }

  const handleChangeDefaultAddress = (e, index) => {
    const temp = {...dataEdit};
    temp.alt_info = temp.alt_info.map((item, idx) => {
      if(idx === index && e.target.checked) {
        item.is_default_address = e.target.checked
      }else {
        item.is_default_address = false;
      }
      return item;
    })

    setDataEdit(temp)
  }
  // View
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  // const [clonedData] = useState(() => [clone(dataEdit)]);
  return (
    <>
      <PageTitle title="Customer" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={8} lg={8}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    id="customerName"
                    label="Name Customer"
                    variant="outlined"
                    margin="dense"
                    name="customer_name"
                    onChange={handleChange}
                    value={dataEdit?.customer_name || ''}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="email"
                    name="email"
                    label="Email"
                    type="text"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit?.email || ''}
                    validators={[
                      'maxStringLength:30',
                      'matchRegexp:^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$'
                    ]}
                    errorMessages={[
                      'Max length is 50',
                      'Wrong email format'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="password"
                    name="password"
                    label= "Password"
                    type="password"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit?.oldpassword || dataEdit?.password}
                    validators={[
                      'matchRegexp:^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
                      // 'maxStringLength:30',

                      // The string must contain at least 1 lowercase alphabetical character.
                      // The string must contain at least 1 uppercase alphabetical character.
                      // The string must contain at least 1 numeric character.
                      // The string must contain at least one special character.
                      // The string must be eight characters or longer.
                    ]}
                    errorMessages={[
                      // 'Max length is 30',
                      'Wrong password format'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="address"
                    label="Nhập địa chỉ (bao gồm phường xã)"
                    type="text"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={handleChangeAddressDes}
                    value={dataEdit?.address && dataEdit?.address.address_des ? dataEdit?.address.address_des : ''}
                  />
                </Grid>

                <Grid item xs={12} sm={6} lg={6}>
                  <Autocomplete
                    options={listProvinces}
                    getOptionLabel={option => option.product_name || option.name || ""}
                    value={address && address.province && listProvinces && (listProvinces.find(c => c.uid === address.province.uid) || [])}
                    style={{ width: "100%" }}
                    onChange={handleChangeAddressProvince}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn Tỉnh/Thành Phố"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <Autocomplete
                    options={listDistricts}
                    getOptionLabel={option => option.product_name || option.name || ""}
                    value={address && address.district && listDistricts && (listDistricts.find(c => c.uid === address.district.uid) || [])}
                    style={{ width: "100%" }}
                    onChange={handleChangeAddressDistrict}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn Quận/Huyện"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>

          </Grid>
          <Grid item xs={12} sm={4} lg={4}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Options
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    options={genders}
                    value={genders?.[dataEdit?.gender] || null}
                    onChange={handleChangeCustomerGender}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Gender"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="customerPhone"
                    name="phone_number"
                    label="Customer Phone Number  "
                    variant="outlined"
                    type="string"
                    margin="dense"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit?.phone_number || ''}
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    options={addresstypes || []}
                    value={dataEdit?.address && dataEdit?.address?.address_type ? mapTypes[dataEdit.address.address_type] : null}
                    onChange={handleChangeAddressType}
                    style={{ width: "100%" }}
                    getOptionLabel={option => option?.type_name || ""}
                    renderInput={params => (
                      <TextField {...params}
                        label="Address Type "
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <ShippingList
          listAddress={dataEdit && dataEdit.alt_info}
          provinces={listProvinces}
          addressTypeList={addresstypes}
          updateData={updateShippingData}
          onAddData={addShippingData}
          handleChangeDefaultAddress={handleChangeDefaultAddress}
        />
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          {/* <Button
                        color="primary"
                        variant="contained"
                        aria-label="add"
                        className={classge.btnControlGeneral}
                        type="submit"
                    >
                        <SaveIcon className={classge.iconback}/>
                        {actionType && actionType === "Edit" ? "Save" : actionType}
                    </Button> */}
          <RoleButton actionType={actionType} />
          {/* { actionType === "Edit" &&
                        <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
