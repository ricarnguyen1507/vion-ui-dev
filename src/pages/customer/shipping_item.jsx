import React, { useMemo, } from 'react'
import Grid from '@material-ui/core/Grid'
import { TextValidator } from 'react-material-ui-form-validator'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'

export default function ({ alt_info, indexItem, provinces = [], addressTypeList, updateData = () => {}}) {
  const address_type = useMemo(() => {

    if(alt_info?.address?.address_type) {
      return addressTypeList.find(item => item.type_value === alt_info.address.address_type) || {}
    }
    return null
  }, [addressTypeList, alt_info])
  const province = useMemo(() => {
    if(alt_info?.address?.province?.uid) {
      return (provinces.find(item => item.uid === alt_info.address.province.uid) || {})
    }else
      return null;
  }, [alt_info, provinces])
  const districtList = useMemo(() => {
    const province_uid = province && province.uid;
    if(!province_uid) return [];
    const temp = provinces.find(p => p.uid === province_uid)
    if(temp && temp.areas)
      return temp.areas
    return []
  }, [province, provinces])
  const district = useMemo(() => {
    if(districtList && alt_info?.address?.district?.uid) {
      if(!districtList.find(item => item.uid === alt_info.address.district.uid)) {
        return (districtList[0])
      }
      return districtList.find(item => item.uid === alt_info.address.district.uid) || {}
    }
    return null
  }, [alt_info, districtList])

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    updateData({
      ...alt_info,
      [name]: value
    })
  }
  const handleChangeAddressDes = (e) => {
    e.preventDefault()
    const { value = "" } = e.target
    updateData({
      ...alt_info,
      address: {
        ...alt_info.address,
        address_des: value
      }
    })
  }
  // const [districtList, setDistrictList] = useState(findDistrict(province && province.uid))

  function handleChangeProvince (e, item) {
    e.preventDefault()
    if(item.uid && item.name) {
      updateData({
        ...alt_info,
        address: {
          ...alt_info.address,
          province: {
            uid: item.uid
          }
        }
      })
    }
  }
  function handleChangeDistrict (e, item) {
    e.preventDefault()
    if(item.uid && item.name) {
      updateData({
        ...alt_info,
        address: {
          ...alt_info.address,
          district: {
            uid: item.uid
          }
        }
      })
    }
  }

  function handleChangeAddressType (e, item) {
    e.preventDefault()
    if(item.uid) {
      updateData({
        ...alt_info,
        address: {
          ...alt_info.address,
          address_type: item.type_value
        }
      })
    }
  }

  return (
    <>
      <Grid container spacing={2} >
        <Grid item xs={12} sm={12} lg={12}>
          <TextValidator
            id={`${indexItem}-customer_name`}
            label="Họ và tên người nhận"
            type="text"
            variant="outlined"
            margin="dense"
            fullWidth
            name="customer_name"
            onChange={handleChange}
            value={alt_info['customer_name']}
          />
        </Grid>
        <Grid item xs={6} sm={6} lg={6}>
          <TextValidator
            id={`${indexItem}-phone_number`}
            label="Số điện thoại nhận hàng"
            type="text"
            variant="outlined"
            margin="dense"
            fullWidth
            name="phone_number"
            onChange={handleChange}
            value={alt_info['phone_number']}
          />
        </Grid>
        <Grid item xs={6} sm={6} lg={6}>
          <Autocomplete
            options={addressTypeList}
            getOptionLabel={option => option.type_name || ""}
            value={address_type}
            style={{ width: "100%" }}
            onChange={handleChangeAddressType}
            renderInput={params => (
              <TextField {...params}
                label="Chọn loại địa chỉ"
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={12} lg={12}>
          <TextValidator
            id={`${indexItem}-address`}
            label="Nhập địa chỉ (bao gồm phường xã)"
            type="text"
            variant="outlined"
            margin="dense"
            fullWidth
            name="address_des"
            onChange={handleChangeAddressDes}
            value={alt_info?.address?.['address_des'] ?? null}
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={6}>
          <Autocomplete
            options={provinces}
            getOptionLabel={option => option.name || ''}
            value={province }
            style={{ width: "100%" }}
            onChange={handleChangeProvince}
            renderInput={params => (
              <TextField {...params}
                label="Chọn Tỉnh/Thành Phố"
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={6}>
          <Autocomplete
            options={districtList}
            getOptionLabel={option => option.name || ''}
            value={district}
            style={{ width: "100%" }}
            onChange={handleChangeDistrict}
            renderInput={params => (
              <TextField {...params}
                label="Chọn Quận/Huyện"
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
      </Grid>
    </>
  )
}