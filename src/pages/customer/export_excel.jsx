import React from "react";
import useStyles from 'pages/style_general'
import ReactExport from "react-data-export";
import GetAppIcon from "@material-ui/icons/GetApp";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { titleCase } from "services/titleCase";
import { clone } from "services/diff";
import Tooltip from "@material-ui/core/Tooltip";
import {changePropName} from "services/changePropName";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ExportExcel ({ data, isEditTable }) {
  const classge = useStyles();

  const dataFields = [
    "address_address_des",
    "address_address_type",
    "address_address_uid",
    "address_district_geo",
    "address_district_geo_region",
    "address_district_ghn_id",
    "address_district_id",
    "address_district_name",
    "address_district_name_url",
    "address_district_uid",
    "address_province_geo",
    "address_province_geo_region",
    "address_province_ghn_id",
    "address_province_id",
    "address_province_name",
    "address_province_name_url",
    "address_province_uid",
    "customer_name",
    "cart_items",
    "email",
    "gender",
    "phone_number",
    "uid",
  ]

  const handlePlainData = (obj, result = {}) => {
    for (let key in obj) {
      if (typeof obj[key] === "object") {
        const linkedFields = combineElement(obj[key], key);
        result = { ...obj, ...result, ...linkedFields };
      }
    }
    return result;
  };

  const combineElement = (obj, keyName, objResult = {}) => {
    if (Array.isArray(obj)) {
      const uidList = obj.map((elm) => elm.uid);
      objResult[keyName] = uidList.toString();
    } else {
      for (let key in obj) {
        const dependKey = keyName.concat("_") + key;
        if (typeof obj[key] === "object") {
          const nestedObj = combineElement(obj[key], dependKey, objResult);
          objResult = {...obj, ...objResult, ...nestedObj}
        }
        const newObj = changePropName(obj, dependKey, key);
        objResult = { ...newObj, ...objResult };
      }
    }
    return objResult;
  };

  const handleData = (arrayData, arrResult = []) => {
    if (arrayData !== null) {
      const cloneArr = clone(arrayData);
      cloneArr.forEach((item) => arrResult.push(handlePlainData(item)));
      return arrResult;
    }
  };

  const excelData = data && handleData(data);

  return (
    <ExcelFile
      element={
        isEditTable ?
          <Button
            variant="contained"
            aria-label="back"
            className={classge.btnExport}
          >
            <GetAppIcon className={classge.expIcon} />
          Export
          </Button>
          :
          <Tooltip title="Export data" aria-label="export data">
            <IconButton variant="contained">
              <GetAppIcon className={classge.expIcon} />
            </IconButton>
          </Tooltip>

      }
    >
      <ExcelSheet data={excelData} name="Customer">
        {excelData && excelData.length
          ? dataFields.map((item, idx) => (
            <ExcelColumn
              key={idx}
              label={`${titleCase(item)} <${item}>`}
              value={item}
            />
          ))
          : {}}
      </ExcelSheet>
    </ExcelFile>
  );
}
