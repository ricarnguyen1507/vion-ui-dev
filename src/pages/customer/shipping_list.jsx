import React, {useMemo} from 'react'
import { makeStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CheckIcon from '@material-ui/icons/Check';
import Tooltip from '@material-ui/core/Tooltip';


import useGeneral from 'pages/style_general'
import ShippingItem from './shipping_item';


const useStyles = makeStyles({
  btnAdd: {
    float: "right",
  },
})

export default function ({ listAddress = [], provinces, addressTypeList, updateData = () => {}, onAddData = () => {}, handleChangeDefaultAddress = () => {}}) {
  const classGen = useGeneral();
  const classStyle = useStyles();
  const listData = useMemo(() => listAddress, [listAddress])
  const ActionIcon = <EditIcon className={classGen.iconAction} />
  return (
    <>
      <Grid container spacing={2} >
        <Grid item xs={12} sm={12} lg={12}>
          <div className={classGen.rootPanel}>
            <div className={classGen.titlePanel}>
              <div className={classGen.iconTitle}>
                {ActionIcon}
              </div>
              <Typography className={classGen.contentTitle} variant="h5" >
                                Danh sách địa chỉ giao hàng
              </Typography>
              <IconButton className={classStyle.btnAdd} onClick={onAddData}>
                <AddBox color="primary"/>
              </IconButton>
            </div>
            <Divider />
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} lg={12}>
                {listData.map((alt_info, index) => (
                  <Accordion key={`address-list-${index}`}>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-label="Expand"
                      aria-controls="additional-actions1-content"
                      id="additional-actions1-header"
                    >
                      <Typography className={classGen.contentTitle} variant="h5" >
                                            Địa chỉ giao hàng #{index + 1}
                        {(!alt_info.is_default_address) ?
                          <FormControlLabel
                            control={
                              <Checkbox
                                style={{marginLeft: "20px"}}
                                id="type_highlight"
                                name="type_highlight"
                                color="primary"
                                defaultChecked={alt_info.is_default_address}
                                onChange={(e) => handleChangeDefaultAddress(e, index)}/>
                            }
                            label="Mặc định"
                          /> : <Tooltip title="Địa chỉ mặc định"><CheckIcon style={{color: '#2E9B93', marginLeft: '20px'}} /></Tooltip>}
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <ShippingItem alt_info={alt_info} indexItem={index} provinces={provinces} addressTypeList={addressTypeList} updateData={(e) => updateData(e, index)}/>
                    </AccordionDetails>
                  </Accordion>
                ))}
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </>
  )
}