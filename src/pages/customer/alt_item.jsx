import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { TextValidator } from 'react-material-ui-form-validator'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'

const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  richTextEditor: {
    width: "100%",
    minHeight: 150,
  },
  btnDelete: {
    float: "right",
  },
  itemCustomer: {
    display: "inline-flex",
  },
  inputAlt: {
    float: "left",
    display: "inline-block"
  },
  titleItemCustomer: {
    width: "200px",
    margin: "15px"
  }

})

export default function ({altName, dataItem, idx, addresstypes, mapTypes, onDelete, updateAlt}) {
  const classes = useStyles()
  const [altItem, setAltItem] = useState(dataItem || '')

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setAltItem(e.target.value)
    updateAlt(name, "", value, idx)
  }
  const handleChangeType = (e, type) => {
    e.preventDefault()
    const val = (type && type.type_value) || ""
    setAltItem({...altItem, address_type: val })
    updateAlt("alt_address", "address_type", val, idx)
  }
  const handleChangeDes = (e) => {
    e.preventDefault()
    setAltItem({...altItem, address_des: e.target.value})
    updateAlt("alt_address", "address_des", e.target.value, idx)
  }
  if (altName.includes('_address') === true) {
    return (
      <>
        <Grid item xs={12} sm={12} lg={12} className={classes.itemCustomer}>
          <Typography variant="h6" className={classes.titleItemCustomer}>
                        Item {altName.toString().replace('alt_', '')} {idx + 1}
          </Typography>
          <Grid container spacing={2} >
            <Grid item xs={12} sm={4} lg={4}>
              <Autocomplete
                options={addresstypes}
                value={mapTypes[altItem.address_type] || null}
                onChange={handleChangeType}
                style={{ width: "100%" }}
                getOptionLabel={option => option.type_name || ""}
                renderInput={params => (
                  <TextField {...params}
                    label="Alternative Address Type "
                    variant="outlined"
                    fullWidth
                    margin="dense"
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={8} lg={8}>
              <TextValidator
                id={`${altName}_${idx}`}
                name="address_des"
                label="Alternative Address Description "
                type="text"
                variant="outlined"
                margin="dense"
                fullWidth
                onChange={handleChangeDes}
                value={altItem.address_des || ''}
                validators={[
                  'required',
                  'minStringLength:3',
                  'maxStringLength:1000'
                ]}
                errorMessages={[
                  'required',
                  'Min length is 3',
                  'Max length is 1000'
                ]}
              />
            </Grid>

          </Grid>
          <IconButton className={classes.btnDelete} onClick={(e) => onDelete(e, altName, idx)}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </>
    )
  } else {
    return (
      <>
        <Grid item xs={12} sm={12} lg={12} className={classes.itemCustomer}>
          <Typography variant="h6" className={classes.titleItemCustomer}>
                        Item {altName.toString().replace('alt_', '')} {idx + 1}
          </Typography>
          { altName.includes('email') === true ? (
            <TextValidator
              fullWidth
              className={classes.inputAlt}
              id={`${altName}_${idx}`}
              label={`Alternative ${altName.toString().replace('alt_', '')} customer`}
              variant="outlined"
              margin="dense"
              name={altName}
              onChange={handleChange}
              value={altItem || ''}
              validators={[
                'required',
                'maxStringLength:30',
                'matchRegexp:^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$'
              ]}
              errorMessages={[
                'This field is required',
                'Max length is 50',
                'Wrong email format'
              ]}
            />
          ) : (altName.includes('phone') ? (
            (
              <TextValidator
                fullWidth
                className={classes.inputAlt}
                id={`${altName}_${idx}`}
                label={`Alternative ${altName.toString().replace('alt_', '')} customer`}
                variant="outlined"
                margin="dense"
                name={altName}
                onChange={handleChange}
                value={altItem || ''}
                validators={[
                  'required',
                  'maxStringLength:11',
                  'matchRegexp:[0-9]'
                ]}
                errorMessages={[
                  'This field is required',
                  'Max phone number is 11',
                  'Number only'
                ]}
              />
            )
          ) : (
            <TextValidator
              fullWidth
              className={classes.inputAlt}
              id={`${altName}_${idx}`}
              label={`Alternative ${altName.toString().replace('alt_', '')} customer`}
              variant="outlined"
              margin="dense"
              name={altName}
              onChange={handleChange}
              value={altItem || ''}
              validators={[
                'required',
                'minStringLength:3',
                'maxStringLength:1000'
              ]}
              errorMessages={[
                'This field is required',
                'Min length is 3',
                'Max length is 1000'
              ]}
            />
          ))
          }
          <IconButton className={classes.btnDelete} onClick={(e) => onDelete(e, altName, idx)}>
            <DeleteIcon />
          </IconButton>
        </Grid>
      </>
    )
  }
}