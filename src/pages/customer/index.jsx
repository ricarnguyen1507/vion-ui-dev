import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCustomer from './table'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const genders = ['Male', 'Female', 'Other']
function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  const [controlEditTable, setControlEditTable] = useState(false)
  // const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [errorPassword, setErrorPassword] = useState(false);
  const [mapTypes, setMapTypes] = useState(null)
  const [addresstypes, setAddressTypes] = useState(null)
  const [provinces, setProvinces] = useState(null)
  // const [areas , setAreas] = useState(null)
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))
  useEffect(() => {
    const fetchs = [
      '/list/addresstype',
      '/list/areas',
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([addresstypes, areas]) => {
      setAddressTypes(addresstypes)
      setMapTypes(addresstypes ? addresstypes.reduce((map, type) => {
        map[type.type_value] = type
        return map
      }, {}) : null)
      setProvinces(areas)
    })

  }, []);

  // Handle function
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row) => {
    setActionType(actionType)
    // setIndexEdit(index)
    setDataEdit(row)
    // setMode("edit")
  }
  const handleDelete = (dataDel) => api.delete(`/customer/${dataDel.uid}`)
    .then(() => {
      functionBack()
    })
    .catch(error => console.log(error))
  const handleClose = () => {
    setErrorPassword(false)
  };
  const handleSubmitData = async (actionType, data) => {
    console.log('data', data)
    await api.post('/customer', data)
      .then(response => {
        if (response.data.statusCode == 205) {
          setErrorPassword(true)
        } else {
          ctx.goBack()
        }
        // functionBack()
      })
      .catch(error => console.log(error))
  }

  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        errorPassword={errorPassword}
        actionType={actionType}
        genders={genders}
        mapTypes={mapTypes}
        addresstypes={addresstypes}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
        listProvinces={provinces}
        handleClose={handleClose}
      />
    </Route>
    <Route exact path={path}>
      <TableCustomer
        ctx={ctx}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        genders={genders}
        mapTypes={mapTypes}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
        listProvinces={provinces}
      />

    </Route>
  </>
}
