import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import IconButton from '@material-ui/core/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import useGeneral from 'pages/style_general'
import AltItem from './alt_item'
import { genUid } from "utils"

const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
})


// Render
export default function ({ actionType, altName, altData, addresstypes, mapTypes, onAddItem, onDeleteItem, updateAltItem}) {
  const classes = useStyles()
  const classge = useGeneral()

  const [dataList, setDataList] = useState((() => {
    let arr = []
    for(var r in altData) {
      arr.push({ uid: genUid('alt'), value: altData[r]})
    }
    return arr
  }) || [])


  const onAdd = (e, field) => {
    e.preventDefault();
    e.stopPropagation();
    onAddItem(e, field)
    if(actionType === "Edit") {
      if(field.includes('_address') === true) {
        dataList.forEach((item, index) => {
          dataList[index] = {
            uid: genUid('alt'),
            ...item
          }
        })
        setDataList({...dataList})
      }
      else{
        dataList.forEach((item, index) => {
          dataList[index] = {uid: genUid('alt'), value: item.value}
        })
        setDataList([...dataList])
      }
    }
    if(field.includes('_address') === true) {
      dataList.push({
        uid: genUid('alt'),
        address_res: '',
        address_type: null
      })
      setDataList([...dataList])
    }
    else{
      dataList.push({uid: genUid('alt'), value: ''})
      setDataList([...dataList])
    }

  }

  const onDelete = (e, field, idx) => {
    e.preventDefault()
    e.stopPropagation()
    dataList.splice(idx, 1)
    setDataList([...dataList])
    onDeleteItem(e, field, idx)
  }

  const updateAlt = (field, childField, data, index) => {
    if(field.includes('_address') === true && childField) {
      dataList[index][childField] = data // data[0].uid = newData
      setDataList([...dataList])
    }
    else{
      dataList[index].value = data
      setDataList([...dataList])
    }
    updateAltItem(field, childField, data, index)
  }
  return (
    <>
      <Accordion className={classes.rootDetail}>
        <AccordionSummary
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Grid container >
            <Grid item xs={12} sm={6} lg={6} className={classes.rootImgIcon}>
              <Typography variant="h6" className={classes.titleImgIcon}>
                {altName.toString().replace('alt_', 'Alternative ')}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} lg={6} >
              <IconButton className={classes.btnDelete} onClick={(e) => onAdd(e, altName)}>
                <AddBox />
              </IconButton>
            </Grid>
          </Grid>
        </AccordionSummary>
        <AccordionDetails>
          <div className={classge.rootAltPanel}>
            <Grid container spacing={2} className={classge.formpd}>
              {dataList.map((item, index) => (
                <AltItem
                  key={item.uid}
                  altName={altName}
                  idx={index}
                  dataItem={item.value}
                  addresstypes={addresstypes}
                  mapTypes={mapTypes}
                  onDelete={onDelete}
                  updateAlt={updateAlt}
                />
              ))}
            </Grid>
          </div>
        </AccordionDetails>
      </Accordion>
    </>
  )
}