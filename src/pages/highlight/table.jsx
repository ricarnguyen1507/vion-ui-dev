import React, { useState } from 'react'
import MaterialTable from 'material-table'
import useStyles from 'pages/style_general'
import Checkbox from '@material-ui/core/Checkbox'

const { cdn_url } = window.appConfigs

export default function TableHighlight ({ dataTable, handleCheck }) {
  const classes = useStyles()
  const [checked, setChecked] = useState(() => dataTable.map(item => item.highlight === 1), [dataTable])

  const onCheck = ({uid, tableData: { id }}) => {
    const status = (checked[id] = !checked[id])
    setChecked([...checked])
    handleCheck(uid, status, id)
  }
  const column = [
    { title: "Product Name", field: "product_name" },
    {
      title: "Cover",
      field: "image_cover",
      render: rowData => <img src={rowData.image_cover ? `${cdn_url}/${rowData.image_cover}` : "thumb.png"} alt={rowData.image_cover} style={{ width: 90 }} />,
      editable: "onUpdate"
    },
    {
      title: "Banner",
      field: "image_banner",
      render: rowData => <img src={rowData.image_banner ? `${cdn_url}/${rowData.image_banner}` : "thumb.png"} alt={rowData.image_banner} style={{ width: 90 }} />,
      editable: "onUpdate",
    },
    {
      title: "Promotion",
      field: "image_promotion",
      render: rowData => <img src={rowData.image_promotion ? `${cdn_url}/${rowData.image_promotion}` : "thumb.png"} alt={rowData.image_promotion} style={{ width: 90 }} />,
      editable: "onUpdate"
    },
    {
      title: "Image Highlight",
      field: "image_highlight",
      render: rowData => <img src={rowData.image_highlight ? `${cdn_url}/${rowData.image_highlight}` : "thumb.png"} alt={rowData.image_highlight} style={{ width: 90 }} />,
      editable: "onUpdate"
    },
    {
      title: "Highlight",
      field: "highlight",
      editable: "onUpdate",
      render: rowData => (
        <Checkbox
          onChange={() => { onCheck(rowData) }}
          checked={checked[rowData.tableData.id] === true }
          tabIndex={-1}
          color="secondary"
        />
      ),
    },
  ]

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={dataTable}
        title={<h1 className={classes.titleTable}>Highlight</h1>}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
        }}
      />
    </div>
  )
}