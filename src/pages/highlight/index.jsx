import React, { useState, useEffect } from 'react';
import api from 'services/api_cms';
import TableHighlight from './table'
import Loading from "components/Loading"

export default function () {
  const [dataTable, setDataTable] = useState(null)

  // Load data
  useEffect(() => {
    api.get(`/list/highlight`)
      .then(res => {
        setDataTable(res.data)
      })
      .catch(error => console.log(error));
  }, []);

  // Handle check highlight send
  const handleCheck = (uid, status) => {
    const mAction = status ? "set" : "del"
    api.post('/highlight', {
      [mAction]: {
        uid,
        "product.collection": { "uid": dataTable.highlight_cat[0].uid }
      }
    }).catch(error => console.log(error))
  }

  return dataTable === null ? <Loading /> : (
    <TableHighlight
      dataTable={dataTable.products}
      handleCheck={handleCheck}
    />
  )
}