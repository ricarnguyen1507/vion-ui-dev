import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import Loading from "components/Loading";

/**
 * Export
 */

export default function () {
  const [dataTable, setDataTable] = useState(null)

  const [controlEditTable, setControlEditTable] = useState(false)
  const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')

  // const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)

  useEffect(() => {
    api.get("/list/region").then(res => {
      setDataTable(res.data.result)
    })
  }, []);

  const functionBack = () => {
    setMode("table")
    setControlEditTable(false)
  }

  const functionEditData = (actionType, row) => {
    setActionType(actionType)
    let editRow = {}
    if (actionType !== "Add" && row['~areas'] && row['~areas'][0]) {
      row['~areas'] = row['~areas'][0]
    }

    editRow = row
    setDataEdit(editRow)
    // setIndexEdit(index)
    setMode("edit")
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang
  const handleSubmitData = (actionType, { set, del }) => {
    // Add / Update
    if (Object.keys(set).length > 0) {
      const formData = new FormData()
      // if (set.vn_all_province === true) {
      //     set.areas = dataTable.filter(r => r?.areas?.length > 0).map(r => { return {uid: r.uid} })
      // } else if (del.vn_all_province !== true && (dataEdit?.areas?.length > 0) && del.uid) {
      //     del.areas = dataEdit.areas.map(r => { return {uid: r.uid} })
      // }
      formData.set('set', JSON.stringify(set))
      formData.set('del', JSON.stringify(del))

      const mutateData = {
        set,
        del
      }

      api.post('/region', mutateData)
        .then(response => {
          setDataTable(response.data.result)
          setMode("table")
          setDataEdit(null)
        })
        .catch(error => console.log(error))
    } else {
      setMode("table")
    }
  }

  const handleDelete = (row) => api.delete(`/region/${row.uid}`)
    .then(res => {
      if (res.status === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r['~areas'] && r['~areas'].uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  if (mode === "table") {
    return dataTable === null ? <Loading /> : (
      <TableCollection
        data={dataTable}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
      />
    )
  }
  if (mode === "edit") {
    return dataTable === null ? <Loading /> : (
      <Edit
        regions={dataTable}
        actionType={actionType}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
      />
    )
  }
}
