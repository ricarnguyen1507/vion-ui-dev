import React, { useState, useMemo } from 'react';
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import { clone, diff } from 'services/diff'
// import ReactExport from "react-data-export";
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'

// const ExcelFile = ReactExport.ExcelFile;
// const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
// const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ({ originData = {}, regions, actionType, functionBack, handleSubmitData }) {
  const classge = useGeneral()

  const [dataEdit, setDataEdit] = useState(() => {
    if(actionType === 'Add') {
      return {}
    }
    if(actionType === 'Edit') {
      return clone(originData)
    }
  })
  const tCollections = useMemo(() => {
    const filter = actionType === 'Add' ? (c) => c.areas || c.is_province === true : (c) => c.areas && originData.uid !== c.uid || c.is_province === true
    return regions.filter(filter)
  }, [originData, actionType, regions])

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setDataEdit({...dataEdit, [name]: value})
  }
  const handleChangeParent = (e, item) => {
    e.preventDefault()
    setDataEdit({...dataEdit, '~areas': item !== null ? { uid: item.uid } : item })
  }
  const handleSubmit = function () {
    const { set, del } = diff(originData, dataEdit, dataEdit.uid)
    if(set || del) {
      handleSubmitData(actionType, {
        set: set || (dataEdit.uid && { uid: dataEdit.uid }) || {},
        del: del || {}
      })
    } else {
      functionBack()
    }
  }

  const handleChangeCheckBox = ({target: {name, checked}}) => {
    setDataEdit({...dataEdit, [name]: checked})
  }

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />

  return (
    <>
      <PageTitle title="Region" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={8} lg={8}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Tên Khu vực"
                    variant="outlined"
                    margin="dense"
                    name="name"
                    onChange={handleChange}
                    value={dataEdit.name || ""}
                    validators={[
                    ]}
                    errorMessages={[

                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Geo Region"
                    variant="outlined"
                    margin="dense"
                    name="geo_region"
                    onChange={handleChange}
                    value={dataEdit.geo_region || ""}
                    validators={[
                    ]}
                    errorMessages={[

                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Tên URL"
                    variant="outlined"
                    margin="dense"
                    name="name_url"
                    onChange={handleChange}
                    value={dataEdit.name_url || ""}
                    validators={[
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[

                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Giao Hàng Nhanh ID"
                    variant="outlined"
                    margin="dense"
                    name="ghn_id"
                    onChange={handleChange}
                    value={dataEdit.ghn_id || ""}
                    validators={[
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[

                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>

          <Grid item xs={12} sm={4} lg={4}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Option
                </Typography>
              </div>
              <Divider />

              {(!originData['~areas'] && !originData['areas']) || (originData['areas'] && originData.vn_all_province) ?
                <Grid item xs={12} sm={12} lg={12}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        className={classge.titleProduct}
                        id="vn_all_province"
                        name="vn_all_province"
                        color="primary"
                        defaultChecked={dataEdit['vn_all_province']}
                        onChange={handleChangeCheckBox}/>
                    }
                    label="Đánh dấu là Toàn quốc"
                  />
                </Grid> : ""}
              {dataEdit.vn_all_province !== true ?
                <Grid item xs={12} sm={12} lg={12}>
                  {!dataEdit['~areas'] || dataEdit['~areas'] === null ?
                    <FormControlLabel
                      control={
                        <Checkbox
                          className={classge.titleProduct}
                          id="is_province"
                          name="is_province"
                          color="primary"
                          defaultChecked={dataEdit['is_province']}
                          onChange={handleChangeCheckBox}/>
                      }
                      label="Tạo Tỉnh/Thành phố."
                    /> : ""}
                  {dataEdit.is_province === undefined || dataEdit.is_province === false ?
                    <Autocomplete
                      onChange={handleChangeParent}
                      options={tCollections}
                      value={regions?.find(c => c.uid === dataEdit['~areas']?.uid) || null}
                      getOptionLabel={option => option.name ?? ""}
                      renderInput={params => (
                        <TextField {...params}
                          label="Chọn tỉnh"
                          fullWidth
                          variant="outlined"
                          margin="dense"
                        />
                      )}
                    /> : ""}

                  <FormControlLabel
                    control={
                      <Checkbox
                        className={classge.titleProduct}
                        id="location_support"
                        name="location_support"
                        color="primary"
                        defaultChecked={dataEdit['location_support']}
                        onChange={handleChangeCheckBox}/>
                    }
                    label="Cho KH chọn để đăng ký Tỉnh/TP"
                  />
                </Grid> : ""}
            </div>
          </Grid>

        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
          {/* { actionType === "Edit" &&
                        <ExcelFile
                          element={
                            <Button
                              variant="contained"
                              aria-label="back"
                              className={classge.btnExport}
                            >
                              <GetAppIcon className={classge.expIcon} />
                              Export
                            </Button>
                          }>
                          <ExcelSheet data={clonedData} name="Manufacturer">
                            { clonedData && clonedData.length ? Object.keys(clonedData[0]).map((item, idx) => (
                              <ExcelColumn
                                key={idx}
                                label={`${titleCase(item)} <${item}>`}
                                value={item}
                              />)) : {}
                            }
                          </ExcelSheet>
                        </ExcelFile>
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
