import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
// import ImgCdn from 'components/CdnImage'

export default function TableCollection ({ data, controlEditTable, setControlEditTable, functionEditData }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const column = [
    {title: "UID", field: "uid", filtering: false},
    {
      title: "Region Name ",
      field: "name",
      render: rowData => rowData.areas ? rowData.name : <p>&nbsp;~&nbsp;&nbsp;{rowData.name}</p>

    }
  ]

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={data.filter(r => r.is_deleted !== true)}
        parentChildData={(row, rows) => rows.find(r => r.areas && !r.vn_all_province && (r.areas.find(a => a.uid === row.uid)))}
        title={<h1 className={classes.titleTable}>Collection</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Collection',
            isFreeAction: true,
            onClick: () => {
              functionEditData('Add')
            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Collection",
            onClick: (event, { tableData, ...rowData }) => {
              functionEditData('Edit', rowData, tableData.id)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}
