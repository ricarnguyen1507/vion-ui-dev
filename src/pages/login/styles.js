import { makeStyles } from "@material-ui/styles";

export default makeStyles(theme => ({
  container: {
    height: "100vh",
    width: "100vw",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: 0,
    left: 0,
  },
  logotypeContainer: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "60%",
    height: "100%",
    [theme.breakpoints.down("lg")]: {
      width: "60%",
    },
    [theme.breakpoints.down("md")]: {
      width: "60%",
    },
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  logotypeText: {
    color: "white",
    letterSpacing: 5,
    fontWeight: 500,
    fontSize: 60,
    [theme.breakpoints.down("md")]: {
      fontSize: 45,
    },
  },
  formContainer: {
    width: "40%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      width: "70%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "80%",
    },
  },
  form: {
    width: "50%",
    [theme.breakpoints.down("lg")]: {
      width: "55%"
    },
    [theme.breakpoints.down("sm")]: {
      width: "65%"
    },
  },
  loginHeader: {
    marginBottom: "2.5rem",
  },
  profileLogo: {
    height: 80,
    marginBottom: 25,
    [theme.breakpoints.down("sm")]: {
      height: 70
    },
    [theme.breakpoints.down("xs")]: {
      height: 60
    }
  },
  loginLabel: {
    textAlign: "center",
    fontSize: "1.5rem",
    fontWeight: 600,
    color: "#000",
    "&:after": {
      content: '" "',
      width: "40%",
      height: "5px",
      background: theme.palette.primary.main,
      display: "block",
      marginTop: "20px",
      borderRadius: "3px",
      marginLeft: "auto",
      marginRight: "auto",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
      "&:after": {
        width: "30%"
      }
    },
  },
  errorNoti: {
    margin: "10px 0px"
  },
  alertTitle: {
    fontSize: "1rem",
    fontWeight: 700
  },
  tab: {
    fontWeight: 400,
    fontSize: 18,
  },
  greeting: {
    fontWeight: 500,
    textAlign: "center",
    marginTop: theme.spacing(4),
  },
  subGreeting: {
    fontWeight: 500,
    textAlign: "center",
    marginTop: theme.spacing(2),
  },
  googleButton: {
    marginTop: theme.spacing(6),
    boxShadow: theme.customShadows.widget,
    backgroundColor: "white",
    width: "100%",
    textTransform: "none",
  },
  googleButtonCreating: {
    marginTop: 0,
  },
  googleIcon: {
    width: 30,
    marginRight: theme.spacing(2),
  },
  creatingButtonContainer: {
    marginTop: theme.spacing(2.5),
    height: 46,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  createAccountButton: {
    height: 46,
    textTransform: "none",
  },
  formDividerContainer: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    display: "flex",
    alignItems: "center",
  },
  formDividerWord: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
  },
  formDivider: {
    flexGrow: 1,
    height: 1,
    backgroundColor: theme.palette.text.hint + "40",
  },
  errorMessage: {
    textAlign: "center",
  },
  inputField: {
    marginBottom: 20,
    border: 0,
    outline: "none",
    borderRadius: "60px",
    width: "100%",
    padding: 16,
    backgroundColor: "rgba(0, 0, 0, 0.12)",
    color: "#000",
    "&:focus": {
      border: 0
    },
    "&:before": {
      backgroundColor: "rgba(0, 0, 0, 0.12)",
    },
    "&:hover:before": {
      backgroundColor: `${theme.palette.primary.light} !important`,
    },
  },
  // textFieldUnderline: {
  //   "&:before": {
  //     borderBottomColor: theme.palette.primary.dark,
  //   },
  //   "&:after": {
  //     borderBottomColor: theme.palette.primary.main,
  //   },
  //   "&:hover:before": {
  //     borderBottomColor: `${theme.palette.primary.light} !important`,
  //   },
  // },
  // textField: {
  //   borderBottomColor: theme.palette.background.light,
  // },
  formButtons: {
    width: "100%",
    marginTop: 20,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    }
  },
  forgetButton: {
    textTransform: "none",
    fontWeight: 400,
    "&:hover": {
      backgroundColor: "transparent"
    },
    [theme.breakpoints.down("xs")]: {
      marginTop: 20
    }
  },
  loginBtn: {
    width: "55%",
    textTransform: "capitalize",
    fontWeight: 600,
    letterSpacing: "2px",
    fontSize: "1.2rem",
    borderRadius: "60px",
    padding: "0.5rem",
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    }
  },
  loginLoaderContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "60%",
    fontSize: "1rem",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
      justifyContent: "center",
      fontSize: "1rem"
    }
  },
  loginLoader: {
    marginLeft: theme.spacing(0.5),
    marginRight: theme.spacing(2)
  },
  copyright: {
    marginTop: theme.spacing(6),
    whiteSpace: "nowrap",
    position: "absolute",
    [theme.breakpoints.up("lg")]: {
      bottom: theme.spacing(2),
    },
    [theme.breakpoints.down("md")]: {
      bottom: "-100%",
    },
    [theme.breakpoints.down("sm")]: {
      bottom: "2%",
      fontSize: "0.8rem"
    },
  },
}));
