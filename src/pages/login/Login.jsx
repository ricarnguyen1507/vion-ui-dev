import React, { useState } from "react";
import {
  Grid,
  CircularProgress,
  Typography,
  Button,
  // Tabs,
  // Tab,
  TextField,
  Fade,
} from "@material-ui/core";
import {
  Alert, AlertTitle
} from "@material-ui/lab";
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';

import { withRouter } from "react-router-dom";

// styles
import useStyles from "./styles";

// logo
import logo from "images/logo.svg";
import textLogo from "images/text_logo.svg";
import profile_icon from "images/profile_icon.svg";
// import google from "images/google.svg";

// context
import { useUserDispatch, loginUser } from "context/UserContext";
import { LoginContext } from "context/LoginContext"

function Login (props) {
  var classes = useStyles();

  // global
  var userDispatch = useUserDispatch();
  var { setIsLogin } = React.useContext(LoginContext);
  // local
  var [isLoading, setIsLoading] = useState(false);
  var [error, setError] = useState(null);
  // var [activeTabId, setActiveTabId] = useState(0);
  var [activeTabId] = useState(0);
  var [nameValue, setNameValue] = useState("");
  var [loginValue, setLoginValue] = useState("");
  var [passwordValue, setPasswordValue] = useState("");

  function doLogin () {
    if (loginValue.length === 0 && passwordValue.length === 0) {
      return
    }
    loginUser(
      userDispatch,
      loginValue,
      passwordValue,
      props.history,
      setIsLoading,
      setError,
      setIsLogin
    )
  }

  return (
    <Grid container className={classes.container}>
      <div className={`${classes.logotypeContainer} animation-bg`}>
        <div className="animation-logo-group">
          <img src={logo} alt="logo" width="165px" className="animation-logo" style={{marginBottom: 16}} />
          {/* <Typography align="center" className={`${classes.logotypeText} animation-text`}>Shopping  TV</Typography> */}
          <img src={textLogo} alt="Vion Mart" height="200px" width="600px" className="animation-text-logo" />
        </div>
      </div>
      <div className={classes.formContainer}>
        <div
          className={`${classes.form} animation-form`}
        >
          <div className={classes.loginHeader}>
            <div style={{textAlign: "center"}}>
              <img src={profile_icon} alt="login picture" className={classes.profileLogo} />
              <Typography className={classes.loginLabel} variant="h2" >Sign into your account</Typography>
            </div>
            {error && <Alert severity="error" icon={<PriorityHighIcon fontSize="inherit" />} className={classes.errorNoti}>
              <AlertTitle className={classes.alertTitle}>Login Failed</AlertTitle>
                  Invalid email or password. Please check again !
            </Alert>}
          </div>

          {/* <Tabs
            value={activeTabId}
            onChange={(e, id) => setActiveTabId(id)}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab label="Login" classes={{ root: classes.tab }} />
            <Tab label="New User" classes={{ root: classes.tab }} />
          </Tabs> */}
          {activeTabId === 0 && (
            <React.Fragment>
              {/* <Typography variant="h1" className={classes.greeting}>
                Good Morning, User
              </Typography> */}
              {/* <Button size="large" className={classes.googleButton}>
                <img src={google} alt="google" className={classes.googleIcon} />
                &nbsp;Sign in with Google
              </Button>
              <div className={classes.formDividerContainer}>
                <div className={classes.formDivider} />
                <Typography className={classes.formDividerWord}>or</Typography>
                <div className={classes.formDivider} />
              </div> */}


              {/* <Fade in={error}>
                <Typography color="secondary" className={classes.errorMessage}>
                  Something is wrong with your login or password :(
                </Typography>
              </Fade> */}
              <input
                id="email"
                className={classes.inputField}
                placeholder="Email"
                type="email"
                value={loginValue}
                onChange={e => setLoginValue(e.target.value)}
              />
              <input
                id="password"
                className={classes.inputField}

                value={passwordValue}
                onChange={e => setPasswordValue(e.target.value)}
                onKeyDown={e => { (e.keyCode === 13) && doLogin() }}
                placeholder="Password"
                type="password"
              />
              {/* <TextField
                id="email"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={loginValue}
                onChange={e => setLoginValue(e.target.value)}
                margin="normal"
                label="email"
                type="email"
                fullWidth
              />
              <TextField
                id="password"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={passwordValue}
                onChange={e => setPasswordValue(e.target.value)}
                margin="normal"
                label="Password"
                type="password"
                fullWidth
              /> */}
              <div className={classes.formButtons}>
                {isLoading ? (
                  <div className={classes.loginLoaderContainer}>
                    <CircularProgress size={26} className={classes.loginLoader} />
                    Please wait for a moment
                  </div>
                ) : (
                  <Button
                    className={classes.loginBtn}
                    disabled={
                      loginValue.length === 0 || passwordValue.length === 0
                    }
                    onClick={doLogin}
                    variant="contained"
                    color="primary"
                    size="large"
                  >
                      Login
                  </Button>
                )}
                <Button
                  color="primary"
                  size="large"
                  className={classes.forgetButton}
                >
                  Forget Password
                </Button>
              </div>
            </React.Fragment>
          )}
          {activeTabId === 1 && (
            <React.Fragment>
              <Typography variant="h1" className={classes.greeting}>
                Welcome!
              </Typography>
              <Typography variant="h2" className={classes.subGreeting}>
                Create your account
              </Typography>
              <Fade in={error}>
                <Typography color="secondary" className={classes.errorMessage}>
                  Something is wrong with your login or password :(
                </Typography>
              </Fade>
              <TextField
                id="name"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={nameValue}
                onChange={e => setNameValue(e.target.value)}
                margin="normal"
                placeholder="Full Name"
                type="email"
                fullWidth
              />
              <TextField
                id="email"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={loginValue}
                onChange={e => setLoginValue(e.target.value)}
                margin="normal"
                placeholder="Email Adress"
                type="email"
                fullWidth
              />
              <TextField
                id="password"
                InputProps={{
                  classes: {
                    underline: classes.textFieldUnderline,
                    input: classes.textField,
                  },
                }}
                value={passwordValue}
                onChange={e => setPasswordValue(e.target.value)}
                margin="normal"
                placeholder="Password"
                type="password"
                fullWidth
              />
              <div className={classes.creatingButtonContainer}>
                {isLoading ? (
                  <CircularProgress size={26} />
                ) : (
                  <Button
                    onClick={() =>
                      loginUser(
                        userDispatch,
                        loginValue,
                        passwordValue,
                        props.history,
                        setIsLoading,
                        setError,
                      )
                    }
                    disabled={
                      loginValue.length === 0 ||
                        passwordValue.length === 0 ||
                        nameValue.length === 0
                    }
                    size="large"
                    variant="contained"
                    color="primary"
                    fullWidth
                    className={classes.createAccountButton}
                  >
                      Create your account
                  </Button>
                )}
              </div>
              {/* <div className={classes.formDividerContainer}>
                <div className={classes.formDivider} />
                <Typography className={classes.formDividerWord}>or</Typography>
                <div className={classes.formDivider} />
              </div>
              <Button
                size="large"
                className={classnames(
                  classes.googleButton,
                  classes.googleButtonCreating,
                )}
              >
                <img src={google} alt="google" className={classes.googleIcon} />
                &nbsp;Sign in with Google
              </Button> */}
            </React.Fragment>
          )}
        </div>
        <Typography color="primary" className={classes.copyright}>
          © Vion Mart 2020.
        </Typography>
      </div>
    </Grid>
  );
}

export default withRouter(Login);
