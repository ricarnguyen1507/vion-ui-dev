import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import { InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'

export default function ({ functionBack }) {
  const history = useHistory()

  const ctx = useContext(context)
  const [dataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_stream_channel",
    "dgraph.type": "Channel"
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  const classge = useGeneral()

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/channel')
  }
  const handleSubmit = function () {
    api.post("/stream-channel", node.getMutationObj())
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const onError = (e) => {
    console.log("lỗi", e.name);
  }

  const ActionIcon = <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Stream Channel" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông channel
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"channel.name"}
                    fullWidth
                    label="Tên channel (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:200',
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 200',
                      'Character is not accept ']}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"channel.link"}
                    fullWidth
                    label="Link stream (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
