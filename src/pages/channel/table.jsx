import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

function getFilterStr ({filters}) {
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  return {strFilter: strFilter.join(' AND ')}
}
export default function TableChanel ({ stateQuery, setStateQuery, setDataTable, handleDelete }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get("/stream-channel", {
      params: {
        number: query.pageSize || 10,
        page: query.page || 0,
        ...{filter: (strFilter ? ' AND ' + strFilter : '')},
      }
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {
    const column = [
      { title: "Tên channel", field: "channel.name", editable: "onUpdate", o: 'eq' },
      { title: "Link stream", field: "channel.link", editable: "onUpdate" },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>Stream Channel List</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add channel',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/Add`)
            }
          },
          {
            icon: 'edit',
            tooltip: "Edit channel",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          }
        ]}
        editable={{
          onRowDelete: handleDelete,
        }}
      />
    </div>
  )
}