import React, { useState } from 'react';
import {
  useHistory,
  Route,
  useRouteMatch
} from "react-router-dom";
import api from 'services/api_cms';
import TableChanel from './table'
import Edit from './edit';
// import MuiAlert from '@material-ui/lab/Alert';
import { Shared } from 'context/Shared'
// import { id } from 'date-fns/esm/locale';
// import { LoadingContext } from "context/LoadingContext"
// function Alert (props) {
//   return <MuiAlert elevation={6} variant="filled" {...props} />;
// }

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState([])
  // const [actionType, setActionType] = useState('')
  // const [openMessage, setOpenMessage] = React.useState(false);
  // const [snackbarMessage, setMessage] = React.useState('');
  // const { setIsLoading } = React.useContext(LoadingContext);

  // const handleOpenMessage = (message) => {
  //   setMessage(message)
  //   setOpenMessage(true);
  // };
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  const handleDelete = (row) => api.delete(`/delete/${row.uid}`)
    .then(() => {
      const idx = dataTable.findIndex(d => d.uid === row.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  // Submit data ( Mode : Edit || Add )

  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          // actionType={actionType}
          data={dataTable}
          functionBack={functionBack}
        />
      </Route>
      <Route exact path={path}>
        <TableChanel
          data={dataTable}
          handleDelete={handleDelete}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setDataTable={setDataTable}
        />
      </Route>
    </Shared>
  )
}