import React, { useState } from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import { InputEdge } from 'components/GraphMutation';

const useStyles = makeStyles({
  listRadioItem: {
    width: "100%",
    display: 'grid',
    gridTemplateColumns: "auto auto auto auto ",
  },
  listItemLabel: {
    marginRight: "80px"
  }
});

export default function ({ item }) {
  const classes = useStyles();
  const [value, setValue] = useState(item.state.nodeData.permission.state || 0);
  const handleChange = (e) => {
    setValue(parseInt(e.target.value))
  };
  return (
    <FormControl component="fieldset" >
      <InputEdge
        Component={RadioGroup}
        node={item.state}
        pred={"permission"}
        onChange={handleChange}
        row
      >
        <FormControlLabel value="0" checked={value === 0} control={<Radio color="primary"/>} label="Toàn quyền" className={classes.listItemLabel}/>
        <FormControlLabel value="1" checked={value === 1} control={<Radio color="primary"/>} label="Xem" className={classes.listItemLabel}/>
        <FormControlLabel value="2" checked={value === 2} control={<Radio color="primary"/>} label="Ẩn" className={classes.listItemLabel}/>
        {item.state.nodeData.function_alias.state === "order" ?
          // Quyền (chỉnh sửa cơ bản) là quyền được edit tất cả, chỉ không được phép edit ở cột "Giảm giá khác" tại Menu "Đơn hàng"
          <FormControlLabel value="3" checked={value === 3} control={<Radio color="primary"/>} label="Chỉnh sửa cơ bản" className={classes.listItemLabel}/> : <></>
        }
      </InputEdge>
    </FormControl>
  );
}
