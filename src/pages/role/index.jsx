import React, { useState } from 'react';
import { useHistory, Route, useRouteMatch } from 'react-router-dom';
import Table from './table';
import Edit from './edit';

import { signOut } from "context/UserContext";

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    goBackUpdate () {
      window.history.back()
      setTimeout(() => {
        signOut()
      }, 2500)
    },
    editData (action, row = null) {
      this.data = row
      history.push(`${path}/${action}`)
    }
  }))

  return <>
    <Route path="/app/role" exact={true} >
      <Table ctx={ctx} />
    </Route>

    <Route path={`${path}/:actionType`}>
      <Edit ctx={ctx} />
    </Route>
  </>
}