import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import TableRoleFunction from './table_rolefunction';
import useGeneral from 'pages/style_general';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import AddBox from '@material-ui/icons/AddBox';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import PageTitle from 'components/PageTitle';
import RoleButton from 'components/Role/Button';
import { Node, InputEdge, ListEdge } from 'components/GraphMutation';

import { multiToOne } from 'modules/listMenu';
import { genUid } from 'utils';
import sortAlphabet from 'services/sortAlphabet';
import api from 'services/api_cms';

function Alert (props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
/// Tạo danh sách mới , thêm permission = 2 vào
function createNewList (value) {
  let arr = []
  const list = multiToOne() // Lấy danh sách
  if(list && Array.isArray(list) === true) {
    list.map((item) => arr.push({
      uid: genUid("role_function"),
      "dgraph.type": "RoleFunction",
      function_name: item.function_name,
      function_alias: item.alias,
      permission: value ? value : 2
    }))
  }
  return sortAlphabet(arr, "function_name")
}

export default function ({ctx}) {
  const classge = useGeneral()
  const {actionType} = useParams()

  const [dataEdit, setDataEdit] = useState(() => {
    const listSideBar = createNewList(2)
    if(ctx.data) {
      if('role.role_function' in ctx.data) {
        for(let s of listSideBar) {
          if(ctx.data['role.role_function'].findIndex(i => i.function_alias === s.function_alias) === -1) {
            ctx.data['role.role_function'].push(s)
          }
        }
        return { ...ctx.data, ['role.role_function']: sortAlphabet(ctx.data['role.role_function'], "function_name")}
      }
      return { ...ctx.data, ['role.role_function']: listSideBar}
    }
    else {
      return {
        uid: '_:new_role',
        "dgraph.type": "Role",
        'role.role_function': listSideBar
      }
    }
  })
  const [node] = useState(() => new Node(dataEdit))

  const handleSubmit = function () {
    api.post('/role/edit', node.getMutationForm()).then(() => {
      setSnackbarMessage("Vui lòng đăng nhập lại sau vài giây!")
      setOpenMessage(true);
      setTimeout(() => {
        ctx.goBackUpdate()
      }, 3000)
    }).catch(error => {
      console.log(error)
    })
  }

  /** =============== Display Update Succes -> Signout =============== **/
  const [openMessage, setOpenMessage] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
  };

  /** ============  Return UI ============ **/
  const onError = (e) => {
    console.log("Lỗi", e.name);
  }
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <Snackbar open={openMessage} autoHideDuration={3000} onClose={handleCloseMessage}>
        <Alert onClose={handleCloseMessage} severity="success"> {snackbarMessage} </Alert>
      </Snackbar>
      <PageTitle title="Role" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Information </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"role_name"}
                    fullWidth
                    label="Name Role"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:200'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 200'
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > The role's permission </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <div>
                    &nbsp;
                    <ListEdge
                      Component={TableRoleFunction}
                      node={node}
                      pred={"role.role_function"}
                    />
                  </div>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>

        <Grid className={classge.rootSubmit}>
          <Button
            style={{ marginRight: '10px' }}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} /> Return
          </Button>
          <RoleButton actionType="Save" />
        </Grid>
      </ValidatorForm>
    </>
  )
}
