import React, { useState, useEffect } from 'react'

import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

import api from 'services/api_cms'
import tblQuery from "services/tblQuery"
import Table from './table'
import Edit from './edit'

const incTypes = ['Thu', 'Chi']
export default function () {
  const history = useHistory()
  const {path} = useRouteMatch()

  const [ready, setReady] = useState(false)
  const [cmsUser, setCmsUser] = useState(null)
  const [products, setProduct] = useState([])
  const [trackingUtm, setTrackingUtm] = useState(null)

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
    },
    tblData: tblQuery("/list/incomes")
  }))


  // Load data
  useEffect(() => {
    const requests = [
      '/cmsUser/get-option'
    ].map(uri => api.get(uri))

    Promise.all(requests)
      .then(responses => responses.map(r => r.data.result))
      .then(([cmsUser]) => {
        setCmsUser(cmsUser)
        setReady(true)
      })
      .catch(error => {
        console.log(error)
      })
  }, [])

  if(ready)
    return <>
      <Route path={`${path}/:actionType`}>
        <Edit
          ctx={ctx}
          cmsUser={cmsUser}
          incTypes = {incTypes}
          products={products}
          setProduct={setProduct}
        />
      </Route>
      <Route exact path={path}>
        <Table
          ctx={ctx}
          cmsUser={cmsUser}
          incTypes = {incTypes}
          setProduct={setProduct}
        />
      </Route>
    </>
  return "loading..."
}