import React, {
  useState
} from 'react'
import {
  useParams
} from "react-router-dom"
import {
  ValidatorForm,
  TextValidator
} from 'react-material-ui-form-validator'
import RoleButton from 'components/Role/Button'
import ReturnIcon from '@material-ui/icons/KeyboardReturn'
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Typography from '@material-ui/core/Typography'
import AddBox from '@material-ui/icons/AddBox'
import Autocomplete from '@material-ui/lab/Autocomplete'

import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'

import {ObjectsToForm} from "utils"
import SelectMultiProduct from 'modules/SelectMulti/products/list_manage'
import api from 'services/api_cms'
import { useUserState, Permission } from "context/UserContext";
import {
  OptionEdge,
  InputEdge,
  Node
} from 'components/GraphMutation'
import income from './index'


export default function ({ ctx, products, setProduct, incTypes}) {
  const classge = useStylesGeneral()
  var { uid } = useUserState();
  const { actionType } = useParams()

  const [dataEdit, setDataEdit] = useState(() => ctx.data ?? {
    uid: '_:new_ads',
    income_type: 0,
    quantity: 1,
    "dgraph.type": "Income"
  })

  const [node] = useState(() => new Node(dataEdit))
  const [pricingNode] = useState(() => node.getEdge("income.pricing", { uid: "_:new_pricing", "dgraph.type": "Pricing" }).state)
  const [priceValidator] = useState(() => ({
    validators: [
      'isNumber'
    ],
    errorMessages: [
      'Price phai la so nguyen duong'
    ]
  }))
  const [updated] = useState({
    price: 0
  })
  const handleIncomeTypeChange = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'income_type': incTypes.indexOf(item) })
    node.setState('income_type', incTypes.indexOf(item))
  }
  const [timer, setTimer] = useState(null)
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&')}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          if(newOption.length){
            for(let obj of newOption){
              if(obj.product_name === queries.fulltext_search){
                updated.price = obj?.cost_price ?? 0
                pricingNode.setState('cost_price_with_vat',parseInt(dataEdit.quantity) * obj?.cost_price)
                setDataEdit({ ...dataEdit, 'quantity': dataEdit.quantity, 'cost_price_with_vat': parseInt(dataEdit.quantity) * obj?.cost_price})
              }
            }
          } 
          setProduct([...newOption])
        })
    }
  }
  const handleChangePrice = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    node.setState(name,value)
    pricingNode.setState('cost_price_with_vat',parseInt(value) * updated.price)
    setDataEdit({ ...dataEdit, 'quantity': value, 'cost_price_with_vat': parseInt(value) * updated.price})
  }
  function handleSubmit () {
    let date_created = new Date().getTime()
    node.setState('created_at',date_created)
    node.setState('income.user', {uid: uid})
    const formData = ObjectsToForm(node.getMutationObj())
    api.post('/income', formData)
      .then(ctx.goBack)
      .catch(err => {
        console.log(err)
      })
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Thu Chi" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
        onError={errors => console.log(errors)}
      >
        <Grid container spacing={2} >
          <Grid item xs={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid style={{paddingTop: 20 }} container spacing={2}>

                <Grid item xs={12}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <OptionEdge
                        node={node}
                        pred={"income.product"}
                        options={products || []}
                        getOptionLabel={option => option?.product_name ?? ""}
                        val={products?.find(g => g.uid === dataEdit?.["income.product"]?.uid) ?? ''}
                        onInputChange={onInputSelectProductChange}
                        renderInput={params => (
                          <TextField {...params}
                            label="Chọn sản phẩm"
                            fullWidth
                            variant="outlined"
                            required={true}
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={12}>
                     <TextField
                        fullWidth
                        id="quantity"
                        label="Số lượng"
                        variant="outlined"
                        margin="dense"
                        name="quantity"
                        onChange={handleChangePrice}
                        value={dataEdit?.cost_price_with_vat ? dataEdit?.quantity : ""}
                        required
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        fullWidth
                        id="cost_price"
                        label="Đơn giá"
                        variant="outlined"
                        margin="dense"
                        name="cost_price"
                        value={updated.price || ""}
                      />
                    </Grid>
                   
                    <Grid item xs={12}>
                    <TextField
                        fullWidth
                        id="cost_price_with_vat"
                        label="Tổng tiền số tiền"
                        variant="outlined"
                        margin="dense"
                        name="cost_price_with_vat"
                        value={dataEdit?.cost_price_with_vat || dataEdit?.['income.pricing']?.cost_price_with_vat}
                        required
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <InputEdge
                        Component={TextField}
                        node={node}
                        pred={"notes"}
                        fullWidth
                        className={classge.inputAddressType}
                        type="text"
                        label="Ghi chú"
                        variant="outlined"
                        margin="dense"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Autocomplete
                            options={incTypes}
                            value={incTypes?.[dataEdit?.income_type] || "Thu"}
                            onChange={handleIncomeTypeChange}
                            style={{ width: "100%" }}
                            renderInput={params => (
                              <TextField {...params}
                                label="Loại chi phí"
                                variant="outlined"
                                fullWidth
                                margin="dense"
                              />
                            )}
                        />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton page="income" actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}