import React, {
  useRef,
  useState,
  useEffect
} from 'react'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
import api from 'services/api_cms'
import tblQuery from "services/tblQuery"
import {createLookup} from "utils"

const activeBox = { width: "40px", height: "40px", border: "1px solid cyan", borderRadius: "25px" }



export default function TableAds ({ctx, cmsUser,incTypes,setProduct}) {
  const classes = useStyles()
  const table = useRef(null)
  const [lookupUser] = useState(() => createLookup(cmsUser, "uid", "user_name"), [cmsUser])

  function onRowDelete ({tableData, ...rowData}) {
    return api.delete(`/ads/${rowData.uid}`).then(() => {
      table.current?.onQueryChange()
    }).catch(err => {
      alert('Xóa không thành công')
      console.log(err)
    })
  }

  const [tableProps] = useState(() => {
    if(ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      data: tblQuery("/list/incomes"),
      columns: [
        { title: "Tên hàng cũ", field: "income_name", editable: "never", operator: "regexp"
        },
        { title: "Tên hàng", field: "product_name", editable: "never", operator: "regexp",
          render: rowData => <span>{rowData?.['income.product']?.product_name ?? ''}</span>
        },
        {
          title: "Người tạo", field: "income.user", editable: "never", operator: "uid_in",
          lookup: lookupUser,
          render (rowData) {
            const uid = rowData["income.user"]?.uid
            return uid ? lookupUser[uid] : ""
          }
        },
        {
          title: "Tổng tiền", field: "cost_price_with_vat", editable: "never", operator: "uid_in",
          render: rowData => <span>{rowData?.['income.pricing']?.cost_price_with_vat ?? ''}</span>
        },
        {
          title: "Trạng thái hiển thị",
          field: "income_type",
          editable: "never",
          render: (rowData) => <div>{incTypes[rowData.income_type]}</div>,
          lookup: incTypes,
        },
        { title: "Ngày tạo", field: "created_at", editable: "never", operator: "regexp",
          render (rowData) {
            const date = new Date(rowData.created_at).toString()
            return date || ""
          }
        },
      ],
      title: <h1 className={classes.titleTable}>Income</h1>,
      onRowClick ({currentTarget: target}) {
        if(selectedRow != target) {
          if(selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          selectedRow = target
          selectedRow.style.backgroundColor = "#EEE"
        }
      },
      options: {
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        filtering: true,
        sorting: true,
        exportButton: true,
        grouping: true,
        pageSize: 10,
        pageSizeOptions: [10, 25, 50],
        actionsColumnIndex: -1
      }
    }
  })

  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tableProps}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Income ',
            isFreeAction: true,
            onClick () {
              ctx.editData();
            }
          },
          {
            icon: "edit",
            tooltip: "Edit Income ",
            onClick (e, { tableData, ...rowData }) {
              if(rowData?.['income.product']){
                setProduct([rowData?.['income.product']])
              }
              ctx.editData(rowData, tableData.id);
            }
          }
        ]}
        editable={{onRowDelete}}
      />
    </div>
  )
}