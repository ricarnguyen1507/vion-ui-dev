import React, { useState, useEffect } from 'react';
import api from 'services/api_cms';
import TableGroupPayment from './table'
import Edit from './edit';
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const display_status = ['PENDING', 'REJECTED', 'APPROVED']
function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  // const [dataTable, setDataTable] = useState(null)
  // const [mode, setMode] = useState("table")
  // const [actionType, setActionType] = useState('')
  // const [indexEdit, setIndexEdit] = useState(null)
  // const [dataEdit, setDataEdit] = useState(null)
  const [payment_methods, setPaymentMethod] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  // Load data
  useEffect(() => {
    const fetchs = [
      '/list/payment_method'
    ].map(url => fetchData(url).then(res => res.data.result))

    Promise.all(fetchs).then(([payment_method]) => {
      setPaymentMethod(payment_method)
    })
  }, []);

  // Function handle
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  // const handleDelete = (row) => api.post('/payment_method/' + row.uid)
  //   .then(res => {
  //     if (res.status === 200) {
  //       const idx = dataTable.findIndex(d => d.uid === row.uid)
  //       dataTable.splice(idx, 1)
  //       setDataTable([...dataTable])
  //     }
  //   })
  //   .catch(error => console.log(error))

  // Submit data ( Mode : Edit || Add )

  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          // actionType={actionType}
          // originData={dataEdit}
          functionBack={functionBack}
          displayStatus={display_status}
          payment_methods={payment_methods}
        />
      </Route>
      <Route exact path={path}>
        <TableGroupPayment
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          // setDataTable={setDataTable}
          setControlEditTable={setControlEditTable}
          displayStatus={display_status}
        />
      </Route>
    </Shared>
  )
}