import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AddBox from '@material-ui/icons/AddBox'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import { OptionEdge, InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'

export default function (props) {
  const { functionBack, displayStatus, payment_methods } = props
  const classge = useStylesGeneral()
  const history = useHistory()
  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_group_payment",
    "dgraph.type": "GroupPayment",
    display_status: 0
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    node.setState('display_status', displayStatus.indexOf(item))
  }
  // Handle function
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/group_payment')
  }
  const handleSubmit = function () {
    api.post("/group_payment", node.getMutationObj())
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Group Payment" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
      >
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Thông tin
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"group_payment_name"}
                    fullWidth
                    label="Tên nhóm các phưởng thức thanh toán (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classge.formpd} style={{ paddingTop: 20 }}>
                <Grid item xs={12} sm={12} lg={12}>
                  <OptionEdge
                    multiple
                    limittags={1}
                    node={node}
                    pred={"payment_methods"}
                    options={payment_methods || []}
                    getOptionLabel={option => option?.payment_name || ""}
                    style={{ width: "100%" }}
                    // onInputChange={handleChangeBrandShop}
                    // disabled={!!((dataEdit?.brand_shop && actionType == 'Edit'))}
                    renderInput={params => (
                      <TextField {...params}
                        label="Phương thức thanh toán"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                  {/* <Autocomplete
                    multiple
                    options={payment_methods}
                    onChange={handleChangePaymentMethod}
                    getOptionLabel={(option) => option.payment_name ?? ""}
                    value={dataEdit?.payment_methods && dataEdit?.payment_methods.map(pm => payment_methods?.find(pmethod => pmethod.uid === pm.uid))}
                    style={{ width: "100%" }}
                    renderInput={params => (

                    )}
                  /> */}
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classge.formpd} style={{ paddingTop: 20 }}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus?.[dataEdit?.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
