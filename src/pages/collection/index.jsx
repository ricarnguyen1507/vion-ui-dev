import React, { useState } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import Loading from "components/Loading";
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
const display_status = ['PENDING', 'REJECTED', 'APPROVED']
/**
 * Export
 */

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  // const { setIsLoading } = React.useContext(LoadingContext);
  const [dataTable, setDataTable] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  const [mode, setMode] = useState("table")
  // const [actionType, setActionType] = useState('')
  // const [mapTypes, setMapTypes] = useState(null)
  const [products] = useState([])
  // const [indexEdit, setIndexEdit] = useState(null)
  // const [dataEdit, setDataEdit] = useState({})
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }

  const functionOrderAscData = () => {
    setMode("loading")
    const ascMutateData = dataTable.map((dt, i) => ({set: {uid: dt.uid, display_order: i + 1}}))
    api.post('/collection/reordering', ascMutateData)
      .then(() => {
        setMode("table")
      })
  }

  const handleDelete = (row) => api.delete(`/collection/${row.uid}`)
    .then(res => {
      if (res.data.statusCode === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      } else {
        alert(res.data.message)
      }
    }).catch(error => console.log(error))

  if (mode === "loading") {
    return <Loading />
  }
  return(<Shared value={sharedData}>
    <Route path={`${path}/:actionType`}>
      <Edit
        collections={dataTable}
        // mapTypes={mapTypes}
        functionBack={functionBack}
        displayStatus={display_status}
        products={products}
      />
    </Route>
    <Route exact path={path}>
      <TableCollection
        data={dataTable}
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
        setDataTable={setDataTable}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        handleDelete={handleDelete}
        functionOrderAscData={functionOrderAscData}
        displayStatus={display_status}
        setMode={setMode}
      />
    </Route>
  </Shared>
  )
}
