import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import { genCdnUrl } from 'components/CdnImage'
// import ExportExcel from "./export_excel";
// import { LoadingContext } from "context/LoadingContext"
import SelectMultiCollection from 'modules/SelectMulti/collections/list_manage'
import ColorPicker from './color_text'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"

export default function ({functionBack, displayStatus }) {
  const classge = useGeneral()
  // const { setIsLoading } = React.useContext(LoadingContext);
  const history = useHistory()

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    "dgraph.type": "Collection",
    "collection_type": 0,
    "is_temporary": false,
    "is_parent": true
  })
  delete dataEdit?.['children|display_order']
  delete dataEdit?.['highlight.products']
  delete dataEdit?.['highlight.products|display_order']

  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  const [updated] = useState({
    values: {
      'display_status': dataEdit?.display_status
    }
  })

  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/collection')
  }
  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files, {childCollection: childCollection})
    api.post("/collectionNewForm/" + dataEdit?.uid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }

  const [childCollection] = useState({
    set: {},
    del: {}
  })
  const updateListCollection = ({set, del}) => {
    childCollection.set = set
    childCollection.del = del

    // let parent_col = node.getState('parent') ?? []
    // if(set.length >= parent_col.length) {
    //   node.setState('parent', set, true)
    //   parent_col = node.getState('parent') ?? []
    // } else if(set.length < del.length) {
    //   if(parent_col.length) {
    //     const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    //     parent_col.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    //   }
    // }
  }

  const [color, setColor] = useState(() => ({
    'color_title': dataEdit?.color_title || "#000",
    'gradient_start': dataEdit?.gradient_start || "#fff",
    'gradient_end': dataEdit?.gradient_end || "#fff",
  }))

  const handleColor = (color_picked, fieldName) => {
    setColor({...color, [fieldName]: color_picked.hex})
    setDataEdit({...dataEdit, [fieldName]: color_picked.hex})
    node.setState(fieldName, color_picked.hex)
  }

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  function invalidCallback (result) {
    setMultiSelectInvalid(result)
  }

  let validateForm = {}
  // validateForm = useMemo(() => {
  //     if(!dataEdit.collection_image || dataEdit.collection_image === ""){
  //         return {
  //             ...validateForm,
  //             icon: {
  //                 error: true,
  //                 helperText: "This field is required"
  //             }
  //         }
  //     } else {
  //         return {}
  //     }
  // }, [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */


  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Collection" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />

              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"collection_name"}
                  fullWidth
                  label="Collection Name"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'required',
                    'minStringLength:3',
                    'maxStringLength:50',
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min length is 3',
                    'Max length is 50']}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={4} lg={4}>
                    <FormControlLabel
                      control={
                        <div className={classge.colorPicker} style={{margin: "15px 0"}}>
                          <ColorPicker
                            style={{marginLeft: "15px !important"}}
                            color={color.color_title}
                            fieldName="color_title"
                            handleColor={handleColor}
                          />
                        </div>
                      }
                      label="&nbsp;&nbsp;Màu (Tiêu đề)"
                    />
                  </Grid>
                  <Grid item xs={12} sm={4} lg={4}>
                    <FormControlLabel
                      control={
                        <div className={classge.colorPicker} style={{margin: "15px 0"}}>
                          <ColorPicker
                            color={color.gradient_start}
                            fieldName="gradient_start"
                            handleColor={handleColor}
                          />
                        </div>
                      }
                      label="&nbsp;&nbsp;Màu Gradient (Đầu)"
                    />
                  </Grid>
                  <Grid item xs={12} sm={4} lg={4}>
                    <FormControlLabel
                      control={
                        <div className={classge.colorPicker} style={{margin: "15px 0"}}>
                          <ColorPicker
                            color={color.gradient_end}
                            fieldName="gradient_end"
                            handleColor={handleColor}
                          />
                        </div>
                      }
                      label="&nbsp;&nbsp;Màu Gradient (Cuối)"
                    />
                  </Grid>
                </Grid>
              </Grid>

              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Danh sách Ngành hàng
                    </Typography>
                  </div>
                  <Divider />
                  <SelectMultiCollection
                    listOption={dataEdit?.children || []}
                    currentList={dataEdit?.children || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCollection}
                    // maxItem="0"
                    // minItem="2"
                    listTitle="Ngành hàng con"
                    facetPrefix="parent"
                    isArray={false}
                    invalidCallback={invalidCallback}
                  />
                </div>
              </Grid>
            </div>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                                    Xét duyệt Ngành hàng
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus[dataEdit?.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>

        <Grid container spacing={2}>
          <Grid item xs={6} sm={6} lg={6}>
            <h5> Collection Icon</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit?.collection_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="collection_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
          <Grid item xs={6} sm={6} lg={6}>
            <h5> Collection Icon V2</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit?.collection_icon, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="collection_icon" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} disabled={Object.keys(validateForm).length !== 0 || multiSelectInvalid}/>
          {/* { actionType === "Edit" &&
                        <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
