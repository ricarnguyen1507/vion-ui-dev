import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import SelectMultiProduct from 'modules/SelectMulti/products/list_manage'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import { InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'

export default function ({ products, setProduct, functionBack, displayStatus }) {
  const classge = useGeneral()
  const history = useHistory()
  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_widget",
    "dgraph.type": "Widget"
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  delete dataEdit['widget.products|display_order']
  if(dataEdit?.['widget.products']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['widget.products']) {
      obj['widget.products|display_order'] = `<widget.products> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const [node] = useState(() => new Node(dataEdit))
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/widget')
  }
  function handleSubmit () {
    api.post("/widget", node.getMutationObj())
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    node.setState('display_status', displayStatus.indexOf(item))
  }
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  const updateListProduct = async ({set}) => {
    let listHighligtOld = dataEdit?.['widget.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'widget.products')
  }

  const [timer, setTimer] = useState(null)
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&')}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProduct([...newOption])
        })
    }
  }
  let validateForm = {}
  // validateForm = useMemo(() => {
  //   if (dataEdit?.target_type === 2) {
  //     setMultiSelectInvalid(false)
  //   }
  //   // let uid = dataEdit?.uid.startsWith('_:')
  //   if (!dataEdit?.widget_name || dataEdit?.widget_name === "") {
  //     return {
  //       ...validateForm,
  //       widget_name: {
  //         error: true,
  //         helperText: "This field is required"
  //       }
  //     }
  //   } else {
  //     return {}
  //   }
  // }, [dataEdit])

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Launcher" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"widget_name"}
                    fullWidth
                    label="Widget Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"display_name"}
                    fullWidth
                    label="Widget name in CMS"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <Divider />
                    <SelectMultiProduct
                      listOption={products}
                      currentList={dataEdit?.['widget.products'] || []}
                      actionType={actionType}
                      dataEdit={dataEdit}
                      updateListManage={updateListProduct}
                      maxItem="50"
                      minItem="1"
                      delAll={dataEdit?.['widget.products']?.length > 0}
                      inputChange={onInputSelectProductChange}
                      listTitle="Sản phẩm"
                      facetPrefix="widget.products"
                    />

                  </div>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootBasic}>
                    <div className={classge.titleProduct}>
                      <Typography className={classge.title} variant="h3">
                                    Xét duyệt Ngành hàng
                      </Typography>
                    </div>
                    <Grid container className={classge.rootListCate} spacing={2}>
                      <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                        <Autocomplete
                          options={displayStatus}
                          value={displayStatus?.[dataEdit?.display_status] || "PENDING"}
                          onChange={handleChangeDisplayStatus}
                          style={{ width: "100%" }}
                          renderInput={params => (
                            <TextField {...params}
                              label="Trạng thái hiển thị"
                              variant="outlined"
                              fullWidth
                              margin="dense"
                            />
                          )}
                        />
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} disabled={Object.keys(validateForm).length !== 0}/>
        </Grid>
      </ValidatorForm>
    </>
  )
}
