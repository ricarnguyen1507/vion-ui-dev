import React, { useState } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableWidget from './table'
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const TypeValueOpt = [
  {
    launcher_type_value: "Livestream",
    type_name: "livestream",
    display_order: 1
  },
  {
    launcher_type_value: "Bộ sưu tập",
    type_name: "collection_temp",
    display_order: 2
  },
  {
    launcher_type_value: "Nghành Hàng",
    type_name: "collection",
    display_order: 3
  },
  {
    launcher_type_value: "Sản phẩm",
    type_name: "product",
    display_order: 4
  },
  {
    launcher_type_value: "Brand Shop",
    type_name: "brandShop",
    display_order: 5
  },
  {
    launcher_type_value: "Home",
    type_name: "home",
    display_order: 6
  }
]

// const target_type = ["Đối tượng nhóm", "Khách hàng", "Tất cả"]
const display_status = ['PENDING', 'REJECTED', 'APPROVED']

/**
 * Export
 */
export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  //PREPARE DATA FOR SUB TYPE VALUE (Livestream, Collection_temp, Collection, Product, Brand)
  const [products, setProduct] = useState([])
  const [controlEditTable, setControlEditTable] = useState(true)
  const [stateQuery, setStateQuery] = useState(() => ({
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }

  const handleDelete = (row) => api.delete(`/layout/${row.uid}`)
    .then(res => {
      if (res.data.statusCode === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      } else {
        alert(res.data.message)
      }
    })
    .catch(error => console.log(error))

  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          products={products}
          setProduct={setProduct}
          functionBack={functionBack}
          TypeValueOpt={TypeValueOpt}
          displayStatus={display_status}
        />
      </Route>
      <Route exact path={path}>
        <TableWidget
          data={dataTable}
          setProduct={setProduct}
          setDataTable={setDataTable}
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          displayStatus={display_status}
        />
      </Route>
    </Shared>
  )
}
