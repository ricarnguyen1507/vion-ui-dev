import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

export default function TableWidget ({ setProduct, stateQuery, setDataTable, setStateQuery, controlEditTable, setControlEditTable, displayStatus }) {
  const classes = useStyles()
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    return api.get("/list/widget", {
      params: {
        number: query.pageSize || 10,
        page: query.page || 0
      }
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  // Set column for table
  const column = [
    {
      title: "Trạng thái hiển thị",
      field: "display_status",
      editable: "never",
      render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
      lookup: displayStatus,
      o: 'eq',
      sorting: false
    },
    { title: "Tên danh sách", field: "display_name" },
    // { title: "Nhóm đối tượng", field: "launcherGroupCustomer" },
  ]

  /**
     * View
     */
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>WIDGET FPT PLAY</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Widget',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/Add`)
            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Layout",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              if (rowData && rowData['widget.products']) {
                setProduct(rowData['widget.products'])
              }
              history.push(`${pathname}/Edit`)

            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}