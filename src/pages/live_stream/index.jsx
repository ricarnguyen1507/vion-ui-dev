import React, { useState } from 'react';
import { useHistory, Route, useRouteMatch } from "react-router-dom";
import Table from './table'
import Edit from './edit';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
function Alert (props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (action, row = null) {
      this.data = row
      history.push(`${path}/${action}`)
    }
  }))

  /**
   * =============== Handle display error ===============
  **/
  const [openMessage, setOpenMessage] = useState(false);
  const [snackbarMessage, setSnackbarMessage] = useState('');
  const handleOpenMessage = (message) => {
    setSnackbarMessage(message)
    setOpenMessage(true);
  }
  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
  };
  /*========================================================*/

  return <>
    <Route path="/app/live-stream" exact={true} >
      <>
        <Snackbar open={openMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
          <Alert onClose={handleCloseMessage} severity="error">
            {snackbarMessage}
          </Alert>
        </Snackbar>
        <Table ctx={ctx} handleOpenMessage={handleOpenMessage} />
      </>
    </Route>

    <Route path={`${path}/:actionType`}>
      <>
        <Snackbar open={openMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
          <Alert onClose={handleCloseMessage} severity="error">
            {snackbarMessage}
          </Alert>
        </Snackbar>
        <Edit ctx={ctx} handleOpenMessage={handleOpenMessage} />
      </>
    </Route>
  </>
}