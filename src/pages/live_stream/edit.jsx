import React, { useState, useMemo } from 'react'
import api from 'services/api_cms'
import RoleButton from 'components/Role/Button'
import ReturnIcon from '@material-ui/icons/KeyboardReturn'
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Media from 'components/Media'
import DateFnsUtils from '@date-io/date-fns'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
// import StreamProduct from 'components/LiveStreams/stream_products'
import StreamProduct from './stream_product'
// import { useParams } from "react-router-dom"
import { genCdnUrl } from 'components/CdnImage'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import { KeyboardDateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers'
import { Node, Edge, InputEdge, ListEdge, CheckBoxEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"


const STATUS_LIST = [
  {
    id: 0,
    name: 'PENDING',
  },
  {
    id: 1,
    name: 'REJECTED',
  },
  {
    id: 2,
    name: 'APPROVED',
  }
]


export default function ({ ctx, handleOpenMessage }) {
  const classge = useGeneral()

  const [dataEdit, setDataEdit] = useState(() => ctx.data ?? {
    uid: '_:new_live_stream',
    "dgraph.type": "VideoStream",
    'stream.name': '',
    'stream.display_name': '',
    'stream.products': [],
    'stream.start_time': new Date().getTime(),
    'stream.end_time': null,
    'stream.is_highlight': false,
    'display_status': 1,
    'stream.channel': [],
    'stream.view_count': 0,
    'stream.view_virtual': 0,
    'stream.video_transcode': ''
  })
  const [node] = useState(() => new Node(dataEdit))
  const [channels, setChannel] = useState(dataEdit?.['stream.channel'] || [])
  const stream_status = STATUS_LIST.find(item => item.id === (dataEdit?.['display_status'] ?? '')) ?? null
  const [files] = useState(() => ({}))

  /** ============  Time start  ============ **/
  function isValidDate (d) {
    return d instanceof Date && !isNaN(d);
  }
  const handleChangeStartTime = (date) => {
    if (isValidDate(date)) {
      setDataEdit({ ...dataEdit, 'stream.start_time': Date.parse(date) })
      node.setState('stream.start_time', Date.parse(date))
    }
  };

  /** ============  Chọn kênh stream  ============ **/
  const stream_channel = useMemo(() => {
    const uid = dataEdit?.['stream.channel']?.length ? dataEdit['stream.channel'][0]['uid'] : null
    if (!uid)
      return null;
    return channels.find(item => item.uid === uid) ?? null
  }, [dataEdit, channels])

  const onInputChannelChange = (e, val) => {
    if (typeof val === 'string' && val) {
      let text = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
      api.post(`/stream-channel-option`, text).then(res => {
        const newOption = channels || []
        const temp = res.data.result.filter(r => (!newOption.find(no => no.uid === r.uid)))
        setChannel([...newOption, ...temp])
      })
    }
  }
  const handleChangeChannelList = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'stream.channel': item ? [item] : null })

    // Xóa Edge thừa trong node[stream.channel] . Do thiết kế kiểu dữ liệu : stream.channel: []
    function changeEdgeChannel (list = [], item = {}) {
      let newList = []
      list.map(gr => {
        if (gr.state.uid !== item?.uid) {
          gr.isDeleted = true
          newList.push(gr)
        }
        else {
          newList.push(gr)
        }
      })
      return newList
    }
    let listChannel = node.getState('stream.channel') ?? []
    if (item && Object.keys(item).length) {
      if (listChannel.length) {
        if (listChannel.find(e => e.state?.uid === item?.uid)) {
          node.setState('stream.channel', changeEdgeChannel(listChannel, item))
        }
        else {
          node.setState('stream.channel', [...changeEdgeChannel(listChannel, item), new Edge(item, true)])
        }
      }
      else {
        node.setState('stream.channel', item ? [item] : null)
      }
    }
  }

  /** ============  Image  ============ **/
  const updateFile = (field, file) => {
    node.setState('mime_type', file.type)
    node.setState(field, `images/stream_media/${file.md5}.${file.type.split('/')[1]}`)
    files[file.md5] = file
  }

  /** ============  Xét duyệt hiển thị  ============ **/
  const handleChangeStreamStatus = function (e, item) {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': item ? item.id : STATUS_LIST[0].id })
    node.setState('display_status', item ? item.id : STATUS_LIST[0].id)
  }

  /** ============  Handle Submit  ============ **/
  function handleSubmit () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post('/video-stream/edit', formData).then(ctx.goBack).catch(error => {
      console.log(error)
      handleOpenMessage(error.response.data.message)
    })
  }

  /** ============  Return UI ============ **/
  const onError = (e) => {
    console.log("Lỗi", e.name);
  }
  const ActionIcon = <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Live Stream" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
        <Grid container spacing={2} >


          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Thông tin cơ bản </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"stream.name"}
                    fullWidth
                    label="Tên live stream (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:200'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 200'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node} pred={"stream.display_name"}
                    fullWidth
                    label="Tên hiển thị (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:40'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 40'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} style={{ width: '100%' }}>
                    <KeyboardDateTimePicker
                      style={{ width: '100%' }}
                      format="HH:mm dd/MM/yyyy"
                      id="date-picker-dialog-date-streams"
                      label="Thời gian phát sóng"
                      inputVariant="outlined"
                      margin="dense"
                      // value={dateStartStreams}
                      value={new Date(dataEdit?.['stream.start_time'])}
                      onChange={handleChangeStartTime}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12} sm={3} lg={3}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"stream.view_virtual"}
                    fullWidth
                    label="Số lượng người xem"
                    variant="outlined"
                    margin="dense"
                    type="number"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={3} lg={3}>
                  <FormControlLabel
                    control={
                      <CheckBoxEdge
                        style={{ marginLeft: "20px" }}
                        Component={Checkbox}
                        node={node}
                        checked={node.getState('is_portrait')}
                        pred={"is_portrait"}
                        variant="outlined"
                        margin="dense"
                        color="primary"
                      />
                    }
                    label="Video dọc"
                    labelPlacement="end"
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>


          <Grid item xs={12} sm={12} lg={12} >
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Video LiveStream </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} style={{ marginTop: '10px' }}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    onChange={handleChangeChannelList}
                    options={channels}
                    value={stream_channel}
                    getOptionLabel={option => option?.['channel.name']}
                    onInputChange={onInputChannelChange}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn kênh stream"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"stream.video_transcode"}
                    fullWidth
                    label="Video Transcode"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>


          <Grid item xs={12} sm={12} lg={12} >
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}> {ActionIcon} </div>
                <Typography className={classge.contentTitle} variant="h5" > Hình ảnh </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} style={{ marginTop: '10px' }}>
                <Grid item >
                  <Card >
                    <CardContent style={{ minHeight: '84px' }}>
                      <Typography gutterBottom variant="h5" component="h2"> Image thumb </Typography>
                    </CardContent>
                    <CardMedia
                      children={
                        <Media
                          src={genCdnUrl(dataEdit['stream.image_thumb'], "image_banner.png")}
                          type="image"
                          style={{ width: 440, height: 225, marginBottom: '10px' }}
                          fileHandle={updateFile}
                          field="stream.image_thumb"
                          accept="image/*"
                        />
                      }
                    />
                  </Card>
                </Grid>
                <Grid item>
                  <Card>
                    <CardContent style={{ minHeight: '84px' }}>
                      <Typography gutterBottom variant="h5" component="h2"> Image fullscreen </Typography>
                    </CardContent>
                    <CardMedia
                      children={
                        <Media
                          src={genCdnUrl(dataEdit['stream.image_fullscreen'], "image_banner.png")}
                          type="image"
                          style={{ width: 440, height: 225, marginBottom: '10px' }}
                          fileHandle={updateFile}
                          field="stream.image_fullscreen"
                          accept="image/*"
                        />
                      }
                    />
                  </Card>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Danh sách sản phẩm live stream </Typography>
                <FormControlLabel
                  control={
                    <CheckBoxEdge
                      Component={Checkbox}
                      node={node}
                      pred={"stream.is_highlight"}
                      variant="outlined"
                      margin="dense"
                      color="primary"
                    />
                  }
                  label="Highlight sản phẩm"
                />
              </div>
              <Divider />
              <ListEdge
                Component={StreamProduct}
                node={node}
                pred={"stream.products"}
                listData={dataEdit['stream.products']}
              />
            </div>
          </Grid>


          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Xét duyệt hiển thị </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} style={{ marginTop: '10px' }}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    onChange={handleChangeStreamStatus}
                    options={STATUS_LIST}
                    value={stream_status}
                    getOptionLabel={option => (option.name || '')}
                    onInputChange={() => { }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>


        <Grid className={classge.rootSubmit}>
          <Button
            style={{ marginRight: '10px' }}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} /> Return
          </Button>
          <RoleButton actionType="Save" />
        </Grid>
      </ValidatorForm>
    </>
  )
}
