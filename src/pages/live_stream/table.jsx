import React, { useRef, useState, forwardRef } from 'react';
import api from 'services/api_cms';
import tblQuery from "services/tblQuery";
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import VisibilityIcon from '@material-ui/icons/Visibility';
import useStyles from 'pages/style_general';
import SettingsIcon from '@material-ui/icons/Settings';
import TimerOffIcon from '@material-ui/icons/TimerOff';

// const RELOAD_CODE = 0;
const productStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

export default function TableLiveStream ({ ctx, handleOpenMessage }) {
  const classes = useStyles()
  const table = useRef(null)

  function checkStatus (rowData) {
    if (Object.keys(rowData).length > 0) {
      const time_now = new Date().getTime()
      const time_start = new Date(rowData['stream.start_time'])
      const time_end = rowData['stream.end_time'] ? new Date(rowData['stream.end_time']) : null
      if (time_end) {
        return 1 // Đóng stream
      }
      else {
        if (time_start > time_now) {
          return 2 // 'Chưa live'
        }
        else if (time_start < time_now) {
          return 3 // 'Đang live'
        }
      }
    }
  }
  function parseDate (unixDate) {
    if (!unixDate)
      return ''
    const d = new Date(unixDate)
    return `${String(d.getHours()).padStart(2, '0')}:${String(d.getMinutes()).padStart(2, '0')} ${String(d.getDate()).padStart(2, '0')}/${String(d.getMonth() + 1).padStart(2, '0')}/${d.getFullYear()}`
  }
  function parseStatus (rowData) {
    if (Object.keys(rowData).length > 0) {
      if (rowData['display_status'] === 2) {
        const timeNow = new Date().getTime()
        if (rowData['stream.end_time']) {
          return 'Đã live'
        }
        else {
          if (rowData['stream.start_time'] < timeNow) {
            return 'Đang live'
          }
          else if (rowData['stream.start_time'] > timeNow) {
            return 'Chưa live'
          }
        }
      }
      else {
        return 'Đóng stream'
      }
    }
  }

  const [tblProps] = useState(() => {
    if (ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      icons: {
        Edit: forwardRef((props, ref) => <SettingsIcon {...props} ref={ref} />)
      },
      data: tblQuery("/video-stream/list"),
      columns: [
        {
          title: "Status", field: "display_status", editable: 'onUpdate',
          lookup: productStatus,
          render: rowData => <span>{productStatus[rowData['display_status']] ?? 'Inactive'}</span>,
          operator: 'eq'
        },
        { title: "UID", field: "uid", editable: 'never', filtering: false },
        { title: "Tên livestream", field: "stream.name", editable: 'never', operator: 'regexp' },
        { title: "Tên hiển thị", field: "stream.display_name", editable: 'never', filtering: false },
        {
          title: "Số sản phẩm", field: "stream.products", editable: 'never', filtering: false,
          render: rowData => <span>{rowData?.['stream.products']?.length ?? 0}</span>
        },
        {
          title: "Thời gian phát sóng", field: "stream.start_time", editable: 'never', filtering: false,
          render: rowData => <span>{parseDate(rowData['stream.start_time'])}</span>
        },
        {
          title: "Thời gian kết thúc", field: "stream.end_time", editable: 'never', filtering: false,
          render: rowData => <span>{parseDate(rowData['stream.end_time'])}</span>
        },
        {
          title: "Trạng thái", field: "display_status", editable: 'never', filtering: false,
          render: rowData => <span>{parseStatus(rowData)}</span>
        },
        { title: "Lượt xem thực tế", field: "stream.view_count", editable: 'never', filtering: false },
      ],
      title: <h1 className={classes.titleTable}>Live Streams</h1>,
      onRowClick (e) {
        if (selectedRow != e.currentTarget) {
          if (selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          e.currentTarget.style.backgroundColor = "#EEE"
          selectedRow = e.currentTarget
        }
      },
      onRowsPerPageChange (v) { this.options.pageSize = v },
      onPageChange (v) { this.options.page = v },
      options: {
        page: 0,
        pageSize: 10,
        pageSizeOptions: [10, 20, 50],
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        search: false,
        filtering: true,
        sorting: true,
        exportButton: true,
        grouping: false,
        actionsColumnIndex: -1
      }
    }
  })

  const handleTurnOffStream = (rowData) => {
    const fields = {
      set: {
        uid: rowData.uid,
        'stream.end_time': new Date().getTime()
      }
    }
    return api.post('/video-stream/edit', { fields }).then(res => {
      if (res.data?.statusCode === 200) {
        table.current?.onQueryChange()
      }
    }).catch(error => {
      console.log(error.response)
      handleOpenMessage(error.response.data.message)
    })
  }
  const handleChangeStatus = (rowData) => {
    const fields = {
      set: {
        uid: rowData?.uid,
        display_status: rowData?.display_status ?? 0
      }
    }
    return api.post('/video-stream/edit', { fields }).then(res => {
      if (res.data?.statusCode === 200) {
        table.current?.onQueryChange()
      }
    }).catch(error => {
      console.log(error.response)
      handleOpenMessage(error.response.data.message)
    })
  }

  // Return UI
  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tblProps}
        actions={[
          {
            icon: AddBox,
            tooltip: "Add LiveStreams",
            isFreeAction: true,
            onClick () {
              ctx.editData("Add");
            },
          },
          rowData => ({
            icon: rowData['display_status'] === 2 && checkStatus(rowData) === 3 ? VisibilityIcon : 'edit',
            tooltip: rowData['display_status'] === 2 && checkStatus(rowData) === 3 ? "View stream" : "Edit stream",
            onClick: (e, { tableData, ...rowData }) => {
              ctx.editData("Edit", rowData)
            },
          }),
          rowData => ({
            disabled: rowData['display_status'] !== 2 ? true : (checkStatus(rowData) === 1),
            icon: TimerOffIcon,
            tooltip: 'Tắt stream',
            onClick: (e, { tableData, ...rowData }) => handleTurnOffStream(rowData),
          })
        ]}
        editable={{
          onRowUpdate: handleChangeStatus,
          editTooltip: () => 'Cập nhật trạng thái live stream'
        }}
      />
    </div>
  )
}