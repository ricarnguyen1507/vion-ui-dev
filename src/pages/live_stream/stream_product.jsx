import React, { useState, useCallback } from 'react'
import api from 'services/api_cms'
import Divider from '@material-ui/core/Divider'
import Button from "@material-ui/core/Button"
import IconButton from '@material-ui/core/IconButton'
import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded'
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
// import useGeneral from 'pages/style_general'
import AddBox from '@material-ui/icons/AddBox'
import { InputEdge, OptionEdge, NodeTypes } from 'components/GraphMutation'

function convertTime (t) {
  return `${parseInt(t / 60 / 60)}h:${parseInt((t % (60 * 60)) / 60)}p:${t % 60}s`
}

function ItemStreamProduct ({ item, updateList, index, itemOption = {} }) {
  const [timer, setTimer] = useState(null)
  const [timeDisplay, setTimeDisplay] = useState(() => convertTime(item.state.getState('time')))
  // const [isDeleted, setIsDeleted] = useState(() => item.isDeleted)
  const [products, setProducts] = useState(() => {
    if (Object.keys(itemOption).length > 0 && itemOption?.['stream_product.product'] && Object.keys(itemOption?.['stream_product.product']).length > 0) {
      return [itemOption['stream_product.product']]
    }
    return []
  });

  const ToogleDelete = useCallback(() => {
    // setIsDeleted(item.isDeleted = !item.isDeleted)
    updateList()
  }, [item, updateList])

  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products || []
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }

  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }

  const onInputTimeChange = (e) => {
    setTimeDisplay(convertTime(e.target.value))
  }

  return (
    <>
      <div>
        <Typography variant="h6" > Sản phẩm # {index + 1}
          <IconButton color="primary" onClick={ToogleDelete}>
            <DeleteForeverRoundedIcon />
          </IconButton>
        </Typography>
      </div>
      <Divider />
      <Grid container spacing={2} >
        <Grid item xs={6} sm={6} lg={6}>
          <OptionEdge
            node={item.state}
            pred={"stream_product.product"}
            options={products}
            getOptionLabel={o => o?.['product_name'] ?? ""}
            style={{ width: "100%" }}
            onInputChange={onInputSelectProductChange}
            fullWidth
            renderInput={params => (
              <TextField {...params}
                label="Chọn sản phẩm"
                fullWidth
                variant="outlined"
                margin="dense"
              />
            )}
          />
        </Grid>
        <Grid item xs={6} sm={6} lg={6}>
          <InputEdge Component={TextField}
            node={item.state}
            pred={"time"}
            fullWidth
            variant="outlined"
            margin="dense"
            label={`Hiển thị sản phẩm lúc ${timeDisplay}`}
            placeholder="Thời gian hiển thị sản phẩm (s)"
            onChange={onInputTimeChange}
          />
        </Grid>
      </Grid>
    </>
  )
}

export default function ({ addItem, updateList, items, listData = [] }) {
  function addProducts () {
    addItem({
      uid: `_:itemListProd_${new Date().getTime()}`,
      "dgraph.type": "StreamProduct",
      time: 0
    }, NodeTypes.ORPHAN)
  }
  return (
    <>
      <Grid item xs={12} sm={12} lg={12} >
        <Button
          variant="contained"
          color="primary"
          component="span"
          startIcon={<AddBox />}
          style={{ marginTop: 20, marginBottom: 20 }}
          onClick={addProducts}
        >
          Thêm sản phẩm
        </Button>
        {items.map((item, index) => <ItemStreamProduct key={item.state.uid} index={index} item={item} updateList={updateList} itemOption={listData[index]} />)}
      </Grid>
    </>
  )
}