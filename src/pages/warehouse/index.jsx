import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
import TableWarehouse from './table'

const genders = ['Male', 'Female', 'Other']
function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [actionType, setActionType] = useState('')

  const [controlEditTable, setControlEditTable] = useState(false)
  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [mapTypes, setMapTypes] = useState(null)
  const [addresstypes, setAddressTypes] = useState(null)
  const [provinces, setProvinces] = useState(null)
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  useEffect(() => {
    const fetchs = [
      '/list/addresstype',
      '/list/areas',
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([addresstypes, areas]) => {
      setAddressTypes(addresstypes)
      setMapTypes(addresstypes.reduce((map, type) => {
        map[type.type_value] = type
        return map
      }, {}))
      setProvinces(areas)
    })

  }, []);

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }

  const functionEditData = (actionType, row = {}, index = -1) => {
    setActionType(actionType)
    setIndexEdit(index)
    setDataEdit(row)
  }
  const handleDelete = (dataDel) => api.delete(`/warehouse/${dataDel.uid}`)
    .then(_ => {
      const idx = dataTable.findIndex(d => d.uid === dataDel.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  const handleSubmitData = (actionType, data) => {
    if(Object.keys(data).length > 0) {
      api.post('/warehouse', data)
        .then(_ => {
          const { set } = data
          if(actionType === 'Add') {
            // set.uid = response.data.uids['new_warehouse']
            setDataTable([set, ...dataTable])
          } else {
            api.get(`/warehouse/${data.set.uid}`).then(res => {
              dataTable[indexEdit] = res.data.result[0]
              setDataTable([...dataTable])
            })
              .catch(error => console.log(error))
          }
          // setMode("table")
          setDataEdit(null)
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      // setMode("table")
      ctx.goBack()
    }
  }

  // Display view
  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        actionType={actionType}
        genders={genders}
        mapTypes={mapTypes}
        addresstypes={addresstypes}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
        listProvinces={provinces}
      />
    </Route>
    <Route exact path={path}>
      <TableWarehouse
        ctx={ctx}
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
        data={dataTable}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        genders={genders}
        mapTypes={mapTypes}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
        listProvinces={provinces}
        setDataTable={setDataTable}
      />
    </Route>
  </>
}
