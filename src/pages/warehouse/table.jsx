import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import api from 'services/api_cms'
import { Grid, IconButton, Tooltip} from "@material-ui/core";

export default function TableWarehouse ({setDataTable, ctx, stateQuery, setStateQuery, controlEditTable, setControlEditTable }) {
  const classes = useStyles()
  /* function getAddressTypeName (type_value) {
    const type = mapTypes[type_value]
    return type ? type.type_name : ""
  } */
  function getFilterStr ({filters}) {
    const strFilter = filters.map(({value, column: {o, field}}) => {
      if(typeof value === 'string') {
        value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
        if(value) {
          if (o) {
            return `${o}(${field},"${value}")`
          }
        }
      } else if(typeof value === 'number' || typeof value === 'boolean') {
        return `${o || 'eq'}(${field},${value})`
      } else if(Array.isArray(value)) {
        return value.map(v => `${o}(${field},${v})`).join(' OR ')
      }
      return ""
    }).filter(v => v.trim() !== "")

    return {strFilter: strFilter.join(' AND ')}
  }
  const [selectedRow, setSelectedRow] = useState(null)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get('/list/warehouse', {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...{filter: (strFilter ? ' and ' + strFilter : '')},
    }
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {
    const column = [
      { title: "Tên kho hàng", field: "name", o: 'eq', editable: "onUpdate" },
      { title: "Mã kho hàng", field: "store_code", o: 'eq', editable: "onUpdate" },
      { title: "Tên người liên hệ", field: "contact_person", o: 'eq', editable: "onUpdate" },
      { title: "Số đt liên hệ", field: "phone_number", o: 'eq', editable: "onUpdate" },
      {
        title: "Address",
        field: "address",
        render: rowData => rowData.address && `${rowData.address.address_des}, ${rowData.address.district && rowData.address.district.name}, ${rowData.address.province && rowData.address.province.name}`,
        filtering: false
      },
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Warehouse</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: _props => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Warehouse</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.editData() }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { data &&
                      <ExportExcel data={data} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),

        }}
        actions={[
          // {
          //     icon: AddBox,
          //     tooltip: 'Add Warehouse',
          //     isFreeAction: true,
          //     onClick: () => {
          //         functionEditData('Add')
          //     }
          // },
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Warehouse",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.editData(rowData)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}