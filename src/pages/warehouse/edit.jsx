import React, { useState } from 'react'
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import RoleButton from 'components/Role/Button';
import Button from "@material-ui/core/Button"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { clone, diff_customer, diffAddress } from 'services/diff'
import useGeneral from 'pages/style_general'
import api from 'services/api_cms'
// import ExportExcel from "./export_excel"

// const listAltFields = ['alt_name','alt_email','alt_phone','alt_address']

export default function ({ originData, actionType, functionBack, handleSubmitData, listProvinces }) {
  const classge = useGeneral()
  const [dataEdit, setDataEdit] = useState(() => {
    if (actionType === 'Add') {
      return {
        uid: '_:new_warehouse',
        address: {
          address_des: ''
        }
      }
    }
    if (actionType === 'Edit') {
      return clone(originData)
    }
  })


  // -------------------------------------------------------------------------
  // Handle Warehouse
  // -------------------------------------------------------------------------
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setDataEdit({ ...dataEdit, [name]: value })
  }
  /* const handleChangeCustomerGender = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'gender': genders.indexOf(item) })
  } */

  const handleSubmit = function () {
    let submitData = actionType === 'Edit' ? diff_customer(originData, dataEdit, dataEdit.uid) : { set: dataEdit }
    let delAddr = {}

    if (dataEdit.address && dataEdit.address.uid) {
      api.get(`/address/${dataEdit.address && dataEdit.address.uid}`)
        .then(({data}) => {
          let oldAddress = {}
          if (data.result) {
            oldAddress = data.result[0] || {}
          }
          const diffAddrData = diffAddress(oldAddress, dataEdit.address, address.uid)
          if (diffAddrData.set && diffAddrData.set.province) {
            dataEdit.address.province = diffAddrData.set.province
          } else {
            delete dataEdit.address.province
          }
          if (diffAddrData.set && diffAddrData.set.district) {
            dataEdit.address.district = diffAddrData.set.district
          } else {
            delete dataEdit.address.district
          }

          if (diffAddrData.del) {
            delAddr = {
              uid: address.uid,
              province: diffAddrData.del && diffAddrData.del.province || {},
              district: diffAddrData.del && diffAddrData.del.district || {}
            }
          }

          submitData = {
            ...submitData,
            set: {
              ...submitData.set,
              uid: dataEdit.uid,
              address: {
                ...dataEdit.address
              }
            },
            del: {
              ...delAddr
            }
          }
          console.log("submitData", submitData)
          handleSubmitData(actionType, submitData)
        })
    } else {
      handleSubmitData(actionType, submitData)
    }

    // submitData.set = {
    //     ...submitData.set,
    //     address: address
    // }

    // let delAddress = {uid: dataEdit.address.uid, province: {uid: dataEdit.address.province && dataEdit.address.province.uid}, district: {uid: dataEdit.address.district && dataEdit.address.district.uid} }
    // submitData.delAddress = delAddress


  }

  /* const handleChangeAddressType = (e, type) => {
    e.preventDefault()
    const val = (type && type.type_value) || ""
    setDataEdit({
      ...dataEdit, address:
            {
              ...dataEdit.address,
              address_type: val,
            }
    })
  } */

  const [listDistricts, setListDistricts] = useState(() => {
    if (dataEdit.address && dataEdit.address.province && dataEdit.address.province.uid) {
      return listProvinces.find(p => p.uid === dataEdit.address.province.uid).areas || []
    }
  })
  const [address, setAddress] = useState(() => ({
    ...dataEdit.address,
    province: {uid: dataEdit.address && dataEdit.address.province && dataEdit.address.province.uid},
    district: {uid: dataEdit.address && dataEdit.address.district && dataEdit.address.district.uid}
  }))

  const handleChangeAddressDes = (e) => {
    e.preventDefault()
    setDataEdit({
      ...dataEdit, address:
            {
              ...dataEdit.address,
              address_des: e.target.value
            }
    })
    setAddress({
      ...address,
      address_des: e.target.value
    })
  }

  const handleChangeAddressProvince = (e, item) => {
    e.preventDefault()
    setAddress({
      ...address,
      province: {
        uid: item && item.uid || null
      }
    })
    setDataEdit({
      ...dataEdit, address: {
        ...dataEdit.address,
        province: {
          uid: item && item.uid || null
        }
      }
    })
    if (item) {
      setListDistricts(listProvinces.find(p => p.uid === item.uid).areas || [])
    }
  }
  const handleChangeAddressDistrict = (e, item) => {
    e.preventDefault()
    setAddress({
      ...address,
      district: {
        uid: item && item.uid || null
      }
    })
    setDataEdit({
      ...dataEdit, address: {
        ...dataEdit.address,
        district: {
          uid: item && item.uid || null
        }
      }
    })
  }

  // View
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />

  // const [clonedData] = useState(() => [clone(dataEdit)]);
  return (
    <>
      <PageTitle title="Kho hàng" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    id="name"
                    label="Tên kho hàng"
                    variant="outlined"
                    margin="dense"
                    name="name"
                    onChange={handleChange}
                    value={dataEdit?.name || ''}
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    id="customerName"
                    label="Mã kho hàng"
                    variant="outlined"
                    margin="dense"
                    name="store_code"
                    onChange={handleChange}
                    value={dataEdit?.store_code || ''}
                    validators={[
                    ]}
                    errorMessages={[
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="contact_person"
                    name="contact_person"
                    label="Tên người liên hệ"
                    type="text"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit?.contact_person || ''}
                    validators={[
                    ]}
                    errorMessages={[
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="customerPhone"
                    name="phone_number"
                    label="Số điện thoại liên hệ"
                    variant="outlined"
                    type="string"
                    margin="dense"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit?.phone_number || ''}
                    validators={[
                      'maxStringLength:11',
                      'matchRegexp:[0-9]'
                    ]}
                    errorMessages={[
                      'Max phone number is 11',
                      'Number only'
                    ]}
                  />
                </Grid>

                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    id="address"
                    label="Nhập địa chỉ (bao gồm phường xã)"
                    type="text"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={handleChangeAddressDes}
                    value={dataEdit?.address && dataEdit?.address?.address_des ? dataEdit?.address?.address_des : ''}
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:1000'
                    ]}
                    errorMessages={[
                      'required',
                      'Min length is 3',
                      'Max length is 1000'
                    ]}
                  />
                </Grid>

                <Grid item xs={12} sm={6} lg={6}>
                  <Autocomplete
                    options={listProvinces}
                    getOptionLabel={option => option.product_name || option.name || ""}
                    value={address && address?.province && listProvinces && (listProvinces?.find(c => c.uid === address?.province?.uid) || [])}
                    style={{ width: "100%" }}
                    onChange={handleChangeAddressProvince}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn Tỉnh/Thành Phố"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <Autocomplete
                    options={listDistricts}
                    getOptionLabel={option => option?.product_name || option?.name || ""}
                    value={address && address.district && listDistricts && (listDistricts?.find(c => c.uid === address?.district?.uid) || [])}
                    style={{ width: "100%" }}
                    onChange={handleChangeAddressDistrict}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn Quận/Huyện"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>


            </div>

          </Grid>
        </Grid>

        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
          {/* { actionType === "Edit" &&
                        <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
