import React, { useState } from 'react';
import { useParams } from "react-router-dom"

// import ExportExcel from "./export_excel";
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import api from 'services/api_cms'
import useGeneral from 'pages/style_general'


import {
  Node,
  InputEdge
} from 'components/GraphMutation'

/* const passValidator = [
  'minStringLength:6',
  'maxStringLength:50'
] */

export default function ({ctx}) {
  const classge = useGeneral()

  const {actionType} = useParams()

  const [dataEdit] = useState(() => ctx.data ?? {
    uid: '_:new_brand',
    "dgraph.type": "Brand"
  })

  const [node] = useState(() => new Node(dataEdit))

  function handleSubmit () {
    api.post('/brand', node.getMutationForm())
      .then(ctx.goBack)
      .catch(err => {
        console.log(err)
      })
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />

  return (
    <>
      <PageTitle title="Brand" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={6} className={classge.inputTxt}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"brand_name"}
                    fullWidth
                    label="Brand của Nhà SX"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg className={classge.inputTxt}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"id"}
                    label="Mã Brand của Nhà SX"
                    variant="outlined"
                    type="string"
                    margin="dense"
                    fullWidth
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg className={classge.inputTxt}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"brand_shop_id"}
                    fullWidth
                    label="Mã Brand của FRT"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'Max length is 50'
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} />
              Return
          </Button>
          <RoleButton actionType={actionType} />
          {/* { actionType === "Edit" && <ExportExcel data={[dataEdit]} isEditTable={true} /> } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
