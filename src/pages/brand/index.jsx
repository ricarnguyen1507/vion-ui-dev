import React, { useState } from 'react'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

import Edit from './edit'
import TableUser from './table'


export default function () {
  const history = useHistory()
  const {path} = useRouteMatch()

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
    }
  }))

  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        ctx={ctx}
      />
    </Route>
    <Route exact path={path}>
      <TableUser ctx={ctx} />
    </Route>
  </>
}
