import React, { useRef, useState } from "react";
import MaterialTable, { MTableToolbar } from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import useStyles from "pages/style_general";
import api from 'services/api_cms';
import tblQuery from "services/tblQuery";
// import ExportExcel from "./export_excel";
import { Grid } from "@material-ui/core";

export default function TableUser ({ ctx }) {
  const classes = useStyles();
  const table = useRef(null)

  const [editable, setEditable] = useState(false)

  function onRowDelete ({tableData, ...rowData}) {
    console.log(tableData);
    return api.delete(`/brand/${rowData.uid}`).then(() => {
      table.current?.onQueryChange()
    }).catch(err => {
      alert('Xóa không thành công')
      console.log(err)
    })
  }

  const [tblProps] = useState(() => {
    if(ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      columns: [
        { title: "UID", field: "uid", filtering: false },
        { title: "Brand Của Nhà SX", field: "brand_name", operator: 'regexp', editable: "never" },
        { title: "Mã Brand Của Nhà SX", field: "id", operator: 'regexp', editable: "never" },
      ],
      data: tblQuery("/list/brand"),
      title: <h1 className={classes.titleTable}>Brand</h1>,
      onRowClick (e) {
        if(selectedRow != e.currentTarget) {
          if(selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          e.currentTarget.style.backgroundColor = "#EEE"
          selectedRow = e.currentTarget
        }
      },
      onRowsPerPageChange (v) { this.options.pageSize = v },
      onPageChange (v) { this.options.page = v },
      components: {
        Toolbar: props => (
          <>
            <div style={{padding: '0px 20px', textAlign: "left"}}>
              <Grid container className={classes.formpd}>
                <div style={{ flex: 24 }}> <MTableToolbar {...props} /> </div>
                {/* <div style={{ flex: 1 }}> { props.data && <ExportExcel data={props.data} isEditTable={false}/>} </div> */}
              </Grid>
            </div>
          </>
        )
      },
      options: {
        page: 0,
        pageSize: 10,
        pageSizeOptions: [10, 25, 50],
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        search: false,
        filtering: true,
        sorting: true,
        exportButton: false,
        grouping: false,
        actionsColumnIndex: -1
      }
    }
  })

  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tblProps}
        actions={[
          {
            icon: AddBox,
            tooltip: "Add Brand",
            isFreeAction: true,
            onClick () {
              ctx.editData();
            },
          },
          {
            icon: "edit",
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick () {
              setEditable(!editable);
            }
          },
          editable ? {
            icon: "edit",
            tooltip: "Edit Brand",
            onClick (e, { tableData, ...rowData }) {
              ctx.editData(rowData, tableData.id);
            },
          } : null
        ]}
        editable={editable ? {
          onRowDelete
        } : null}
      />
    </div>
  );
}