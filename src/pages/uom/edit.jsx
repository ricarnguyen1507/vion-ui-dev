import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AddBox from '@material-ui/icons/AddBox'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { InputEdge, Node } from 'components/GraphMutation'

export default function (props) {
  const { functionBack} = props
  const classge = useStylesGeneral()
  const history = useHistory()
  const ctx = useContext(context)
  const [dataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_UOM",
    "dgraph.type": "UOM"
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/uom')
  }
  function handleSubmit () {
    api.post("/uom", node.getMutationForm())
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="UOM" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
        onError={errors => console.log(errors)}
      >
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"UOM_name"}
                    fullWidth
                    label="Name UOM"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
