import React, {useState, useContext} from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

export default function TableUOM ({setDataTable, stateQuery, setStateQuery, controlEditTable, setControlEditTable }) {
  const classes = useStyles()
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  const [selectedRow, setSelectedRow] = useState(null)
  // Load data
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    return api.get("/list/uom", {
      number: query.pageSize || 10,
      page: query.page || 0
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const column = [
    { title: "UID", field: "uid", filtering: false },
    { title: "UOM Name", field: "UOM_name", editable: "onUpdate" },
  ]
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>UOM Type</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add UOM',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/Add`)
            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit UOM",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}