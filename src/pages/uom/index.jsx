import React, { useState } from 'react';
import api from 'services/api_cms';
import TableUOM from './table'
import Edit from './edit';
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  const [stateQuery, setStateQuery] = useState(() => ({
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }

  const handleDelete = (row) => api.delete(`/delete/${row.uid}`)
    .then(() => {
      const idx = dataTable.findIndex(d => d.uid === row.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  // Submit data ( Mode : Edit || Add )
  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          functionBack={functionBack}
        />
      </Route>
      <Route exact path={path}>
        <TableUOM
          data={dataTable}
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          setDataTable={setDataTable}
        />
      </Route>
    </Shared>
  )
}