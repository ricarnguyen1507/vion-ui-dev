import React from 'react';
import MaterialTable, { MTableToolbar } from 'material-table'
import SDStoreage from '@material-ui/icons/SdStorage'
import CloudUpload from '@material-ui/icons/CloudUpload'
import CloudDownload from '@material-ui/icons/CloudDownload'
import useStyles from 'pages/style_general'
import Chip from '@material-ui/core/Chip'
import api from 'services/api_cms'
export default function TableBackup () {
  const classes = useStyles()

  const startDumpDB = (e, type = 'all') => {
    if (type.length > 0) {
      api.get(`/backup_data/${type}`)
        .then(response => {
          console.log(response)
        })
        .catch(err => console.log(err))
    }
  }
  const startRecoveryDB = (e, type = 'all') => {
    if (type.length > 0) {
      api.get(`/recovery_data/${type}`)
        .then(response => {
          console.log(response)
        })
        .catch(err => console.log(err))
    }
  }
  const clonePartnerToSubOrder = () => {
    api.get(`/updatePartnerToSubOrder`)
      .then(response => {
        console.log(response)
      })
      .catch(err => console.log(err))
  }

  return (
    <div className="fade-in-table">
      <MaterialTable
        title={<h1 className={classes.titleTable}>Backup</h1>}
        options={{
          search: false
        }}
        components={{
          Toolbar: props => (
            <div>
              <MTableToolbar {...props} />
              <div style={{padding: '0px 10px', textAlign: "center"}}>
                <Chip
                  icon={<SDStoreage />}
                  label="DB Backup"
                  onClick={startDumpDB}
                  color="primary"
                  deleteIcon={<CloudDownload />}
                  variant="outlined"
                />
                <Chip
                  style={{margin: '0 0 0 25px'}}
                  icon={<SDStoreage />}
                  label="DB Dump Data"
                  clickable
                  onClick={startRecoveryDB}
                  color="secondary"
                  deleteIcon={<CloudUpload />}
                  variant="outlined"
                />
                <Chip
                  style={{margin: '0 0 0 25px'}}
                  icon={<SDStoreage />}
                  label="Clone Partner to SubOrder"
                  clickable
                  onClick={clonePartnerToSubOrder}
                  color="secondary"
                  deleteIcon={<CloudUpload />}
                  variant="outlined"
                />
              </div>
            </div>
          ),
        }}
      />
    </div>
  )
}