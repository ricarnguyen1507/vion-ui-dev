import React, { useState } from 'react';
import TableBackup from './table'

export default function () {
  const [mode] = useState("table")
  // Load data

  if (mode === "table") {
    return <TableBackup />
  }

}