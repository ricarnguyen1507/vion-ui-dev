import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
function parseDate (unixDate) {
  const d = new Date(unixDate)
  return `${String(d.getDate()).padStart(2, '0')}/${String(d.getMonth() + 1).padStart(2, '0')}/${d.getFullYear()}  ${String(d.getHours()).padStart(2, '0')}:${String(d.getMinutes()).padStart(2, '0')}`
}
function getFilterStr ({filters}) {
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  return {strFilter: strFilter.join(' AND ')}
}
export default function TableCollection ({ setCustomers, setProducts, setNegativeProducts, stateQuery, setStateQuery, setDataTable, controlEditTable, setControlEditTable, displayStatus, voucherType, referenceType }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const { strFilter} = getFilterStr(query)
    return api.get("/list/voucher?t=300", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...{filter: (strFilter ? ' and ' + strFilter : '')},
    }})
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        result.map(r => {
          let start = '', stop = ''
          if (r.start_at) {
            start = parseDate(r.start_at)
          }
          if (r.stop_at) {
            stop = parseDate(r.stop_at)
          }
          r.date_effect = `${start} - ${stop}`
        })
        // Data Collection
        // collectionData = setOrderList(data?.filter(r => r.is_deleted !== true));
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq'
      },
      {
        title: "Ngày hiệu lực",
        field: "date_effect",
      },
      {
        title: "Tên Voucher",
        field: "collection_name",
        render: rowData => !rowData.parent ? rowData.collection_name : <p>&nbsp;~&nbsp;&nbsp;{rowData.collection_name}</p>,
        o: 'eq'
      },
      {
        title: "Mã Voucher",
        field: "voucher_code",
        o: 'eq'
      },
      {
        title: "Loại Voucher",
        field: "voucher_type",
        render: rowData => <div>{voucherType[rowData.voucher_type]} - Giá trị({rowData.voucher_value})</div>
      },
      {
        title: "SKUs Áp dụng",
        field: "reference_type",
        render: rowData => <div>{referenceType[rowData.reference_type]}</div>
      },
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        parentChildData={(row, rows) => rows.find(r => row['~highlight.collections'] && row['~highlight.collections'][0] && (r.uid === row['~highlight.collections'][0].uid))}
        // title={<h1 className={classes.titleTable}>E-Voucher</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>E-Voucher</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { data &&
                                        <ExportExcel data={data.filter(r => r.is_deleted !== true)} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          // {
          //     icon: AddBox,
          //     tooltip: 'Add E-Voucher',
          //     isFreeAction: true,
          //     onClick: () => {
          //         functionEditData('Add')
          //     }
          // },
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit E-Voucher",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              if (rowData && rowData['voucher.customers']) {
                setCustomers(rowData['voucher.customers'])
              }
              if (rowData && rowData['highlight.products']) {
                setProducts(rowData['highlight.products'])
              }
              if (rowData && rowData['highlight.negative_products']) {
                setNegativeProducts(rowData['highlight.negative_products'])
              }
              history.push(`${pathname}/Edit`)

            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}
