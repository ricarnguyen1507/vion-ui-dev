import React, { useState, useMemo, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup';
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import { genCdnUrl } from 'components/CdnImage'
import ListManage from 'modules/SelectMulti/select_multi/list_manage'
import SelectMultiProduct from 'modules/SelectMulti/products/list_manage'
import SelectMultiCollection from 'modules/SelectMulti/collections/list_manage'
import DateFnsUtils from '@date-io/date-fns';
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import SectionManage from './select_multi/list_manage'
import { genUid } from "utils"
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { InputEdge, Node, CheckBoxEdge } from 'components/GraphMutation'

import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
// import ExportExcel from "./export_excel";
import { ObjectsToForm } from "utils"

export default function ({collections, functionBack, displayStatus, referenceType, voucherType, targetType, conditions, customers, setCustomers, products, setProducts, negativeProducts, setNegativeProducts, voucherSectionOpt, voucherOpt }) {
  const classge = useGeneral()
  const history = useHistory()

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    "dgraph.type": "Collection",
    "collection_type": 300,
    "reference_type": 0,
    "condition_type": 0,
    "target_type": 0,
    "voucher_type": 0
  })
  delete dataEdit['highlight.collections|display_order']
  if(dataEdit?.['highlight.collections']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['highlight.collections']) {
      obj['highlight.collections|display_order'] = `<highlight.collections> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  delete dataEdit['highlight.negative_products|display_order']
  if(dataEdit?.['highlight.negative_products']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['highlight.negative_products']) {
      obj['highlight.negative_products|display_order'] = `<highlight.negative_products> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  delete dataEdit['voucher.conditions|display_order']
  if(dataEdit?.['voucher.conditions']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['voucher.conditions']) {
      obj['voucher.conditions|display_order'] = `<voucher.conditions> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  // const checkDecimal = /^[-+]?[0-9]+\.[0-9]+$/
  const [node] = useState(() => new Node(dataEdit))
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/e_voucher')
  }
  function handleSubmit () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/voucher", formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [timer, setTimer] = useState(null)
  const [options] = useState(() => voucherSectionOpt.map(ls => ({
    uid: genUid("voucher_section"),
    ...ls
  })))

  const [updated] = useState({
    values: {
      'display_status': dataEdit?.display_status,
    }
  })

  const tCollections = useMemo(() => collections, [collections])
  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection_image/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  // const [validateVoucherCode, setValidateVoucherCode] = useState({})
  /* useMemo(() => {
    if (dataEdit?.list_voucher_code && dataEdit?.list_voucher_code !== "") {
      setValidateVoucherCode({})
    } else if (!dataEdit?.voucher_code || dataEdit?.voucher_code === "") {
      setValidateVoucherCode({error: true, helperText: "This field is required"})
    } else {
      clearTimeout(timer)
      setTimer(setTimeout(() => {
        api.post('/voucher/check-voucher', {
          voucher_code: dataEdit?.voucher_code || "",
          start_at: dataEdit?.start_at || new Date().getTime()
        }).then(res => {
          if (res?.data?.result && res.data.result.length > 0) {
            const result = res.data.result
            let is_owner = false
            for(let i = 0; i < result.length; i++) {
              if (result[i].uid === dataEdit.uid) {
                is_owner = true
              }
            }
            if (!is_owner) {
              setValidateVoucherCode({error: true, helperText: "This Voucher Code is exist"})
            }
          } else {
            setValidateVoucherCode({})
          }
        })
      }, 550))

    }

  }, [dataEdit?.voucher_code, dataEdit?.list_voucher_code, dataEdit?.start_at]) */

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    node.setState(name, value)
    setDataEdit({...dataEdit, [name]: value})
  }
  const handleChangeInputEdge = ({target: {name, value}}) => {
    setDataEdit({...dataEdit, [name]: value})
  }

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }

  const handleChangeReferenceType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'reference_type': referenceType.indexOf(item) })
    updated.values['reference_type'] = referenceType.indexOf(item)
    node.setState('reference_type', referenceType.indexOf(item))
    // if (referenceType.indexOf(item) === 2) {
    // Truong hop chon Tat ca san pham
    // invalidCallback(false)
    // }
  }
  const handleChangeConditionType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'condition_type': conditions.indexOf(item) })
    updated.values['condition_type'] = conditions.indexOf(item)
    node.setState('condition_type', conditions.indexOf(item))
  }

  const handleChangeVoucherType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'voucher_type': voucherType.indexOf(item) })
    updated.values['voucher_type'] = voucherType.indexOf(item)
    node.setState('voucher_type', voucherType.indexOf(item))
  }
  const handleChangeTargetType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'target_type': targetType.indexOf(item) })
    updated.values['target_type'] = targetType.indexOf(item)
    node.setState('target_type', targetType.indexOf(item))
  }

  const updateListProduct = async ({set}) => {
    let listHighligtOld = dataEdit?.['highlight.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'highlight.products')
  }
  const updateListNegProduct = ({set}) => {
    let listNegOld = dataEdit?.['highlight.negative_products'] ?? []
    const removeItem = listNegOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('highlight.negative_products', mergeArr, false)
      let negative_products = node.getState('highlight.negative_products') ?? []
      negative_products.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('highlight.negative_products', set, true)
    }
  }

  const updateListCollection = ({set}) => {
    let listHighlight_ColOld = dataEdit?.['highlight.collection'] ?? []
    const removeItem = listHighlight_ColOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('highlight.collection', mergeArr, false)
      let highlight_col = node.getState('highlight.collection') ?? []
      highlight_col.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('highlight.collection', set, true)
    }
  }

  const updateListCondition = ({set}) => {
    let listConditionsOld = dataEdit?.['voucher.conditions'] ?? []
    const removeItem = listConditionsOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('voucher.conditions', mergeArr, false)
      let conditions = node.getState('voucher.conditions') ?? []
      conditions.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('voucher.conditions', set, true)
    }
  }

  const updateListCustomer = ({set, del}) => {
    let voucher_cus = node.getState('voucher.customers') ?? []
    if(set.length >= voucher_cus.length) {
      node.setState('voucher.customers', set, true)
      voucher_cus = node.getState('voucher.customers') ?? []
    } else if(set.length < del.length) {
      if(voucher_cus.length) {
        const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
        voucher_cus.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
      }
    }
  }
  const phone_number_valid = /^\d+$/

  const onInputSelectCustomerChange = (e, val) => {
    let queries = ''
    if (phone_number_valid.test(val)) {
      queries = {phone_number: val.replace(/["\\]/g, '\\$&').trim()}
    } else if (val !== '') {
      queries = {customer_name: val.replace(/["\\]/g, '\\$&').trim()}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      if (queries !== "") {
        api.post(`/list/customer-options`, queries)
          .then(res => {
            const newOption = customers
            console.log(res.data.result)
            res.data.result.forEach(r => {
              if (!newOption.find(no => no.uid === r.uid)) {
                newOption.push(r)
              }
            })
            setCustomers([...newOption])
          })
      }
    }, 550))
  }

  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&')}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          // const newOption = []
          res.data.result.forEach(r => {
            if (!products.find(no => no.uid === r.uid)) {
              // newOption.push(r)
              setProducts([...products, r])
            }
          })
          // setProducts([...products, ...newOption])
        })
    }
  }

  const onInputSelectNegProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&')}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchNegProdOption(queries)
    }, 550))
  }
  function fetchNegProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          res.data.result.forEach(r => {
            if (!negativeProducts.find(no => no.uid === r.uid)) {
              setNegativeProducts([...negativeProducts, r])
            }
          })
        })
    }
  }

  // const [disableInput, setDisabledInput] = useState(false)
  const [selectedDateFrom, setSelectedDateFrom] = useState(() => {
    let from = new Date()
    /* if (dataEdit?.start_at && dataEdit?.start_at <= from.getTime()) {
      setDisabledInput(true)
    } */

    if (dataEdit?.start_at) {
      from = new Date(dataEdit?.start_at)
    }
    setDataEdit({...dataEdit, start_at: from.getTime()})
    return from
  });

  const handleDateFromChange = (date) => {
    setSelectedDateFrom(date)
    setDataEdit({...dataEdit, start_at: new Date(date).getTime()})
    node.setState('start_at', new Date(date).getTime())
  };
  const [selectedDateTo, setSelectedDateTo] = useState(() => {
    let to = new Date()
    if (dataEdit?.stop_at) {
      to = new Date(dataEdit?.stop_at)
    }
    setDataEdit({...dataEdit, stop_at: to.getTime()})
    return to
  });
  const handleDateToChange = (date) => {
    setSelectedDateTo(date)
    setDataEdit({...dataEdit, stop_at: new Date(date).getTime()})
    node.setState('stop_at', new Date(date).getTime())
  }

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  // const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  // function invalidCallback (result) {
  //   setMultiSelectInvalid(result)
  // }

  let validateForm = {}
  validateForm = useMemo(() => {
    if(dataEdit.condition_type === 1 && (!dataEdit.condition_value || dataEdit.condition_value === "")) {
      return {
        ...validateForm,
        condition_value: {
          error: true,
          helperText: "This field is required"
        }
      }
    } else {
      return {}
    }
  }, [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Vouchers" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Information
                    </Typography>
                  </div>
                  <Divider />
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6} lg={6}>
                      <FormControlLabel
                        control={
                          <CheckBoxEdge
                            Component={Checkbox}
                            node={node}
                            pred={"is_internal"}
                            fullWidth
                            variant="outlined"
                            margin="dense"
                            color="primary"
                            style={{margin: "25px 10px"}}
                          />
                        }
                        label="Voucher nội bộ"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={6}>
                      <FormControlLabel
                        control={
                          <CheckBoxEdge
                            Component={Checkbox}
                            node={node}
                            pred={"auto_apply"}
                            fullWidth
                            variant="outlined"
                            margin="dense"
                            color="primary"
                            style={{margin: "25px 10px"}}
                          />
                        }
                        label="Tự động áp dụng"
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6} lg={6}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"voucher_label"}
                        fullWidth
                        label="Nhãn hiển thị lên TV"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'required',
                        ]}
                        errorMessages={[
                          'This field is required',
                        ]}
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={6}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"collection_name"}
                        fullWidth
                        label="Tên Voucher chi tiết dễ hiểu chỉ hiển thị trong CMS"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'required',
                          'minStringLength:3',
                        ]}
                        errorMessages={[
                          'This field is required',
                          'Min length is 3']}
                      />
                    </Grid>
                  </Grid>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"list_voucher_code"}
                    fullWidth
                    label="Danh sách mã Voucher (Schema voucher)"
                    variant="outlined"
                    margin="dense"
                  />
                  {!dataEdit?.list_voucher_code || dataEdit?.list_voucher_code?.length === 0 ?
                    <InputEdge
                      Component={TextField}
                      node={node}
                      pred={"voucher_code"}
                      fullWidth
                      label="Mã Voucher"
                      variant="outlined"
                      margin="dense"
                      validators={[
                        'required',
                        'minStringLength:3',
                      ]}
                      errorMessages={[
                        'This field is required',
                        'Min length is 3']}
                    /> : ""}

                  <Autocomplete
                    options={voucherType}
                    value={voucherType?.[dataEdit?.voucher_type || 0]}
                    onChange={handleChangeVoucherType}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Loại Voucher"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} lg={12}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"voucher_value"}
                        fullWidth
                        label="Giá trị của Voucher"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'required',
                        ]}
                        errorMessages={[
                          'This field is required']}
                      />
                    </Grid>
                  </Grid>
                  { dataEdit?.voucher_type === 1 ?
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={12} lg={12}>
                        <InputEdge
                          Component={TextValidator}
                          node={node}
                          pred={"max_applied_value"}
                          fullWidth
                          label="Giá trị tối đa được áp dụng"
                          variant="outlined"
                          margin="dense"
                          validators={[
                            'required',
                          ]}
                          errorMessages={[
                            'This field is required']}
                        />
                      </Grid>
                    </Grid> : '' }
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"redeem"}
                    fullWidth
                    label="Số lần sử dụng trên mỗi đối tượng được áp dụng"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                    ]}
                    errorMessages={[
                      'This field is required']}
                  />
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"limit_redeem"}
                    fullWidth
                    label="Số lượng phát hành của voucher"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                    ]}
                    errorMessages={[
                      'This field is required']}
                  />
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} lg={12}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justifyContent="space-around">
                          <KeyboardDatePicker
                            format="dd/MM/yyyy"
                            margin="normal"
                            id="date-picker-dialog"
                            label="Ngày bắt đầu"
                            value={selectedDateFrom}
                            onChange={handleDateFromChange}
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                          />
                          <KeyboardTimePicker
                            margin="normal"
                            label="Thời gian từ"
                            value={selectedDateFrom}
                            onChange={handleDateFromChange}
                            KeyboardButtonProps={{
                              'aria-label': 'change time',
                            }}
                          />
                          <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            label="Ngày hết hạn"
                            format="dd/MM/yyyy"
                            value={selectedDateTo}
                            onChange={handleDateToChange}
                            KeyboardButtonProps={{
                              'aria-label': 'change date',
                            }}
                          />
                          <KeyboardTimePicker
                            margin="normal"
                            label="Thời gian đến"
                            value={selectedDateTo}
                            onChange={handleDateToChange}
                            KeyboardButtonProps={{
                              'aria-label': 'change time',
                            }}
                          />
                        </Grid>
                      </MuiPickersUtilsProvider>
                    </Grid>
                  </Grid>
                </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                      SKUs Áp dụng
                    </Typography>
                  </div>
                  <Divider />
                  <Autocomplete
                    options={referenceType}
                    value={referenceType?.[dataEdit?.reference_type || 0]}
                    onChange={handleChangeReferenceType}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="SKUs Áp dụng"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                  <SelectMultiCollection
                    isShow={dataEdit?.reference_type === 1}
                    listOption={tCollections}
                    currentList={dataEdit?.['highlight.collections'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCollection}
                    maxItem="-1"
                    minItem="1"
                    listTitle="Thêm ngành hàng"
                    facetPrefix="highlight.collections"
                    // invalidCallback={invalidCallback}
                  />
                  <SelectMultiProduct
                    isShow={dataEdit?.reference_type === 0}
                    listOption={products}
                    currentList={dataEdit?.['highlight.products'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListProduct}
                    maxItem="-1"
                    minItem="1"
                    listTitle="Thêm sản phẩm"
                    facetPrefix="highlight.products"
                    inputChange={onInputSelectProductChange}
                    // invalidCallback={invalidCallback}
                  />
                  <Autocomplete
                    options={conditions}
                    value={conditions?.[dataEdit?.condition_type || 0]}
                    onChange={handleChangeConditionType}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Điều kiện áp dụng"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                  {dataEdit?.condition_type === 1 ?
                    <InputEdge
                      Component={TextField}
                      {...validateForm['condition_value'] || {}}
                      node={node}
                      pred={"condition_value"}
                      fullWidth
                      label="Giá trị của điều kiện"
                      variant="outlined"
                      margin="dense"
                      onChange={handleChangeInputEdge}
                    /> : ""}
                </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Vùng điều kiện
                    </Typography>
                  </div>
                  <RadioGroup value={dataEdit?.operator} name="operator">
                    <FormControlLabel
                      value="AND"
                      key="operator_and"
                      control={
                        <Radio
                          color="primary"
                          onChange={ e => { handleChange(e, "AND") } }
                        />
                      }
                      label="AND"
                    />
                    <FormControlLabel
                      value="OR"
                      key="operator_or"
                      control={
                        <Radio
                          color="primary"
                          onChange={ e => { handleChange(e, "OR") } }
                        />
                      }
                      label="OR"
                    />
                  </RadioGroup>
                  <Divider />
                  <SectionManage
                    listOption={options}
                    voucherOpt={voucherOpt}
                    currentList={dataEdit?.['voucher.conditions'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCondition}
                    maxItem="5"
                    listTitle="Thêm điều kiện"
                    facetPrefix="voucher.conditions"
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Sản phẩm KHÔNG áp dụng
                    </Typography>
                  </div>
                  <Divider />
                  <ListManage
                    listOption={negativeProducts}
                    currentList={dataEdit?.['highlight.negative_products'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListNegProduct}
                    maxItem="-1"
                    listTitle="Thêm sản phẩm KHÔNG áp dụng"
                    facetPrefix="highlight.negative_products"
                    inputChange={onInputSelectNegProductChange}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Đối tượng Áp dụng
                    </Typography>
                  </div>
                  <Divider />
                  <Autocomplete
                    options={targetType}
                    value={targetType?.[dataEdit?.target_type || 0]}
                    onChange={handleChangeTargetType}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Đối tượng áp dụng"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                  <ListManage
                    isShow={dataEdit?.target_type !== 0}
                    listOption={customers}
                    currentList={dataEdit?.['voucher.customers'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCustomer}
                    maxItem="-1"
                    listTitle="Thêm khách hàng"
                    inputChange={onInputSelectCustomerChange}
                  />
                </div>
              </Grid>
            </Grid>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                                    Xét duyệt
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus?.[dataEdit?.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <h5>Image (400x400)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit?.collection_image, "collection_image.png")} type="image" style={{ width: 256, height: 256 }} fileHandle={updateImage} field="collection_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
          {/* <Grid item>
                        <h5>Pop-up (1600x900)</h5>
                        <Card>
                            <CardMedia children={<Media src={genCdnUrl(dataEdit.image_highlight, "image_highlight.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="image_highlight" accept="image/jpeg, image/png" />} />
                        </Card>
                    </Grid> */}
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType}
          // disabled={Object.keys(validateForm).length !== 0}
          />
          {/* { actionType === "Edit" &&
                         <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
