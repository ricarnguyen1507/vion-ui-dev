import React, { useState } from 'react'
import Item from './item'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import AddBox from '@material-ui/icons/AddBox'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import { clone } from 'services/diff'
import Typography from '@material-ui/core/Typography'
import RootRef from "@material-ui/core/RootRef";
import useGeneral from 'pages/style_general'
import ImportExportIcon from '@material-ui/icons/ImportExport';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { genUid } from "utils"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
/* const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
}) */

export default function ({listOption, currentList = [], voucherOpt, updateListManage, dataEdit, isArray = true, listTitle = "", label = "Vị trí số: ", keyName = false, maxItem = -1, isShow = true, inputChange, facetPrefix }) {
  const classge = useGeneral()
  const [dataMap, setDataMap] = useState(() => {
    if (currentList.length > 0) {
      return currentList.map((cur, index) => ({number: index, ...cur}))
    } else {
      return [
        {
          number: 0,
          uid: null,
          section_value: null,
          section_name: null,
          section_type: null,
          section_ref: null
        }
      ]
    }
  })

  const [newList, setNewList] = useState(() => {
    if (dataMap?.length === 1 && dataMap?.[0]?.uid === null) {
      return [
        {
          ...dataMap[0],
          uid: genUid('drag_item')
        }
      ]
    } else {
      return clone(dataMap)
    }

  })
  const updateItem = (uid, originItem, subItemUid = null) => {
    console.log('Update Ref', uid, originItem, subItemUid)

    newList.forEach(n => {
      if (n.number === originItem.number) {
        if (!subItemUid) {
          const reNew = (uid !== null && uid.startsWith("_:")) ? genUid('voucher_section') : uid
          const selected = listOption.find(l => l.uid === uid)
          n.uid = reNew || genUid('drag_item')
          n.section_value = selected?.section_value
          n.section_name = selected?.section_name
          n.section_type = selected?.section_type
          n.section_ref = selected?.section_ref || null
          n['dgraph.type'] = "VoucherCondition"
        } else {
          const selected = listOption.find(l => l.section_value === originItem.section_value)
          n.section_ref = subItemUid || selected?.section_ref || null
        }
      }
    })
    callUpdateList(newList)
  }


  const onAdd = () => {
    if (maxItem == -1 || dataMap.length < maxItem) {
      setDataMap([...dataMap, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
      setNewList([...newList, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
    }
  }

  // HANDLE DRAG AND DROP IN TABLE
  // Arrange order of data list
  function reorderList (list, from, to) {
    const result = Array.from(list)
    const [removed] = result.splice(from, 1)
    result.splice(to, 0, removed)
    return result
  }

  const onDragEnd = result => {
    // dropped outside the list
    const { source, destination} = result;

    if (!result && !destination) {
      return;
    }
    if (!source || !destination) {
      return;
    }
    const orderedList = reorderList(newList, source.index, destination.index)
    // Reorder ordinal number of array data
    for (let i = 0; i < orderedList.length; i++) {
      orderedList[i].number = i
    }
    setNewList(orderedList)

    callUpdateList(orderedList)
  }

  function callUpdateList (orderedList) {
    const facetDisplayOrder = `${facetPrefix}|display_order`
    let set = orderedList.filter(li => !li.uid.startsWith("_:drag_item") && li.uid).map(n => ({uid: n.uid, display_order: n.number, [facetDisplayOrder]: `<${facetPrefix}> <${n.uid}> (display_order=${n.number}) .`, ...n, 'dgraph.type': "VoucherCondition"}))
    let del = dataMap.filter(li => li.uid && !li.uid.startsWith("_:")).map(n => ({uid: n.uid}))
    updateListManage({set, del})
  }

  // Render
  return (
    <div>
      { isShow ?
        <>
          <div className={classge.titlePanel} style={{margin: "20px 15px 0 0"}}>
            <Typography className={classge.contentTitle} variant="h5" >
              {listTitle} {maxItem === "-1" ? " (unlimited)" : maxItem === "0" ? "" : ` (tối đa: ${maxItem})`}
              {isArray ?
                <IconButton className={classge.btnDelete} onClick={onAdd}>
                  <AddBox />
                </IconButton>
                :
                ""}
            </Typography>
          </div>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided) => (
                <RootRef rootRef={provided.innerRef}>
                  <List >
                    {/* <Grid container spacing={2}> */}
                    {newList.map((value, idx) => (
                      <Draggable key={value.uid} draggableId={value.uid} index={idx}>
                        {(provided) => (
                          // <Grid item xs={12} sm={(12/cols)} lg={(12/cols)} >
                          <ListItem
                            classes={{
                              secondaryAction: classge.secondaryActionRoot,
                            }}
                            style={{display: "flex", alignItems: "center", padding: "5px", cursor: "grab"}}
                            ContainerComponent="li"
                            ContainerProps={{ ref: provided.innerRef }}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Item
                              key={keyName ? keyName : dataEdit.uid + "-dentify-item-" + value.uid}
                              listOption={listOption}
                              voucherOpt={voucherOpt}
                              originItem={value}
                              dataEdit={dataEdit}
                              label={label + (value.number + 1)}
                              updateItem={updateItem}
                              inputChange={inputChange}

                            />
                            <ImportExportIcon style={{marginLeft: "6px"}}/>
                            <ListItemSecondaryAction >
                            </ListItemSecondaryAction>
                          </ListItem>
                          // </Grid>
                        )}
                      </Draggable>
                    ))}
                    {/* </Grid> */}
                    {provided.placeholder}
                  </List>
                </RootRef>
              )}
            </Droppable>
          </DragDropContext>
        </>
        : ""}
    </div>
  )
}
