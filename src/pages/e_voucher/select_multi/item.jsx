import React, { useState, useMemo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = {}, listOption, voucherOpt, updateItem, label, inputChange}) {

  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  const gridColl = useMemo(() => {
    if (selectedItem?.section_value === 'previous_voucher' || selectedItem?.section_value === 'min_amount') {
      return 4
    } else {
      return 12
    }
  }, [selectedItem])
  const listSubOption = useMemo(() => {
    if (selectedItem?.section_value === 'previous_voucher') {
      return voucherOpt
    } else {
      return []
    }
  }, [selectedItem])


  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()

    setSelectedItem(item)
    updateItem(item?.uid || null, originItem)
  }

  const handleChangeSubItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem({...selectedItem, section_ref: item?.uid || null})
    updateItem(selectedItem?.uid || null, originItem, item?.uid || null)
  }
  const handleChangeSubItemValue = (e) => {
    e.preventDefault()
    const { value } = e.target
    setSelectedItem({...selectedItem, section_ref: value || null})
    updateItem(selectedItem?.uid || null, originItem, value)
  }

  function optionLabel (option) {
    if (option.product_name) {
      if (option.sku_id) {
        return `${option.product_name} - ${option.sku_id}`
      } else {
        return option.product_name
      }
    } else if (option.collection_name) {
      return option.collection_name
    } else if (option.phone_number) {
      if (option.customer_name) {
        return `${option.phone_number} - ${option.customer_name} - ${option.uid}`
      } else {
        return `${option.phone_number} - ${option.uid}`
      }
    } else if (option.section_name) {
      return option.section_name
    }
  }

  // Render
  return (
    originItem ?
      <Grid container spacing={2}>
        <Grid item xs={12} sm={gridColl} lg={gridColl}>
          <Autocomplete
            options={listOption}
            getOptionLabel={option => optionLabel(option) || ''}
            value={selectedItem && selectedItem.section_value && (listOption.find(c => c.section_value === selectedItem.section_value) || null)}
            style={{ width: "100%" }}
            onChange={handleChangeItemOptions}
            onInputChange={inputChange}
            renderInput={params => (
              <TextField {...params}
                label={ label }
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        {selectedItem?.section_value === 'previous_voucher' ?
          <Grid item xs={12} sm={8} lg={8}>
            <Autocomplete
              options={listSubOption}
              getOptionLabel={option => optionLabel(option) || ''}
              value={selectedItem && selectedItem.section_ref && (listSubOption.find(c => c.uid === selectedItem.section_ref) || null)}
              style={{ width: "100%" }}
              label="Giá trị"
              onChange={handleChangeSubItemOptions}
              renderInput={params => (
                <TextField {...params}
                  variant="outlined"
                  fullWidth
                  margin="dense"
                />
              )}
            />
          </Grid>
          : selectedItem?.section_value === 'min_amount' ?
            <Grid item xs={12} sm={8} lg={8}>
              <TextField
                fullWidth
                id="min_amount_value"
                label="Giá trị"
                variant="outlined"
                margin="dense"
                name="min_amount_value"
                onChange={handleChangeSubItemValue}
                defaultValue={selectedItem.section_ref || ""}
              />
            </Grid>
            : ""}
      </Grid>
      : ""
  )
}
