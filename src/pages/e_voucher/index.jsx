import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const display_status = ['PENDING', 'REJECTED', 'APPROVED']
const reference_type = ["Sản phẩm", "Ngành Hàng", "Tất cả sản phẩm"]
const voucher_type = ["Giảm giá trực tiếp", "Giảm theo %", "Freeship"]
const target_type = ["Tất cả Customer có Mã Voucher", "Chọn Customer"]
const conditions = ["Không có điều kiện gì khác", "Tổng tiền đơn hàng tối thiểu"]
/**
 * Export
 */

const VoucherSectionOpt = [
  {
    section_value: 'min_amount',
    section_name: "Đơn hàng tối thiểu",
    section_type: null,
    section_ref: null,
    display_order: 1
  },
  {
    section_value: 'first_order',
    section_name: "Đơn hàng đầu tiên",
    section_type: null,
    section_ref: null,
    display_order: 2
  },
  {
    section_value: 'previous_voucher',
    section_name: "Phải dùng Voucher này trước",
    section_type: null,
    section_ref: null, //0xa24f1 || FPTER150
    display_order: 3
  },

]

function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  // const [mapTypes, setMapTypes] = useState(null)
  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [collections, setCollections] = useState(null)
  const [products, setProducts] = useState([])
  const [negativeProducts, setNegativeProducts] = useState([])
  const [customers, setCustomers] = useState([])

  useEffect(() => {
    const fetchs = [
      "/list/collection?t=0"
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([
      collections
    ]) => {
      setCollections(collections)
    })
  }, []);

  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      } else {
        setDataEdit([])
      }
      if (row && row['voucher.customers']) {
        setCustomers(row['voucher.customers'])
      }
      if (row && row['highlight.products']) {
        setProducts(row['highlight.products'])
      }
      if (row && row['highlight.negative_products']) {
        setNegativeProducts(row['highlight.negative_products'])
      }
    }
  }))
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setIndexEdit(index)
    // setMode("edit")
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang
  const handleSubmitData = async (actionType, { set, del, files }) => {
    // Add / Update
    if (Object.keys(set).length > 0) {
      const formData = new FormData()
      formData.set('set', JSON.stringify(set))
      formData.set('del', JSON.stringify(del))
      for(let field in files) {
        formData.set(field, files[field])
      }
      api.post('/voucher', formData, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data"
        }
      })
        .then(res => {
          const response = res.data['new_collection']
          let new_collection = response.result && response.result[0] || {}
          if (actionType === 'Add') {
            setDataTable([new_collection, ...dataTable])
          } else if (actionType === 'Edit') {
            dataTable[indexEdit] = {...new_collection}
            setDataTable([...dataTable])
          }
          // setMode("table")
          setDataEdit(null)
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      // setMode("table")
      ctx.goBack()
    }
  }

  const handleDelete = (row) => api.delete(`/voucher/${row.uid}`)
    .then(res => {
      if (res.status === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          collections={collections}
          actionType={actionType}
          originData={dataEdit}
          functionBack={functionBack}
          handleSubmitData={handleSubmitData}
          displayStatus={display_status}
          referenceType={reference_type}
          voucherType={voucher_type}
          targetType={target_type}
          conditions={conditions}
          fetchData={fetchData}
          customers={customers}
          setCustomers={setCustomers}
          products={products}
          setProducts={setProducts}
          negativeProducts={negativeProducts}
          setNegativeProducts={setNegativeProducts}
          // setMode={setMode}
          voucherSectionOpt={VoucherSectionOpt}
          voucherOpt={dataTable}
        />
      </Route>
      <Route exact path={path}>
        <TableCollection
          data={dataTable}
          setCustomers={setCustomers}
          setProducts={setProducts}
          setNegativeProducts={setNegativeProducts}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setDataTable={setDataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          displayStatus={display_status}
          referenceType={reference_type}
          voucherType={voucher_type}
          targetType={target_type}
          conditions={conditions}
        />
      </Route>
    </Shared>
  )
}
