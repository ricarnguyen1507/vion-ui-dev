import React, { useState, useCallback } from 'react'
import { withStyles } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import MuiTableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'

import TextField from '@material-ui/core/TextField'
import Button from "@material-ui/core/Button"
import DeleteIcon from '@material-ui/icons/Delete'
import AddBox from '@material-ui/icons/AddBox'

import { NodeTypes } from 'components/GraphMutation'

const TableCell = withStyles({
  root: {
    borderBottom: "none"
  }
})(MuiTableCell)

function TrackingParameter ({ node, item, updateList, items, parameterUtms }) {
  const [isDeleted, setIsDeleted] = useState(() => item.isDeleted)

  const ToogleDelete = useCallback(() => {
    setIsDeleted(item.isDeleted = !item.isDeleted)
    updateList()
  }, [item, updateList])

  const optionsFilter = useCallback((options) => {
    const result = options.filter(option => !items.some(e => e.state.getUid('parameter_utm') === option.uid))
    return result
  }, [items])

  const [parameterUtm] = useState(() => parameterUtms.find(({uid}) => uid === node.getUid('parameter_utm')) || null)

  const [tracking_values, setTrackingValues] = useState(() => parameterUtm?.tracking_values || [])

  const [tracking_value, setTrackingValue] = useState(() => tracking_values.find(({uid}) => uid === node.getUid('tracking_value')) ?? null)

  function onSelectParameterUtm (e, paramUtm) {
    const tracking_values = paramUtm?.tracking_values || []
    const initValue = tracking_values[0] || null
    // Reset to default
    setTrackingValues(tracking_values)
    setTrackingValue(initValue)
    node.setState('parameter_utm', paramUtm)
    node.setState('tracking_value', initValue)
  }
  function onSelectTrackingValue (e, trackVal) {
    setTrackingValue(trackVal)
    node.setState('tracking_value', trackVal)
  }

  return (
    <TableRow>
      <TableCell>
        <Autocomplete
          filterOptions={optionsFilter}
          options={parameterUtms}
          defaultValue={parameterUtm}
          getOptionLabel={o => o.tracking_code || ""}
          onChange={onSelectParameterUtm}
          renderInput={params => (
            <TextField {...params}
              label="Mã tracking"
              variant="outlined"
              fullWidth
              margin="dense"
            />
          )}
        />
      </TableCell>
      <TableCell>
        <Autocomplete
          options={tracking_values}
          getOptionLabel={o => o.val_name || ""}
          value={tracking_value}
          onChange={onSelectTrackingValue}
          renderInput={params => (
            <TextField {...params}
              label="Giá trị"
              variant="outlined"
              fullWidth
              margin="dense"
            />
          )}
        />
      </TableCell>
      <TableCell style={{width: 150, textAlign: 'center'}}>
        <Button
          startIcon={<DeleteIcon />}
          onClick={ToogleDelete}
        >
          { isDeleted ? 'Undelete' : 'Delete' }
        </Button>
      </TableCell>
    </TableRow>
  )
}

export function TrackingParameters ({addItem, updateList, items, parameterUtm}) {
  function addParam () {
    if(items.length < parameterUtm.length) {
      addItem({
        uid: `_:trackingParam_${new Date().getTime()}`,
        "dgraph.type": "TrackingParameter"
      }, NodeTypes.ORPHAN)
    }
  }
  return <>
    <Button
      variant="contained"
      startIcon={<AddBox />}
      style={{ marginTop: 35, marginBottom: 15 }}
      onClick={addParam}
    >
        Add Param
    </Button>
    <Table size="small">
      <TableBody>
        {items.map(item => <TrackingParameter key={item.state.uid} node={item.state} parameterUtms={parameterUtm} items={items} item={item} updateList={updateList} />)}
      </TableBody>
    </Table>
  </>
}