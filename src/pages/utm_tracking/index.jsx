import React, {
  useState,
  useEffect
} from 'react'
import {
  Route,
  useRouteMatch
} from "react-router-dom"

import Table from './table'
import Edit from './edit'

import api from 'services/api_cms'

import { Shared } from 'context/Shared'

export default function () {
  const { path } = useRouteMatch()

  const [sharedData] = useState(() => new Map())

  const [parameterUtm, setParameterUtm] = useState(() => [])

  useEffect(() => {
    api.get('/parameterUtm').then(response => {
      setParameterUtm(response.data.result)
    })
  }, [])

  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit parameterUtm={parameterUtm} />
      </Route>
      <Route exact path={path}>
        <Table />
      </Route>
    </Shared>
  )
}