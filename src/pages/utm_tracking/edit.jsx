import React, {
  useContext,
  useState
} from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import TextField from '@material-ui/core/TextField'
import api from 'services/api_cms'

import { TrackingParameters } from './tracking_parameters'

import { context } from 'context/Shared'

import { InputEdge, ListEdge, Node } from 'components/GraphMutation'

export default function ({parameterUtm}) {
  const classge = useGeneral()

  const history = useHistory()

  const ctx = useContext(context)

  const [actionType] = useState(() => ctx.get('dataEdit') ? 'Edit' : 'Add')

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/tracking-utm')
  }

  const [node] = useState(() => new Node(ctx.get('dataEdit') || {
    "uid": "_:trackingUtm",
    "dgraph.type": "TrackingUtm",
    "tracking_parameters": []
  }))

  function handleSubmit () {
    api.post('/trackingUtm', node.getMutationForm())
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Tracking Utm" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container>
          <Grid item xs={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Thông Tin
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"tracking_name"}
                    fullWidth
                    label="Tên Tracking"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"short_desc"}
                    fullWidth
                    label="Diễn giải"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
              </Grid>
              <div>
                                &nbsp;
                <ListEdge
                  Component={TrackingParameters}
                  node={node}
                  pred={"tracking_parameters"}
                  parameterUtm={parameterUtm}
                />
              </div>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            startIcon={<ReturnIcon />}
            onClick={goBack}
          >
            Return
          </Button>
          <RoleButton page="collection" actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}