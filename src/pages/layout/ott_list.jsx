import React, { useState } from 'react'
import Checkbox from '@material-ui/core/Checkbox'
// import RadioGroup from '@material-ui/core/RadioGroup';
// import Radio from '@material-ui/core/Radio'
import useStyles from './edit_style'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function CheckBoxGroupOTT ({ node, pred, listData = [], type }) {
  const classes = useStyles()
  const [checkedItems] = useState(() => node.getState(pred)?.map(item => item.state.uid) ?? [])
  const [allItems] = useState(() => {
    node.setState(pred, listData, false)
    return node.getState(pred) ?? []
  })
  function isChecked (item) {
    return checkedItems.includes(item.state.uid)
  }
  function handleChange (e, item) {
    if (isChecked(item)) {
      item.isDeleted = !e.target.checked
    } else {
      item.selected = e.target.checked
    }
  }
  return (
    <FormGroup className={classes.listItemCate} >
      {
        allItems.map((item) => (
          <FormControlLabel
            className={classes.listItemLabel}
            key={item.uid}
            control={
              <Checkbox
                color="primary"
                defaultChecked={isChecked(item)}
                onChange={e => { handleChange(e, item) }} />
            }
            label={item?.state?.nodeData?.ott_name.state}
          />
        ))
      }
    </FormGroup>

  )
}
