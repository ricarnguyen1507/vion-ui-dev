import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableLayout from './table'
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
const LayoutSectionOpt = [
  {
    section_value: "livestream",
    section_name: "Livestream",
    section_type: "dgraph.type",
    section_ref: "VideoStream",
    section_limit: 20,
    display_order: 1
  },
  {
    section_value: "end_stream",
    section_name: "Đã Live",
    section_type: "dgraph.type",
    section_ref: "VideoStream",
    section_limit: 20,
    display_order: 1
  },
  {
    section_value: "collection_temp",
    section_name: "Bộ sưu tập",
    section_type: "item",
    section_ref: null,
    section_limit: 20,
    display_order: 2
  },
  {
    section_value: "collection",
    section_name: "Ngành hàng",
    section_type: "dgraph.type",
    section_ref: 'collection',
    section_limit: 20,
    display_order: 3
  },
  {
    section_value: "favourite",
    section_name: "Sản phẩm yêu thích",
    section_type: "list_item",
    section_ref: 'favourite',
    section_limit: 20,
    display_order: 4
  },
  // {
  //     section_value: "campaign",
  //     section_name: "Campaign",
  //     section_type: "",
  //     section_ref: null,
  //     display_order: 4
  // },
  // {
  //     section_value: "cart_template",
  //     section_name: "Đi chợ giùm bạn",
  //     section_type: "dgraph.type",
  //     section_ref: "CartTemplate",
  //     display_order: 5
  // },
  // {
  //     section_value: "keywords",
  //     section_name: "Keywords",
  //     section_type: "",
  //     section_ref: null,
  //     display_order: 6
  // },
  {
    section_value: "viewed_prod",
    section_name: "Đã xem",
    section_type: null,
    section_ref: 'viewed_prod',
    section_limit: 20,
    display_order: 7
  },
  {
    section_value: "viewed_history",
    section_name: "Sản phẩm bạn đã xem",
    section_type: "",
    section_ref: null,
    display_order: 8
  },
  {
    section_value: "brand_shop",
    section_name: "Brand Shop",
    section_type: "dgraph.type",
    section_ref: "BrandShop",
    section_limit: 20,
    display_order: 9
  },
  {
    section_value: "promotions",
    section_name: "Khuyến mãi",
    section_type: "dgraph.type",
    section_ref: "promotion",
    display_order: 10
  },

]

const target_type = ["Đối tượng nhóm", "Đối tượng khách hàng", "Tất cả customer"]
const display_status = ['PENDING', 'REJECTED', 'APPROVED']

/**
 * Export
 */
export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [collections, setCollections] = useState(null)
  const [collectionTemp, setCollectionTemp] = useState(null)
  const [otts, setOtts] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(true)

  const [customers, setCustomers] = useState([])
  const [groupCustomers, setGroupCustomers] = useState([])
  const [hybridLayouts, setHybridLayouts] = useState([])
  const [brandshopGroups, setBrandshopGroups] = useState([])
  const [livestreamGroups, setLivestreamGroups] = useState([])
  const [promotions, setPromotions] = useState([])

  useEffect(() => {
    const fetchs = [
      '/list/collection?t=0',
      '/list/collection?t=0&is_temp=true',
      '/group_customer/get-option',
      'list/hybrid_layout_options',
      '/list/brandshop_group/options',
      '/list/livestream_group/options',
      '/list/promotions',
      '/list/ott'
    ].map(url => api.get(url).then(res => res.data.result))
    Promise.all(fetchs).then(([collection, collection_temp, group_customers, hybrid_layouts, brandshop_groups, live_stream_groups, promotions, otts]) => {
      setCollections(collection)
      setCollectionTemp(collection_temp)
      setGroupCustomers(group_customers)
      setHybridLayouts(hybrid_layouts)
      setBrandshopGroups(brandshop_groups)
      setLivestreamGroups(live_stream_groups)
      setPromotions(promotions)
      setOtts(otts)
    })
  }, []);

  // Function handle
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang

  const handleDelete = (row) => api.delete(`/layout/${row.uid}`)
    .then(res => {
      if (res.data.statusCode === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      } else {
        alert(res.data.message)
      }
    })
    .catch(error => console.log(error))

  // Display view
  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          collections={collections}
          collectionTemp={collectionTemp}
          functionBack={functionBack}
          LayoutSectionOpt={LayoutSectionOpt}
          targetType={target_type}
          groupCustomerOpt={groupCustomers}
          customers={customers}
          setCustomers={setCustomers}
          displayStatus={display_status}
          hybridLayouts={hybridLayouts}
          brandshopGroups={brandshopGroups}
          livestreamGroups={livestreamGroups}
          promotions={promotions}
          otts={otts}
        />
      </Route>
      <Route exact path={path}>
        <TableLayout
          data={dataTable}
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setDataTable={setDataTable}
          setControlEditTable={setControlEditTable}
          collections={collections}
          handleDelete={handleDelete}
          displayStatus={display_status}
          setCustomers={setCustomers}
        />
      </Route>
    </Shared>
  )
}
