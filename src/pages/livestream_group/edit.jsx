import React, { useState, useContext, useEffect } from 'react'
import {
  useHistory
} from "react-router-dom"
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import RoleButton from 'components/Role/Button';
import Button from "@material-ui/core/Button"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import TextField from "@material-ui/core/TextField"
import api from 'services/api_cms'
import SelectMultiLivestream from "modules/SelectMulti/live_streams/list_manage"
import Autocomplete from '@material-ui/lab/Autocomplete'
import { context } from 'context/Shared'
import { InputEdge, Node, OptionEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"

// import { LoadingContext } from "context/LoadingContext"

function fetchData (url) {
  if (url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }
}

export default function ({ displayStatus, liveStreams, setLiveStream, brandShop, selectedBrandShop, layoutType }) {
  const classge = useGeneral()
  const history = useHistory()
  const ctx = useContext(context)

  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    uid: '_:livestream_group',
    "dgraph.type": "LivestreamGroup",
    display_status: 0,
    'brand_shop_name': selectedBrandShop ? selectedBrandShop.brand_shop_name : "Trang chủ",
    "layout_type": 1
  })
  delete dataEdit['livestream_group.videostreams|display_order']
  if (dataEdit?.['livestream_group.videostreams']?.length > 0) {
    let number = 0
    for (let obj of dataEdit?.['livestream_group.videostreams']) {
      obj['livestream_group.videostreams|display_order'] = `<livestream_group.videostreams> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  if (selectedBrandShop && selectedBrandShop.uid) {
    node.setState('livestream_group.brand_shop', { uid: selectedBrandShop.uid }, true)
    console.log(node)
  }
  // const [layoutTypeList] = useState(() => (dataEdit && dataEdit?.layout_type && [dataEdit?.layout_type]) || layoutType)

  useEffect(() => {
    // Load data sub options
    let video_stream = { url: '/video-stream-options', method: "get" }
    if (selectedBrandShop?.uid && selectedBrandShop?.uid !== null) {
      video_stream = { url: `/video-stream-options?brand_shop_uid=${selectedBrandShop.uid}`, method: "get" }
    }

    const fetchs = [
      { ...video_stream },
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([live_streams]) => {
      setLiveStream(live_streams)
    })
  }, dataEdit)

  const [cLivestreams] = useState({
    set: {},
    del: {}
  })
  const updateListLivestream = ({ set, del }) => {
    cLivestreams.set = set
    cLivestreams.del = del
    let listHighlight_ColOld = dataEdit?.['livestream_group.videostreams'] ?? []
    const removeItem = listHighlight_ColOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if (removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('livestream_group.videostreams', mergeArr, false)
      let highlight_col = node.getState('livestream_group.videostreams') ?? []
      highlight_col.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('livestream_group.videostreams', set, true)
    }
  }

  /* const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setDataEdit({ ...dataEdit, [name]: value })
  } */

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    node.setState('display_status', displayStatus.indexOf(item), true)
  }

  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj())
    api.post("/livestream_group", formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/live-stream-group')
  }

  const handleChangeLayoutType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'layout_type': item.code })
    node.setState('layout_type', item.code)
  }

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  /* const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  function invalidCallback (result) {
    setMultiSelectInvalid(result)
  } */

  // let validateForm = {}
  // validateForm = useMemo(() => ({})
  // if (!dataEdit.brand_shop) {
  //   return {
  //     ...validateForm,
  //     brandShop: {
  //       error: true,
  //       helperText: "This field is required"
  //     }
  //   }
  // } else {
  //   return {}
  // }
  // , [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // View
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="LiveStream Group" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Thông tin </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={6} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"livestream_group_name"}
                    fullWidth
                    label="Livestream Group Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={6} sm={6} lg={6}>
                  <Autocomplete
                    options={layoutType}
                    getOptionLabel={option => option.label}
                    value={layoutType.find(l => l.code === dataEdit?.layout_type) || null}
                    onChange={handleChangeLayoutType}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Kiểu hiển thị trên UI client"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  {node.getState("livestream_group.brand_shop") ?
                    <OptionEdge
                      node={node}
                      pred={"livestream_group.brand_shop"}
                      options={brandShop || []}
                      getOptionLabel={option => option?.brand_shop_name || ""}
                      style={{ width: "100%" }}
                      renderInput={params => (
                        <TextField {...params}
                          label="BrandShop"
                          fullWidth
                          variant="outlined"
                          margin="dense"
                        />
                      )}
                    /> : ""
                  }
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Điều chỉnh danh sách </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12} >
                  <SelectMultiLivestream
                    listOption={liveStreams}
                    setListOption={setLiveStream}
                    currentList={dataEdit?.['livestream_group.videostreams'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListLivestream}
                    maxItem="-1"
                    minItem="0"
                    listTitle="Thêm Livestream"
                    facetPrefix="livestream_group.videostreams"
                    // invalidCallback={invalidCallback}
                    delAll={true}
                    addAll={true}
                    selectedBrandShop={selectedBrandShop}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>

          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Xét duyệt hiển thị </Typography>
              </div>
              <Divider />
              <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                <Autocomplete
                  options={displayStatus}
                  value={displayStatus[dataEdit.display_status] || "PENDING"}
                  onChange={handleChangeDisplayStatus}
                  style={{ width: "100%" }}
                  renderInput={params => (
                    <TextField {...params}
                      label="Trạng thái hiển thị"
                      variant="outlined"
                      fullWidth
                      margin="dense"
                    />
                  )}
                />
              </Grid>
            </div>
          </Grid>
        </Grid>


        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={goBack}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
