import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableLiveStreamGroup from './table'
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory,
  useLocation
} from "react-router-dom"

import BrandShopSelections from 'components/BrandShopSelections'

const genders = ['Male', 'Female', 'Other']
function fetchData (url) {
  if (url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }
}

const display_status = ['PENDING', 'REJECTED', 'APPROVED']

const layout_type = [
  {
    code: 0,
    label: 'Single'
  },
  {
    code: 1,
    label: 'Multi'
  }
]

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [actionType, setActionType] = useState('')
  const [controlEditTable, setControlEditTable] = useState(false)
  const [dataEdit, setDataEdit] = useState(null)
  const [liveStreams, setLiveStream] = useState([])
  const [brandShop, setBrandShop] = useState([])
  const { pathname } = useLocation()
  const [ctx] = useState(() => ({
    goBack () {
      setLiveStream([])
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))
  useEffect(() => {
    const fetchs = [
      { url: '/video-stream-options', method: "get" },
      { url: '/brand-shop', method: "get" },
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([live_streams, brand_shops]) => {
      setLiveStream(live_streams)
      brand_shops.unshift({
        uid: null,
        brand_shop_name: "Trang chủ"
      })
      setBrandShop(brand_shops)
    })

  }, []);

  /**
   * START Dialog BrandShopSelections
   */
  const [open, setOpen] = useState(false);
  const [selectedBrandShop, setSelectedBrandShop] = useState(() => brandShop[0], [brandShop])

  const handleClickOpen = () => {
    setOpen(true)
  };

  const handleClose = () => {
    setOpen(false)
  };
  const handleNext = () => {
    setOpen(false)

    /**
     * check suboptions
     */
    if (liveStreams?.length > 0) {
      history.push(`${pathname}/Add`)
    } else {
      alert("Brandshop bạn chọn, không có Video stream nào đang APPROVED")
    }
  };

  useEffect(() => {
    // Load data sub options
    let video_stream = { url: '/video-stream-options', method: "get" }
    if (selectedBrandShop?.uid && selectedBrandShop?.uid !== null) {
      video_stream = { url: `/video-stream-options?brand_shop_uid=${selectedBrandShop.uid}`, method: "get" }
    }

    const fetchs = [
      { ...video_stream },
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([live_streams]) => {
      setLiveStream(live_streams)
    })
  }, [selectedBrandShop]);
  /**
   * END Dialog BrandShopSelections
   */

  return <Shared value={sharedData}>
    <Route path={`${path}/:actionType`}>
      <Edit
        ctx={ctx}
        actionType={actionType}
        genders={genders}
        originData={dataEdit}
        handleClose={handleClose}
        displayStatus={display_status}
        liveStreams={liveStreams}
        setLiveStream={setLiveStream}
        brandShop={brandShop}
        selectedBrandShop={selectedBrandShop}
        layoutType={layout_type}
      />
    </Route>
    <Route exact path={path}>
      <TableLiveStreamGroup
        ctx={ctx}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        genders={genders}
        displayStatus={display_status}
        handleClickOpen={handleClickOpen}
        brandShop={brandShop}
        setSelectedBrandShop={setSelectedBrandShop}
      />
      <BrandShopSelections
        selectedBrandShop={selectedBrandShop}
        setSelectedBrandShop={setSelectedBrandShop}
        open={open}
        onClose={handleClose}
        onNext={handleNext}
        brandShop={brandShop}
        defaultHomeUid="home"
      />
    </Route>
  </Shared>
}
