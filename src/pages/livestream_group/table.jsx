import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

import getFilterStr from 'services/tableFilterString'

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

export default function TableLiveStreamGroup ({ controlEditTable, setControlEditTable, displayStatus, handleClickOpen, brandShop, setSelectedBrandShop }) {
  const classes = useStyles()
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0
  }))
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const { strFilter } = getFilterStr(query)
    return api.get("/list/livestream_group", {
      params: {
        number: query.pageSize,
        page: query.page,
        ...(strFilter && { filter: ' AND ' + strFilter }),
      }
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        return {
          data: result,
          page: query.page,
          pageSize: query.pageSize,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [selectedRow, setSelectedRow] = useState(null)
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq',
        sorting: false
      },
      {
        title: "BrandShop Name",
        field: "livestream_group.brand_shop",
        editable: "never",
        filtering: false,
        render: (rowData) => <div>{rowData?.['livestream_group.brand_shop']?.brand_shop_name || "Trang chủ"}</div>
      },
      {
        title: "Livestream Group Name",
        field: "livestream_group_name",
        editable: "onUpdate",
        o: 'regexp',
        cellStyle: {
          width: 20,
          maxWidth: 20
        },
        headerStyle: {
          width: 20,
          maxWidth: 20
        }
      },
      {
        title: "Số lượng LiveStream",
        field: "livestream_group.videostreams",
        editable: "onUpdate",
        filtering: false,
        render: (rowData) => <div>{rowData?.['livestream_group.videostreams']?.length || 0}</div>
      },
    ]
    for (let { value, column: { field } } of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>LiveStream Group</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),

        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Livestream Group',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              handleClickOpen()

            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Livestream Group",
            onClick: (event, { tableData, ...rowData }) => {
              if (rowData?.['livestream_group.brand_shop']) {
                setSelectedBrandShop(rowData['livestream_group.brand_shop'])
              } else {
                setSelectedBrandShop(brandShop[0])
              }
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}