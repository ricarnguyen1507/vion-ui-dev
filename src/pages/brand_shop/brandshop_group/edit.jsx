import React, { useState } from 'react'
import {
  useHistory
} from "react-router-dom"
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import RoleButton from 'components/Role/Button';
import Button from "@material-ui/core/Button"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import TextField from "@material-ui/core/TextField"
import api from 'services/api_cms'
import { clone, diff } from 'services/diff'
import SelectMultiBrandshop from "modules/SelectMulti/brandshops/list_manage"
import Autocomplete from '@material-ui/lab/Autocomplete'

import { LoadingContext } from "context/LoadingContext"

export default function ({originData, actionType, ctx, displayStatus, brandShops, setBrandShop, handleSubmitData}) {
  const classge = useGeneral()
  const history = useHistory()
  const [timer, setTimer] = useState(0)
  const { setIsLoading } = React.useContext(LoadingContext);

  const [dataEdit, setDataEdit] = useState(() => {
    if (actionType === 'Add') {
      return {
        uid: '_:brandshop_group',
        "dgraph.type": "BrandshopGroup",
        display_status: 0
      }
    }
    if (actionType === 'Edit') {
      return clone(originData)
    }
  })


  const [cBrandshops] = useState({
    set: {},
    del: {}
  })
  const updateListBrandshop = ({set, del}) => {
    cBrandshops.set = set
    cBrandshops.del = del
  }

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setDataEdit({...dataEdit, [name]: value})
  }

  const handleChangeBrandShop = async (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShops ?? []
          res.data.result.forEach(r => {
            if (!newOption?.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setBrandShop([...newOption])
        })
    }
  }

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
  }

  const handleSubmit = function () {
    setIsLoading(true);
    const { set, del } = diff(originData || [], dataEdit, dataEdit.uid)
    let formSet = set || {}
    let formDel = del || {}

    formSet = {
      uid: dataEdit.uid,
      ...formSet,
      'dgraph.type': "BrandshopGroup",
      'brandshop_group.brandshops': cBrandshops.set
    }
    formDel = actionType === "Edit" ? {
      uid: dataEdit.uid,
      ...formDel,
      'brandshop_group.brandshops': cBrandshops.del
    } : {...formDel}

    handleSubmitData(actionType, {
      set: formSet,
      del: formDel,
    })
  }

  function goBack () {
    ctx.goBack()
    history.replace('/app/brandshop_group')
  }


  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  // const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  // function invalidCallback (result) {
  //   setMultiSelectInvalid(result)
  // }

  // let validateForm = {}
  // validateForm = useMemo(() =>
  // ({})
  // if (!dataEdit.brand_shop) {
  //   return {
  //     ...validateForm,
  //     brandShop: {
  //       error: true,
  //       helperText: "This field is required"
  //     }
  //   }
  // } else {
  //   return {}
  // }
  // , [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // View
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Brandshop Group" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Brandshop Group Name"
                    variant="outlined"
                    margin="dense"
                    name="brandshop_group_name"
                    onChange={handleChange}
                    value={dataEdit?.brandshop_group_name || ""}
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <SelectMultiBrandshop
                    listOption={brandShops}
                    setListOption={setBrandShop}
                    currentList={dataEdit['brandshop_group.brandshops']}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListBrandshop}
                    maxItem="-1"
                    minItem="0"
                    inputChange={handleChangeBrandShop}
                    listTitle="Thêm Brandshop"
                    facetPrefix="brandshop_group.brandshops"
                    // invalidCallback={invalidCallback}
                    delAll={true}
                    addAll={true}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus[dataEdit.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>

          </Grid>

        </Grid>

        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={goBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
