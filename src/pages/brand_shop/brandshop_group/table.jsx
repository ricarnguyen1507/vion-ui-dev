import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import api from 'services/api_cms'

import getFilterStr from 'services/tableFilterString'

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

export default function TableBrandShopGroup ({ ctx, controlEditTable, setControlEditTable, displayStatus }) {
  const classes = useStyles()
  // const [dataTable, setDataTable] = useState(null)
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0
  }))
  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const { strFilter } = getFilterStr(query)
    return api.get("/list/brandshop_group", {params: {
      number: query.pageSize,
      page: query.page,
      ...(strFilter && {filter: ' AND ' + strFilter}),
    }})
      .then(response => {
        const { summary: [{totalCount}], result } = response.data
        // setDataTable(result);
        return {
          data: result,
          page: query.page,
          pageSize: query.pageSize,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [selectedRow, setSelectedRow] = useState(null)
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq',
        sorting: false
      },
      {
        title: "Brandshop Group Name",
        field: "brandshop_group_name",
        editable: "onUpdate",
        o: 'regexp',
        cellStyle: {
          width: 20,
          maxWidth: 20
        },
        headerStyle: {
          width: 20,
          maxWidth: 20
        }
      },
      {
        title: "Số lượng BrandShop",
        field: "brandshop_group.brandshops",
        editable: "onUpdate",
        filtering: false,
        render: (rowData) => <div>{rowData?.['brandshop_group.brandshops']?.length || 0}</div>
      },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Customer</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>BrandShop Group</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.editData() }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),

        }}
        actions={[
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Customer",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.editData(rowData)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}