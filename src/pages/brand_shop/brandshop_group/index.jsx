import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableBrandShopGroup from './table'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const genders = ['Male', 'Female', 'Other']
function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

const display_status = ['PENDING', 'REJECTED', 'APPROVED']

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()
  const [actionType, setActionType] = useState('')
  const [controlEditTable, setControlEditTable] = useState(false)
  // const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [errorPassword, setErrorPassword] = useState(false);
  const [mapTypes, setMapTypes] = useState(null)
  const [addresstypes, setAddressTypes] = useState(null)
  const [provinces, setProvinces] = useState(null)
  const [brandShops, setBrandShop] = useState([])
  // const [areas , setAreas] = useState(null)
  const [ctx] = useState(() => ({
    goBack () {
      setBrandShop([])
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
      if (row && row['brandshop_group.brandshops']) {
        setBrandShop(row['brandshop_group.brandshops'])
      }
    }
  }))
  useEffect(() => {
    const fetchs = [
      '/list/addresstype',
      '/list/areas',
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([addresstypes, areas]) => {
      setAddressTypes(addresstypes)
      setMapTypes(addresstypes ? addresstypes.reduce((map, type) => {
        map[type.type_value] = type
        return map
      }, {}) : null)
      setProvinces(areas)
    })

  }, []);

  /* const handleDelete = (dataDel) => api.delete(`/customer/${dataDel.uid}`)
    .then(res => {
      ctx.goBack()
    })
    .catch(error => console.log(error)) */

  const handleClose = () => {
    setErrorPassword(false)
  }
  const handleSubmitData = async (actionType, data) => {
    const formData = new FormData()
    formData.set("data", JSON.stringify(data))

    await api({
      method: 'post',
      url: '/brandshop_group',
      data: formData
    })
    ctx.goBack()
  }

  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        ctx={ctx}
        errorPassword={errorPassword}
        actionType={actionType}
        genders={genders}
        mapTypes={mapTypes}
        addresstypes={addresstypes}
        originData={dataEdit}
        handleSubmitData={handleSubmitData}
        listProvinces={provinces}
        handleClose={handleClose}
        displayStatus={display_status}
        brandShops={brandShops}
        setBrandShop={setBrandShop}
      />
    </Route>
    <Route exact path={path}>
      <TableBrandShopGroup
        ctx={ctx}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        genders={genders}
        mapTypes={mapTypes}
        listProvinces={provinces}
        displayStatus={display_status}
      />

    </Route>
  </>
}
