import React, { useState, useMemo, useEffect, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import useStyles from './edit_style'
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
// import { clone } from 'services/diff'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import { genCdnUrl } from 'components/CdnImage'
import SelectMultiProduct from "modules/SelectMulti/products/list_manage"
import SelectMultiCollection from "modules/SelectMulti/collections/list_manage"
// import ExportExcel from "./export_excel";
import ColorPicker from './color_text'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import { InputEdge, Node, CheckBoxEdge } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"

export default function ({collections, functionBack, displayStatus, referenceType, layoutType, products, setProducts }) {
  const history = useHistory()
  const classge = useGeneral()
  const classes = useStyles()


  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    "dgraph.type": "Collection",
    "is_temporary": true,
    "reference_type": 0,
    "layout_type": 1,
    "collection_type": 0
  })
  delete dataEdit['highlight.products|display_order']
  if(dataEdit?.['highlight.products']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['highlight.products']) {
      obj['highlight.products|display_order'] = `<highlight.products> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))

  const [layoutTypeList, setLayoutTypeList] = useState(layoutType[0])

  const [originData, setOriginData] = useState({});
  const [brandShopValue, setBrandShopValue] = useState({});
  const [brandShopList, setBrandShopList] = useState([]);
  const brandShopUid = useMemo(() => brandShopValue?.uid ?? null, [brandShopValue])
  const [updated] = useState({
    values: {
      'display_status': dataEdit.display_status,
      'reference_type': 0
    }
  })

  const tCollections = useMemo(() => {
    const filter = actionType === 'Add' ? (c) => !c['highlight.collections'] : (c) => !c['highlight.collections'] && originData.uid !== c.uid
    return collections.filter(filter)
  }, [collections])

  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection_images/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/brand-shop-collection-temporary')
  }
  const handleSubmit = function () {
    if(!dataEdit?.uid || !dataEdit?.uid.length || dataEdit.uid.includes('_:')) {
      node.setState('created_at', new Date().getTime(), true)
    }else {
      node.setState('updated_at', new Date().getTime(), true)
    }
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/brand-shop/collection/" + dataEdit.uid + "/" + brandShopUid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }


  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }

  const handleChangeReferenceType = (e, item) => {
    e.preventDefault()
    console.log(item);
    if (item === 'Sản phẩm') {
      setDataEdit({ ...dataEdit, 'reference_type': referenceType.indexOf(item) })
      updated.values['reference_type'] = referenceType.indexOf(item)
      setLayoutTypeList(layoutType[0])
      node.setState('reference_type', referenceType.indexOf(item))
    } else if (item === 'Cửa hàng') {
      setDataEdit({ ...dataEdit, 'reference_type': referenceType.indexOf(item) })
      updated.values['reference_type'] = referenceType.indexOf(item)
      setLayoutTypeList(layoutType[1])
      node.setState('reference_type', referenceType.indexOf(item))
    } else {
      setDataEdit({ ...dataEdit, 'reference_type': referenceType.indexOf(item) })
      updated.values['reference_type'] = referenceType.indexOf(item)
      setLayoutTypeList(layoutType[2])
      node.setState('reference_type', referenceType.indexOf(item))
    }

  }

  const handleChangeLayoutType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'layout_type': item.code })
  }
  const layoutTypeSelected = useMemo(() => layoutTypeList.find(l => l.code === dataEdit.layout_type), [dataEdit.layout_type, layoutTypeList])

  const updateListProduct = async ({set}) => {
    let listHighligtOld = dataEdit?.['highlight.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'highlight.products')
  }

  const updateListCollection = ({set}) => {
    let listHighlight_ColOld = dataEdit?.['highlight.collection'] ?? []
    const removeItem = listHighlight_ColOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('highlight.collection', mergeArr, false)
      let highlight_col = node.getState('highlight.collection') ?? []
      highlight_col.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('highlight.collection', set, true)
    }
  }

  const [color, setColor] = useState(() => ({
    'color_title': dataEdit.color_title || "#f48024",
    'color_price': dataEdit.color_price || "#f48024",
    'color_discount': dataEdit.color_discount || "#f48024",
  }))

  const handleColor = (color_picked, fieldName) => {
    setColor({...color, [fieldName]: color_picked.hex})
    setDataEdit({...dataEdit, [fieldName]: color_picked.hex})
    node.setState(fieldName, color_picked.hex)
  }

  const [timer, setTimer] = useState(null)
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&'), brand_shop_uid: brandShopUid}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/brand-shop/product-option`, queries)
        .then(res => {
          const newOption = products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }

  const handleChangeBrandShop = (e, item) => {
    setDataEdit({
      ...dataEdit,
      brand_shop: item
    })
    setBrandShopValue(item)
  }

  const onInputSelectBrandShopChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShopList
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setBrandShopList([...newOption])
        })
    }
  }

  //load data
  useEffect(() => {
    if(dataEdit.uid.startsWith('_:')) {
      return
    }
    api.get(`/brand-shop/collection-detail/${dataEdit.uid}`).then(res => {
      setOriginData(res?.data ?? {})
      setDataEdit(res?.data ?? {})
      setProducts(res?.data?.['highlight.products'] ?? [])
      setBrandShopValue(res?.data?.['brand_shop']?.[0] ?? null)
      setBrandShopList(res?.data?.['brand_shop'] ?? [])
      setLayoutTypeList((res?.data != undefined) ? res?.data?.reference_type == 0 ? layoutType[0] : res?.data?.reference_type == 1 ? layoutType[1] : res?.data?.tag_category == 2 ? layoutType[2] : layoutType[0] : layoutType[0])
    }).catch(err => {
      console.log(err)
    })
  }, []);


  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  function invalidCallback (result) {
    setMultiSelectInvalid(result)
  }

  let validateForm = {}
  validateForm = useMemo(() => {
    if (!dataEdit.brand_shop) {
      return {
        ...validateForm,
        brandShop: {
          error: true,
          helperText: "This field is required"
        }
      }
    } else {
      return {}
    }
  }, [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />

  // const [clonedData] = useState(() => [clone(dataEdit)]);
  return (
    <>
      <PageTitle title="Bộ sưu tập" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Information
                    </Typography>
                  </div>
                  <Divider />
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <CheckBoxEdge
                            Component={Checkbox}
                            node={node}
                            pred={"type_highlight"}
                            fullWidth
                            variant="outlined"
                            margin="dense"
                            color="primary"
                            style={{margin: "25px 10px"}}
                          />
                        }
                        label="Highlight (hiện ảnh 16:9)"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <div className={classes.colorPicker} style={{margin: "15px 0"}}>
                            <ColorPicker
                              color={color.color_title}
                              fieldName="color_title"
                              handleColor={handleColor}
                            />
                          </div>
                        }
                        label="&nbsp;&nbsp;Màu (Tiêu đề)"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <div className={classes.colorPicker} style={{margin: "15px 0"}}>
                            <ColorPicker
                              color={color.color_price}
                              fieldName="color_price"
                              handleColor={handleColor}
                            />
                          </div>
                        }
                        label="&nbsp;&nbsp;Màu (Giá bán)"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <div className={classes.colorPicker} style={{margin: "15px 0"}}>
                            <ColorPicker
                              color={color.color_discount}
                              fieldName="color_discount"
                              handleColor={handleColor}
                            />
                          </div>
                        }
                        label="&nbsp;&nbsp;Màu (Giá gốc)"
                      />
                    </Grid>
                    <Grid item xs={6} sm={6} lg={6}>
                      <Autocomplete
                        disabled={!dataEdit.uid.startsWith('_:') && brandShopValue}
                        onChange={handleChangeBrandShop}
                        options={brandShopList}
                        value={brandShopValue}
                        getOptionLabel={option => (option.brand_shop_name || '')}
                        onInputChange={onInputSelectBrandShopChange}
                        renderInput={params => (
                          <TextField {...params}
                            {...(validateForm['brandShop'] || {})}
                            label="Brand Shop"
                            fullWidth
                            variant="outlined"
                            margin="dense"
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={6} sm={6} lg={6}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"collection_name"}
                        fullWidth
                        label="Collection Name"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'required',
                          'minStringLength:3',
                          'maxStringLength:50',
                        ]}
                        errorMessages={[
                          'This field is required',
                          'Min length is 3',
                          'Max length is 50']}
                      />
                    </Grid>
                  </Grid>
                </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Loại Tham chiếu
                    </Typography>
                  </div>
                  <Divider />
                  <Grid container spacing={2}>
                    <Grid item xs={6} sm={6} lg={6}>
                      <Autocomplete
                        options={referenceType}
                        value={referenceType[dataEdit.reference_type] || "Sản phẩm"}
                        onChange={handleChangeReferenceType}
                        style={{ width: "100%" }}
                        renderInput={params => (
                          <TextField {...params}
                            label="Loại tham chiếu"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={6} sm={6} lg={6}>
                      <Autocomplete
                        options={layoutTypeList}
                        getOptionLabel={option => option.label}
                        value={layoutTypeSelected}
                        onChange={handleChangeLayoutType}
                        style={{ width: "100%" }}
                        renderInput={params => (
                          <TextField {...params}
                            label="Kiểu hiện thị"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                          />
                        )}
                      />
                    </Grid>
                  </Grid>

                  {brandShopValue?.uid &&
                  <SelectMultiCollection
                    isShow={dataEdit.reference_type === 1}
                    listOption={tCollections}
                    currentList={dataEdit['highlight.collections'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCollection}
                    maxItem="-1"
                    minItem="1"
                    listTitle="Thêm ngành hàng"
                    facetPrefix="highlight.collections"
                    invalidCallback={invalidCallback}
                  />}
                  {products && brandShopValue?.uid &&
                  <SelectMultiProduct
                    isShow={dataEdit.reference_type === 0}
                    listOption={products}
                    currentList={dataEdit['highlight.products'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListProduct}
                    maxItem="-1"
                    minItem="1"
                    inputChange={onInputSelectProductChange}
                    listTitle="Thêm sản phẩm"
                    facetPrefix="highlight.products"
                    invalidCallback={invalidCallback}
                  />}
                </div>
              </Grid>
            </Grid>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                                    Xét duyệt
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus[dataEdit.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <h5> Image (1600x900)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit.collection_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="collection_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
          <Grid item>
            <h5> Background</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit.background_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="background_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton page="collection_temporary" actionType={actionType} disabled={Object.keys(validateForm).length !== 0 || multiSelectInvalid}/>
          {/* { actionType === "Edit" &&
                       <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
