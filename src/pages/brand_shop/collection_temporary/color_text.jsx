import React, {useState} from 'react'
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'

export default function ({color, handleColor, fieldName}) {
  const [displayColorPicker, setDisplayColorPicker] = useState(false)
  const [hover, setHover] = useState(false)
  const styles = reactCSS({
    'default': {
      defaultColor: {
        textAlign: "center",
        display: "inline-block",
      },
      tooltiptext: {
        visibility: "visible",
        width: "120px",
        backgroundColor: "black",
        color: " #fff",
        textAlign: "center",
        borderRadius: "6px",
        padding: "5px 0",

        /* Position the tooltip */
        position: "absolute",
        zIndex: 1,
        top: "25px",
        left: "70px"
      },
      color: {
        width: '80px',
        height: '20px',
        borderRadius: '2px',
        position: "relative",
        background: color.startsWith('#') === true ? color : `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`,
      },
      swatch: {
        marginTop: "5px",
        float: "left",
        padding: '5px',
        background: '#fff',
        borderRadius: '1px',
        boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
        cursor: 'pointer',
      },
      popover: {
        position: 'absolute',
        zIndex: '2',
      },
      cover: {
        position: 'fixed',
        top: '0px',
        right: '0px',
        bottom: '0px',
        left: '0px',
      },
    },
  });

  const handleClick = () => {
    setDisplayColorPicker(!displayColorPicker)
  };

  const handleHover = () => {
    setHover(!hover)
  }
  const handleClose = () => {
    setDisplayColorPicker(false)
  };

  const callbackHandle = (str) => {
    handleColor(str, fieldName)
  }

  return (
    <div style={styles.defaultColor}>
      <div style={styles.swatch} onClick={handleClick} onMouseEnter={handleHover} onMouseLeave={handleHover}>
        <div style={styles.color}>
          { hover ? <span style={styles.tooltiptext} > Color Of Product </span> : null }
        </div>
      </div>
      {displayColorPicker ? <div style={styles.popover}>
        <div style={styles.cover} onClick={handleClose} />
        <SketchPicker color={color} onChange={callbackHandle} />
      </div> : null}
    </div>
  )
}
