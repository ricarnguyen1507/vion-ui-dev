import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { context } from 'context/Shared'
import { useHistory, useLocation } from "react-router-dom";
import api from 'services/api_cms'

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
function getFilterStr ({filters}) {
  let strFunc = filters.filter(value => typeof value === 'string').map(({value, column: {o}}) => {
    value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
    if(value) {
      if (!o) {
        return value
      }
    }
    return null;
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(field == "brand_shop_name") {
      return `alloftext(brand_shop_name,"${value}")`
    }
    else if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o ?? 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(brand_shop_name, "${strFunc.join(' ')}")` : strFunc = "";
  return {strFunc, strFilter: strFilter.join(' AND ')}
}
export default function TableCollection ({defaultQuery, stateQuery, setStateQuery, setDataTable, controlEditTable, setControlEditTable, displayStatus }) {
  const history = useHistory()
  const ctx = useContext(context)
  const { pathname } = useLocation()
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)

  function parseDate (unixDate) {
    if(!unixDate)
      return ''
    const d = new Date(unixDate)
    return `${String(d.getHours()).padStart(2, '0')}:${String(d.getMinutes()).padStart(2, '0')} ${String(d.getDate()).padStart(2, '0')}/${String(d.getMonth() + 1).padStart(2, '0')}/${d.getFullYear()}`
  }
  function getData (query = {}, collection_type = [0], useDefault = false) {
    const queryData = useDefault ? {...defaultQuery} : {
      ...stateQuery,
      ...query
    }
    setStateQuery(queryData)
    const {strFilter} = getFilterStr(queryData)

    collection_type = collection_type || [400]
    let strFilterFull = `(${collection_type.map(value => `eq(collection_type, ${value})`).join(' or ')})`
    if(strFilter.length) {
      strFilterFull += ' AND ' + strFilter
    }
    return api.get(`/brand-shop/list/collection?is_temp=true&number=${queryData.pageSize}&page=${queryData.page}&filter_str=${(queryData?.filters[0]?.column?.field == "brand_shop_name") ? strFilter : strFilterFull}&typeFilter=${ queryData?.filters[0]?.column?.field}`).then(res => {
      const { summary: {totalCount, page, offset}, result } = res.data
      setDataTable(result)
      setStateQuery({
        ...queryData,
        totalCount,
        page,
        offset
      })
      return {
        data: result,
        page: query.page,
        totalCount
      }
    })
      .catch(err => {
        console.log(err)
      })
  }

  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq'
      },
      {
        title: "Tên Ngành hàng",
        field: "collection_name",
        render: rowData => !rowData.parent ? rowData.collection_name : <p>&nbsp;~&nbsp;&nbsp;{rowData.collection_name}</p>,
        o: 'eq'
      },
      {
        title: "Tên Brand shop",
        field: "brand_shop_name",
        editable: 'never',
        o: 'alloftext',
        render: rowData => <span>{rowData?.brand_shop?.[0]?.brand_shop_name ?? ''}</span>
      },
      {
        title: "Ngày tạo", field: "created_at", editable: 'never', filtering: false,
        render: rowData => <span>{parseDate(rowData['created_at'])}</span>
      }
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })


  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        parentChildData={(row, rows) => rows.find(r => row['~highlight.collections'] && row['~highlight.collections'][0] && (r.uid === row['~highlight.collections'][0].uid))}
        // title={<h1 className={classes.titleTable}>Deal</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Ngành hàng tạm</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { data &&
                      <ExportExcel data={data} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          // {
          //     icon: AddBox,
          //     tooltip: 'Add Deal',
          //     isFreeAction: true,
          //     onClick: () => {
          //         functionEditData('Add')
          //     }
          // },
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Collection",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}
