import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import { Shared } from 'context/Shared'
import {
  useHistory,
  Route,
  useRouteMatch
} from "react-router-dom";

const display_status = ['PENDING', 'REJECTED', 'APPROVED']
const reference_type = ["Sản phẩm"]
const layout_type = [
  [
    {
      code: 0,
      label: 'SingleProduct'
    },
    {
      code: 1,
      label: 'MultiProduct'
    },
    {
      code: 2,
      label: 'Top10'
    }
  ]
]
/**
 * Export
 */

function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  // const { setIsLoading } = React.useContext(LoadingContext);
  const [dataTable, setDataTable] = useState(null)

  const [controlEditTable, setControlEditTable] = useState(false)
  // const [actionType, setActionType] = useState('')

  // const [indexEdit, setIndexEdit] = useState(null)
  // const [dataEdit, setDataEdit] = useState(null)
  const [collections, setCollections] = useState(null)
  const defaultQuery = {
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }
  const [stateQuery, setStateQuery] = useState(() => ({
    ...defaultQuery
  }))
  useEffect(() => {
    const fetchs = [
      "/brand-shop/list/collection?t=0",
    ].map(url => fetchData(url).then(res => res.data.result))
    Promise.all(fetchs).then(([
      collections
    ]) => {
      setCollections(collections)
    })
  }, []);

  const functionBack = () => {
    history.push('/app/brand-shop-collection-temporary')
    setControlEditTable(false)
  }
  const [products, setProducts] = useState([])
  const functionEditData = (actionType, row) => {
    history.push(`/app/brand-shop-collection-temporary/${row?.uid ?? 'new'}`)
  }

  const handleDelete = (row) => api.delete(`/collection/${row.uid}`)
    .then(res => {
      if (res.status === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  /* const validDataTable = (data) => {
    if (data === null) {
      // setIsLoading(true);
      return true;
    }
    // setIsLoading(false);
    return false;
  } */

  return (
    <Shared value={sharedData}>
      <Route
        path={path}
        exact={true}
      >
        <TableCollection
          data={dataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          displayStatus={display_status}
          referenceType={reference_type}
          layoutType={layout_type}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setDataTable={setDataTable}
          defaultQuery={defaultQuery}
        />
      </Route>
      <Route
        path={`${path}/:collection_id`}
        exact={true}
      >
        <Edit
          collections={collections}
          // actionType={actionType}
          functionBack={functionBack}
          displayStatus={display_status}
          referenceType={reference_type}
          layoutType={layout_type}
          fetchData={fetchData}
          products={products}
          setProducts={setProducts}
        />
      </Route>
    </Shared>
  )

}
