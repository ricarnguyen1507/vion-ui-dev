import React, { useState } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import { Shared } from 'context/Shared'
import {
  useHistory,
  Route,
  useRouteMatch
} from "react-router-dom";
const display_status = ['PENDING', 'REJECTED', 'APPROVED']
/**
 * Export
 */

// function fetchData (url) {
//   return api.get(url, {
//     responseType: "json"
//   })
// }

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()

  // const { setIsLoading } = React.useContext(LoadingContext);
  const [brandShopChecked, setBrandShopChecked] = useState(true)

  const [dataTable, setDataTable] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  // const [mode, setMode] = useState("table")
  // const [actionType, setActionType] = useState('')
  // const [mapTypes, setMapTypes] = useState(null)

  // const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  // const [dataOption, setDataOption] = useState(null)
  const defaultQuery = {
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }
  const [stateQuery, setStateQuery] = useState(() => ({
    ...defaultQuery
  }))


  const functionBack = () => {
    history.push(`${path}`)
  }

  const functionEditData = (actionType, row) => {
    // setActionType(actionType)
    setDataEdit(row)
    // setIndexEdit(index)
    // setMode("edit")
  }

  const functionOrderAscData = () => {
    // setMode("loading")
    const ascMutateData = dataTable.map((dt, i) => ({set: {uid: dt.uid, display_order: i + 1}}))
    api.post('/collection/reordering', ascMutateData)
      .catch(err => { console.log(err) })
    // .then(response => {
    //   setMode("table")
    // })
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang

  const handleDelete = (row) => api.delete(`/collection/${row.uid}`)
    .then(res => {
      if (res.data.statusCode === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      } else {
        alert(res.data.message)
      }
    })
    .catch(error => console.log(error))

  /* const validDataTable = (data) => {
    if (data === null) {
      // setIsLoading(true);
      return true;
    }
    // setIsLoading(false);
    return false;
  } */

  return (
    <Shared value={sharedData}>
      <Route path={path} exact={true}>
        <TableCollection
          data={dataTable}
          setDataTable={setDataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          functionOrderAscData={functionOrderAscData}
          displayStatus={display_status}
          // setMode={setMode}
          brandShopChecked={brandShopChecked}
          setBrandShopChecked={setBrandShopChecked}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          defaultQuery={defaultQuery}
        />
      </Route>
      <Route path={`${path}/:collection_id`} exact={true}>
        <Edit
          collections={dataTable}
          originData={dataEdit}
          // dataOption={dataOption}
          functionBack={functionBack}
          displayStatus={display_status}
        />
      </Route>
    </Shared>
  )
}
