import React, { useState, useMemo, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from '../../style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import { genCdnUrl } from 'components/CdnImage'
import SelectMultiProduct from "modules/SelectMulti/products/list_manage"
import FormControlLabel from "@material-ui/core/FormControlLabel";
import ColorPicker from "../../collection/color_text";
import { InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"


export default function ({ functionBack, displayStatus }) {
  const history = useHistory()
  const classge = useGeneral()
  const [timer, setTimer] = useState(0)
  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    'dgraph.type': "Collection",
    'collection_type': 400,
  })
  const [products, setProducts] = useState(dataEdit['highlight.products'] || [])
  delete dataEdit['highlight.products|display_order']
  if(dataEdit?.['highlight.products']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['highlight.products']) {
      obj['highlight.products|display_order'] = `<highlight.products> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }

  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))

  const [brandShopValue, setBrandShopValue] = useState(() => dataEdit?.brand_shop?.[0] ?? {});
  const [brandShopList, setBrandShopList] = useState([]);
  const brandShopUid = useMemo(() => brandShopValue?.uid ?? null, [brandShopValue])
  const [color, setColor] = useState(() => ({
    'color_title': dataEdit.color_title || "#000",
    'gradient_start': dataEdit.gradient_start || "#fff",
    'gradient_end': dataEdit.gradient_end || "#fff",
  }))
  const [updated] = useState({
    values: {
      'display_status': dataEdit.display_status
    }
  })

  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection_images/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/brand-shop-collection')
  }
  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/brand-shop/collection/" + dataEdit.uid + "/" + brandShopUid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }
  const [childProduct] = useState({
    set: {},
    del: {}
  })

  const updateListProduct = async ({set}) => {
    setListProductUids(set.map(s => s.uid).join(','))
    let listHighligtOld = dataEdit?.['highlight.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'highlight.products')
  }

  const handleChangeBrandShop = (e, item) => {
    setDataEdit({
      ...dataEdit,
      // [name]: value,
      'products': [],
      brand_shop: item
    })
    childProduct.set = []
    childProduct.del = dataEdit?.['highlight.products']?.map(r => ({uid: r.uid}))
    setBrandShopValue(item)
  }

  const onInputSelectBrandShopChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShopList
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setBrandShopList([...newOption])
        })
    }
  }
  const handleColor = (color_picked, fieldName) => {
    setColor({...color, [fieldName]: color_picked.hex})
    setDataEdit({...dataEdit, [fieldName]: color_picked.hex})
    node.setState(fieldName, color_picked.hex)
  }

  const [listProductUids, setListProductUids] = useState(() => dataEdit['highlight.products']?.map(p => p.uid).join(','))
  const isuid = /^0[xX][0-9A-F]+$/i
  const handleChangeUids = async (e) => {
    e.preventDefault()
    const { value = "" } = e.target

    setListProductUids(value)

    const lastChar = value.substr(value.length - 1);
    const lastWord = value.split(',');
    let checkUid = isuid.test(lastWord[lastWord.length - 1])

    clearTimeout(timer)
    setTimer(setTimeout(() => {
      if (lastChar === ',' && checkUid !== true) {
        alert("chuỗi uids sai định dạng vui lòng kiểm tra lại!")
      } else {
        fetchProductFromUids(value)
      }
    }, 1000))
  }

  const fetchProductFromUids = async (value) => {
    const formData = new FormData();
    formData.set("uid", value)
    const res = await api.post(`/brand-shop/list/product-by-uids/${brandShopUid}`, formData)
    const responseProd = []
    value.split(',').map((v, idx) => {
      let find = res?.data?.result?.find(r => r.uid == v) ?? {}
      if (Object.keys(find).length > 0) {
        responseProd.push({
          uid: find.uid,
          'highlight.products|display_order': `<highlight.products> <${find.uid}> (display_order=${idx}) .`
        })
      }
    })
    let listHighligtOld = dataEdit?.['highlight.products'] ?? []
    // let del = listHighligtOld?.map(r => ({uid: r.uid}))
    if(responseProd.length) {
      await updateMultiSelectProduct(responseProd, listHighligtOld, node, 'highlight.products')
    }
    // childProduct.set = responseProd
    // childProduct.del = originData?.['highlight.products']?.map(r => ({uid: r.uid}))
  }

  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&'), brand_shop_uid: brandShopUid}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }

  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/brand-shop/product-option`, queries)
        .then(res => {
          const newOption = products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  let validateForm = {}
  validateForm = useMemo(() => {
    if (!dataEdit.brand_shop) {
      return {
        ...validateForm,
        brandShop: {
          error: true,
          helperText: "This field is required"
        }
      }
    } else {
      return {}
    }
  }, [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Collection" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"collection_name"}
                    fullWidth
                    label="Collection Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <Autocomplete
                  disabled={actionType === 'Edit' && brandShopValue}
                  onChange={handleChangeBrandShop}
                  options={brandShopList}
                  value={brandShopValue}
                  getOptionLabel={option => (option.brand_shop_name || '')}
                  onInputChange={onInputSelectBrandShopChange}
                  renderInput={params => (
                    <TextField {...params}
                      {...(validateForm['brandShop'] || {})}
                      label="Brand Shop"
                      fullWidth
                      variant="outlined"
                      margin="dense"
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={4} lg={4}>
                    <FormControlLabel
                      control={
                        <div className={classge.colorPicker} style={{margin: "15px 0"}}>
                          <ColorPicker
                            style={{marginLeft: "15px !important"}}
                            color={color.color_title}
                            fieldName="color_title"
                            handleColor={handleColor}
                          />
                        </div>
                      }
                      label="&nbsp;&nbsp;Màu (Tiêu đề)"
                    />
                  </Grid>
                  <Grid item xs={12} sm={4} lg={4}>
                    <FormControlLabel
                      control={
                        <div className={classge.colorPicker} style={{margin: "15px 0"}}>
                          <ColorPicker
                            color={color.gradient_start}
                            fieldName="gradient_start"
                            handleColor={handleColor}
                          />
                        </div>
                      }
                      label="&nbsp;&nbsp;Màu Gradient (Đầu)"
                    />
                  </Grid>
                  <Grid item xs={12} sm={4} lg={4}>
                    <FormControlLabel
                      control={
                        <div className={classge.colorPicker} style={{margin: "15px 0"}}>
                          <ColorPicker
                            color={color.gradient_end}
                            fieldName="gradient_end"
                            handleColor={handleColor}
                          />
                        </div>
                      }
                      label="&nbsp;&nbsp;Màu Gradient (Cuối)"
                    />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                                            Danh sách Sản Phẩm
                    </Typography>
                  </div>
                  <Divider />
                  <p style={{paddingTop: '20px'}}>Danh sách UID sản phẩm</p>
                  <TextField
                    id="list_product_highlight"
                    name="list_product_highlight"
                    type="text"
                    size="small"
                    variant="outlined"
                    fullWidth
                    value={listProductUids}
                    onChange={handleChangeUids}
                  />
                  <SelectMultiProduct
                    listOption={products}
                    currentList={dataEdit['highlight.products']}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListProduct}
                    maxItem="-1"
                    minItem="0"
                    inputChange={onInputSelectProductChange}
                    listTitle="Sản phẩm"
                    facetPrefix="highlight.products"
                  />
                </div>
              </Grid>
            </div>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                                    Xét duyệt Ngành hàng
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus[dataEdit.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <h5> Collection Icon (2000x1000)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit.collection_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="collection_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} disabled={Object.keys(validateForm).length !== 0}/>
        </Grid>
      </ValidatorForm>
    </>
  )
}
