import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from '../../style_general'
import ImgCdn from 'components/CdnImage'
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { context } from 'context/Shared'
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { useHistory, useLocation } from "react-router-dom";
import api from 'services/api_cms'

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
function getFilterStr ({filters}) {
  let strFunc = filters.filter(value => typeof value === 'string').map(({value, column: {o}}) => {
    value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
    if(value) {
      if (!o) {
        return value
      }
    }
    return null;
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(field == "brand_shop_name") {
      return `alloftext(brand_shop_name,"${value}")`
    }
    else if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o ?? 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(brand_shop_name, "${strFunc.join(' ')}")` : strFunc = "";
  return {strFunc, strFilter: strFilter.join(' AND ')}
}
export default function TableCollection ({defaultQuery, setStateQuery, brandShopChecked, setBrandShopChecked, data, getData, stateQuery, controlEditTable, setControlEditTable, displayStatus, setDataTable }) {
  const history = useHistory()
  const ctx = useContext(context)
  const { pathname } = useLocation()
  // const tableRef = React.createRef();
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  function getData (query = {}, collection_type = [400], useDefault = false) {
    const queryData = useDefault ? {...defaultQuery} : {
      ...stateQuery,
      ...query
    }
    setStateQuery(queryData)
    const {strFilter} = getFilterStr(queryData)

    collection_type = collection_type || [400]
    let strFilterFull = `(${collection_type.map(value => `eq(collection_type, ${value})`).join(' or ')})`
    if(strFilter.length) {
      strFilterFull += ' AND ' + strFilter
    }
    return api.get(`/brand-shop/list/collection?is_temp=false&is_parent=false&number=${queryData.pageSize}&page=${queryData.page}&filter_str=${(queryData?.filters[0]?.column?.field == "brand_shop_name") ? strFilter : strFilterFull}&typeFilter=${ queryData?.filters[0]?.column?.field}`).then(res => {
      const { summary: {totalCount, page, offset}, result } = res.data
      setDataTable(result)
      setStateQuery({
        ...queryData,
        totalCount,
        page,
        offset
      })
      return {
        data: result,
        page: query.page,
        totalCount
      }
    })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {

    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq',
        sorting: false
      },
      {
        title: "UID",
        field: "uid",
        render: rowData => rowData?.uid,
        sorting: false,
        filtering: false
      },
      {
        title: "Tên Brand shop",
        field: "brand_shop_name",
        editable: 'never',
        o: 'alloftext',
        // render: rowData => rowData?.brand_shop_name ?? '',
      },
      {
        title: "Tên ngành hàng",
        field: "collection_name",
        render: rowData => rowData?.collection_name,
        o: 'eq',
        sorting: false
      },
      {
        title: "Ngành hàng cha",
        field: "collection_name",
        render: rowData => rowData?.parent?.collection_name,
        o: 'eq',
        sorting: false
      },
      {
        title: "Số sản phẩm active",
        field: "products",
        render: rowData => rowData?.['highlight.products']?.length,
        filtering: false,
        sorting: false
      },
      {
        title: "Collection Image",
        field: "collection_image",
        filtering: false,
        render: rowData => <ImgCdn src={rowData.collection_image} style={{ width: 128, height: 72 }} />,
        sorting: false
      },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  const handleChangeCheckBox = ({target: {checked}}) => {
    if(checked) {
      getData({}, [400], true)
    } else {
      getData({}, null, true)
    }
    setBrandShopChecked(checked)
  }
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // parentChildData={(row, rows) => rows.find(r => row.parent && (r.uid === row.parent.uid))}
        // title={<h1 className={classes.titleTable}>Collection</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        onFilterChange={e => getData({filters: e})}
        onPageChange={page => getData({page})}
        onRowsPerPageChange={pageSize => getData({pageSize})}
        onSearchChange={e => getData({search: e})}
        onQueryChange={e => console.log(e)}
        page={stateQuery?.page ?? 0}
        totalCount={stateQuery?.totalCount ?? data?.length ?? 0}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Ngành hàng Brand shop</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          className={classes.titleProduct}
                          id="instock"
                          name="instock"
                          color="primary"
                          checked={brandShopChecked}
                          onChange={handleChangeCheckBox}/>
                      }
                      label="Ngành hàng brand shop"
                    />
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )
        }}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          rowDataTable => {
            if(rowDataTable.parent) {
              return null
            }
            return ({
              icon: "edit",
              tooltip: "Edit Collection",
              onClick: (event, { tableData, ...rowData }) => {
                ctx.set('dataEdit', rowData)
                history.push(`${pathname}/Edit`)
              }
            })
          }
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}