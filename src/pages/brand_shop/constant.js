export const RELOAD_CODE = "RELOAD";
export const DISPLAY_STATUS = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

export const LayoutSectionOpt = [
  {
    section_value: "livestream",
    section_name: "Livestream",
    section_type: "dgraph.type",
    section_ref: "VideoStream",
    display_order: 1
  },
  {
    section_value: "end_stream",
    section_name: "Đã Live",
    section_type: "dgraph.type",
    section_ref: "VideoStream",
    display_order: 1
  },
  {
    section_value: "collection_temp",
    section_name: "Bộ sưu tập",
    section_type: "item",
    section_ref: null,
    display_order: 2
  },
  {
    section_value: "collection",
    section_name: "Ngành hàng",
    section_type: "dgraph.type",
    section_ref: null,
    display_order: 3
  },
  {
    section_value: "favourite",
    section_name: "Sản phẩm yêu thích",
    section_type: "list_item",
    section_ref: null,
    display_order: 4
  },
  // {
  //     section_value: "campaign",
  //     section_name: "Campaign",
  //     section_type: "",
  //     section_ref: null,
  //     display_order: 4
  // },
  // {
  //     section_value: "cart_template",
  //     section_name: "Đi chợ giùm bạn",
  //     section_type: "dgraph.type",
  //     section_ref: "CartTemplate",
  //     display_order: 5
  // },
  // {
  //     section_value: "keywords",
  //     section_name: "Keywords",
  //     section_type: "",
  //     section_ref: null,
  //     display_order: 6
  // },
  {
    section_value: "viewed_prod",
    section_name: "Đã xem",
    section_type: null,
    section_ref: null,
    display_order: 7
  },
  {
    section_value: "viewed_history",
    section_name: "Sản phẩm bạn đã xem",
    section_type: "list_item",
    section_ref: null,
    display_order: 8
  },

]
export const TARGET_TYPE = ["Đối tượng nhóm", "Đối tượng khách hàng", "Tất cả customer"]
export const reference_type = ["Sản phẩm", "Video"]
export const layout_type = [
  [
    {
      code: 0,
      label: 'SingleProduct'
    },
    {
      code: 1,
      label: 'MultiProduct'
    },
    {
      code: 2,
      label: 'Top10'
    }
  ],
  // [
  //     {
  //         code: 3,
  //         label: 'Cửa hàng'
  //     }
  // ],
  [
    {
      code: 4,
      label: 'Livestream'
    }
  ]
]