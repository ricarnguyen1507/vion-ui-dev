import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
import { Grid } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import api from 'services/api_cms'

function getFilterStr ({filters}) {
  let strFunc = filters.filter(value => typeof value === 'string').map(({value, column: {o}}) => {
    value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
    if(value) {
      if (!o) {
        return value
      }
    }
    return null;
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(field == "brand_shop_name") {
      return `alloftext(brand_shop_name,"${value}")`
    }
    else if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o ?? 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(brand_shop_name, "${strFunc.join(' ')}")` : strFunc = "";
  return {strFunc, strFilter: strFilter.join(' AND ')}
}
export default function TableCollection ({ stateQuery, setMapTypes, defaultQuery, setStateQuery, setDataTable, ReferenceType }) {
  const history = useHistory()
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  function parseDate (unixDate) {
    if(!unixDate)
      return ''
    const d = new Date(unixDate)
    return `${String(d.getHours()).padStart(2, '0')}:${String(d.getMinutes()).padStart(2, '0')} ${String(d.getDate()).padStart(2, '0')}/${String(d.getMonth() + 1).padStart(2, '0')}/${d.getFullYear()}`
  }
  function getData (query = {}, collection_type = [0], useDefault = false) {
    const queryData = useDefault ? {...defaultQuery} : {
      ...stateQuery,
      ...query
    }
    setStateQuery(queryData)
    const {strFilter} = getFilterStr(queryData)

    collection_type = collection_type || [0]
    let strFilterFull = `(${collection_type.map(value => `eq(collection_type, ${value})`).join(' or ')})`
    if(strFilter.length) {
      strFilterFull += ' AND ' + strFilter
    }
    return api.get(`/brand-shop/list/collection?is_temp=false&number=${queryData.pageSize}&page=${queryData.page}&filter_str=${(queryData?.filters[0]?.column?.field == "brand_shop_name") ? strFilter : strFilterFull}&typeFilter=${ queryData?.filters[0]?.column?.field}`).then(res => {
      const { summary: {totalCount, page, offset}, result } = res.data
      setDataTable(result)
      setStateQuery({
        ...queryData,
        totalCount,
        page,
        offset
      })
      setMapTypes(result.reduce((map, type) => {
        map[type.uid] = type
        return map
      }, {}))
      return {
        data: result,
        page: query.page,
        totalCount
      }
    })
      .catch(err => {
        console.log(err)
      })
  }

  const [column] = useState(() => {
    const column = [
      {
        title: "Loại Tham chiếu",
        field: "reference_type",
        render: (rowData) => <div>{ReferenceType[rowData.reference_type] || "Products"}</div>
      },
      {
        title: "Tên Ngành Hàng",
        field: "collection_name",
        filtering: false,
        render: rowData => !rowData.parent ? rowData.collection_name : <p>&nbsp;~&nbsp;&nbsp;{rowData.collection_name}</p>

      },
      {
        title: "Tên hiển thị",
        field: "highlight_name",
        o: 'eq'
      },
      {
        title: "Ảnh 16:9 của Highlight",
        field: "image_highlight",
        filtering: false,
        render: rowData => <ImgCdn src={rowData.image_highlight} style={{ width: 128, height: 72 }} />
      },
      {
        title: "Tên Brand shop",
        field: "brand_shop_name",
        editable: 'never',
        o: 'alloftext',
        render: rowData => <span>{rowData?.brand_shop?.[0]?.brand_shop_name ?? ''}</span>,
      },
      {
        title: "Ngày tạo", field: "created_at", editable: 'never', filtering: false,
        render: rowData => <span>{parseDate(rowData['created_at'])}</span>
      }
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        parentChildData={(row, rows) => rows.find(r => row.parent && (r.uid === row.parent.uid))}
        // title={<h1 className={classes.titleTable}>Ngành hàng</h1>}
        title={<div style={{padding: '0px 20px', textAlign: "right", }}>
          <Grid container className={classes.formpd}>
            <Grid item xs={12} sm={12} lg={12} style={{textAlign: "left"}}>
              <h1 className={classes.titleTable}>Ngành hàng nổi bật</h1>
            </Grid>
          </Grid>
        </div>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Thêm mới',
            isFreeAction: true,
            onClick: () => {
              history.push(`/app/brand-shop-highlight/new`)
            }
          },
          rowData => ({
            icon: 'edit',
            tooltip: "Chỉnh sửa",
            onClick: () => {
              history.push(`/app/brand-shop-highlight/${rowData?.uid ?? 'new'}`)
            },
          })
        ]}
      />
    </div>
  )
}
