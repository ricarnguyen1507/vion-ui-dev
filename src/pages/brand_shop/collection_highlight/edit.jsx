import React, { useState, useMemo, useEffect, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import { genCdnUrl } from 'components/CdnImage'
import SelectMultiProduct from "modules/SelectMulti/products/list_manage"
import SelectMultiCollection from "modules/SelectMulti/collections/list_manage"
import { InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"

export default function ({functionBack, products, setProducts, collections }) {
  const history = useHistory()
  const classge = useGeneral()
  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    'dgraph.type': "Collection",
    'collection_type': 0
  })

  delete dataEdit['highlight.products|display_order']
  if(dataEdit?.['highlight.products']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['highlight.products']) {
      obj['highlight.products|display_order'] = `<highlight.products> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))

  const [originData, setOriginData] = useState({});
  const [brandShopValue, setBrandShopValue] = useState({});
  const [brandShopList, setBrandShopList] = useState([]);
  const brandShopUid = useMemo(() => brandShopValue?.uid ?? null, [brandShopValue])
  const tCollections = useMemo(() => {
    const filter = actionType === 'Add' ? (c) => !c.parent : (c) => originData.uid !== c.uid && c.collection_type !== 200
    return collections.filter(filter)
  }, [originData, actionType, collections])


  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection_images/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/brand-shop-highlight')
  }
  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/brand-shop/collection/" + dataEdit.uid + "/" + brandShopUid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [highlightCollection] = useState({
    set: {},
    del: {}
  })

  const updateListProduct = async ({set}) => {
    let listHighligtOld = dataEdit?.['highlight.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'highlight.products')
  }

  const updateListCollection = ({set, del}) => {
    highlightCollection.set = set
    highlightCollection.del = del
  }

  const [timer, setTimer] = useState(null)
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&'), brand_shop_uid: brandShopUid}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/brand-shop/product-option`, queries)
        .then(res => {
          const newOption = products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }

  const handleChangeBrandShop = (e, item) => {
    setBrandShopValue(item)
  }

  const onInputSelectBrandShopChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShopList
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setBrandShopList([...newOption])
        })
    }
  }

  //load data
  useEffect(() => {
    if(dataEdit.uid.startsWith('_:')) {
      return
    }
    api.get(`/brand-shop/collection-detail/${dataEdit.uid}`).then(res => {
      setOriginData(res?.data ?? {})
      setDataEdit(res?.data ?? {})
      setProducts(res?.data?.['highlight.products'] ?? [])
      setBrandShopValue(res?.data?.['brand_shop']?.[0] ?? null)
      setBrandShopList(res?.data?.['brand_shop'] ?? [])
    }).catch(err => {
      console.log(err)
    })
  }, []);


  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  let validateForm = {}

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Nổi bật" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Tên hiển thị
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={6} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"highlight_name"}
                    fullWidth
                    label="Tên gọi gói nổi bật"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                </Grid>
                <Grid item xs={6} sm={6} lg={6}>
                  <Autocomplete
                    disabled={!dataEdit.uid.startsWith('_:') && brandShopValue}
                    onChange={handleChangeBrandShop}
                    options={brandShopList}
                    value={brandShopValue}
                    getOptionLabel={option => (option.brand_shop_name || '')}
                    onInputChange={onInputSelectBrandShopChange}
                    renderInput={params => (
                      <TextField {...params}
                        label="Brand Shop"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        inputProps={{
                          ...params.inputProps,
                          autoComplete: "brand_shop",
                          required: brandShopValue?.length === 0
                        }}
                        required={true}
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
          { dataEdit.reference_type === 1 ?
            <Grid item xs={12} sm={12} lg={12}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Danh sách Ngành hàng
                  </Typography>
                </div>
                <Divider />
                {products && brandShopValue?.uid ?
                  <SelectMultiCollection
                    listOption={tCollections}
                    currentList={dataEdit['highlight.collections'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCollection}
                    maxItem="3"
                    minItem="1"
                    listTitle="Thêm ngành hàng"
                    facetPrefix="highlight.collections"
                  /> : ""
                }
              </div>
            </Grid> :
            <Grid item xs={12} sm={12} lg={12}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Danh sách Sản Phẩm
                  </Typography>
                </div>
                <Divider />
                {products && brandShopValue?.uid ?
                  <SelectMultiProduct
                    listOption={products}
                    currentList={dataEdit['highlight.products'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListProduct}
                    maxItem="30"
                    minItem="1"
                    listTitle="Thêm sản phẩm"
                    facetPrefix="highlight.products"
                    inputChange={onInputSelectProductChange}
                  /> : ""
                }
              </div>
            </Grid>
          }
        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <h5> Ảnh 16:9 của Highlight (1600 x 900)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit.image_highlight, "image_highlight.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="image_highlight" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
        <Grid container spacing={2}>

        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton page="collection_highlight" actionType={actionType} disabled={Object.keys(validateForm).length !== 0}/>

        </Grid>
      </ValidatorForm>
    </>
  )
}
