import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import { Shared } from 'context/Shared'
import {
  useHistory,
  Route,
  useRouteMatch
} from "react-router-dom";

import Edit from './edit'
import TableCollection from './table'
import {LoadingContext} from "context/LoadingContext";

function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

/**
 * Export
 */

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const { setIsLoading } = React.useContext(LoadingContext);
  const [dataTable, setDataTable] = useState(null)
  const [insteadTempCollections, setInsteadTempCollections] = useState(null)

  const [controlEditTable, setControlEditTable] = useState(false)
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  const [mapTypes, setMapTypes] = useState(null)

  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [products, setProducts] = useState([])
  const reference_type = ['Products', 'Collection']
  const defaultQuery = {
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }
  const [stateQuery, setStateQuery] = useState(() => ({
    ...defaultQuery
  }))
  useEffect(() => {
    const fetchs = [
      "/brand-shop/list/collection?t=200",
      "/brand-shop/list/collection?t=0"
    ].map(url => fetchData(url).then(res => res.data.result))
    Promise.all(fetchs).then(([
      hotdeals,
      instead_temp_collecitons
    ]) => {
      hotdeals.forEach(h => {
        dataTable.unshift(h)
      })
      setInsteadTempCollections(instead_temp_collecitons)
    })
  }, []);

  const functionBack = () => {
    history.push('/app/brand-shop-highlight')
    setControlEditTable(false)
    setDataEdit({})
  }

  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setDataEdit(row)
    if (row && row['highlight.products']) {
      setProducts(row['highlight.products'])
    }
    setIndexEdit(index)
    history.push(`app/brand-shop-highlight/${row?.['brand-shop']?.uid ?? 'new'}`)
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang
  const handleSubmitData = async (actionType, { set, del, files }, brandShopId) => {
    // Add / Update
    if (Object.keys(set).length > 0 && brandShopId) {
      const formData = new FormData()
      formData.set('set', JSON.stringify(set))
      formData.set('del', JSON.stringify(del))
      for(let field in files) {
        formData.set(field, files[field])
      }
      api.post(`/brand-shop/collection/${brandShopId}`, formData, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data"
        }
      })
        .then(response => {
          if (actionType === 'Add') {
            set.uid = response?.data?.['new_collection']?.[0]?.uid
            setDataTable([set, ...dataTable])
          } else if (actionType === 'Edit') {
            const updateItem = response?.data?.['new_collection']?.['result']?.[0]
            dataTable[indexEdit] = { ...dataTable[indexEdit], ...updateItem }
            setDataTable([...dataTable])
          }
          setIsLoading(false)
          history.push('/app/brand-shop-highlight')
          setDataEdit(null)
        })
        .catch(error => console.log(error))
    } else {
      history.push('/app/brand-shop-highlight')
    }
  }

  const handleDelete = (row) => api.delete(`/collection/${row.uid}`)
    .then(res => {
      if (res.status === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  /* const validDataTable = (data) => {
    if (data === null) {
      // setIsLoading(true);
      return true;
    }
    // setIsLoading(false);
    return false;
  } */

  return (
    <Shared value={sharedData}>
      <Route path={path} exact={true}>
        <TableCollection
          data={dataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          ReferenceType={reference_type}
          setDataTable={setDataTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          defaultQuery={defaultQuery}
          setMapTypes={setMapTypes}
        />
      </Route>
      <Route path={`${path}/:collection_id`} exact={true}>
        <Edit
          actionType={actionType}
          originData={dataEdit}
          functionBack={functionBack}
          handleSubmitData={handleSubmitData}
          ReferenceType={reference_type}
          products={products}
          setProducts={setProducts}
          collections={insteadTempCollections}
          mapTypes={mapTypes}
        />
      </Route>
    </Shared>
  )
}
