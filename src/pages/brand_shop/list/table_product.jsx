import React, { useState } from 'react'
import MaterialTable from 'material-table'
import { Grid, Tooltip, IconButton } from '@material-ui/core'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
// import { LoginContext } from "context/LoginContext"
import api from 'services/api_cms'

const productStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED',
  '3': 'IMPORT',
  '4': 'CONVERTED'
}
// let linkExport = window.location.href.split("product")
// console.log(linkExport[0]);
function calFinalPrice ({ sell_price = 0, discount = 0 }) {
  return discount > 0 ? sell_price * (1 - discount / 100) : sell_price
}

// function prepareData (products) {
//   products.forEach(r => {
//     r.final_price = calFinalPrice(r)
//     if (r['product.pricing']) {
//       r.price_with_vat = r['product.pricing'].price_with_vat || 0
//     }
//   })
// }

function getFilterStr ({filters}) {
  let strFunc = filters.map(({value, column: {o}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (!o) {
          return value
        }
      }
    }
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') != "" ? strFunc = `AND alloftext(fulltext_search, "${strFunc.join(' ')}")` : strFunc = ""
  return {strFunc, strFilter: strFilter.join(' AND ')}
}

// function newFilterStr ({filters}) {
//   const str = `func: type(Product)) @filter(not eq(is_deleted, true)`
// }

const displayStatus = ['PENDING', 'REJECTED', 'APPROVED', 'IMPORT', 'CONVERTED']
export default function TableProductBrandShop ({ brand_shop_id, functionEditData, handleDelete }) {
  const classes = useStyles()
  // const [dataTable, setDataTable] = useState(null)
  // const [controlEditTable, setControlEditTable] = useState(false)
  const [selectedRow] = useState(null) // Selected row
  // const { isLogin, setIsLogin } = React.useContext(LoginContext)
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))
  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFunc, strFilter} = getFilterStr(query)
    ///${query.pageSize}/${query.page}/${query.search || "all"}`
    return api.get("/brand-shop/product", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      brand_shop_id: brand_shop_id,
      ...{filter: (strFilter ? ' AND ' + strFilter : '')},
      ...(strFunc && {func: strFunc})
    }
    }).then(res => {
      const { summary: [{ totalCount }], result } = res.data
      // prepareData(result)
      return {
        data: result,
        page: query.page,
        totalCount
      }
    }).catch(() => {
      // setLoadingCode(2)
      // setLoadingText(err.response?.data?.message ?? 'Không tìm thấy thông tin brand shop')
    })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: productStatus,
        o: 'eq'
      },
      { title: "UID ", field: "uid", editable: "never"},
      { title: "SKU ", field: "sku_id", editable: "never"},
      { title: "Tên sản phẩm", field: "product_name", editable: "never" },
      {
        title: "Giá bán (Có VAT)",
        field: "price",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      }
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  /* const options = useState(() => {
        return {
            search: false,
            // searchText: stateQuery.search,
            headerStyle: {
                fontWeight: 600,
                background: "#f3f5ff",
                color: "#6e6e6e",
            },
            debounceInterval: 500,
            filtering: true,
            sorting: true,
            exportButton: true,
            grouping: true,
            pageSize: 10,
            pageSizeOptions: [10, 25, 50],
            actionsColumnIndex: -1,
            rowStyle: rowData => ({
                backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
            })
        }
    }) */

  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        // onSearchChange={onSearchChange}
        data={getData}
        columns={column}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Product</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { functionEditData("Add") }} target="_blank" tooltip="Add Supplier">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          search: false,
          // searchText: stateQuery.search,
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          debounceInterval: 500,
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          })
        }}
        // title={<h1 className={classes.titleTable}>Product</h1>}
        editable={{
          onRowDelete: handleDelete
        }}
      />
    </div>
    // </>
  )
}