import React, { useState, useMemo, useContext } from 'react';
import api from 'services/api_cms';
import { useHistory } from "react-router-dom";
import AddIcon from '@material-ui/icons/LibraryAdd';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import SaveIcon from '@material-ui/icons/Save';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import SelectMultiCollection from 'modules/SelectMulti/collections/list_manage'

import { genCdnUrl } from 'components/CdnImage'
import useGeneral from 'pages/style_general'
import TextEditor from "components/Editor/text-editor"
import TableProductBrandShop from "./table_product"
import { context } from 'context/Shared'
import { InputEdge, Node, CheckBoxEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"
// import ListManage from 'modules/SelectMulti/select_multi/list_manage'

const STATUS_LIST = [
  {
    id: 0,
    name: 'PENDING',
  },
  {
    id: 1,
    name: 'REJECTED',
  },
  {
    id: 2,
    name: 'APPROVED',
  }
]

export default function () {
  const history = useHistory();
  //Loading
  // const [loadingText, setLoadingText] = useState('');
  // const [loadingCode, setLoadingCode] = useState();
  const [files] = useState({})
  const [imageLogo, setImageLogo] = useState({})
  const [imageCover, setImageCover] = useState({})
  const classge = useGeneral()

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_brand_shop",
    "dgraph.type": "BrandShop",
    "skin_display_at_brandshop": true,
    "skin_display_at_home": true
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  delete dataEdit['brand_shop.collection|display_order']
  if(dataEdit?.['brand_shop.collection']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['brand_shop.collection']) {
      obj['brand_shop.collection|display_order'] = `<brand_shop.collection> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const [node] = useState(() => new Node(dataEdit))
  // const [allImageDetails] = useState(() => node.getState('image_details') ?? [])
  // const [imageDetails, setImageDetails] = useState(() => [{
  //   uid: 'new',
  //   source: '',
  //   image_type: 'image',
  //   ratio: '16:9',
  //   title: 'Banner 1',
  // }])
  // const [imageDetailCount, setImageDetailCount] = useState(0)
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/brand-shop')
  }
  const handleSubmit = () => {
    if(updated.image_details.length) {
      node.setState("image_details", [...updated.image_details])
    }
    // setLoadingText('Đang lưu thông tin brand shop')
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post('/brand-shop' + dataEdit.uid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  };
  const [updated] = useState({
    image_details: []
  })
  const onError = (e) => {
    console.log("lỗi", e.name);
  }
  // const updateFileDetails = (field, file) => {
  //   const [pred, field_media, file_uid, index, title, ratio] = field.split('|')
  //   const imageObject = {
  //     [field_media]: `images/brand_shop/${file.md5}.${file.type.split('/')[1]}`,
  //     media_type: 'image',
  //     uid: file_uid === 'new' ? '_:new_media' + index : file_uid,
  //     "dgraph.type": "Media",
  //     title,
  //     ratio,
  //     display_order: index
  //   }
  //   const tempIndx = imageDetails.findIndex(item => item.uid === file_uid);
  //   setImageDetailCount(tempIndx >= 0 ? imageDetails.length : imageDetails.length + 1)
  //   if (tempIndx >= 0) {
  //     imageDetails[tempIndx] = imageObject;
  //   } else {
  //     imageDetails.push(imageObject)
  //   }
  //   updated.image_details.push(imageObject)

  // }

  function handleImageLogo (field, file) {
    setImageLogo({ ...imageLogo, [field]: `images/brand_shop/${file.md5}.${file.type.split('/')[1]}` })
    node.setState(field, `images/brand_shop/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  function handleImageCover (field, file) {
    setImageCover({ ...imageCover, [field]: `images/brand_shop/${file.md5}.${file.type.split('/')[1]}` })
    node.setState(field, `images/brand_shop/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  function handleFileChange (field, file) {
    node.setState(field, `images/brand_shop/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  const stream_status = useMemo(() => STATUS_LIST.find(item => item.id === (dataEdit['display_status'] ?? 0)) ?? null, [dataEdit])

  const handleChangeStatus = function (e, item) {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': item ? item.id : STATUS_LIST[0].id })
    node.setState('display_status', item ? item.id : STATUS_LIST[0].id)
  }

  // const onAddImageDetail = () => {
  //   if (imageDetails.length > 5) {
  //     return;
  //   }
  //   const temp = {
  //     uid: 'new',
  //     source: '',
  //     image_type: 'image',
  //     ratio: '16:9',
  //     title: 'Banner ' + (imageDetails.length + 1),
  //   }
  //   setImageDetails([...imageDetails, temp])
  // }

  // const onDeleteImageDetail = (idx) => {
  //   setImageDetails(imageDetails.filter((item, index) => index !== idx))
  //   let deleteItem = imageDetails.filter((item, index) => index == idx)
  //   if(allImageDetails.length) {
  //     allImageDetails.map((item) => (deleteItem[0].uid === item.state.uid ? item.isDeleted = true : item.isDeleted = false))
  //   }
  // }
  const updateListBrandShopCollection = ({set, del}) => {
    let brandShop_col = node.getState('brand_shop.collection') ?? []
    if(set.length >= brandShop_col.length) {
      node.setState('brand_shop.collection', set, true)
      brandShop_col = node.getState('brand_shop.collection') ?? []
    } else if(set.length < del.length) {
      if(brandShop_col.length) {
        const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
        brandShop_col.map(bc => removeItem.find(ri => bc.state.uid == ri.uid ? bc.isDeleted = true : bc.isDeleted = false))
      }
    }
  }
  //Load data
  // useEffect(() => {
  //   if (!dataEdit?.uid.startsWith('_:')) {
  //     setLoadingText('Đang lấy hình ảnh brand shop')
  //     api.get(`/brand-shop/${dataEdit?.uid}`).then(res => {
  //       if (res.data.result) {
  //         if (res.data.result?.['image_details']) {
  //           setImageDetails(res.data.result?.['image_details'])
  //         }
  //         setLoadingText('')
  //       } else {
  //         throw new Error("Không có data trả về")
  //       }
  //     }).catch(err => {
  //       setLoadingCode(2)
  //       setLoadingText(err.response?.data?.message ?? 'Không tìm thấy thông tin brand shop')
  //     })
  //   }
  // }, [dataEdit])

  let validateForm = {}
  validateForm = useMemo(() => {
    if (!imageLogo?.image_logo && (!dataEdit?.image_logo || dataEdit?.image_logo == "")) {
      return {
        ...validateForm,
        image_logo: {
          error: true,
          helperText: `Please upload Logo`
        }
      }
    } else if (!imageCover?.image_banner && (!dataEdit?.image_banner || dataEdit?.image_banner == "")) {
      return {
        ...validateForm,
        image_banner: {
          error: true,
          helperText: `Please upload Banner`
        }
      }
    } else if(dataEdit['skin_tag'] == true && !dataEdit['skin_image']) {
      return {
        ...validateForm,
        skin_image: {
          error: true,
          helperText: `Please select image`
        }
      }
    } else {
      return {}
    }
  }, [dataEdit, imageLogo, imageCover])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  const ActionIcon = <EditIcon className={classge.iconAction} />

  // if (loadingText) {
  //   return (
  //     <Loading statusCode={loadingCode} statusText={loadingText}>
  //       {loadingCode === 1 && <Button onClick={() => { setLoadingText('') }}>Trở lại</Button>}
  //       {loadingCode === 2 && <Button onClick={() => { history.back() }}>Trở lại</Button>}
  //     </Loading>
  //   )
  // }
  return (
    <>
      <PageTitle title="BrandShop" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"brand_shop_name"}
                    fullWidth
                    label="Tên brand shop"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'maxStringLength:40'
                    ]}
                    errorMessages={[
                      'Max length is 40 character'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"display_name"}
                    fullWidth
                    label="Tên hiển thị"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'maxStringLength:40'
                    ]}
                    errorMessages={[
                      'Max length is 40 character'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"display_name_detail"}
                    fullWidth
                    label="Tên hiển thị chi tiêt"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'maxStringLength:80'
                    ]}
                    errorMessages={[
                      'Max length is 80 character'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"display_name_in_product"}
                    fullWidth
                    label="Tên hiển thị trong sản phẩm"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"brand_shop_id"}
                    fullWidth
                    label="Mã brand shop"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required !'
                    ]}
                  />
                </Grid>
                <Grid item xs={6} sm={6} lg={4}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"hotline"}
                    fullWidth
                    label="Hotline"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={6} sm={6} lg={8}>
                  <FormControlLabel
                    control={
                      <CheckBoxEdge
                        Component={Checkbox}
                        node={node}
                        pred={"certification_tag"}
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        color="primary"
                      />
                    }
                    label="Tag chứng nhận"
                  />
                </Grid>

                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <div className={classge.titlePanel}>
                      <div className={classge.iconTitle}>
                        {ActionIcon}
                      </div>
                      <Typography className={classge.contentTitle} variant="h5" >
                                              Danh sách Ngành hàng
                      </Typography>
                    </div>
                    <Divider />
                    <SelectMultiCollection
                      listOption={dataEdit['brand_shop.collection'] || []}
                      currentList={dataEdit['brand_shop.collection'] || []}
                      actionType={actionType}
                      dataEdit={dataEdit}
                      updateListManage={updateListBrandShopCollection}
                      maxItem="0"
                      // minItem="2"
                      listTitle="Ngành hàng của BrandShop"
                      facetPrefix="brand_shop.collection"
                      isArray={false}
                    />
                  </div>
                </Grid>

              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Banner
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Card style={{ display: 'flex', flexDirection: 'row' }}>
                    <CardMedia children={<>
                      <CardContent style={{ padding: '10px 0 0 10px' }}>
                        <Typography gutterBottom variant="h5" component="h2">
                          Logo
                        </Typography>
                      </CardContent>
                      <Media validateForm={validateForm['image_logo'] || {}} src={genCdnUrl(dataEdit['image_logo'], "image_banner.png")} type="image" style={{ width: 220, height: 220, margin: '10px' }} fileHandle={handleImageLogo} field="image_logo" accept="image/*" />
                    </>} />
                    <CardMedia children={<>
                      <CardContent style={{ padding: '10px 0 0 10px' }}>
                        <Typography gutterBottom variant="h5" component="h2">
                          Banner
                        </Typography>
                      </CardContent>
                      <Media validateForm={validateForm['image_banner'] || {}} src={genCdnUrl(dataEdit['image_banner'], "image_banner.png")} type="image" style={{ width: 440, height: 220, margin: '10px' }} fileHandle={handleImageCover} field="image_banner" accept="image/*" />
                    </>} />
                  </Card>
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Brand Skin
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} lg={6}>
                  <FormControlLabel
                    control={
                      <CheckBoxEdge
                        Component={Checkbox}
                        node={node}
                        pred={"skin_display_at_home"}
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        color="primary"
                      />
                    }
                    label="Hiển thị ở Home"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <FormControlLabel
                    control={
                      <CheckBoxEdge
                        Component={Checkbox}
                        node={node}
                        pred={"skin_display_at_brandshop"}
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        color="primary"
                      />
                    }
                    label="Hiển thị ở Brandshop"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Grid container spacing={2}>
                    <Grid item xs={8} sm={8} lg={8}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"skin_name"}
                        fullWidth
                        label="Tên brand skin"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'required',
                          'maxStringLength:40'
                        ]}
                        errorMessages={[
                          'This field id required',
                          'Max length is 40 character'
                        ]}
                      />
                    </Grid>
                    <Grid item xs={4} sm={4} lg={4}>
                      <Card style={{maxWidth: 180}}>
                        <CardMedia children={<Media src={genCdnUrl(dataEdit.skin_image, "image_cover.png")} type="image" style={{ width: 180, height: 90 }} fileHandle={handleFileChange} field="skin_image" accept="image/jpeg, image/png" />} />
                      </Card>
                    </Grid>
                  </Grid>

                </Grid>
              </Grid>
            </div>
          </Grid>
          {/* <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Chi tiết
                </Typography>
                <IconButton color="primary" onClick={onAddImageDetail}>
                  <AddBox />
                </IconButton>
              </div>
              <Divider />
              <Grid container spacing={2}>
                {imageDetails.map((item, idx) =>
                  <Grid item xs={6} sm={6} lg={4} key={item.uid + idx}>
                    <Typography gutterBottom variant="h6" component="h2" style={{ paddingLeft: '10px' }}>
                      {item.title ?? 'Banner ' + (idx + 1)} {item.ratio ?? ''}
                      {idx >= 1 ?
                        <IconButton color="primary" onClick={() => onDeleteImageDetail(idx)}>
                          <DeleteForeverRoundedIcon />
                        </IconButton> : ''}
                    </Typography>
                    <Media
                      // validateForm={validateForm['banner_1'] || {}}
                      src={genCdnUrl(item.source, "image_banner.png")}
                      type="image" style={{ width: 440, height: 220, margin: '10px' }}
                      fileHandle={updateFileDetails} field={`image_details|source|${item.uid}|${idx}|${item.title ?? 'Banner'}|${item.ratio ?? '16:9'}`}
                      accept="image/*" />
                  </Grid>
                ) ?? <></>}
              </Grid>
            </div>
          </Grid> */}
          {/* <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >Giới thiệu</Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Card>
                    <Editor
                      data={dataEdit?.description ?? ''}
                      dataEdit={dataEdit}
                      setDataEdit={setDataEdit}
                      fieldName="description"
                      isUseTools
                    />
                  </Card>
                </Grid> */}

          {/* TEXT-EDITOR */}
          <Grid item xs={12} sm={12} lg={12}>
            <TextEditor
              files={files}
              node={node}
              pred={"description_html"}
            />
          </Grid>
          {/* TEXT-EDITOR */}
          {/* <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    // required
                    className={classge.inputData}
                    id="brand_shop_description"
                    name="brand_shop_description"
                    label="Giới Thiệu Cửa Hàng"
                    type="text"
                    variant="outlined"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit.brand_shop_description || ''}
                    validators={[
                      // 'required',
                      // 'minStringLength:3',
                      'maxStringLength: 20',
                      // 'matchRegexp: ^[A-Za-z0-9 ]$'
                    ]}
                    errorMessages={[
                      // 'This field is required',
                      // 'Min length is 3 character ',
                      'Max length is 20 character',
                      // 'Character is not accept ! '
                    ]}
                  />
                </Grid>
              </Grid>
            </div> */}
          {/* </Grid> */}
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >Sản phẩm</Typography>
              </div>
              <Divider />
              {/* <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <ListManage
                    isDeleteManage={true}
                    isDragDisabled={true}
                    listOption={products}
                    currentList={dataEdit['brand_shop.product'] ?? []}
                    dataEdit={dataEdit}
                    updateListManage={updateListProduct}
                    maxItem="-1"
                    inputChange={onInputSelectProductChange}
                    listTitle="Thêm sản phẩm"
                    isArray={true}
                    label='Sản phẩm số: '
                    delAll={true}
                  />
                </Grid>
              </Grid> */}
              <TableProductBrandShop brand_shop_id={dataEdit?.uid}/>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                  Trạng thái
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    onChange={handleChangeStatus}
                    options={STATUS_LIST}
                    value={stream_status}
                    getOptionLabel={option => (option.name || '')}
                    onInputChange={() => { }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnSubmit}
            onClick={history.goBack}
            style={{ marginRight: '10px' }}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>
          <Button
            color="primary"
            variant="contained"
            aria-label="add"
            className={classge.btnSubmit}
            disabled={Object.keys(validateForm).length !== 0}
            type="submit"
          >
            {actionType === "Edit" ?
              <><SaveIcon className={classge.iconback} />Save</>
              :
              <><AddIcon className={classge.iconback} />{'Add'}</>
            }
          </Button>
        </Grid>
      </ValidatorForm>
    </>
  )
}
