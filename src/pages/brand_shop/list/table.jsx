import api from 'services/api_cms';
import React, { useState, forwardRef, useEffect, useContext} from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general';
import { useHistory, useLocation } from "react-router-dom";
// import { Edit } from '@material-ui/icons';
import SettingsIcon from '@material-ui/icons/Settings';
import {DISPLAY_STATUS} from '../constant';
import { context } from 'context/Shared'

export default function TableBrandShop () {
  const history = useHistory()
  const ctx = useContext(context)
  //data
  const { pathname } = useLocation()
  const [dataTable, setDataTable] = useState([])
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái", field: "display_status", editable: 'onUpdate',
        lookup: DISPLAY_STATUS,
        render: rowData => <span>{DISPLAY_STATUS[rowData?.['display_status']] ?? 'Inactive'}</span>,
        o: 'eq'
      },
      { title: "UID", field: "uid", editable: 'never', filtering: false },
      { title: "Tên Brand shop", field: "brand_shop_name", editable: 'never' },
      { title: "Tên hiển thị", field: "display_name", editable: 'never' },
      { title: "Tên chi tiết hiển thị", field: "display_name_detail", editable: 'never' },
      { title: "Số sản phẩ Active", field: "brand_shop_name", editable: 'never' },
      { title: "Tổng sản phẩm", field: "brand_shop_name", editable: 'never' },
      { title: "Ngày tạo", field: "brand_shop.created_date", editable: 'never' },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  });
  const tableIcons = {
    Edit: forwardRef((props, ref) => <SettingsIcon {...props} ref={ref} />)
  }

  //handle data
  function getFilterStr ({filters}) {
    let strFunc = filters.map(({value, column: {o}}) => {
      if(typeof value === 'string') {
        value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
        if(value) {
          if (!o) {
            return value
          }
        }
      }
    })

    const strFilter = filters.map(({value, column: {o, field}}) => {
      if(typeof value === 'string') {
        value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
        if(value) {
          if (o) {
            return `${o}(${field},"${value}")`
          }
        }
      } else if(typeof value === 'number' || typeof value === 'boolean') {
        return `${o ?? 'eq'}(${field},${value})`
      } else if(Array.isArray(value)) {
        return value.map(v => `${o}(${field},${v})`).join(' OR ')
      }
      return ""
    }).filter(v => v.trim() !== "")

    strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(brand_shop_name, "${strFunc.join(' ')}")` : strFunc = "";
    return {strFunc, strFilter: strFilter.join(' AND ')}
  }

  function getData (query = {}) {
    const queryData = {
      ...stateQuery,
      ...query
    }
    setStateQuery(queryData)
    const {strFunc, strFilter} = getFilterStr(queryData)
    try{
      api.post(`/brand-shop-filter`, {
        number: queryData.pageSize,
        page: queryData.page,
        ...(strFilter && {filter: strFilter}),
        ...(strFunc && {func: strFunc})
      }).then(res => {
        const { summary: {totalCount, page, offset}, data } = res.data
        setDataTable(data)
        setStateQuery({
          ...stateQuery,
          totalCount,
          page,
          offset
        })
      }).catch(() => {
        setDataTable([])
        setStateQuery({
          ...stateQuery,
          totalCount: 0,
          page: 0,
          offset: 0
        })
      })
    } catch (err) {
      setDataTable([])
      setStateQuery({
        ...stateQuery,
        totalCount: 0,
        page: 0,
        offset: 0
      })
    }
  }

  //Load data
  useEffect(() => {
    getData({});
  }, [])

  return (
    <div className="fade-in-table">
      <MaterialTable
        icons={tableIcons}
        columns={column}
        data={dataTable ?? []}
        title={<h1 className={classes.titleTable}>Brand Shop List</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        onFilterChange={e => getData({filters: e})}
        onPageChange={page => getData({page})}
        onRowsPerPageChange={pageSize => getData({pageSize})}
        onSearchChange={e => getData({search: e})}
        onQueryChange={e => console.log(e)}
        page={stateQuery?.page ?? 0}
        totalCount={stateQuery?.totalCount ?? dataTable?.length ?? 0}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        localization={{
          body: {
            editRow: {
              deleteText: ''
            }
          }
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Brand Shop',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/Add`)
            }
          },
          {
            icon: 'edit',
            tooltip: "Chỉnh sửa brand shop",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          }
        ]}
      />
    </div>
  )
}