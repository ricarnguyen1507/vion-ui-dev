import React, { useState } from "react";
import {
  Route, useRouteMatch
} from "react-router-dom";
import { Shared } from 'context/Shared'
import Table from './table'
import Edit from './edit';


export default function () {
  const [sharedData] = useState(() => new Map())
  const {path} = useRouteMatch()
  // const history = useHistory()
  return <Shared value={sharedData}>
    <Route
      path={path}
      exact={true}
    >
      <Table />
    </Route>
    <Route
      path={`${path}/:actionType`}
      exact={true}
    >
      <Edit />
    </Route>
  </Shared>
}