import React, { useState, useEffect } from 'react'
import {
  useHistory,
  Route,
  useRouteMatch
} from "react-router-dom";
import api from 'services/api_cms'
import Edit from './edit'
import TableLayout from './table'
import { Shared } from 'context/Shared'
const LayoutSectionOpt = [
  {
    section_value: "livestream",
    section_name: "Livestream",
    section_type: "dgraph.type",
    section_ref: "VideoStream",
    display_order: 1
  },
  {
    section_value: "end_stream",
    section_name: "Đã Live",
    section_type: "dgraph.type",
    section_ref: "VideoStream",
    display_order: 2
  },
  {
    section_value: "collection_temp",
    section_name: "Bộ sưu tập",
    section_type: "item",
    section_ref: null,
    section_limit: 20,
    display_order: 3
  },
  {
    section_value: "collection",
    section_name: "Ngành hàng",
    section_type: "dgraph.type",
    section_ref: "collection",
    display_order: 4
  },
  {
    section_value: "favourite",
    section_name: "Sản phẩm yêu thích",
    section_type: "list_item",
    section_ref: "favourite",
    display_order: 5
  },
  // {
  //     section_value: "campaign",
  //     section_name: "Campaign",
  //     section_type: "",
  //     section_ref: null,
  //     display_order: 4
  // },
  // {
  //     section_value: "cart_template",
  //     section_name: "Đi chợ giùm bạn",
  //     section_type: "dgraph.type",
  //     section_ref: "CartTemplate",
  //     display_order: 5
  // },
  // {
  //     section_value: "keywords",
  //     section_name: "Keywords",
  //     section_type: "",
  //     section_ref: null,
  //     display_order: 6
  // },
  {
    section_value: "viewed_prod",
    section_name: "Đã xem",
    section_type: null,
    section_ref: "viewed_prod",
    display_order: 6
  },
  {
    section_value: "viewed_history",
    section_name: "Sản phẩm bạn đã xem",
    section_type: "",
    section_ref: null,
    display_order: 8
  },
  {
    section_value: "brandshop_collection",
    section_name: "Ngành hàng Brand Shop",
    section_type: "dgraph.type",
    section_ref: "brandshop_collection",
    display_order: 9
  },
  {
    section_value: "promotions",
    section_name: "Khuyến mãi Brand Shop",
    section_type: "dgraph.type",
    section_ref: "Promotion",
    display_order: 10
  }

]

const target_type = ["Đối tượng nhóm", "Đối tượng khách hàng", "Tất cả customer"]
const display_status = ['PENDING', 'REJECTED', 'APPROVED']

function fetchData (url) {
  if(url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }
}

/**
 * Export
 */
export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [collections, setCollections] = useState([])
  const [collectionTemp, setCollectionTemp] = useState(null)
  const [hybrid, setHybridLayout] = useState(null)
  // const [isLoading, setLoading] = useState(false)

  const [controlEditTable, setControlEditTable] = useState(true)
  const [actionType] = useState('')

  const [dataEdit] = useState({})
  const [customers, setCustomers] = useState([])
  const [groupCustomers, setGroupCustomers] = useState([])
  const [livestreamGroups, setLivestreamGroups] = useState([])
  const [selectedBrandShop, setSelectedBrandShop] = useState({})

  const [dataTable, setDataTable] = useState([])
  const defaultQuery = {
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }
  const [stateQuery, setStateQuery] = useState(() => ({
    ...defaultQuery
  }))

  useEffect(() => {
    // setLoading(true)
    let collE = {url: '/brand-shop/list/collection?t=0', method: "get"}
    let collectionTemp = {url: '/brand-shop/list/collection?t=0&is_temp=true', method: "get"}
    let groupCustomer = {url: '/group_customer/get-option', method: "get"}
    const fetchs = [
      {...collE},
      {...collectionTemp},
      {...groupCustomer},
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([collection, collection_temp, group_customers]) => {
      setCollections(collection)
      setCollectionTemp(collection_temp)
      setGroupCustomers(group_customers)
      // setLoading(false)
    }).catch(() => {
      // setLoading(false)
    })
  }, []);

  useEffect(() => {
    // setLoading(true)
    let livestreamGroup = {url: 'list/livestream_group/options', method: "get"}
    if (selectedBrandShop?.uid && selectedBrandShop?.uid !== null) {
      livestreamGroup = {url: `/list/livestream_group/options?brand_shop_uid=${selectedBrandShop.uid}`, method: "get"}
    }
    const fetchs = [
      {...livestreamGroup}
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([livestream_groups]) => {
      setLivestreamGroups(livestream_groups)
      // setLoading(false)
    }).catch(() => {
      // setLoading(false)
    })
  }, [selectedBrandShop]);


  const functionBack = () => {
    history.push(`${path}`)
  }

  const handleDelete = (row) => api.delete(`/layout/${row.uid}`)
    .then(res => {
      if (res.data.statusCode === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      } else {
        alert(res.data.message)
      }
    })
    .catch(error => console.log(error))

  return (
    <Shared value={sharedData}>
      <Route path={path} exact={true}>
        <TableLayout
          data={dataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          collections={collections}
          handleDelete={handleDelete}
          displayStatus={display_status}
          stateQuery={stateQuery}
          defaultQuery={defaultQuery}
          setDataTable={setDataTable}
          setStateQuery={setStateQuery}
          setSelectedBrandShop={setSelectedBrandShop}
          setCustomers={setCustomers}
        />
      </Route>
      <Route path={`${path}/:collection_id`} exact={true}>
        <Edit
          actionType={actionType}
          originData={dataEdit}
          collections={collections}
          collectionTemp={collectionTemp}
          setCollectionTemp={setCollectionTemp}
          hybrid = {hybrid}
          setHybridLayout = {setHybridLayout}
          functionBack={functionBack}
          LayoutSectionOpt={LayoutSectionOpt}
          targetType={target_type}
          groupCustomerOpt={groupCustomers}
          customers={customers}
          setCustomers={setCustomers}
          displayStatus={display_status}
          livestreamGroups={livestreamGroups}
          selectedBrandShop={selectedBrandShop}
        />
      </Route>
    </Shared>
  )
}
