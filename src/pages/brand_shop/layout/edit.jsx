import React, { useEffect, useState, useMemo, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import SelectMultiLayout from 'modules/SelectMulti/layouts/list_manage'
import SelectMultiGroupCustomer from 'modules/SelectMulti/group_customers/list_manage'
import SelectMultiCustomer from 'modules/SelectMulti/customers/list_manage'
import { updateMultiSelectLayout } from 'services/updateMultiSelect'
import { genUid } from "utils"
import Loading from 'components/Loading';
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { InputEdge, Node, OptionEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"
// Add style

export default function ({ collectionTemp, setCollectionTemp, hybrid, setHybridLayout, functionBack, LayoutSectionOpt, targetType, customers, setCustomers, groupCustomerOpt, displayStatus, livestreamGroups }) {
  const classge = useGeneral()
  const [isLoading, setLoading] = useState(false)
  const [timer, setTimer] = useState(null)
  const history = useHistory()
  const ctx = useContext(context)
  const [files] = useState({})
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_layout",
    "dgraph.type": "Layout",
    "target_type": 0,
    'display_status': 0,
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  delete dataEdit['layout.layout_section|display_order']
  delete dataEdit['layout.customers|display_order']
  if(dataEdit?.['layout.customers']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['layout.customers']) {
      obj['layout.customers|display_order'] = `<layout.customers> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  delete dataEdit['layout.group_customers|display_order']
  if(dataEdit?.['layout.group_customers']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['layout.group_customers']) {
      obj['layout.group_customers|display_order'] = `<layout.group_customers> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  const [node] = useState(() => new Node(dataEdit))
  const [options] = useState(() => {
    if(!LayoutSectionOpt)
      return null
    return LayoutSectionOpt.map(ls => ({
      uid: genUid("layout_section"),
      ...ls
    }))
  })
  const [brandShopValue, setBrandShopValue] = useState({});
  const [brandShopList, setBrandShopList] = useState([]);
  let [brand_shop] = useState(() => ({}))
  const [layoutSections, setLayoutSections] = useState(() => dataEdit?.['layout.layout_section'] || [])

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/brand-shop-layout')
  }
  async function handleSubmit () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post(`/brand-shop/layout/${dataEdit?.uid}/${brand_shop?.["brand_shop_uid"] ?? ''}`, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const updateListCollection = async ({set}) => {
    let listLaySecOld = dataEdit?.['layout.layout_section'] ?? []
    await updateMultiSelectLayout(set, listLaySecOld, node, 'layout.layout_section')
    setLayoutSections(set)
  }

  const handleChangeTargetType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'target_type': targetType.indexOf(item) })
    node.setState('target_type', targetType.indexOf(item))
    if(targetType.indexOf(item) == 2) {
      let grCustomer = node.getState('layout.group_customers') ?? []
      if(grCustomer.length) {
        grCustomer.map(gr => gr.isDeleted = true)
        node.setState('layout.group_customers', grCustomer)
      }
      let customer = node.getState('layout.customers') ?? []
      if(customer.length) {
        customer.map(cs => cs.isDeleted = true)
        node.setState('layout.customers', customer)
      }
    }
  }


  const updateListCustomer = ({ set, del }) => {
    let grCustomer = node.getState('layout.group_customers') ?? []
    if(grCustomer.length) {
      grCustomer.map(gr => gr.isDeleted = true)
      node.setState('layout.group_customers', grCustomer)
    }
    let customer = node.getState('layout.customers') ?? []
    if(set.length >= customer.length) {
      for(let obj of set) {
        obj['layoutCustomer|display_order'] = `<layout.customers> <${obj.uid}> (display_order=${obj.display_order}) .`
        delete obj.display_order
      }
      node.setState('layout.customers', set, true)
      customer = node.getState('layout.customers') ?? []
    } else if(set.length < del.length) {
      if(customer.length) {
        const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
        customer.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
      }
    }
  }
  const updateListGroupCustomer = ({ set, del }) => {
    let customer = node.getState('layout.customers') ?? []
    if(customer.length) {
      customer.map(cs => cs.isDeleted = true)
      node.setState('layout.customers', customer)
    }
    let grCustomer = node.getState('layout.group_customers') ?? []
    if(set.length >= grCustomer.length) {
      for(let obj of set) {
        obj['layoutGroupCustomer|display_order'] = `<layout.group_customers> <${obj.uid}> (display_order=${obj.display_order}) .`
        delete obj.display_order
      }
      node.setState('layout.group_customers', set, true)
      grCustomer = node.getState('layout.group_customers') ?? []
    } else if(set.length < del.length) {
      if(grCustomer.length) {
        const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
        grCustomer.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
      }
    }
  }

  const phone_number_valid = /^\d+$/

  const onInputSelectCustomerChange = (e, val) => {
    let queries = ''
    if (phone_number_valid.test(val)) {
      queries = {phone_number: val.replace(/["\\]/g, '\\$&').trim()}
    } else if (val !== '') {
      queries = {customer_name: val.replace(/["\\]/g, '\\$&').trim()}
    } else {
      queries = ''
    }
    if(timer)
      clearTimeout(timer)
    setTimer(setTimeout(() => {
      if (queries !== "") {
        api.post(`/list/customer-options`, queries)
          .then(res => {
            const newOption = customers
            res.data.result.forEach(r => {
              if (!newOption.find(no => no.uid === r.uid)) {
                newOption.push(r)
              }
            })
            setCustomers([...newOption])
          })
      }
    }, 550))
  }

  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    node.setState('display_status', displayStatus.indexOf(item))
  }
  const handleChangeBrandShop = (e, item) => {
    setBrandShopValue(item)
  }
  const onInputSelectBrandShopChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShopList
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
            brand_shop["brand_shop_uid"] = r.uid
          })
          setBrandShopList([...newOption])
        })
    }
  }

  const collectionTempState = useMemo(() => collectionTemp ?? [], [collectionTemp])
  const hybridState = useMemo(() => hybrid ?? [], [hybrid])
  //load data
  useEffect(() => {
    if(dataEdit?.uid.startsWith('_:')) {
      return
    }
    setLoading(true)
    const fetchs = [
      `/brand-shop/layout-detail/${dataEdit?.uid}`,
    ].map(url => api.get(url).then(res => res))
    Promise.all(fetchs).then(([res]) => {
      setDataEdit(res?.data ?? {})
      setBrandShopValue(res?.data?.['brand_shop']?.[0] ?? null)
      setBrandShopList(res?.data?.['brand_shop'] ?? [])
      setLayoutSections(res?.data?.['layout.layout_section'] || [])
    }).catch(() => {
      setLoading(false)
    })
  }, []);

  useEffect(() => {
    if (brandShopValue?.uid) {
      setLoading(true)
      const fetchs = [
        `/brand-shop/list/collection?t=0&is_temp=true&brand_shop_uid=${brandShopValue.uid}`,
        `/list/hybrid_layout?brand_shop_uid=${brandShopValue.uid}`
      ].map(url => api.get(url).then(res => res.data.result))
      Promise.all(fetchs).then(([collection_temp, hybrids]) => {
        setCollectionTemp(collection_temp)
        setHybridLayout(hybrids)
        setLoading(false)
      }).catch(() => {
        setLoading(false)
      })
    }
  }, [brandShopValue]);

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  function invalidCallback (result) {
    setMultiSelectInvalid(result)
  }

  let validateForm = {}
  validateForm = useMemo(() => ({}), [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  if(isLoading)
    return <Loading />
  return (
    <>
      <PageTitle title="Layout" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"layout_name"}
                    fullWidth
                    label="Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50',
                      'Character is not accept ']}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    disabled={!dataEdit?.uid.startsWith('_:') && brandShopValue}
                    onChange={handleChangeBrandShop}
                    options={brandShopList}
                    value={brandShopValue}
                    getOptionLabel={option => (option.brand_shop_name || '')}
                    onInputChange={onInputSelectBrandShopChange}
                    renderInput={params => (
                      <TextField {...params}
                        label="Brand Shop"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <OptionEdge
                    node={node}
                    pred={"layout.hybrid_layout"}
                    options={hybridState || []}
                    getOptionLabel={option => option?.layout_name || ""}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Hybrid Layout"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <Divider />
                    <SelectMultiLayout
                      listOption={options}
                      collectionTemp={collectionTempState}
                      currentList={layoutSections}
                      actionType={actionType}
                      dataEdit={dataEdit}
                      livestreamGroups={livestreamGroups}
                      updateListManage={updateListCollection}
                      maxItem="-1"
                      minItem="2"
                      listTitle="Thêm Section"
                      facetPrefix="layout.layout_section"
                      validateForm={validateForm['layout_section'] || {}}
                      invalidCallback={invalidCallback}
                    />
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <div className={classge.titlePanel}>
                      <div className={classge.iconTitle}>
                        {ActionIcon}
                      </div>
                      <Typography className={classge.contentTitle} variant="h5" >
                                                Đối tượng Customer
                      </Typography>
                    </div>
                    <Divider />
                    <Autocomplete
                      options={targetType}
                      value={targetType[dataEdit.target_type || 0]}
                      onChange={handleChangeTargetType}
                      style={{ width: "100%" }}
                      renderInput={params => (
                        <TextField {...params}
                          label="Đối tượng áp dụng"
                          variant="outlined"
                          fullWidth
                          margin="dense"
                        />
                      )}
                    />
                    <SelectMultiGroupCustomer
                      isShow={dataEdit.target_type === 0}
                      listOption={groupCustomerOpt}
                      currentList={dataEdit['layout.group_customers'] || []}
                      actionType={actionType}
                      dataEdit={dataEdit}
                      updateListManage={updateListGroupCustomer}
                      maxItem="-1"
                      minItem="1"
                      listTitle="Thêm nhóm khách hàng"
                      facetPrefix="layoutGroupCustomer"
                      invalidCallback={invalidCallback}
                    />
                    <SelectMultiCustomer
                      isShow={dataEdit.target_type === 1}
                      listOption={customers}
                      currentList={dataEdit['layout.customers'] || []}
                      actionType={actionType}
                      dataEdit={dataEdit}
                      updateListManage={updateListCustomer}
                      maxItem="-1"
                      minItem="1"
                      listTitle="Thêm khách hàng"
                      facetPrefix="layoutCustomer"
                      inputChange={onInputSelectCustomerChange}
                      invalidCallback={invalidCallback}
                    />
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootBasic}>
                    <div className={classge.titleProduct}>
                      <Typography className={classge.title} variant="h3">
                                    Xét duyệt Ngành hàng
                      </Typography>
                    </div>
                    <Grid container className={classge.rootListCate} spacing={2}>
                      <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                        <Autocomplete
                          options={displayStatus}
                          value={displayStatus[dataEdit.display_status] || "PENDING"}
                          onChange={handleChangeDisplayStatus}
                          style={{ width: "100%" }}
                          renderInput={params => (
                            <TextField {...params}
                              label="Trạng thái hiển thị"
                              variant="outlined"
                              fullWidth
                              margin="dense"
                            />
                          )}
                        />
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton page="layout-page" actionType={actionType} disabled={Object.keys(validateForm).length !== 0 || multiSelectInvalid}/>
        </Grid>
      </ValidatorForm>
    </>
  )
}
