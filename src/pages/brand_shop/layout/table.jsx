import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import { useHistory, useLocation } from "react-router-dom";
import api from 'services/api_cms'
import { context } from 'context/Shared'

function getFilterStr ({filters}) {
  let strFunc = filters.filter(value => typeof value === 'string').map(({value, column: {o}}) => {
    value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
    if(value) {
      if (!o) {
        return value
      }
    }
    return null;
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o ?? 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(brand_shop_name, "${strFunc.join(' ')}")` : strFunc = "";
  return {strFunc, strFilter: strFilter.join(' AND ')}
}
export default function TableLayout ({ setCustomers, defaultQuery, setDataTable, stateQuery, setStateQuery, data, controlEditTable, setControlEditTable, displayStatus, setSelectedBrandShop }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  const history = useHistory()
  const ctx = useContext(context)
  const { pathname } = useLocation()
  function getData (query = {}, useDefault = false) {
    const queryData = {
      ...(useDefault ? defaultQuery : stateQuery),
      ...query
    }
    setStateQuery(queryData)
    const {strFunc, strFilter} = getFilterStr(queryData)
    return api.post(`/brand-shop/layout-list`, {
      number: queryData.pageSize,
      page: queryData.page,
      ...(strFilter && {filter: strFilter}),
      ...(strFunc && {func: strFunc})
    }).then(res => {
      const { summary: {totalCount, page, offset}, result } = res.data
      setDataTable(result)
      setStateQuery({
        ...queryData,
        totalCount,
        page,
        offset
      })
      return {
        data: result,
        page: query.page,
        totalCount
      }
    })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: displayStatus,
        o: 'eq',
        sorting: false
      },
      { title: "Name", field: "layout_name" },
      { title: "Default", field: "is_default" },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  /**
     * View
     */
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>Layout</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        onFilterChange={e => getData({filters: e}, true)}
        onPageChange={page => getData({page})}
        onRowsPerPageChange={pageSize => getData({pageSize})}
        onSearchChange={e => getData({search: e})}
        page={stateQuery?.page ?? 0}
        totalCount={stateQuery?.totalCount ?? data?.length ?? 0}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Layout',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/Add`)
            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Layout",
            onClick: (event, { tableData, ...rowData }) => {
              if (rowData?.['brand_shop']?.[0]) {
                setSelectedBrandShop(rowData['brand_shop'][0])
              }
              if (rowData['layout.customers']) {
                setCustomers([...rowData['layout.customers']])
              }
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}