import React, { useState } from 'react'
import TableProduct from './table'
import {
  Route,
  useRouteMatch
} from "react-router-dom"

// function fetchData (url) {
//   return api.get(url, {
//     responseType: "json"
//   })
// }
const display_status = ['PENDING', 'REJECTED', 'APPROVED', 'IMPORT']

export default function () {
  // const history = useHistory()
  const {path} = useRouteMatch()
  // const userData = useUserState();
  // const [loadingText, setLoadingText] = useState()
  // const [loadingCode, setLoadingCode] = useState()
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))

  // if(loadingText) {
  //   return (
  //     <Loading statusCode={loadingCode} statusText={loadingText}>
  //       { loadingCode === 1 && <button onClick={() => { setLoadingText("") }}>Trở lại</button> }
  //     </Loading>
  //   )
  // }
  return <>

    <Route exact path={path}>
      <TableProduct
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
        displayStatus={display_status}
      />
    </Route>
  </>
}