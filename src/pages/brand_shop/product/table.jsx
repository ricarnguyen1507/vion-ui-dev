import React, { useState } from 'react'
import MaterialTable from 'material-table'
import { Grid } from '@material-ui/core'
import useStyles from 'pages/style_general'
// import { LoginContext } from "context/LoginContext"
import ImgCdn from 'components/CdnImage'
import api from 'services/api_cms'

const productStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED',
  '3': 'IMPORT',
  '4': 'CONVERTED'
}
// let linkExport = window.location.href.split("product")
// console.log(linkExport[0]);
function calFinalPrice ({ sell_price = 0, discount = 0 }) {
  return discount > 0 ? sell_price * (1 - discount / 100) : sell_price
}

function prepareData (products) {
  products.forEach(r => {
    r.final_price = calFinalPrice(r)
    if (r['product.pricing']) {
      r.cost_price_without_vat = r['product.pricing'].cost_price_without_vat || 0
      r.cost_price_with_vat = r['product.pricing'].cost_price_with_vat || 0
      r.listed_price_without_vat = r['product.pricing'].listed_price_without_vat || 0
      r.listed_price_with_vat = r['product.pricing'].listed_price_with_vat || 0
      r.price_without_vat = r['product.pricing'].price_without_vat || 0
      r.price_with_vat = r['product.pricing'].price_with_vat || 0
      r.from_date = r['product.pricing'].from_date || ""
      r.to_date = r['product.pricing'].to_date | ""
    }

  })
}

function getFilterStr ({filters}) {
  let typeFilter = ''
  let strFunc = filters.map(({value, column: {o, field}}) => {
    typeFilter = field
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (!o) {
          return value
        }
      }
    }
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') != "" ? typeFilter == "brand_shop_name" ? strFunc = `alloftext(brand_shop_name, "${strFunc.join(' ')}")` : strFunc = `func: alloftext(fulltext_search, "${strFunc.join(' ')}")` : strFunc = ""
  return {strFunc, strFilter: strFilter.join(' AND '), typeFilter}
}

// function newFilterStr ({filters}) {
//   const str = `func: type(Product)) @filter(not eq(is_deleted, true)`
// }

export default function TableProduct ({ stateQuery, setStateQuery, displayStatus }) {
  const classes = useStyles()
  // const [dataTable, setDataTable] = useState(null)
  // const [controlEditTable, setControlEditTable] = useState(false)
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  // const { isLogin, setIsLogin } = React.useContext(LoginContext)
  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFunc, strFilter, typeFilter} = getFilterStr(query)
    ///${query.pageSize}/${query.page}/${query.search || "all"}`
    return api.post("/list/product", {
      number: query.pageSize,
      page: query.page,
      typeFilter: typeFilter,
      ...{filter: ' and has(~brand_shop.product)' + (strFilter ? ' and ' + strFilter : '')},
      ...(strFunc && {func: strFunc})
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        prepareData(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: productStatus,
        o: 'eq'
      },
      {
        title: "Tên Brand shop",
        field: "brand_shop_name",
        editable: 'never',
        render: rowData => <span>{rowData?.brand_shop?.[0]?.brand_shop_name ?? ''}</span>,
      },
      { title: "UID ", field: "uid", editable: "never", filtering: false},
      { title: "GHN Code ", field: "ghn_code", editable: "never", filtering: false},
      { title: "SKU ", field: "sku_id", editable: "never" },
      { title: "Tên sản phẩm", field: "product_name", editable: "never" },
      { title: "Tên hiển thị", field: "display_name_detail", editable: "never" },
      {
        title: "Giá mua từ NCC (Có VAT)",
        field: "cost_price_with_vat",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      },
      {
        title: "Giá mua từ NCC (No VAT)",
        field: "cost_price_without_vat",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      },
      {
        title: "Giá bán (Có VAT)",
        field: "price_with_vat",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      },
      {
        title: "Giá bán (No VAT)",
        field: "price_without_vat",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      }
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        data={getData}
        columns={column}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Product</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          search: false,
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          debounceInterval: 500,
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          })
        }}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        detailPanel={rowData => (
          <>
            <Grid container className={classes.rootDetailTable} spacing={2}>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_cover} style={{ width: 100, height: 100 }} />
              </Grid>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_banner} style={{ width: 178, height: 100 }} />
              </Grid>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_promotion} style={{ width: 178, height: 100 }} />
              </Grid>
            </Grid>
          </>
        )}
      />
    </div>
    // </>
  )
}