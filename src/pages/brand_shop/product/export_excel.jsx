import React from "react";
import GetAppIcon from "@material-ui/icons/GetApp";
const { api_url } = window.appConfigs
export default function ExportExcel () {
  /* const handlePlainData = (obj, result = {}) => {
    for (let key in obj) {
      if (typeof obj[key] === "object") {
        if (key.indexOf(".") > 0) {
          const newKey = key.replace(".", "__");
          changePropName(obj, newKey, key);
          const linkedFields = combineElement(obj[newKey], newKey);
          // delete obj[newKey];
          // delete result[key];
          result = { ...obj, ...result, ...linkedFields };
        }
        else {
          const linkedFields = combineElement(obj[key], key);
          result = { ...obj, ...result, ...linkedFields };
        }
      }
    }
    return result;
  }; */

  /* const combineElement = (obj, keyName, objResult = {}) => {
    if (Array.isArray(obj)) {
      const uidList = obj.map((elm) => elm.uid);
      objResult[keyName] = uidList.toString();
    } else {
      for (let key in obj) {
        const dependKey = keyName.concat("_") + key;
        const newObj = changePropName(obj, dependKey, key);
        objResult = { ...newObj };
      }
    }
    return objResult;
  }; */

  // const handleData = (arrayData, arrResult = []) => {
  //   if (arrayData !== null) {
  //     const cloneArr = clone(arrayData);
  //     cloneArr.forEach((item) => arrResult.push(handlePlainData(item)));
  //     return arrResult;
  //   }
  // };

  // const excelData = data && handleData(data);

  return (

    <a style={{color: "gray"}} href={`${api_url}/product/product_export.xlsx`} target="_blank" tooltip="Export Here" rel="noreferrer">
      <GetAppIcon />
    </a>
  );
}
