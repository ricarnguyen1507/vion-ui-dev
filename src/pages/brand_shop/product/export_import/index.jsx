import React from 'react'
import Divider from '@material-ui/core/Divider'
import ExportExcel from "./../export_excel"
import ImportExcel from "./../import_excel"


export default function () {

  // Render
  return <div className="fade-in-table">
    <ExportExcel />
    <Divider />
    <ImportExcel />
  </div>

}