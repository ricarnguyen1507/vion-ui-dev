import React, {useState, useContext, forwardRef} from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import VisibilityIcon from '@material-ui/icons/Visibility';
// import { useHistory } from "react-router-dom";
import useStyles from 'pages/style_general';
// import { Edit } from '@material-ui/icons';
import SettingsIcon from '@material-ui/icons/Settings';
import TimerOffIcon from '@material-ui/icons/TimerOff';
import { useHistory, useLocation } from "react-router-dom";
import api from 'services/api_cms'
import { context } from 'context/Shared'

// const RELOAD_CODE = 0;

const productStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
function getFilterStr ({filters}) {
  let strFunc = filters.map(({value, column: {o}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (!o) {
          return value
        }
      }
    }
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o ?? 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(stream.name, "${strFunc.join(' ')}")` : strFunc = "";
  return {strFunc, strFilter: strFilter.join(' AND ')}
}
export default function TableMusic ({ setStateQuery, stateQuery, handleChangeStatus, handleOpenMessage}) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const history = useHistory()
  const ctx = useContext(context)
  const { pathname } = useLocation()
  // const history = useHistory()
  function handleTurnOffStream (rowData) {
    const data = {
      uid: rowData?.uid ?? null,
      'stream.end_time': new Date().getTime(),
    }
    const formData = new FormData()
    formData.set('set', JSON.stringify(data))
    return api.post('/video-stream', formData, {
      headers: {
        "Accept": "application/json",
        "Content-Type": "multipart/form-data"
      }
    }).then(() => {
      getData({})
    }).catch(error => {
      console.log(error.response)
      handleOpenMessage(error.response.data.message)
    })
  }
  function parseDate (unixDate) {
    if(!unixDate)
      return ''
    const d = new Date(unixDate)
    return `${String(d.getHours()).padStart(2, '0')}:${String(d.getMinutes()).padStart(2, '0')} ${String(d.getDate()).padStart(2, '0')}/${String(d.getMonth() + 1).padStart(2, '0')}/${d.getFullYear()}`
  }

  function isLive (rowData) {
    const time_start = new Date(rowData['stream.start_time']);
    const time_end = rowData['stream.end_time'] ? new Date(rowData['stream.end_time']) : null;
    const now = new Date();

    if(now >= time_start && !time_end)
      return 0;
    else if(now < time_start)
      return -1;
    else
      return 1;
  }
  function getData (query = {}) {
    const queryData = {
      ...stateQuery,
      ...query
    }
    setStateQuery(queryData)
    const {strFunc, strFilter} = getFilterStr(queryData)
    return api.post(`/brand-shop/video-stream-filter`, {
      number: query.pageSize,
      page: query.page,
      ...(strFilter && {filter: strFilter}),
      ...(strFunc && {func: strFunc})
    }).then(res => {
      const { summary: {totalCount, page, offset}, result } = res.data
      setStateQuery({
        ...stateQuery,
        totalCount,
        page,
        offset
      })
      return {
        data: result,
        page: query.page,
        totalCount
      }
    })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Status", field: "display_status", editable: 'onUpdate',
        lookup: productStatus,
        render: rowData => <span>{productStatus[rowData['display_status']] ?? 'Inactive'}</span>,
        o: 'eq'
      },
      { title: "UID", field: "uid", editable: 'never', filtering: false },
      { title: "Tên livestream", field: "stream.name", editable: 'never' },
      { title: "Tên hiển thị", field: "stream.display_name", editable: 'never', filtering: false},
      { title: "Số sản phẩm", field: "stream.products", editable: 'never', filtering: false,
        render: rowData => <span>{rowData?.['stream.products']?.length ?? 0}</span>
      },
      {
        title: "Thời gian phát sóng", field: "stream.start_time", editable: 'never', filtering: false,
        render: rowData => <span>{parseDate(rowData['stream.start_time'])}</span>
      },
      // {
      //   title: "Thời gian kết thúc", field: "stream.end_time", editable: 'never', filtering: false,
      //   render: rowData => <span>{parseDate(rowData['stream.end_time'])}</span>
      // },
      {
        title: "Trạng thái", field: "display_status", editable: 'never', filtering: false,
        render: rowData => <span>{rowData['display_status'] !== 2 ? 'Đóng stream' : isLive(rowData) === 0 ? 'Đang live' : isLive(rowData) === -1 ? 'Chưa live' : 'Đã live'}</span>
      },
      { title: "Lượt xem thực tế", field: "stream.view_count", editable: 'never', filtering: false},
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  });
  const tableIcons = {
    Edit: forwardRef((props, ref) => <SettingsIcon {...props} ref={ref} />)
  }
  return (
    <div className="fade-in-table">
      <MaterialTable
        icons={tableIcons}
        columns={column}
        data={getData ?? []}
        title={<h1 className={classes.titleTable}>Live Stream List</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        onFilterChange={e => getData({filters: e})}
        onPageChange={(page, pageSize) => getData({page, pageSize})}
        onSearchChange={e => getData({search: e})}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        localization={{
          body: {
            editRow: {
              deleteText: ''
            }
          }
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add streams',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/Add`)
            }
          },
          rowData => ({
            icon: isLive(rowData) === 0 && +rowData['display_status'] === 2 ? VisibilityIcon : 'edit',
            tooltip: isLive(rowData) === 0 && +rowData['display_status'] === 2 ? "View stream" : "Edit stream",
            onClick: (event, { tableData, ...rowData }) => {
              if(isLive(rowData) === 0 && +rowData['display_status'] === 2) {
                history.push(`/app/live-stream/view/${rowData.uid}`)
              }else{
                ctx.set('dataEdit', rowData)
                history.push(`/app/brand-shop-live-stream/Edit`)
              }
            }
          }),
          rowData => ({
            disabled: isLive(rowData) !== 0,
            icon: TimerOffIcon,
            tooltip: 'Tắt stream',
            onClick: () => handleTurnOffStream(rowData),
          }),

        ]}
        editable={{
          onRowUpdate: handleChangeStatus,
          editTooltip: () => 'Cập nhật trạng thái live stream'
        }}
      />
    </div>
  )
}