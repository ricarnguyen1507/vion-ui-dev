import React, { useState } from 'react';
import {
  useHistory,
  Route,
  useRouteMatch
} from "react-router-dom";
import api from 'services/api_cms';
import Table from './table'
import Edit from './edit';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { Shared } from 'context/Shared'
// import { id } from 'date-fns/esm/locale';
function Alert (props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

// const RELOAD_CODE = 0;
export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  // const [loadingText, setLoadingText] = useState('Đang lấy dữ liệu');
  // const [loadingCode, setLoadingCode] = useState();
  const [dataTable] = useState([])
  // const [actionType, setActionType] = useState('')
  const [openMessage, setOpenMessage] = React.useState(false);
  const [snackbarMessage, setMessage] = React.useState('');
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))

  const handleOpenMessage = (message) => {
    setMessage(message)
    setOpenMessage(true);
  };

  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenMessage(false);
  };

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }

  const handleChangeStatus = (rowData) => {
    const data = {
      uid: rowData?.uid ?? null,
      display_status: rowData?.display_status ?? 0,
    }
    const formData = new FormData()
    formData.set('set', JSON.stringify(data))
    return api.post('/video-stream', formData, {
      headers: {
        "Accept": "application/json",
        "Content-Type": "multipart/form-data"
      }
    }).then(() => {
    }).catch(error => {
      console.log(error.response)
      handleOpenMessage(error.response.data.message)
    })
  }


  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Snackbar open={openMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
          <Alert onClose={handleCloseMessage} severity="error">
            {snackbarMessage}
          </Alert>
        </Snackbar>
        <Edit
          // actionType={actionType}
          data={dataTable}
          functionBack={functionBack}
        />
      </Route>
      <Route exact path={path}>
        <Table
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          handleChangeStatus={handleChangeStatus}
          handleOpenMessage={handleOpenMessage}
        />
      </Route>
    </Shared>
  )

}