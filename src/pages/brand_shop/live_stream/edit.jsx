import React, { useState, useMemo, useEffect, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent';
import Media from 'components/Media'
import { genCdnUrl } from 'components/CdnImage'
import DateFnsUtils from '@date-io/date-fns';
import IconButton from '@material-ui/core/IconButton'
import AddBox from '@material-ui/icons/AddBox'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import { InputEdge, Node, CheckBoxEdge } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import StreamProduct from 'components/BrandShop_LiveStreams/stream_products'
import Loading from 'components/Loading';
import { ObjectsToForm } from "utils"
import { genUid } from "utils"

export default function ({ functionBack}) {
  const history = useHistory()

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_video_stream",
    "dgraph.type": "VideoStream",
    "display_status": 0,
    "stream.is_highlight": false,
    "stream.products": []
  })
  const actionType = dataEdit?.uid.startsWith('_:') ? 'Add' : 'Edit'
  if(actionType == 'Edit' && !dataEdit?.['stream.products']) {
    setDataEdit({
      ...dataEdit,
      'stream.products': []
    })
  }
  const [node] = useState(() => new Node(dataEdit))
  // const { isLoading, setIsLoading } = useContext(LoadingContext);
  // const [loadingText, setLoadingText] = useState()
  // const [loadingCode, setLoadingCode] = useState()
  // Load data
  const [channels, setChannel] = useState([])
  const [brandShopValue, setBrandShopValue] = useState({});
  const [brandShopList, setBrandShopList] = useState([]);

  const classge = useGeneral()
  const [files] = useState({})

  const STATUS_LIST = [
    {
      id: 0,
      name: 'PENDING',
    },
    {
      id: 1,
      name: 'REJECTED',
    },
    {
      id: 2,
      name: 'APPROVED',
    }
  ]
  const stream_channel = useMemo(() => {
    const uid = dataEdit?.['stream.channel']?.length ? dataEdit['stream.channel'][0]['uid'] : null
    if(!uid)
      return null;
    return channels.find(item => item.uid === uid) ?? null
  }, [dataEdit, channels])
  const [timer, setTimer] = useState(null)

  const onInputSelectChannelChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchChannelOption(queries)
    }, 550))
  }
  function fetchChannelOption (queries) {
    if (queries !== "") {
      api.post(`/stream-channel-option`, queries)
        .then(res => {
          const newOption = channels || []
          const temp = res.data.result.filter(r => (!newOption.find(no => no.uid === r.uid)))
          setChannel([...newOption, ...temp])
        })
    }
  }

  const handleChangeChannelList = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'stream.channel': item ? [{ uid: item.uid }] : null })
    let oldChannel = node.getState('stream.channel')?.map(d => d = {uid: d.state.uid}) ?? []
    if(item) {
      if(oldChannel.length) {
        let mergeArr = oldChannel.concat([{ uid: item.uid }])
        node.setState('stream.channel', mergeArr, false)
        let stream_channel = node.getState('stream.channel') ?? []
        stream_channel.map(hl => oldChannel.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.state.isNew = true))
      }else {
        node.setState('stream.channel', [{ uid: item.uid }], true)
      }

    }else{
      let deleteItem = node.getState('stream.channel')
      deleteItem.map(d => d.isDeleted = true)
    }
  }
  // const [files, setFiles] = React.useState({});


  const stream_status = useMemo(() => STATUS_LIST.find(item => item.id === (dataEdit?.['display_status'] ?? '')) ?? null, [dataEdit, STATUS_LIST])

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/brand-shop-live-stream')
  }
  const handleSubmit = function () {
    let listOldStream = node.getState('stream.products') ?? []
    if(dataEdit?.['stream.products'].length || listOldStream.length) {
      listOldStream = listOldStream?.map(l => l = {uid: l.state.uid})
      const removeItem = listOldStream?.filter(({uid: uid1}) => !dataEdit?.['stream.products']?.some(({ uid: uid2 }) => uid1 === uid2));
      if(removeItem.length > 0) {
        let mergeArr = dataEdit?.['stream.products'].concat(removeItem)
        node.setState('stream.products', mergeArr, false)
        let stream_products = node.getState('stream.products') ?? []
        stream_products.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
      } else {
        node.setState('stream.products', dataEdit?.['stream.products'], true)
      }
    }
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post(`/brand-shop/video-stream/${dataEdit?.uid}/${brandShopValue?.uid ?? ''}`, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const onError = (e) => {
    console.log("lỗi", e.name);
  }

  // Lưu lại những image thay  đổi
  const updateFile = (field, file) => {
    node.setState(field, `images/live_stream/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  function isValidDate (d) {
    return d instanceof Date && !isNaN(d);
  }

  const dateStartStreams = useMemo(() => {
    if(dataEdit['stream.start_time']) {
      return new Date(dataEdit['stream.start_time']);
    }
    return null;
  }, [dataEdit])
  const handleChangeDateStart = (date) => {
    if(isValidDate(date)) {
      setDataEdit({ ...dataEdit,
        'stream.start_time': new Date(date).getTime(),
      })
      node.setState('stream.start_time', new Date(date).getTime())
    }
  };
  const onAddProducts = () => {
    setDataEdit({
      ...dataEdit,
      'stream.products': [
        ...dataEdit['stream.products'],
        {
          uid: genUid("new_stream_product"),
          time: 0,
          "dgraph.type": "StreamProduct"
        }
      ]
    })
  }

  const handleChangeData = (data, index) => {
    if(index >= 0) {
      const products = dataEdit['stream.products'].map((item, idx) => {
        if(idx === index)
          return data;
        return item;
      })
      setDataEdit({
        ...dataEdit,
        'stream.products': products
      })
    }
  }
  const handleDeleteProduct = (index) => {
    setDataEdit({
      ...dataEdit,
      'stream.products': dataEdit?.['stream.products']?.filter((item, idx) => idx !== index)
    })
  }
  const handleChangeStreamStatus = function (e, item) {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': item ? item.id : STATUS_LIST[0].id })
    node.setState('display_status', item ? item.id : STATUS_LIST[0].id)
  }

  const handleChangeCheckBox = ({target: {name, checked}}) => {
    setDataEdit({...dataEdit, [name]: checked})
    node.setState(name, checked)
  }

  const handleChangeBrandShop = (e, item) => {
    setBrandShopValue(item)
  }

  const onInputSelectBrandShopChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShopList
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setBrandShopList([...newOption])
        })
    }
  }

  useEffect(() => {
    if(!dataEdit.uid.startsWith("_:")) {
      api.get(`/brand-shop/video-stream/${dataEdit.uid}`).then(res => {
        setDataEdit({
          ...(res?.data?.result?.[0] ?? {})
        })
        setBrandShopValue(res?.data?.result?.[0]?.['brand_shop']?.[0] ?? null)
        setBrandShopList(res?.data?.result?.[0]?.['brand_shop'] ?? [])
        setChannel(res?.data?.result?.[0]?.['stream.channel'] ?? [])
      })
        .catch(error => console.log(error));
    }
  }, [dataEdit.uid]);
  const ActionIcon = <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Live Stream" />
      {!dataEdit || !Object.keys(dataEdit).length && !dataEdit.uid.startsWith("_:") ? <Loading /> :
        <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
          <Grid container spacing={2} >
            <Grid item xs={12} sm={12} lg={12}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin cơ bản
                  </Typography>
                </div>
                <Divider />
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={12} lg={12}>
                    <InputEdge
                      Component={TextValidator}
                      node={node}
                      pred={"stream.name"}
                      fullWidth
                      label="Tên live stream (*)"
                      variant="outlined"
                      margin="dense"
                      validators={[
                        'required',
                        'minStringLength:3',
                        'maxStringLength:200'
                      ]}
                      errorMessages={[
                        'This field is required',
                        'Min length is 3',
                        'Max length is 200'
                      ]}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} lg={12}>
                    <InputEdge
                      Component={TextValidator}
                      node={node}
                      pred={"stream.display_name"}
                      fullWidth
                      label="Tên hiển thị (*)"
                      variant="outlined"
                      margin="dense"
                      validators={[
                        'required',
                        'minStringLength:3',
                        'maxStringLength:40'
                      ]}
                      errorMessages={[
                        'This field is required',
                        'Min length is 3',
                        'Max length is 40'
                      ]}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} lg={12}>
                    <Autocomplete
                      disabled={!dataEdit.uid.startsWith("_:") && brandShopValue}
                      onChange={handleChangeBrandShop}
                      options={brandShopList}
                      value={brandShopValue}
                      getOptionLabel={option => (option.brand_shop_name || '')}
                      onInputChange={onInputSelectBrandShopChange}
                      renderInput={params => (
                        <TextField {...params}
                          label="Brand Shop"
                          fullWidth
                          variant="outlined"
                          margin="dense"
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} lg={12}>
                    <InputEdge
                      Component={TextValidator}
                      node={node}
                      pred={"stream.view_virtual"}
                      fullWidth
                      label="Số lượng người xem"
                      variant="outlined"
                      margin="dense"
                      validators={[
                        'required',
                      ]}
                      errorMessages={[
                        'This field is required',
                      ]}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} lg={12}>
                    <FormControlLabel
                      control={
                        <CheckBoxEdge
                          Component={Checkbox}
                          node={node}
                          pred={"is_portrait"}
                          fullWidth
                          variant="outlined"
                          margin="dense"
                          color="primary"
                        />
                      }
                      label="Video dọc"
                    />
                  </Grid>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} style={{ width: '100%' }}>
                    <Grid item xs={6} sm={6} lg={6}>
                      <KeyboardDateTimePicker
                        style={{ width: '100%' }}
                        format="HH:mm dd/MM/yyyy"
                        margin="normal"
                        id="date-picker-dialog-date-streams"
                        label="Thời gian phát sóng"
                        value={dateStartStreams}
                        onChange={handleChangeDateStart}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </Grid>
                  </MuiPickersUtilsProvider>
                </Grid>
              </div>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6} xl={4}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Video Live
                  </Typography>
                </div>
                <Divider />
                <Grid container spacing={2} style={{marginTop: '10px'}}>
                  <Grid item xs={12} sm={12} lg={12}>
                    <Autocomplete
                      onChange={handleChangeChannelList}
                      options={channels}
                      value={stream_channel}
                      getOptionLabel={option => option?.['channel.name'] ?? ''}
                      onInputChange={onInputSelectChannelChange}
                      renderInput={params => (
                        <TextField {...params}
                          label="Chọn kênh stream"
                          fullWidth
                          variant="outlined"
                          margin="dense"
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} lg={12}>
                    <InputEdge
                      Component={TextValidator}
                      node={node}
                      pred={"stream.video_transcode"}
                      fullWidth
                      label="Video Transcode"
                      variant="outlined"
                      margin="dense"
                      validators={[
                        'required',
                      ]}
                      errorMessages={[
                        'This field is required',
                      ]}
                    />
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Hình ảnh
                  </Typography>
                </div>
                <Divider />
                <Grid container spacing={2} style={{marginTop: '10px'}}>
                  <Grid item>
                    <Card>
                      <CardContent style={{minHeight: '84px'}}>
                        <Typography gutterBottom variant="h5" component="h2">
                                                Image thumb
                        </Typography>
                      </CardContent>
                      <CardMedia children={<Media src={genCdnUrl(dataEdit['stream.image_thumb'], "image_banner.png")} type="image" style={{ width: 440, height: 225, marginBottom: '10px' }} fileHandle={updateFile} field="stream.image_thumb" accept="image/*" />} />
                    </Card>
                  </Grid>
                  <Grid item>
                    <Card>
                      <CardContent style={{minHeight: '84px'}}>
                        <Typography gutterBottom variant="h5" component="h2">
                                                Image fullscreen
                        </Typography>
                      </CardContent>
                      <CardMedia children={<Media src={genCdnUrl(dataEdit['stream.image_fullscreen'], "image_banner.png")} type="image" style={{ width: 440, height: 225, marginBottom: '10px' }} fileHandle={updateFile} field="stream.image_fullscreen" accept="image/*" />} />
                    </Card>
                  </Grid>
                </Grid>
              </div>
            </Grid>
            <Grid item xs={12} sm={12} lg={12}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Danh sách sản phẩm live stream
                  </Typography>
                  <IconButton color="primary" onClick={onAddProducts}>
                    <AddBox />
                  </IconButton>
                  <FormControlLabel
                    control={
                      <Checkbox
                        className={classge.titleProduct}
                        id="stream.is_highlight"
                        name="stream.is_highlight"
                        color="primary"
                        defaultChecked={dataEdit['stream.is_highlight']}
                        onChange={handleChangeCheckBox}/>
                    }
                    label="Highlight sản phẩm"
                  />
                </div>
                <Divider />
                <Grid container spacing={2} style={{marginTop: '10px'}}>
                  {dataEdit['stream.products']?.map((product, index) =>
                    (<Grid key={product.uid + index} item xs={12} sm={12} lg={12}>
                      <StreamProduct brandShopUid={brandShopValue?.uid ?? ''} indexProduct={index} product={product} handleChangeData={handleChangeData} handleDeleteProduct={handleDeleteProduct} />
                    </Grid>)
                  ) ?? <Grid item />}
                </Grid>
              </div>
            </Grid>
            <Grid item xs={12} sm={12} lg={12}>
              <div className={classge.rootPanel}>
                <div className={classge.titlePanel}>
                  <div className={classge.iconTitle}>
                    {ActionIcon}
                  </div>
                  <Typography className={classge.contentTitle} variant="h5" >
                                    Trạng thái
                  </Typography>
                </div>
                <Divider />
                <Grid container spacing={2} style={{marginTop: '10px'}}>
                  <Grid item xs={6} sm={6} lg={3}>
                    <Autocomplete
                      onChange={handleChangeStreamStatus}
                      options={STATUS_LIST}
                      value={stream_status}
                      getOptionLabel={option => (option.name || '')}
                      onInputChange={() => {}}
                      renderInput={params => (
                        <TextField {...params}
                          label="Trạng thái hiển thị"
                          fullWidth
                          variant="outlined"
                          margin="dense"
                        />
                      )}
                    />
                  </Grid>

                </Grid>
              </div>
            </Grid>
          </Grid>
          {/* Button submit */}
          <Grid className={classge.rootSubmit}>
            <Button
              style={{marginRight: '10px'}}
              color="secondary"
              variant="contained"
              aria-label="back"
              className={classge.btnControlGeneral}
              onClick={functionBack}
            >
              <ReturnIcon className={classge.iconback} />
                        Return
            </Button>
            <RoleButton page="live-stream" actionType={actionType} />
          </Grid>
        </ValidatorForm>
      }
    </>
  )
}
