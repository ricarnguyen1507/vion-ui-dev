import React, { useState, useContext } from 'react'
import MaterialTable from 'material-table'
import { Grid, Tooltip, IconButton } from '@material-ui/core'
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from "@material-ui/icons/Edit"
import ImportExportIcon from '@material-ui/icons/ImportExport';
import SearchIcon from '@material-ui/icons/Search'
import FileCopy from '@material-ui/icons/FileCopy'
import useStyles from 'pages/style_general'
// import { LoginContext } from "context/LoginContext"
import ImgCdn from 'components/CdnImage'
import api from 'services/api_cms'
import ExportExcel from "./export_excel"
// import sendTracking from "../../tracking";
import {
  useHistory,
  useLocation
} from "react-router-dom"
import { context } from 'context/Shared'
const productStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED',
  '3': 'IMPORT'
}
// const { api_url } = window.appConfigs
// let linkExport = window.location.href.split("product")
// console.log(linkExport[0]);
function calFinalPrice ({ sell_price = 0, discount = 0 }) {
  return discount > 0 ? sell_price * (1 - discount / 100) : sell_price
}


function getFilterStr ({filters}) {
  let typeFilter = ''
  let strFunc = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string' && field !== "brand_shop_name") {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (!o) {
          return value
        }
      }
    }
  })
  let fitlerBrandShop = filters.map(({value, column: {o, field}}) => {
    typeFilter = field
    if(typeof value === 'string' && field === "brand_shop_name") {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (!o) {
          return value
        }
      }
    }
  })
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")
  if(strFunc && strFunc.length > 0 && strFunc.join('') != "" && fitlerBrandShop.join('') == "") {
    strFunc = `func: alloftext(fulltext_search, "${strFunc.join(' ')}")`
    fitlerBrandShop = ""
  } else if(fitlerBrandShop && fitlerBrandShop.length > 0 && fitlerBrandShop.join('') != "") {
    if(strFunc && strFunc.length > 0 && strFunc.join('') != "") {
      strFunc = ' AND ' + `alloftext(fulltext_search, "${strFunc.join(' ')}")`
    }else {
      strFunc = ""
    }
    fitlerBrandShop = `func: alloftext(brand_shop_name, "${fitlerBrandShop.join(' ')}")`
  } else {
    strFunc = ""
  }
  return {strFunc, strFilter: strFilter.join(' AND '), typeFilter, fitlerBrandShop}
}

/* function newFilterStr ({filters}) {
  const str = `func: type(Product)) @filter(not eq(is_deleted, true)`
} */

export default function TableProduct ({setBrandShop, setDataTable, setActionType, stateQuery, setStateQuery, handleDelete, displayStatus, setMode }) {
  const { pathname } = useLocation()
  const classes = useStyles()
  const history = useHistory()
  const ctx = useContext(context)
  // const [dataTable, setDataTable] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  // const { isLogin, setIsLogin } = React.useContext(LoginContext)
  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFunc, strFilter, typeFilter, fitlerBrandShop} = getFilterStr(query)
    ///${query.pageSize}/${query.page}/${query.search || "all"}`
    return api.post("/list/product", {
      number: query.pageSize,
      page: query.page,
      typeFilter: typeFilter,
      ...{filter: (strFilter ? ' AND ' + strFilter : '')},
      ...(strFunc && {func: strFunc}),
      ...(fitlerBrandShop && {brand_shop_filter: fitlerBrandShop})
    })
      .then(response => {

        const { totalCount, result } = response.data
        setDataTable(result)
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  // IMPORT DATA PRODUCT FROM EXCEL FILE
  function functionImportExport (type) {
    if(type === 'import')
      setMode("import")
    // sendTracking({pages: 'product', actionType: type, userData})
  }
  /* function onSearchChange() {
        getData(stateQuery)
    } */
  /* const closeSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setIsLogin(false);
  }
  const snackbarTransition = (props) => <Slide {...props} direction="left" /> */

  const createSearchData = () => {
    api.get('/product/create-search-data')
      .then(res => console.log(res))
  }
  const copyAreasToAnOther = () => {
    api.get('/product/copy-areas')
      .then(res => console.log(res))
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: productStatus,
        o: 'eq'
      },
      { title: "UID ", field: "uid", editable: "never", filtering: false},
      { title: "Tên sản phẩm", field: "product_name", editable: "never" },
      { title: "Tên hiển thị", field: "display_name_detail", editable: "never" },
      {
        title: "Giá mua từ NCC (Có VAT)",
        field: "price",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      },
      {
        title: "Giá bán (VAT)",
        field: "cost_price",
        type: "currency",
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        cellStyle: {
          textAlign: "left"
        },
        editable: "onUpdate",
        filtering: false
      }
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  /* const options = useState(() => {
        return {
            search: false,
            // searchText: stateQuery.search,
            headerStyle: {
                fontWeight: 600,
                background: "#f3f5ff",
                color: "#6e6e6e",
            },
            debounceInterval: 500,
            filtering: true,
            sorting: true,
            exportButton: true,
            grouping: true,
            pageSize: 10,
            pageSizeOptions: [10, 25, 50],
            actionsColumnIndex: -1,
            rowStyle: rowData => ({
                backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
            })
        }
    }) */

  // Render
  return (
  // <>
  //     <Snackbar
  //         anchorOrigin={{ vertical: "top", horizontal: "right" }}
  //         open={isLogin}
  //         onClose={closeSnackbar}
  //         autoHideDuration={8000}
  //         TransitionComponent={snackbarTransition}
  //         style={{
  //             top: 70,
  //         }}
  //     >
  //         <Alert
  //             icon={false}
  //             style={{
  //                 padding: "10px 14px",
  //                 backgroundColor: "rgb(235, 244, 245,0.7)",
  //                 borderRadius:"3px",
  //                 borderLeft: "3px solid #27ae60",
  //                 boxShadow: "0 4px 8px 0 rgba(32, 33, 36, .28)",
  //                 color: "#27ae60",
  //                 fontSize: "1rem",
  //                 fontWeight: 550
  //             }}

  //             classes={{message: classes.snackbarMessage, standardSuccess: classes.snackbarAlert}}
  //         >
  //             Hello Admin. Welcome back !
  //         </Alert>
  //     </Snackbar>
    <div className="fade-in-table">
      <MaterialTable
        // onSearchChange={onSearchChange}
        data={getData}
        columns={column}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Product</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank" tooltip="Add Supplier">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }} tooltip="Display Edit">
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Import" aria-label="import">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { history.push(`${pathname}/import`)
                        setActionType('import') }} tooltip="Display Import">
                        <ImportExportIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Export" aria-label="export">
                      <IconButton onClick={() => functionImportExport('export')} style={{color: "gray", cursor: "pointer"}} tooltip="Display Export">
                        <ExportExcel />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Create Search Data" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { createSearchData() }} target="_blank" tooltip="Add Supplier">
                        <SearchIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Copy Areas to an other" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { copyAreasToAnOther() }} target="_blank" tooltip="Add Supplier">
                        <FileCopy />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          search: false,
          // searchText: stateQuery.search,
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          debounceInterval: 500,
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          })
        }}
        // title={<h1 className={classes.titleTable}>Product</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        detailPanel={rowData => (
          <>
            <Grid container className={classes.rootDetailTable} spacing={2}>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_cover} style={{ width: 100, height: 100 }} />
              </Grid>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_banner} style={{ width: 178, height: 100 }} />
              </Grid>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_promotion} style={{ width: 178, height: 100 }} />
              </Grid>
            </Grid>
          </>
        )}
        actions={[
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Product",
            onClick: (event, { tableData, ...rowData }) => {
              // ctx.editData(rowData)
              ctx.set('dataEdit', rowData)
              setBrandShop(rowData['brand_shop'])
              // setActionType("Edit")
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: handleDelete
        }}
      />
    </div>
    // </>
  )
}