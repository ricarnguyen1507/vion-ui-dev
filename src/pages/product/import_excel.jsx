import React, { useState, useEffect } from "react";
import {
  useHistory
} from "react-router-dom"
import { ExcelRenderer } from "react-excel-renderer";
import GetAppIcon from "@material-ui/icons/GetApp";
import MaterialTable from "material-table";
import { Button } from "@material-ui/core";
import DoneIcon from '@material-ui/icons/Done';
import UploadIcon from '@material-ui/icons/Backup';
import Grid from "@material-ui/core/Grid"
import ReturnIcon from "@material-ui/icons/KeyboardReturn"
import useStyles from 'pages/style_general'
import { parseFileUpload } from "services/handleImportExcel";
import api from 'services/api_cms';
import Loading from "components/Loading"
import Modal from '@material-ui/core/Modal';
import { clone } from 'services/diff'
import { genUid } from "utils"

const { api_url } = window.appConfigs
const isuid = /^0[xX][0-9A-F]+$/i
const decode = (str) => !str ? '' : str.normalize('NFD')
  .replace(" ", '').replace(/Đ/g, 'D')
const dialogStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED',
  '3': 'IMPORT'
}

function getModalStyle () {
  const top = 45
  const left = 45

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

export default function ImportExcel ({}) {
  const history = useHistory()
  const [loadingImport, setLoadingImport] = useState()
  const classge = useStyles();
  const [modalStyle] = useState(getModalStyle);
  const [data, setData] = useState([]);
  const [dataUpdated, setDataUpdated] = useState([]);
  const [fileOrigin, setFileOrigin] = useState([]);
  const [dataDialog, setDataDialog] = useState(null);
  const [open, setOpen] = useState(false);
  const [successImport, setSuccessImport] = useState(false);
  const [openSubmit, setOpenSubmit] = useState(false);
  let formData = new FormData();

  const ErrorMessage = {
    STRING_IS_EMPTY: 'Thiếu thông tin',
    MAX_LENGTH: 'Vượt quá ký tự cho phép',
    DUPLICATE: 'Trùng mã sản phẩm',
    UID_FORMAT: 'Thông tin sai',
    WRONG_FORMAT_VALUE: 'Thông tin nhâp sai định dạng'
  }

  useEffect(() => {
    api.get("/list/dialog")
      .then(response => {
        let dataTableDialog = response.data.result;
        for (let i = 0; i < dataTableDialog.length; i++) {
          dataTableDialog[i].updatedBy = dataTableDialog[i].updatedBy.user_name;
        }
        setDataDialog(response.data.result)
      })
      .catch(err => {
        console.log(err)
      })
  }, [])

  const [errors] = useState([])
  const validationFile = async (object, idx) => {
    let checkError = {};
    let errorStatus = false;

    if (Object.keys(object).length === 0) {
      return
    }
    // CHECK PRODUCT NAME
    checkError.product_name = object.product_name == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.product_name !== '' && errors.push({
      'stt': idx,
      'field_name': 'Tên sản phẩm',
      'error_value': checkError.product_name
    })
    // CHECK DISPLAY BILL NAME
    checkError.display_bill_name = object.display_bill_name == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.display_bill_name !== '' && errors.push({
      'stt': idx,
      'field_name': 'Tên theo hoá đơn',
      'error_value': checkError.display_bill_name
    })
    // CHECK DISPLAY NAME
    // checkError.display_name = object.display_name == undefined ? ErrorMessage.STRING_IS_EMPTY : object.display_name.length > 40 ? ErrorMessage.MAX_LENGTH : '';
    // checkError.display_name !== '' && errors.push({
    //   'stt': idx,
    //   'field_name': 'Tên hiển thị',
    //   'error_value': checkError.display_name
    // })

    //CHECK DISPLAY NAMEDETAIL
    checkError.display_name_detail = object.display_name_detail == undefined ? ErrorMessage.STRING_IS_EMPTY : object.display_name_detail.length > 80 ? ErrorMessage.MAX_LENGTH : '';
    checkError.display_name_detail !== '' && errors.push({
      'stt': idx,
      'field_name': 'Tên hiển thị chi tiết',
      'error_value': checkError.display_name_detail
    })

    // CHECK UID PARTNER
    object.partner_uid = decode(object.partner_uid);
    checkError.partner_uid = (object.partner_uid == undefined || object.partner_uid == '') ? ErrorMessage.STRING_IS_EMPTY : (isuid.test(object.partner_uid) == false) ? ErrorMessage.UID_FORMAT : '';

    checkError.partner_uid !== '' && errors.push({
      'stt': idx,
      'field_name': 'ID nhà cung cấp',
      'error_value': checkError.partner_uid
    })
    // CHECK UID MANUFACTURER
    object.manufacturer_uid = decode(object.manufacturer_uid);
    checkError.manufacturer_uid = (object.manufacturer_uid == undefined || object.manufacturer_uid == '') ? ErrorMessage.STRING_IS_EMPTY : (isuid.test(object.manufacturer_uid) == false) ? ErrorMessage.UID_FORMAT : '';
    checkError.manufacturer_uid !== '' && errors.push({
      'stt': idx,
      'field_name': 'ID nhà sản xuất',
      'error_value': checkError.manufacturer_uid
    })
    // CHECK UID BRAND
    object.brand_uid = decode(object.brand_uid);
    checkError.brand_uid = (object.brand_uid == undefined || object.brand_uid == '') ? ErrorMessage.STRING_IS_EMPTY : (isuid.test(object.brand_uid) == false) ? ErrorMessage.UID_FORMAT : '';
    checkError.brand_uid !== '' && errors.push({
      'stt': idx,
      'field_name': 'Tên thương hiệu',
      'error_value': checkError.brand_uid
    })
    // CHECK UID COLLECTION
    object.collection_uid = decode(object.collection_uid);
    checkError.collection_uid = (object.collection_uid == undefined || object.collection_uid == '') ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.collection_uid !== '' && errors.push({
      'stt': idx,
      'field_name': 'Nghành hàng 2',
      'error_value': checkError.collection_uid
    })
    // CHECK ORIGINAL
    // checkError.original = object.original == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
    // checkError.original !== '' && errors.push({
    //   'stt': idx,
    //   'field_name': 'Xuất xứ',
    //   'error_value': checkError.original
    // })
    // CHECK UNIT
    // checkError.unit = object.unit == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
    // checkError.unit !== '' && errors.push({
    //   'stt': idx,
    //   'field_name': 'Đơn vị tính',
    //   'error_value': checkError.unit
    // })
    // CHECK SHORT DESCRIPTION
    // checkError.short_desc = object.short_desc == undefined ? ErrorMessage.STRING_IS_EMPTY : object.short_desc.length > 200 ? ErrorMessage.MAX_LENGTH : '';
    // if (checkError.short_desc !== '') {
    //   errors.push({
    //     'stt': idx,
    //     'field_name': 'Mô tả ngắn',
    //     'error_value': checkError.short_desc
    //   })
    // }

    // CHECK KEYWORDS
    // checkError.keywords = object.keywords == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
    // checkError.keywords !== '' && errors.push({
    //   'stt': idx,
    //   'field_name': 'Keywords',
    //   'error_value': checkError.keywords
    // })
    // CHECK UOM UID
    object.UOM_uid = decode(object.UOM_uid);
    checkError.UOM_uid = (object.UOM_uid == undefined || object.UOM_uid == '') ? ErrorMessage.STRING_IS_EMPTY : (isuid.test(object.UOM_uid) == false) ? ErrorMessage.UID_FORMAT : '';
    checkError.UOM_uid !== '' && errors.push({
      'stt': idx,
      'field_name': 'Đơn vị đo',
      'error_value': checkError.UOM_uid
    })
    // CHECK UOM QUANTITY
    if (object.uom_quantity != undefined && !isNaN(object.uom_quantity) == false) {
      checkError.uom_quantity !== '' && errors.push({
        'stt': idx,
        'field_name': 'Khối lượng',
        'error_value': ErrorMessage.WRONG_FORMAT_VALUE
      })
    } else {
      checkError.uom_quantity = object.uom_quantity == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
      checkError.uom_quantity !== '' && errors.push({
        'stt': idx,
        'field_name': 'Khối lượng',
        'error_value': checkError.uom_quantity
      })
    }

    // CHECK LONG
    if (object.long != undefined && !isNaN(object.long) == false) {
      checkError.long !== '' && errors.push({
        'stt': idx,
        'field_name': 'Dài',
        'error_value': ErrorMessage.WRONG_FORMAT_VALUE
      })
    } else {
      checkError.long = object.long == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
      checkError.long !== '' && errors.push({
        'stt': idx,
        'field_name': 'Dài',
        'error_value': checkError.long
      })
    }

    // CHECK WIDTH
    if (object.width != undefined && !isNaN(object.width) == false) {
      checkError.width !== '' && errors.push({
        'stt': idx,
        'field_name': 'Rộng',
        'error_value': ErrorMessage.WRONG_FORMAT_VALUE
      })
    } else {
      checkError.width = object.width == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
      checkError.width !== '' && errors.push({
        'stt': idx,
        'field_name': 'Rộng',
        'error_value': checkError.width
      })
    }

    // CHECK HEIGHT
    if (object.height != undefined && !isNaN(object.height) == false) {
      checkError.height !== '' && errors.push({
        'stt': idx,
        'field_name': 'Cao',
        'error_value': ErrorMessage.WRONG_FORMAT_VALUE
      })
    } else {
      checkError.height = object.height == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
      checkError.height !== '' && errors.push({
        'stt': idx,
        'field_name': 'Cao',
        'error_value': checkError.height
      })
    }
    if (Object.keys(object).sku_id) {
      // CHECK COST PRICE WITHOUT VAT
      checkError.sku_id = object.sku_id == undefined ? ErrorMessage.STRING_IS_EMPTY : object.sku_id;
      if (object.cost_price_without_vat != undefined && !isNaN(object.cost_price_without_vat) == false) {
        checkError.cost_price_without_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá mua từ Ncc (No VAT)',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.cost_price_without_vat = object.cost_price_without_vat == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
        checkError.cost_price_without_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá mua từ Ncc (No VAT)',
          'error_value': checkError.cost_price_without_vat
        })
      }

      // CHECK COST PRICE WITH VAT
      if (object.cost_price_with_vat != undefined && !isNaN(object.cost_price_with_vat) == false) {
        checkError.cost_price_with_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá mua từ Ncc (Có VAT)',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.cost_price_with_vat = object.cost_price_with_vat == undefined ? ErrorMessage.STRING_IS_EMPTY : ''
        checkError.cost_price_with_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá mua từ Ncc (Có VAT)',
          'error_value': checkError.cost_price_with_vat
        })
      }

      // CHECK LISTED PRICE WITHOUT VAT
      if (object.listed_price_without_vat != undefined && !isNaN(object.listed_price_without_vat) == false) {
        checkError.listed_price_without_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá niêm yết (No VAT)',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.listed_price_without_vat = object.listed_price_without_vat == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
        checkError.listed_price_without_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá niêm yết (No VAT)',
          'error_value': checkError.listed_price_without_vat
        })
      }

      // CHECK LISTED PRICE WITH VAT
      if (object.listed_price_with_vat != undefined && !isNaN(object.listed_price_with_vat) == false) {
        checkError.listed_price_with_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá niêm yết (Có VAT)',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.listed_price_with_vat = object.listed_price_with_vat == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
        checkError.listed_price_with_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá niêm yết (Có VAT)',
          'error_value': checkError.listed_price_with_vat
        })
      }

      // CHECK PRICE WITHOUT VAT
      if (object.price_without_vat != undefined && !isNaN(object.price_without_vat) == false) {
        checkError.price_without_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá bán (No VAT)',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.price_without_vat = object.price_without_vat == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
        checkError.price_without_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá bán (No VAT)',
          'error_value': checkError.price_without_vat
        })
      }

      // CHECK PRICE WITH VAT
      if (object.price_with_vat != undefined && !isNaN(object.price_with_vat) == false) {
        checkError.price_with_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá bán (Có VAT)',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.price_with_vat = object.price_with_vat == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
        checkError.price_with_vat !== '' && errors.push({
          'stt': idx,
          'field_name': 'Giá bán (Có VAT)',
          'error_value': checkError.price_with_vat
        })
      }
      // CHECK FRONT MARGIN
      if (object.front_margin != undefined && !isNaN(object.front_margin) == false) {
        checkError.front_margin !== '' && errors.push({
          'stt': idx,
          'field_name': 'Front Margin',
          'error_value': ErrorMessage.WRONG_FORMAT_VALUE
        })
      } else {
        checkError.front_margin = object.front_margin == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
        checkError.front_margin !== '' && errors.push({
          'stt': idx,
          'field_name': 'Front Margin',
          'error_value': checkError.front_margin
        })
      }
    }
    // CHECK REGION UID
    object.region_uid = decode(object.region_uid);
    checkError.region_uid = (object.region_uid == undefined || object.region_uid == '') ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.region_uid !== '' && errors.push({
      'stt': idx,
      'field_name': 'Khu vực',
      'error_value': checkError.region_uid
    })
    checkError.spec_type_uid_1 = (object.spec_type_value_1 && object.specs_type_uid_1 == undefined) ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.spec_type_uid_1 !== '' && errors.push({
      'stt': idx,
      'field_name': 'ID nhóm phân loại 1',
      'error_value': checkError.spec_type_uid_1
    })
    checkError.spec_type_uid_2 = (object.spec_type_value_2 && object.specs_type_uid_2 == undefined) ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.spec_type_uid_2 !== '' && errors.push({
      'stt': idx,
      'field_name': 'ID nhóm phân loại 2',
      'error_value': checkError.spec_type_uid_2
    })
    object.specs_type_uid_1 = decode(object.specs_type_uid_1);
    checkError.spec_type_value_1 = (object.specs_type_uid_1 && object.spec_type_value_1 == undefined) ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.spec_type_value_1 !== '' && errors.push({
      'stt': idx,
      'field_name': 'Gía trị phân loại 1',
      'error_value': checkError.spec_type_value_1
    })
    object.specs_type_uid_2 = decode(object.specs_type_uid_2);
    checkError.spec_type_value_2 = (object.specs_type_uid_2 && object.spec_type_value_2 == undefined) ? ErrorMessage.STRING_IS_EMPTY : '';
    checkError.spec_type_value_2 !== '' && errors.push({
      'stt': idx,
      'field_name': 'Gía trị phân loại 2',
      'error_value': checkError.spec_type_value_2
    })
    // CHECK SHIPPING FEE
    // checkError.shipping_fee = object.shipping_fee == undefined ? ErrorMessage.STRING_IS_EMPTY : '';
    // checkError.shipping_fee !== '' && errors.push({
    //   'stt': idx,
    //   'field_name': 'Phí ship',
    //   'error_value': checkError.shipping_fee
    // })

    for (var key in checkError) {
      if (checkError[key] != '' && checkError[key] != 0) {
        checkError.errorStatus = true
        break;
      } else {
        checkError.errorStatus = errorStatus;
      }
    }
    return checkError;
  }

  const fileHandler = async (event) => {
    setLoadingImport(true)
    let fileObj = event.target.files[0];
    setFileOrigin(fileObj)
    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, async (err, resp) => {
      if (err) {
        console.log('err', err);
        alert('Upload file error');
      } else {
        resp.rows[0].unshift("Stt")
        let checkFileStatus = resp.rows[0][1]
        resp.rows.splice(1, 1);
        for (let i = 1; i < resp.rows.length; i++) {
          if (resp.rows[i].length) {
            resp.rows[i].unshift(i + 2);
          }
        }

        const fieldArr = resp.rows[0].map(col => {
          const nameIdx = col.indexOf("<");
          return col.substring(nameIdx + 1);
        });
        if (checkFileStatus == 'sku_id') {
          let dataImport = parseFileUpload(resp.rows, fieldArr)
          if (dataImport.length) {
            await checkUIDEdge(dataImport, checkFileStatus)
          }

        } else {
          let dataImportUpdate = parseFileUpload(resp.rows, fieldArr)
          if (dataImportUpdate.length) {
            await checkUIDEdge(dataImportUpdate, checkFileStatus)
          }
        }
      }
    });
  };
  let columns = [
    {
      title: "Dòng",
      field: "stt",
      editable: "never",
    },
    {
      title: "Thông tin",
      field: "field_name",
      editable: "never",
    },
    {
      title: "Lí do",
      field: "error_value",
      editable: "never",
    }
  ]
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        // render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: dialogStatus,
        o: 'eq'
      },
      { title: "Ngày nhập file ", field: "date_import", editable: "never", filtering: false },
      { title: "Được đăng bởi ", field: "updatedBy", editable: "never", filtering: false },
      { title: "Danh mục nhập file ", field: "type_import", editable: "never" },
      { title: "Số lượng SKU ", field: "item_volume", editable: "never" },
      { title: "Đường dẫn", field: "path", editable: "never" },
      { title: "Đường dẫn file có lỗi", field: "pathErr", editable: "never" },
      { title: "Trạng thái nhập file", field: "import_status", editable: "never" },
    ]
    return column
  })
  const submitData = async () => {
    setOpenSubmit(false);
    setLoadingImport(true)
    console.log('data', data)
    console.log('dataUpdated', dataUpdated)
    let dataImport = clone(data)
    let dataUpdateImport = clone(dataUpdated)
    let validImportArr = [];
    // VALIDATE VALUE BEFORE IMPORT (EX: SKU NOT DUPLICATE, PRODUCT_NAME STRING LENGTH NO MORE THAN 200 CHARACTER)
    console.log('dataImport', dataImport)
    if (data.length > 0) {
      for (let i = 0; i < dataImport.length; i) {

        let checkSKU = dataImport.splice(i, 1);
        if (Object.keys(checkSKU[0]).length !== 0) {
          const found = dataImport.some(el => el.sku_id === checkSKU[0].sku_id);
          if (found == true) {
            errors.push({
              'stt': checkSKU[0].Stt,
              'field_name': 'Mã sản phẩm',
              'error_value': 'Trùng mã sản phẩm'
            })
            await validationFile(checkSKU[0], checkSKU[0].Stt);
            const idx = []
            for (let di in dataImport) {
              if (dataImport[di].sku_id === checkSKU[0].sku_id) {
                let index = dataImport[di].Stt
                idx.push(index);
              }
            }
            for (var y = 0; y < idx.length; y++) {
              for (var d = 0; d < dataImport.length; d++) {
                if (dataImport[d].Stt == idx[y]) {
                  errors.push({
                    'stt': idx[y],
                    'field_name': 'Mã sản phẩm',
                    'error_value': 'Trùng mã sản phẩm'
                  })
                  let objDuplicate = dataImport.splice(d, 1);
                  await validationFile(objDuplicate[0], objDuplicate[0].Stt);
                  break;
                }
              }
            }

          } else {
            let checkObj = await validationFile(checkSKU[0], checkSKU[0].Stt);
            if (checkObj.errorStatus == false) {
              checkSKU[0]['created_at'] = new Date().getTime()
              validImportArr.push(checkSKU[0])
            }
          }
        }
      }
      // END VALIDATE VALUE BEFORE IMPORT (EX: SKU NOT DUPLICATE, PRODUCT_NAME STRING LENGTH NO MORE THAN 200 CHARACTER)
      if (errors.length > 0) {
        console.log('errors', errors)
        setOpenSubmit(false)
        setOpen(true);
      } else {
        if (validImportArr.length > 0) {
          let skuArr = [];
          for (let obj of validImportArr) {
            let sku = JSON.stringify(obj.sku_id);
            preparePricingData(obj);
            for (let key in obj) {
              if (obj[key] == '') {
                obj[key] = null
              }
            }
            skuArr.push(sku);
          }
          formData.set("checkSku", JSON.stringify(skuArr))
          let errorSku = await checkSkuWithSystem(formData).then(res => res)
          console.log('checkSku', errorSku);
          if (errorSku !== undefined && errorSku.length == 0) {

            formData.set("dataValid", JSON.stringify(validImportArr))
            formData.set('file', fileOrigin);
            api.post('/product/import', formData).catch(error => console.log(error))
            setLoadingImport(false)
            setOpen(false);
            setSuccessImport(true)
          } else if (errorSku !== undefined && errorSku.length > 0) {
            for (let i = 0; i < errorSku.length; i++) {
              for (let y = 0; y < validImportArr.length; y++) {
                if (validImportArr[y].sku_id == errorSku[i].sku_id) {
                  errors.push({
                    'stt': validImportArr[y].Stt,
                    'field_name': 'Mã sản phẩm',
                    'error_value': 'Trùng mã sản phẩm với hệ thống'
                  })
                  break;
                }
              }
            }
            setOpenSubmit(false)
            setOpen(true);
          } else if (errorSku == undefined) {
            setOpen(true);
          }
        }
      }
    } else {
      for (let i = 0; i < dataUpdateImport.length; i) {
        let checkUID = dataUpdateImport.splice(i, 1);
        if (Object.keys(checkUID[0]).length !== 0) {
          const found = dataUpdateImport.some(el => el.uid === checkUID[0].uid);
          if (found == true) {
            errors.push({
              'stt': checkUID[0].Stt,
              'field_name': 'UID sản phẩm',
              'error_value': 'Trùng uid sản phẩm'
            })
            await validationFile(checkUID[0], checkUID[0].Stt);
            const idx = []
            for (let di in dataImport) {
              if (dataImport[di].uid === checkUID[0].uid) {
                let index = dataImport[di].Stt
                idx.push(index);
              }
            }
            for (var y = 0; y < idx.length; y++) {
              for (var d = 0; d < dataImport.length; d++) {
                if (dataImport[d].Stt == idx[y]) {
                  errors.push({
                    'stt': idx[y],
                    'field_name': 'UID sản phẩm',
                    'error_value': 'UID mã sản phẩm'
                  })
                  let objDuplicate = dataImport.splice(d, 1);
                  await validationFile(objDuplicate[0], objDuplicate[0].Stt);
                  break;
                }
              }
            }

          } else {
            let checkObj = await validationFile(checkUID[0], checkUID[0].Stt);
            if (checkObj.errorStatus == false) {
              checkUID[0]['created_at'] = new Date().getTime()
              validImportArr.push(checkUID[0])
            }
          }
        }
      }
      if (errors.length > 0) {
        console.log('errors', errors)
        setOpenSubmit(false)
        setOpen(true);
      } else {
        console.log('validImportArr', validImportArr)
        if (validImportArr.length > 0) {
          let uidArr = [];
          for (let obj of validImportArr) {
            for (let key in obj) {
              if (obj[key] == '') {
                obj[key] = null
              }
            }
            uidArr.push(obj.uid);
          }
          formData.set("checkUID", JSON.stringify(uidArr))
          let availableUID = await checkUIDWithSystem(formData).then(res => res)

          if (availableUID.length < uidArr.length) {
            var difference = uidArr.filter(x => availableUID.indexOf(x) === -1)
            if (difference.length) {
              for (let i = 0; i < difference.length; i++) {
                for (let y = 0; y < validImportArr.length; y++) {
                  if (validImportArr[y].uid == difference[i]) {
                    errors.push({
                      'stt': validImportArr[y].Stt,
                      'field_name': 'UID sản phẩm',
                      'error_value': 'UID sản phẩm không tồn tại trong hệ thống'
                    })
                    break;
                  }
                }
              }
            }
            setOpenSubmit(false)
            setOpen(true);
          } else {
            updateProduct(validImportArr)
            // formData.set("checkBrandShopUID", JSON.stringify(validImportArr))
            // let errBrandShopUID = await checkBrandShopUid(formData).then(res => res)
            // if (errBrandShopUID?.length) {
            //   for (let i = 0; i < errBrandShopUID.length; i++) {
            //     for (let y = 0; y < validImportArr.length; y++) {
            //       if (validImportArr[y].uid == errBrandShopUID[i]) {
            //         delete validImportArr[y]['brand_shop.product'];
            //         break;
            //       }
            //     }
            //   }
            //   updateProduct(validImportArr)
            // } else {
            // updateProduct(validImportArr)
            // }
          }
        }
      }
    }
  }
  const preparePricingData = (obj) => {
    let cost_price_with_vat = (obj.cost_price_with_vat).toFixed() ?? 0
    let cost_price_without_vat = (obj.cost_price_without_vat).toFixed() ?? 0
    let listed_price_with_vat = (obj.listed_price_with_vat).toFixed() ?? 0
    let listed_price_without_vat = (obj.listed_price_without_vat).toFixed() ?? 0
    let price_with_vat = (obj.price_with_vat).toFixed() ?? 0
    let price_without_vat = (obj.price_without_vat).toFixed() ?? 0
    obj['product.pricing'] = {
      cost_price_with_vat: cost_price_with_vat,
      cost_price_without_vat: cost_price_without_vat,
      listed_price_with_vat: listed_price_with_vat,
      listed_price_without_vat: listed_price_without_vat,
      price_with_vat: price_with_vat,
      price_without_vat: price_without_vat,
      front_margin: obj.front_margin,
      "dgraph.type": "Pricing",
      uid: genUid("new_pricing")
    }
  }
  const checkSkuWithSystem = async (object) => {
    try {
      let res = await api({
        url: '/product/check_sku',
        method: 'post',
        timeout: 60000,
        data: object
      })
      if (res.status == 200) {
        // test for status you want, etc
        console.log('res.status', res.status)
      }
      // Don't forget to return something
      return res.data
    }
    catch (err) {
      console.error(err);
    }
  }
  const updateProduct = async (validImportArr) => {
    formData.set("dataUpdateValid", JSON.stringify(validImportArr))
    formData.set('file', fileOrigin);
    api.post('/product/importUpdate', formData).catch(error => console.log(error))
    setLoadingImport(false)
    setOpen(false);
    setSuccessImport(true)
  }
  const checkUIDWithSystem = async (object) => {
    try {
      let res = await api({
        url: '/product/check_uid',
        method: 'post',
        timeout: 60000,
        data: object
      })
      if (res.status == 200) {
        // test for status you want, etc
        console.log('res.status', res.status)
      }
      // Don't forget to return something
      return res.data
    }
    catch (err) {
      console.error(err);
    }
  }
  /* const checkBrandShopUid = async (arr) => {
    try {
      let res = await api({
        url: '/product/check/brandShop_uid',
        method: 'post',
        timeout: 60000,
        data: arr
      })
      if (res.status == 200) {
        // test for status you want, etc
        console.log('res.status', res.status)
      }
      // Don't forget to return something
      return res.data
    }
    catch (err) {
      console.error(err);
    }
  }
  const checkNodeEdge = async (arr) => {
    try {
      let res = await api({
        url: '/product/check/nodeEdge',
        method: 'post',
        timeout: 60000,
        data: arr
      })
      if (res.status == 200) {
        // test for status you want, etc
        console.log('res.status', res.status)
      }
      // Don't forget to return something
      return res.data
    }
    catch (err) {
      console.error(err);
    }
  } */
  const generateError = async (arr) => {
    for (let obj of arr) {
      for (let key in obj) {
        if (obj[key] != "" && key != 'stt') {
          errors.push({
            'stt': obj.stt,
            'field_name': key,
            'error_value': 'UID Không tồn tại'
          })
        }
      }
    }
  }
  const checkUIDEdge = async (arr, checkFileStatus) => {
    formData.set("dataNodeEdge", JSON.stringify(arr))
    try {
      api({
        url: '/product/check/nodeEdge',
        method: 'post',
        // timeout: 60000,
        data: formData
      }).then(async (checkEdge) => {
        if (checkEdge?.data?.length > 0) {
          await generateError(checkEdge.data)
          setOpenSubmit(false)
          setOpen(true);
        } else if (checkFileStatus == 'sku_id') {
          setData(arr)
          setLoadingImport(false)
          setOpenSubmit(true);
        } else {
          setDataUpdated(arr)
          setLoadingImport(false)
          setOpenSubmit(true);
        }
      })
      // if (res.status == 200) {
      //   // test for status you want, etc
      //   console.log('res.status', res.status)
      // }
      // // Don't forget to return something
      // return res.data
    }
    catch (err) {
      console.error(err);
    }
    // await checkNodeEdge(formData)
  }
  function goBack () {
    history.replace('/app/product')
  }
  const handleClose = () => {
    setOpen(false);
    goBack()
  };
  const handleSubmitClose = () => {
    setOpenSubmit(false);
    goBack()
  };

  // VIEW
  return (
    (dataDialog !== null) ? <div >
      <div className="upload-btn-wrapper">
        <Modal
          open={openSubmit}
          onClose={handleSubmitClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          <div style={modalStyle} className={classge.paper}>
            <h2 id="simple-modal-title">Upload file thành công</h2>
            <p id="simple-modal-description">
              File đã được tải lên thành công. Bấm tiếp tục để kiểm tra và nhập dữ liệu vào Hệ thống.
            </p>
            <div style={{ textAlign: "right" }}>
              <Button onClick={handleSubmitClose} color="primary" autoFocus>
                Huỷ
              </Button>
              <Button className="upload-btn" onClick={submitData}>
                Tiếp Tục
              </Button>
            </div>
          </div>

        </Modal>
      </div>
      {/* <OutTable data={dataExcel.rows} columns={dataExcel.rows} /> */}
      <MaterialTable
        title={<h2 className={classge.titleTable}>Import Product</h2>}
        columns={column}
        data={dataDialog}
        options={{
          pageSize: 10,
          pageSizeOptions: [10, 25, 50]
        }}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve();
              }, 1000)
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setData([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
      />
      <Grid className={classge.btnSubmit}>
        <div className="return-btn-wrapper">
          <Button
            color="secondary"
            aria-label="add"
            variant="contained"
            className={classge.returnBtn}
            onClick={goBack}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>
        </div>
        <div className="upload-btn-wrapper">
          <Button
            color="primary"
            aria-label="add"
            variant="contained"
            className={classge.btnData}
            onChange={fileHandler}
          >
            <UploadIcon className={classge.iconback} />
            Upload file
          </Button>
          <input type="file" onChange={fileHandler} accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
        </div>
      </Grid>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div style={modalStyle} className={classge.formError}>
          <h2 id="simple-modal-title"></h2>
          <p id="simple-modal-description">
            <MaterialTable
              title={<h2 className={classge.titleTable}>Dữ liệu nhập lỗi</h2>}
              columns={columns}
              data={errors}
              options={{
                pageSize: 10,
                pageSizeOptions: [10, 25, 50],
                search: false
              }}
            />
          </p>
          <a style={{ color: "gray" }} href={`${api_url}/product/import_error_export.xlsx?export=` + JSON.stringify(errors)} target="_blank" tooltip="Export Here" rel="noreferrer">
            <Button className="upload-btn" >
              <GetAppIcon />
              Tải xuống
            </Button>
          </a>
          <Button className="upload-btn" style={{ left: 550 }} onClick={handleClose}>
            <DoneIcon />
            Đóng
          </Button>
        </div>
      </Modal>
      <Modal
        open={successImport}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div style={modalStyle} className={classge.paper}>
          <h2 id="simple-modal-title">Nhập dữ liệu thành công</h2>
          <p id="simple-modal-description">
          </p>
          <Button className="upload-btn" style={{ left: 250 }} onClick={handleClose}>
            Tiếp tục
          </Button>
        </div>
      </Modal>
      <Modal
        open={loadingImport}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div style={modalStyle} className={classge.paper}>
          <h2 id="simple-modal-title"></h2>
          <p id="simple-modal-description">
            Loading....
          </p>
        </div>
      </Modal>

    </div> : <Loading />
  );
}
