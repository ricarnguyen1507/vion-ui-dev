import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import TableProduct from './table'
import Edit from './edit'
import Loading from "components/Loading"
import ImportExcel from "./import_excel"
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
import { Shared } from 'context/Shared'
// import { useUserState } from "context/UserContext";

function fetchData (url) {
  if(url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }
}
const display_status = ['PENDING', 'REJECTED', 'APPROVED', 'IMPORT', 'CONVERTED']

export default function () {
  // const userData = useUserState();
  const [sharedData] = useState(() => new Map())
  const [loadingText, setLoadingText] = useState()
  const [loadingCode, setLoadingCode] = useState()
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))
  const history = useHistory()
  const {path} = useRouteMatch()


  const [dataTable, setDataTable] = useState([]);

  const [dataEdit, setDataEdit] = useState(null)
  const [actionType, setActionType] = useState('')
  // const [indexEdit, setIndexEdit] = useState(null)

  const [brands, setBrands] = useState(null)
  const [companies, setCompanies] = useState(null)
  // const [collections, setCollections] = useState(null)
  const [subCollections, setSubCollections] = useState(null)
  const [types, setTypes] = useState(null)
  const [platforms, setPlatforms] = useState(null)
  const [icons, setIcons] = useState(null)
  const [producttypes, setProductTypes] = useState(null)
  const [partners, setPartners] = useState(null)
  const [uoms, setUoms] = useState(null)
  const [manufacturer, setManufacturer] = useState(null)
  const [areas, setAreas] = useState(null)
  const [tags, setTags] = useState(null)
  const [otts, setOtts] = useState(null)
  const [brandShops, setBrandShop] = useState([])
  const [specsTypes, setSpecsTypes] = useState([])
  const [SpecsSectionOpt, setSpecsSectionOpt] = useState([
    {
      section_value: null,
      section_name: null,
      section_type: null,
      section_ref: null,
      section_limit: 20,
      display_order: 1
    }
  ])

  const generateSpecsSectionOpt = (list) => {
    if (list.length) {
      const opt = list.map((l, idx) => ({
        section_value: l.uid || null,
        section_name: l.specs_type_name || "",
        section_ref: l.uid || null,
        display_order: idx
      }))

      setSpecsSectionOpt(opt)
    }
  }


  const [ctx] = useState(() => ({
    goBack () {
      setBrandShop([])
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if(row) {
        setDataEdit(row)
      }
      if (row && row['brand_shop']) {
        setBrandShop(row['brand_shop'])
      }
    },
    import () {
      history.push(`${path}/import`)
      setActionType('import')
    }
  }))
  // const functionEditData = (actionType, row, index) => {
  //   setActionType(actionType)
  //   setDataEdit(row)
  //   // setIndexEdit(index)
  //   setMode("edit")
  // }
  const [group_payments, setGroupPayment] = useState(null)
  useEffect(() => {
    const fetchCollection = [
      {url: "/list/collection?is_parent=false&t=0&option_fields=true", method: "get"},
      {url: "/list/collection?is_parent=false&t=200&option_fields=true", method: "get"},
      {url: "/list/collection?is_parent=false&t=100&option_fields=true", method: "get"},
      {url: "/list/brand", method: "get"},
      {url: "/list/partner", method: "get"},
      {url: "/list/manufacturer", method: "get"},
      {url: "/list/uom", method: "get"},
      {url: "/list/product_type", method: "get"},
      {url: "/list/areas", method: "get"},
      {url: "/list/group_payment", method: "get"},
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchCollection).then(([
      collections_0,
      collections_200,
      collections_100,
      brands,
      partners,
      manufacturer,
      uoms,
      producttypes,
      areas,
      group_payments,
    ]) => {
      setSubCollections(collections_0)
      setTypes(collections_200)
      setPlatforms(collections_100)
      setBrands(brands)
      setPartners(partners)
      setManufacturer(manufacturer)
      setUoms(uoms)
      setProductTypes(producttypes)
      setAreas(areas)
      setGroupPayment(group_payments)
      setLoadingText("")

    }).catch(err => {
      setLoadingText("Lỗi: Không thể kết nối đến server API")
      setLoadingCode(2)
      console.log("can not connect api server", err)
    })
  }, []);

  const functionBack = () => {
    history.push(`${path}`)
  }
  const handleDelete = (row) =>
    api.delete(`/product/${row.uid}`)
  /* .then(res => {
                const idx = products.indexOf(row)
                products.splice(idx, 1)
                setProducts([...products])
            })
            .catch(error => console.log(error)) */


  async function handleSubmitData (submitData) {
    const formData = new FormData()
    formData.set("delEdges", JSON.stringify(submitData.delEdges))
    formData.set("delRecords", JSON.stringify(submitData.delRecords))
    formData.set("data", JSON.stringify(submitData.data))
    formData.set("details", JSON.stringify(submitData.details))

    for(let field in submitData.images) {
      if (field.startsWith('previews|square')) {
        formData.set(field, submitData.images[field])
      } else {
        formData.set(field, submitData.images[field])
      }
    }
    const isNew = submitData.uid.startsWith('_:')
    try {
      setLoadingText('0%')
      await api({
        method: 'post',
        url: '/product/' + (isNew ? 'new' : submitData.uid),
        data: formData,
        onUploadProgress (e) {
          setLoadingText(`${((e.loaded * 100) / e.total).toFixed()}%`)
          // TODO: show phần trăm (lấy giá trị của state loadingText) upload lên giao diện upload loading
        }
      })
      // Notice: Chỉ back lại nếu upload thành công
      setLoadingText("")
      // sendTracking({pages: 'product', dataOrigin: dataEdit, dataEdit: submitData, actionType, userData})
      ctx.goBack()
    } catch (err) {
      // TODO: cần thêm show popup khi có error không upload được, thay vì console.log
      setLoadingCode(1)
      setLoadingText('Lỗi: Upload không thành công')
      console.log(err)
      functionBack()
    }
  }

  if(loadingText) {
    return (
      <Loading statusCode={loadingCode} statusText={loadingText}>
        { loadingCode === 1 && <button onClick={() => { setLoadingText("") }}>Trở lại</button> }
        { loadingCode === 2 && <button onClick={functionBack}>Trở lại</button> }
      </Loading>
    )
  }
  // Display view

  return(
    <Shared value={sharedData}>
      {actionType == 'import' ?
        <Route path={`${path}/import`}>
          <ImportExcel
            functionBack={functionBack}
          />
        </Route>
        :
        <Route path={`${path}/:actionType`}>
          <Edit
            actionType={actionType}
            originData={dataEdit}
            subCollections={subCollections}
            setSubCollections={setSubCollections}
            platforms={platforms}
            setPlatforms={setPlatforms}
            types={types}
            companies={companies}
            brands={brands}
            producttypes={producttypes}
            partners={partners}
            uoms={uoms}
            manufacturer={manufacturer}
            displayStatus={display_status}
            areas={areas}
            group_payments={group_payments}
            functionBack={functionBack}
            handleSubmitData={handleSubmitData}
            brandShops={brandShops}
            setBrandShop={setBrandShop}
            specsTypes={specsTypes}
          />
        </Route>
      }

      <Route exact path={path}>
        <TableProduct
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          handleDelete={handleDelete}
          displayStatus={display_status}
          dataTable={dataTable}
          setDataTable={setDataTable}
          setActionType={setActionType}
          setBrandShop={setBrandShop}
          ctx={ctx}
        />
      </Route>
    </Shared>
  )
}
