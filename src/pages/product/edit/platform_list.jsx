import React from 'react'
import Checkbox from '@material-ui/core/Checkbox'
import useStyles from './edit_style'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function CollectionList ({items, checkedState }) {
  const classes = useStyles()
  function handleChange (e, item) {
    (checkedState[item.uid] || (checkedState[item.uid] = [false, false]))[1] = e.target.checked
  }
  function isChecked (item) {
    return checkedState[item.uid] ? checkedState[item.uid][1] : false
  }
  return (
    <FormGroup className={classes.listItemCate} >
      {
        items?.map((item) => (
          <FormControlLabel
            className={classes.listItemLabel}
            key={item.uid}
            control={
              <Checkbox
                color="primary"
                defaultChecked={isChecked(item)}
                onChange={ e => { handleChange(e, item) } } />
            }
            label={item.collection_name}
          />
        ))
      }
    </FormGroup>
  )
}