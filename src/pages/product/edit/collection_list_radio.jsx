import React, { useState } from 'react'
import Radio from '@material-ui/core/Radio'
import useStyles from './edit_style'
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default function RadioBoxes ({ node, pred, listData = [] }) {
  const classes = useStyles()
  // const [checkedItems] = useState(() => node.getEdge(pred, []))
  const [checkedItems, setCheckedItem] = useState(() => node.getState(pred)?.map(item => item.state.uid) ?? [])
  const [allItems] = useState(() => {
    node.setState(pred, listData, false)
    return node.getState(pred) ?? []
  })
  function isChecked (item) {
    return !!checkedItems?.find(ci => ci === item.state.uid)
  }
  function handleChange (e, item) {
    if (checkedItems?.length && isChecked(item) === false) {
      allItems.map(ai => checkedItems?.find(ci => {
        if (ci == ai.state.uid) {
          ai.isDeleted = true
        }
      }))
      item.selected = e.target.checked
      setCheckedItem([item.state.uid])
    } else {
      item.selected = e.target.checked
      setCheckedItem([item.state.uid])
    }
    console.log('allItems', allItems)
  }

  return (
    <RadioGroup className={classes.listItemCate} >
      {
        allItems?.map((item) => (
          <FormControlLabel
            value={item?.state?.uid}
            className={classes.listItemLabel}
            key={item?.state?.uid}
            control={
              <Radio
                color="primary"
                onChange={e => { handleChange(e, item) }}
                checked={checkedItems?.find(ci => ci === item?.state?.uid)}
                value={item?.state?.uid}
              />
            }
            label={item?.state?.nodeData?.collection_name?.state}
          />
        ))
      }
    </RadioGroup>
  )
}