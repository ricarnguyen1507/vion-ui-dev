import React, { useState } from 'react'
import Grid from "@material-ui/core/Grid"
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Media from "components/Media/index"
import { genCdnUrl } from 'components/CdnImage'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

export default function Preview ({ idx, uid, preview, fileChange, onDelete }) {

  const fileHandle = (field, file) => {
    if(field.startsWith('square')) {
      fileChange(`previews|${field}`, file)
    } else{
      fileChange(`previews|${field}|${preview.uid}|${preview[field] || ""}`, file, preview.uid)
    }

  }

  const [square, setSquare] = useState(() => {
    if (preview.square !== false && preview.square !== true) {
      fileHandle(`square|${preview.uid}`, true) //default square
      return true
    } else {
      return preview.square
    }
  })

  const handleChangeCheckBox = ({ target: { name, checked } }) => {
    setSquare(checked)
    fileHandle(`${name}|${preview.uid}`, checked)
  }

  /* const handleChangeVideoTranScode = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    fileHandle(`video_transcode|${preview.uid}`, value)
  } */
  return (
    <Grid item>
      <Paper style={{ margin: 5, padding: 5, textAlign: 'center' }}>
        <Card style={{ marginBottom: 5 }}>
          <CardMedia
            image={"1920x960.png"}
            children={
              <Media
                src={genCdnUrl(uid && preview.source, "", preview.media_type)}
                mediaType={preview.media_type}
                fileHandle={fileHandle} style={{ width: 100, height: 100 }}
                field={`source`} accept="image/*, video/*"
              />
            }
          />
        </Card>
        <Card>
          <CardMedia
            image={"400x400.png"}
            children={
              <Media
                src={genCdnUrl(uid && preview.thumb, "")}
                mediaType={"image"}
                fileHandle={fileHandle} style={{ width: 100, height: 100 }}
                field={`thumb`} accept="image/*"
              />
            }
          />
        </Card>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} lg={12}>
            <FormControlLabel
              control={
                <Checkbox
                  className="checkbox"
                  id="instock"
                  name="square"
                  color="primary"
                  defaultChecked={square || false}
                  onChange={handleChangeCheckBox} />
              }
              label="square"
            />
          </Grid>
        </Grid>

        <IconButton onClick={() => onDelete(preview.uid, idx)}><DeleteIcon /></IconButton>
      </Paper>
    </Grid>
  )
}
