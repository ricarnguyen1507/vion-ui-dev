import React, { useState, useMemo, useContext } from 'react'
import {
  useHistory
} from "react-router-dom"
import useStyles from './edit_style'
import useStylesGeneral from 'pages/style_general'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import RoleButton from 'components/Role/Button'
import ReturnIcon from "@material-ui/icons/KeyboardReturn"
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Divider from '@material-ui/core/Divider'
import { genUid } from "utils"
import AddDetail from './add_details'
// import { clone, conpare_diff_arr_obj } from 'services/diff'
import ColorPicker from './color_product'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import ExportExcel from "../export_excel"
// import DateFnsUtils from '@date-io/date-fns';
import { context } from 'context/Shared'
import { InputEdge, Node, OptionEdge, CheckBoxEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"
// import Radio from '@material-ui/core/Radio'
// import RadioGroup from '@material-ui/core/RadioGroup';
// import {
//   MuiPickersUtilsProvider,
//   KeyboardDatePicker,
// } from '@material-ui/pickers';

import api from 'services/api_cms'

const LIST_NUMBER_DESC = [1, 2, 3, 4]
// import { LoadingContext } from "context/LoadingContext";

export default function ({ subCollections, platforms, brands, icons, setIcons, producttypes, partners, uoms, manufacturer, functionBack, displayStatus, areas, tags, group_payments, brandShops, setBrandShop, otts }) {
  // const { setIsLoading } = React.useContext(LoadingContext);
  const history = useHistory()
  const classes = useStyles()
  const classge = useStylesGeneral();


  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_product",
    "dgraph.type": "Product"
  })
  delete dataEdit["product.specs_sections|display_order"]
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  // const checkDecimal = /^[-+]?[0-9]+\.[0-9]+$/
  const [node] = useState(() => new Node(dataEdit))
  const [pricingNode] = useState(() => node.getEdge("product.pricing", { uid: "_:new_pricing", "dgraph.type": "Pricing" }).state)
  const [priceValidator] = useState(() => ({
    validators: [
      'isNumber'
    ],
    errorMessages: [
      'Price phai la so nguyen duong'
    ]
  }))
  const [files] = useState(() => ({}))

  const [shortDescriptionList, setShortDescriptionList] = useState(() => {
    const result = []
    LIST_NUMBER_DESC.forEach(index => {
      if (dataEdit?.[`short_description_${index}`])
        result.push(dataEdit?.[`short_description_${index}`])
    })

    if (dataEdit?.short_descriptions)
      result.push(dataEdit?.short_descriptions)
    return result
  });
  const [color, setColor] = useState(dataEdit?.color || "#f48024")

  const handleColor = (color) => {
    setColor(color.hex)
  }
  // Tạo mới 1 details
  const [details, setDetails] = useState(dataEdit?.details ? [dataEdit?.details] : [])
  const [updated] = useState({
    values: {
      'display_status': dataEdit?.display_status,
      'autoplay_video': dataEdit?.autoplay_video,
      'description': dataEdit?.description || "",
      'description_html': dataEdit?.description_html || "",
      'use_time': "",
    },
    'product.tag': [],
    details: {},
    previews: [],
    hasFile: [],
    video_transcodes: []
  })

  // On-off dialog ( detail + icon)
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // =========================================================================
  // Handle Details
  // =========================================================================
  // Tạo + cập nhật details
  const addDetail = (title) => {
    if (details.length >= 1) {
      console.log("Chúng ta chỉ dùng 1 cấp mô tả chi tiết")
    } else {
      try {
        const newDetail = { uid: genUid('detail'), title, content: '' }
        setDetails([newDetail, ...details])
        updated.details[newDetail.uid] = newDetail
        node.getEdge("details", { uid: "_:new_detail", "dgraph.type": "Detail", title })
        handleClose()
      }
      catch (error) {
        console.log(error)
      }
    }

  }
  // Update nội dung cơ bản - ko chọn icon
  const updateDetail = function (uid, field, value) {
    if (updated.details[uid]) {
      updated.details[uid][field] = value
    } else {
      updated.details[uid] = { uid, [field]: value }
    }
  }
  // Delete details
  const deleteDetail = (uid, idx) => {
    if (!uid.startsWith("_:")) {
      updated.delEdges["details"].push({ uid })
      updated.delRecords.push({ uid })
    }
    if (updated.details[uid]) {
      delete updated.details[uid]
    }
    details.splice(idx, 1)
    setDetails([...details])
  }
  // function handleSubmit() {}
  function change_alias (str) {
    return !str ? '' : str.normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd').replace(/Đ/g, 'D');
  }
  // Handle submit form
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/product')
  }
  function syncSpecsValue (product_uid) {
    api.get("/specs-type/update-specs-values/" + product_uid)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  function handleSubmit () {
    let images = {
      ...updated.previews,
      ...updated.video_transcodes
    }

    let availablePreview = {}
    if(Object.keys(images)?.length) {
      if(updated.hasFile?.length) {
        for(let obj of updated.hasFile) {
          availablePreview[obj.image_name] = images[obj.image_name]
          let square = ''
          let keyCut = ''
          if(obj.image_name.includes("source")) {
            square = obj.image_name.replace("source", "square")
            keyCut = square.slice(-1)
            if(keyCut == '|') {
              square = square.slice(0, -1)
            }
            availablePreview[square] = images[square]
          } else if(obj.image_name.includes("thumb")) {
            square = obj.image_name.replace("thumb", "square")
            keyCut = square.slice(-1)
            if(keyCut == '|') {
              square = square.slice(0, -1)
            }
            availablePreview[square] = images[square]
          }
        }
      }
    }

    let brand_name = node.getState('product.brand') || ''
    if (brand_name) {
      brand_name = brand_name?.nodeData?.brand_name?.state
    }

    let fullTextSearch = [
      node.getState('product_name'), change_alias(node.getState('product_name')),
      node.getState('display_name'), change_alias(node.getState('display_name')),
      node.getState('display_name_detail'), change_alias(node.getState('display_name_detail')),
      node.getState('sku_id'), change_alias(node.getState('sku_id')),
      node.getState('uid'), change_alias(node.getState('uid'))
    ].join(' ')
    node.setState('fulltext_search', fullTextSearch)

    const formData = ObjectsToForm(node.getMutationObj(), files, availablePreview)

    api.post("/product/" + dataEdit.uid, formData)
      .then((response) => {
        /** Gọi thêm 1 bước này nữa, rồi mới goBack() */
        syncSpecsValue(response.data.uid)
      })
      .catch(err => {
        console.log(err)
      })
  }


  const handleChangeDisplayStatus = (e, item) => {
    setDataEdit({ ...dataEdit, display_status: displayStatus.indexOf(item)})
    node.setState('display_status', displayStatus.indexOf(item), true)
  }

  const shortListDescriptionValue = {}

  LIST_NUMBER_DESC.forEach(index => {
    shortListDescriptionValue[index] = useMemo(() => {
      if (!dataEdit?.[`short_description_${index}`]?.uid) {
        return null
      }
      return shortDescriptionList?.find(item => item.uid === dataEdit?.[`short_description_${index}`]?.uid) ?? null
    }, [dataEdit, shortDescriptionList])
  })
  const [timer, setTimer] = useState(null)

  const onInputShortDescriptionChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { func: `func: anyofterms(display_name, "${val.replace(/["\\]/g, '\\$&').trim()}")` }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchShortDescriptionOption(queries)
    }, 550))
  }
  function fetchShortDescriptionOption (queries) {
    if (queries !== "") {
      api.post(`/short-description-list`, queries)
        .then(res => {
          const newOption = shortDescriptionList
          res.data.data.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setShortDescriptionList([...newOption])
        })
    }
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classes.iconProduct} /> : <EditIcon className={classes.iconProduct} />
  return <>
    <PageTitle title="Product" />
    <AddDetail
      open={open}
      handleClose={handleClose}
      addDetail={addDetail}
    />
    <ValidatorForm autocomplete="off" onSubmit={handleSubmit} className={classes.root}>
      <Grid container spacing={2} >
        <Grid item xs={12} sm={8} lg={8}>
          <div className={classes.rootBasic}>
            <div className={classes.titleProduct}>
              <Typography className={classes.title} variant="h3">
                Tình trạng tồn kho
              </Typography>
            </div>
            <Grid container className={classes.rootListCate} spacing={2}>
              <Grid item xs={12} sm={12} lg={12} className={classes.listCate}>
                <Grid container className={classes.rootListCate} spacing={2}>
                  <Grid item xs={12} sm={4} lg={4} className={classes.listCate}>
                    <FormControlLabel
                      control={
                        <CheckBoxEdge
                          Component={Checkbox}
                          node={node}
                          pred={"instock"}
                          fullWidth
                          variant="outlined"
                          margin="dense"
                          color="primary"
                        />
                      }
                      label="Còn hàng"
                    />
                  </Grid>
                  <Grid item xs={12} sm={8} lg={8} className={classes.listCate}>
                    <InputEdge
                      Component={TextField}
                      node={node}
                      pred={"stock"}
                      fullWidth
                      label="Số lượng tồn kho"
                      variant="outlined"
                      margin="dense"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </div>
          <div className={classes.rootBasic}>
            <div className={classes.titleProduct}>
              <div className={classes.containIcon}>
                {ActionIcon}
              </div>
              <Typography className={classes.title} variant="h3" >
                Thông tin cơ bản
              </Typography>
              <div className={classes.colorPicker}>
                <ColorPicker
                  color={color}
                  handleColor={handleColor}
                />
              </div>
            </div>
            <Divider />
            <Grid container spacing={2} className={classes.formpd}>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"product_name"}
                  fullWidth
                  label="Tên Sản Phẩm (*)"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'required'
                  ]}
                  errorMessages={[
                    'This field is required'
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"display_bill_name"}
                  fullWidth
                  label="Tên trên hoá đơn (*)"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'required'
                  ]}
                  errorMessages={[
                    'This field is required'
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"display_name"}
                  fullWidth
                  label="Tên hiển thị (display name)"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"display_name_detail"}
                  fullWidth
                  label="Tên hiển thị chi tiết (display name detail)"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'required'

                  ]}
                  errorMessages={[
                    'This field is required'
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"barcode"}
                  fullWidth
                  label="Barcode Sản Phẩm"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"unit"}
                  fullWidth
                  label="Đơn vị tính"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'maxStringLength:500',
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    'Max length is 500',
                    // 'Character is not accept '
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={6} lg={6}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"sub_unit"}
                  fullWidth
                  label="Đơn vị con"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'maxStringLength:500',
                  ]}
                  errorMessages={[
                    'Max length is 500',
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={6} lg={6}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"sub_unit_quantity"}
                  fullWidth
                  label="Số lượng đơn vị con"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={6} lg={6}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"packaging_unit"}
                  fullWidth
                  label="Quy cách đóng gói"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'maxStringLength:50'
                  ]}
                  errorMessages={[
                    'Max length is 50'
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={6} lg={6}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"packaging_unit_quantity"}
                  fullWidth
                  label="Số lượng đơn vị trong package"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'maxStringLength:50'
                  ]}
                  errorMessages={[
                    'Max length is 50'
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"promotion_desc"}
                  fullWidth
                  label="Chương trình Khuyến Mãi"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"promotion_detail"}
                  fullWidth
                  label="Chi tiết Khuyến Mãi"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"return_terms"}
                  fullWidth
                  label="Chính sách đổi trả"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  // {...(validateForm['warranty_policy'] || {})}
                  Component={TextField}
                  node={node}
                  pred={"warranty_policy"}
                  fullWidth
                  label="Chính sách Bảo hành (60 ký tự)"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'required',
                    'maxStringLength:60'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Max length is 60'
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextField}
                  node={node}
                  pred={"shelf_life"}
                  fullWidth
                  label="Thời hạn sử dụng"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
            </Grid>
          </div>
          <div className={classes.rootBasic}>
            <div className={classes.titleProduct}>
              <div className={classes.containIcon}>
                {ActionIcon}
              </div>
              <Typography className={classes.title} variant="h3" >
                Giá
              </Typography>
            </div>
            <Divider />
            <Grid container spacing={2} className={classes.formpd}>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  {...priceValidator}
                  type="number"
                  node={node}
                  pred={"price"}
                  fullWidth
                  label="Giá mua từ NCC (VAT)"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  {...priceValidator}
                  type="number"
                  node={node}
                  pred={"cost_price"}
                  fullWidth
                  label="Giá bán (VAT)"
                  variant="outlined"
                  margin="dense"
                />
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item xs={12} sm={4} lg={4}>
          <div className={classes.rootBasic}>
            <div className={classes.titleProduct}>
              <div className={classes.containIcon}>
                {ActionIcon}
              </div>
              <Typography className={classes.title} variant="h3" >
                Các liên kết
              </Typography>
            </div>
            <Divider />
            <Grid container spacing={2} className={classes.formpd}>
              <Grid item xs={12} sm={12} lg={12} >
                <OptionEdge
                  node={node}
                  pred={"product.manufacturer"}
                  options={manufacturer || []}
                  getOptionLabel={option => option?.manufacturer_name || ""}
                  style={{ width: "100%" }}
                  renderInput={params => (
                    <TextField {...params}
                      label="Nhà Sản Xuất"
                      fullWidth
                      variant="outlined"
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12} >
                <OptionEdge
                  node={node}
                  pred={"product.brand"}
                  options={brands || []}
                  getOptionLabel={option => option?.brand_name || ""}
                  style={{ width: "100%" }}
                  renderInput={params => (
                    <TextField {...params}
                      label="Brand Của Nhà SX"
                      fullWidth
                      variant="outlined"
                    />
                  )}
                />
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
      <div className={classes.rootBasic}>
        <div className={classes.titleProduct}>
          <Typography className={classes.title} variant="h3">
            Xét duyệt sản phẩm
          </Typography>
        </div>
        <Grid container className={classes.rootListCate} spacing={2}>
          <Grid item xs={12} sm={12} lg={12} className={classes.listCate}>
            <Autocomplete
              options={displayStatus || []}
              value={displayStatus[dataEdit?.display_status]}
              onChange={handleChangeDisplayStatus}
              style={{ width: "100%" }}
              renderInput={params => (
                <TextField {...params}
                  required
                  label="Trạng thái hiển thị"
                  variant="outlined"
                  fullWidth
                  margin="dense"
                  validators={[
                    'required'
                  ]}
                  errorMessages={[
                    'Please select display status'
                  ]}
                />
              )}
            />
          </Grid>
        </Grid>
      </div>

      {/* Button submit */}
      <Grid className={classes.btnSubmit}>
        <Button
          color="secondary"
          aria-label="add"
          variant="contained"
          className={classes.btnData}
          onClick={functionBack}
        >
          <ReturnIcon className={classge.iconback} />
          Return
        </Button>
        <RoleButton actionType={actionType} />
        {actionType === "Edit" &&
          <ExportExcel data={[dataEdit]} isEditTable={true} />
        }
      </Grid>
    </ValidatorForm>
  </>
}
