import React from 'react'
import Grid from "@material-ui/core/Grid"
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'

import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Media from "components/Media/index"
import {genCdnUrl} from 'components/CdnImage'
// import Checkbox from '@material-ui/core/Checkbox'
// import FormControlLabel from '@material-ui/core/FormControlLabel'
import TextField from '@material-ui/core/TextField'

export default function Preview ({idx, uid, videoTranscode, fileChange, onDelete}) {

  const fileHandle = (field, file) => {
    fileChange(`video_transcodes|${field}|${videoTranscode.uid}|${videoTranscode[field] || ""}`, file)
  }

  const handleChangeVideoTranScode = (e) => {
    e.preventDefault()
    const { value } = e.target
    fileHandle(`source|${videoTranscode.uid}`, value)
  }
  return (
    <Grid item xs={12} sm={3} lg={3}>
      <Paper style={{margin: 5, padding: 5, textAlign: 'center'}}>
        <TextField
          style={{marginTop: 20}}
          id="source"
          name="source"
          label="Video Transcode"
          variant="outlined"
          fullWidth
          onChange={handleChangeVideoTranScode}
          defaultValue={videoTranscode.source}
        />
        <Card>
          <CardMedia
            image={"1920x960.png"}
            children={
              <Media
                src={genCdnUrl(uid && videoTranscode.thumb, "")}
                mediaType={"image"}
                fileHandle={fileHandle} style={{ width: 400, height: 200}}
                field={`thumb`} accept="image/*"
              />
            }
          />
        </Card>
        <IconButton onClick={() => onDelete(videoTranscode.uid, idx)}><DeleteIcon /></IconButton>
      </Paper>
    </Grid>
  )
}
