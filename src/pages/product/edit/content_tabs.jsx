import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete'
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import RichTextEditor from 'react-rte';
import DialogListIcons from "components/DialogListIcons"
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import { genCdnUrl } from "components/CdnImage"

const toolbarConfig = {
  display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
  INLINE_STYLE_BUTTONS: [
    { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
    { label: 'Italic', style: 'ITALIC' },
    { label: 'Underline', style: 'UNDERLINE' }
  ],
  BLOCK_TYPE_DROPDOWN: [
    { label: 'Normal', style: 'unstyled' },
    { label: 'Heading Large', style: 'header-one' },
    { label: 'Heading Medium', style: 'header-two' },
    { label: 'Heading Small', style: 'header-three' }
  ],
  BLOCK_TYPE_BUTTONS: [
    { label: 'UL', style: 'unordered-list-item' },
    { label: 'OL', style: 'ordered-list-item' }
  ]
}

const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  richTextEditor: {
    width: "100%",
    minHeight: 150,
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  cardImgIcon: {
    maxWidth: 50,
    maxHeight: 50,
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
  media: {
    height: 50,
    width: 50,
  }
});


// Render
export default function ({ icons, setIcons, detail, idx, updateDetail, onDelete }) {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  const [iconImage, setIconImage] = useState(genCdnUrl(detail["detail.icon"], '200x200.png'))
  function showDialogIcons (e) {
    e.stopPropagation()
    setOpen(true)
  }
  function selectIcon (icon) {
    if(icon && icon !== null)
    {
      setIconImage(icon.base64 ? icon.source : genCdnUrl(icon.source))
      updateDetail(detail.uid, "detail.icon", { uid: icon.uid })
    }
    setOpen(false)
  }
  // -------------------------------------------------------------------------
  // Handle Richtext
  // -------------------------------------------------------------------------
  const [content, setContent] = useState(RichTextEditor.createValueFromString(detail.content || "", 'html'))
  const onChange = (value) => {
    setContent(value)
    updateDetail(detail.uid, "content", value.toString('html'))
  }

  return (
    <>
      <Accordion className={classes.rootDetail}>
        <AccordionSummary
          // expandIcon={<ExpandMoreIcon />}
          aria-label="Expand"
          aria-controls="additional-actions1-content"
          id="additional-actions1-header"
        >
          <Grid container spacing={2} >
            <Grid item xs={12} sm={6} lg={6} className={classes.rootImgIcon}>
              {/* Hiển thị icon hình ảnh  */}
              <Card className={classes.cardImgIcon} onClick={showDialogIcons}>
                <CardMedia
                  className={classes.media}
                  image={iconImage}
                  title="Contemplative Reptile"
                />
              </Card>
              {/* -------------------------- */}
              <Typography variant="h6" className={classes.titleImgIcon}>
                {detail.title}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6} lg={6} >
              <IconButton className={classes.btnDelete} onClick={() => onDelete(detail.uid, idx)}>
                <DeleteIcon />
              </IconButton>
            </Grid>
          </Grid>
        </AccordionSummary>

        {/* Nội dung chính  */}
        <AccordionDetails>
          <RichTextEditor
            className={classes.richTextEditor}
            toolbarConfig={toolbarConfig}
            value={content}
            onChange={onChange} />
        </AccordionDetails>
      </Accordion>
      <DialogListIcons {...{icons, setIcons, open, setOpen, onSelect: selectIcon}} />
    </>
  );
}