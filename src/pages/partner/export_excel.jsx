import React from "react";
import useStyles from 'pages/style_general'
import ReactExport from "react-data-export";
import GetAppIcon from "@material-ui/icons/GetApp";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import { titleCase } from "services/titleCase";
import { changePropName } from "services/changePropName";
import { clone } from "services/diff";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ExportExcel ({ data, isEditTable }) {
  const classge = useStyles();

  const dataFields = [
    "account_name",
    "account_number",
    "address_str",
    "bank_branch_name",
    "bank_name",
    "contact_person",
    "contract_number",
    "contract_type",
    "email",
    "id",
    "partner__collection",
    "partner__pricing_back_margin",
    "partner__pricing_back_margin_terms",
    "partner__pricing_from_date",
    "partner__pricing_uid",
    "partner__pricing_updated_at",
    "partner_name",
    "payment_terms",
    "phone_number",
    "return_terms",
    "shipping_method",
    "tax_id",
    "uid"
  ]
  const handlePlainData = (obj, result = {}) => {
    for (let key in obj) {
      if (typeof obj[key] === "object") {
        if (key.indexOf(".") > 0) {
          const newKey = key.replace(".", "__");
          changePropName(obj, newKey, key);
          const linkedFields = combineElement(obj[newKey], newKey);
          delete obj[newKey];
          delete result[key];
          result = { ...obj, ...result, ...linkedFields };
        }
        else {
          const linkedFields = combineElement(obj[key], key);
          result = { ...obj, ...result, ...linkedFields };
        }
      }
    }
    return result;
  };

  const combineElement = (obj, keyName, objResult = {}) => {
    if (Array.isArray(obj)) {
      const uidList = obj.map((elm) => elm.uid);
      objResult[keyName] = uidList.toString();
    } else {
      for (let key in obj) {
        const dependKey = keyName.concat("_") + key;
        const newObj = changePropName(obj, dependKey, key);
        objResult = { ...newObj };
      }
    }
    return objResult;
  };

  const handleData = (arrayData, arrResult = []) => {
    if (arrayData !== null) {
      const cloneArr = clone(arrayData);
      cloneArr.forEach((item) => arrResult.push(handlePlainData(item)));
      return arrResult;
    }
  };

  const excelData = data && handleData(data);

  return (
    <ExcelFile
      element={
        isEditTable ?
          <Button
            variant="contained"
            aria-label="back"
            className={classge.btnExport}
          >
            <GetAppIcon className={classge.expIcon} />
          Export
          </Button>
          :
          <Tooltip title="Export data" aria-label="export data">
            <IconButton variant="contained">
              <GetAppIcon className={classge.expIcon} />
            </IconButton>
          </Tooltip>

      }
    >
      <ExcelSheet data={excelData} name="Partner">
        {excelData && excelData.length
          ? dataFields.map((item, idx) => (
            <ExcelColumn
              key={idx}
              label={`${titleCase(item)} <${item}>`}
              value={item}
            />
          ))
          : {}}
      </ExcelSheet>
    </ExcelFile>
  );
}
