import React, { useState, useContext } from 'react'
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AddBox from '@material-ui/icons/AddBox'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import TextField from "@material-ui/core/TextField"
import Autocomplete from '@material-ui/lab/Autocomplete'
// import ExportExcel from "./export_excel";
import { InputEdge, Node, OptionEdge } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'

export default function (props) {
  const { functionBack, collection, shippingMethod, warehouse } = props
  const history = useHistory()
  const classge = useStylesGeneral();

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_partner",
    "dgraph.type": "Partner",
    shipping_method: 0
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  // const checkDecimal = /^[-+]?[0-9]+\.[0-9]+$/
  const [node] = useState(() => new Node(dataEdit))
  const [pricingNode] = useState(() => node.getEdge("partner.pricing", { uid: "_:new_pricing", "dgraph.type": "Pricing" }).state)
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/partner')
  }
  function handleSubmit () {
    api.post("/partner", node.getMutationObj())
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const handleChangeShippingMethod = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'shipping_method': shippingMethod.indexOf(item) })
    node.setState('shipping_method', shippingMethod.indexOf(item), true)
  }
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Supplier" />
      <ValidatorForm className={classge.root} onSubmit={handleSubmit}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Thông tin
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"partner_name"}
                    fullWidth
                    label="Tên Nhà Cung Cấp (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"id"}
                    fullWidth
                    label="Mã Nhà Cung Cấp (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"contract_number"}
                    fullWidth
                    label="Số Hợp Đồng"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"contract_type"}
                    fullWidth
                    label="Hình thức Hợp Đồng"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"tax_id"}
                    fullWidth
                    label="Mã Số Thuế"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Liên Hệ
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"email"}
                    fullWidth
                    label="Email NCC (*)"
                    variant="outlined"
                    margin="dense"
                    validators={['isEmail']}
                    errorMessages={['email is not valid']}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"address_str"}
                    fullWidth
                    label="Địa chỉ"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"contact_person"}
                    fullWidth
                    label="Người Liên Hệ"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"phone_number"}
                    fullWidth
                    label="Số Điện Thoại"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Tài Khoản Ngân Hàng
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"bank_name"}
                    fullWidth
                    label="Tên Ngân Hàng"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"bank_branch_name"}
                    fullWidth
                    label="Tên Chi Nhánh"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"account_name"}
                    fullWidth
                    label="Tên Tài Khoản"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"account_number"}
                    fullWidth
                    label="Số Tài Khoản"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Lợi nhuận
                </Typography>
              </div>
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={pricingNode}
                    pred={"back_margin"}
                    fullWidth
                    label="Chiết Khấu Sau"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'matchRegexp:[0-9]'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Number only'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={pricingNode}
                    pred={"back_margin_terms"}
                    fullWidth
                    label="Thời gian thanh toán của Back Margin (Quý/Năm)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={pricingNode}
                    pred={"from_date"}
                    typeInput="date"
                    fullWidth
                    label="Hiệu lực Từ"
                    variant="outlined"
                    margin="dense"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextField}
                    node={pricingNode}
                    pred={"to_date"}
                    typeInput="date"
                    fullWidth
                    label="Hiệu lực Đến"
                    variant="outlined"
                    margin="dense"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Các điều khoản, và điều kiện
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"return_terms"}
                    fullWidth
                    label="Điều kiện Bảo Hành / Đổi trả"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"payment_terms"}
                    fullWidth
                    label="Điều khoản Thanh Toán"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"required_order_value"}
                    fullWidth
                    label="Điều kiện giá trị đơn hàng tối thiểu"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextField}
                    node={node}
                    pred={"shipping_time"}
                    fullWidth
                    label="Thời gian giao hàng dự kiến"
                    variant="outlined"
                    margin="dense"
                  />
                </Grid>
              </Grid>
            </div>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Phương Thức Giao Hàng
                </Typography>
              </div>
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    options={shippingMethod}
                    value={shippingMethod[dataEdit.shipping_method] || "FORWARDING"}
                    onChange={handleChangeShippingMethod}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Phương thức "
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                {dataEdit.shipping_method === 2 ?
                  <Grid item xs={12} sm={12} lg={12} >
                    <OptionEdge
                      multiple
                      limittags={2}
                      id="multiple-limit-tags"
                      node={node}
                      pred={"partner.warehouse"}
                      options={warehouse || []}
                      getOptionLabel={option => option?.name || ""}
                      style={{ width: "100%" }}
                      renderInput={params => (
                        <TextField {...params}
                          label="Chọn kho hàng"
                          variant="outlined"
                          fullWidth
                          margin="dense"
                        />
                      )}
                    />
                  </Grid> : ""}
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>
          <RoleButton actionType={actionType} />
          {/* {actionType === "Edit" &&
            <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
