import React, { useState, useEffect } from 'react';
import api from 'services/api_cms';
import TablePartner from './table'
import Edit from './edit';
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [actionType, setActionType] = useState('')
  const [dataEdit, setDataEdit] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  const [collection, setCollection] = useState([])
  const [warehouse, setWarehouse] = useState(false)
  const shippingMethod = ['FORWARDING', 'STOCKING', 'STOCK_AT_SUP']
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  /* const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  })) */
  // Load data
  useEffect(() => {
    const fetchs = [
      "/list/collection?t=0&is_temp=false&option_fields=true",
      "/list/warehouse-options"
    ].map(url => api.get(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([
      collections,
      ware_houses
    ]) => {
      setCollection(collections)
      setWarehouse(ware_houses)
    })
  }, []);

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row) => {
    setActionType(actionType)
    setDataEdit(row)
  }
  const handleDelete = (row) => api.post('/partner', { del: { uid: row.uid } })
    .then(res => {
      if (res.status === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  return(
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          actionType={actionType}
          originData={dataEdit}
          functionBack={functionBack}
          collection={collection}
          shippingMethod={shippingMethod}
          warehouse={warehouse}
        />
      </Route>
      <Route exact path={path}>
        <TablePartner
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          data={dataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          shippingMethod={shippingMethod}
          setDataTable={setDataTable}
        />
      </Route>
    </Shared>
  )
}