import React, {useContext, useState, useMemo} from 'react';
// import MaterialTable, { MTableToolbar } from 'material-table'
import MaterialTable from 'material-table'
import DetailOrder from './detail_order'
import api from 'services/api_cms'
import useStyles from 'pages/style_general'
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from '@material-ui/pickers';
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { Permission } from "context/UserContext";
import PermissionToggle from 'components/Role/Toggle'

const { api_url } = window.appConfigs

const payStatus = {
  '1': 'Success',
  '2': 'Failed',
  '3': 'Pending'
}

const check_status = (newData) => {
  const { order_status, sub_orders } = newData
  if ([4, "4"].includes(order_status) && sub_orders && sub_orders.length > 0) {
    let check = sub_orders.findIndex(i => i?.order_status && [4, 6, 8, 9, "4", "6", "8", "9"].includes(i.order_status) === false)
    return check !== -1
  }
  else {
    return false
  }
}

export default function TableOrder ({ stateQuery, setStateQuery, parseOrderData, getFilterStr, controlEditTable, setControlEditTable, status, calcPay, shippingMethod, shippingPartner, reason, requestDeliveryTime, payGetway }) {
  // const { data , functionChangeMode} = props
  // Handle open - close dialog
  // const userData = useUserState();
  const permission = useContext(Permission)

  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const [dataDetail, setDataDetail] = useState([])
  const [open, setOpen] = React.useState(false);
  const [data, setData] = useState(null)
  const statusLookup = {}
  const reasonLookup = {}
  // const displayStatus = []
  status.map(s => statusLookup[s.status_value] = s.status_name)
  reason.map(s => reasonLookup[s.reason_value] = s.reason_name)
  const handleClickOpen = (value) => {
    setDataDetail(value)
    setOpen(true)
    // sendTracking({pages: 'order', dataOrigin: value, dataEdit: null, actionType: 'view', userData})
  };
  const handleClose = () => {
    setOpen(false);
  };

  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const strFilter = getFilterStr(query)
    ///${query.pageSize}/${query.page}/${query.search || "all"}`
    return api.post("/list/order", {
      number: query.pageSize,
      page: query.page,
      ...(strFilter && {filter: ' AND ' + strFilter})
    })
      .then(response => {
        const { summary: [{totalCount}], data } = response.data
        setData(parseOrderData(data))
        return {
          data,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [selectedDateFrom, setSelectedDateFrom] = useState(() => {
    let from = new Date()
    from.setHours(0, 0, 0, 1)
    return from
  });

  const handleDateFromChange = (date) => {
    setSelectedDateFrom(date)
  };
  const [selectedDateTo, setSelectedDateTo] = useState(() => {
    let to = new Date()
    to.setHours(23, 59, 59, 999)
    return to
  });
  const handleDateToChange = (date) => {
    setSelectedDateTo(date)
  }

  const query_string = useMemo(() => {
    let query = {}
    query.selectedDateFrom = (new Date(selectedDateFrom).getTime())
    query.selectedDateTo = (new Date(selectedDateTo).getTime())
    if (!Array.isArray(query)) {
      let k
      let queryStr = ""
      for(k in query) {
        queryStr === "" ? queryStr += `?${k}=${query[k] || ""}` : queryStr += `&${k}=${query[k] || ""}`
      }
      return queryStr
    } else {
      return ""
    }
  }, [selectedDateFrom, selectedDateTo])

  function setControlEdit (e) {
    e.preventDefault()
    setControlEditTable(!controlEditTable)
  }
  // Handle data view on dialog


  /* let childItem = function (items, field = 'uid', secondF = null) {
    const html =
            <table>
              <tbody>
                {items.map((item) =>
                  <tr key={item.uid}>
                    {!secondF ? <td>{ item[field] }</td> : <td>{ item[field] && item[field][secondF] }</td>}
                  </tr>
                )}
              </tbody>
            </table>
    return html
  } */

  /* let statusItem = function (items, field = 'order_status') {
    const html =
            <table>
              <tbody>
                {items.map((item) =>
                  <tr key={item.uid}>
                    <td>{ (status.find(s => s.status_value == item[field])).status_name }</td>
                  </tr>
                )}
              </tbody>
            </table>
    return html
  } */

  const [column] = useState(() => {
    const column =
        [
          {
            title: "Thời gian KH đặt hàng",
            field: "created_at",
            editable: "never"
          },
          {
            title: "Mã đơn hàng",
            field: "order_id",
            editable: "never",
            o: 'regexp'
          },
          {
            title: "Người nhận",
            field: "customer_name",
            editable: "onUpdate",
            o: 'allofterms'
          },
          {
            title: "SĐT Người nhận",
            field: "phone_number",
            editable: "onUpdate",
            o: 'regexp'
          },
          {
            title: "Địa chỉ nhận",
            field: "address_des",
            editable: "onUpdate",
            o: 'allofterms'
          },
          {
            title: "TP/Tỉnh",
            field: "province",
            editable: "never",
            render: rowData => rowData?.province?.name,
            o: 'allofterms'
          },
          {
            title: "Quận/Huyện",
            field: "district",
            editable: "never",
            render: rowData => rowData?.district?.name
          },
          {
            title: "Tổng tiền chưa giảm",
            type: "currency",
            cellStyle: {
              textAlign: "left"
            },
            currencySetting: {
              locale: "vi-VI",
              currencyCode: "VND",
              maximumFractionDigits: 3,
              minimumFractionDigits: 0
            },
            field: "total_initial_amount",
            editable: "never",
            filtering: false
          },
          {
            title: "VoucherID",
            field: "voucher_usage",
            filtering: false,
            render: rowData => rowData?.voucher_usage?.[0]?.voucher_uid && rowData?.voucher_usage?.[0]?.voucher_uid + ' - ' + rowData?.voucher_usage?.[0]?.voucher_usage_code
          },
          {
            title: "Giá trị voucher",
            type: "currency",
            cellStyle: {
              textAlign: "left"
            },
            currencySetting: {
              locale: "vi-VI",
              currencyCode: "VND",
              maximumFractionDigits: 3,
              minimumFractionDigits: 0
            },
            field: "applied_value",
            editable: "never",
            filtering: false,
          },
          {
            title: "Giảm giá khác",
            type: "currency",
            cellStyle: {
              textAlign: "left"
            },
            currencySetting: {
              locale: "vi-VI",
              currencyCode: "VND",
              maximumFractionDigits: 3,
              minimumFractionDigits: 0
            },
            field: "extra_applied_value",
            filtering: false,
            editable: `${permission === 3 ? "never" : "onUpdate" }`
          },
          {
            title: "Tổng tiền đã giảm (Doanh thu)",
            type: "currency",
            cellStyle: {
              textAlign: "left"
            },
            currencySetting: {
              locale: "vi-VI",
              currencyCode: "VND",
              maximumFractionDigits: 3,
              minimumFractionDigits: 0
            },
            field: "total_pay",
            editable: "never",
            filtering: false,
          },
          {
            title: "Lợi nhuận",
            type: "currency",
            cellStyle: {
              textAlign: "left"
            },
            currencySetting: {
              locale: "vi-VI",
              currencyCode: "VND",
              maximumFractionDigits: 3,
              minimumFractionDigits: 0
            },
            field: "revenue",
            editable: "never",
            filtering: false,
          },
          {
            title: "Hình thức thanh toán",
            field: "pay_gateway",
            o: 'eq',
            lookup: payGetway
          },
          {
            title: "Trạng thái TT",
            field: "pay_status",
            editable: "onUpdate",
            lookup: payStatus,
            o: 'eq'
          },
          {
            title: "Trạng thái ĐH Tổng",
            field: "order_status",
            editable: "onUpdate",
            lookup: statusLookup,
            o: 'eq'
          },
          {
            title: "Lý do",
            field: "reason",
            editable: "onUpdate",
            lookup: reasonLookup,
            o: 'eq'
          },
          {
            title: "Thời gian giao hàng mong muốn",
            field: "request_delivery_time",
            editable: "never",
            lookup: requestDeliveryTime
          },
          { title: "Ghi chú của CS", field: "notes", editable: "onUpdate" },
          { title: "OTT", field: "ott_name", editable: "onUpdate", render: rowData => rowData?.['user.ott']?.ott_name },
          {
            title: "Ngày giao hàng thành công",
            field: "date_delivery_success",
            render: rowData => rowData?.date_delivery_success ? <p>{ new Date(rowData.date_delivery_success).toLocaleString("vi-VN")}</p> : "",
            editable: "never",
          }
        ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  function onRowUpdate (newData, oldData) {
    const { uid, order_status, customer_name, phone_number, address_des, notes, reason, extra_applied_value } = newData
    let indexRow = data.indexOf(oldData)
    let flag = check_status(newData) // true = (order_status === "Đã giao") && (Order con sai order_status in [4,6,8])

    //

    return api.post('/order', { uid, order_status: flag === true ? oldData?.order_status : order_status, customer_name, phone_number, address_des, notes, reason, extra_applied_value})
      .then(res => {
        // Lấy ngày giao hàng thành công - cập nhật table
        if(parseInt(order_status) === 4) {
          newData.date_delivery_success = res.data?.date_delivery_success
        }
        data[indexRow] = {...newData, order_status: flag === true ? oldData?.order_status : order_status }
        setData([...data])
        // sendTracking({pages: 'order', dataOrigin: oldData, dataEdit: newData, actionType: 'edit', userData})
        if(flag === true) {
          alert("Tất cả đơn hàng con chưa chuyển sang trạng thái : Đã Giao / Đã Hủy / Đã Hoàn Trả Đơn Hàng / Đã Hoàn Tiền")
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  // Render
  return (
    <div className="fade-in-table">
      <DetailOrder
        open = {open}
        dataDetail = {dataDetail}
        setDataDetail = {setDataDetail}
        handleClose = {handleClose}
        calcPay={calcPay}
        status={status}
        shippingMethod={shippingMethod}
        shippingPartner={shippingPartner}
        reason={reason}
      />
      <MaterialTable
        // title={<h1 className={classes.titleTable}>Đơn Hàng</h1>}
        columns={column}
        data={getData}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          search: false,
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: false,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          controlEditTable === false ? {
            icon: 'visibility',
            tooltip: 'View Order',
            onClick: (event, rowData) => {
              // setDataDetail(rowData['order.items'])
              handleClickOpen(rowData)
            },
            // disabled: rowData => rowData.birthYear < 2000
          } : null,
        ]}
        editable={{
          onRowUpdate: (permission === 0 || permission === 3) && controlEditTable === false && onRowUpdate
        }}
        components={{
          Toolbar: () => (
            <div>
              {/* <MTableToolbar {...props} /> */}
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container spacing={2} className={classes.formpd}>
                  <Grid item xs={12} sm={12} lg={2} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Đơn Hàng</h1>
                  </Grid>
                  <Grid item xs={12} sm={10} lg={9}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Grid container justifyContent="space-around">
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Đơn hàng đặt sau ngày"
                          value={selectedDateFrom}
                          onChange={handleDateFromChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                        <KeyboardTimePicker
                          margin="normal"
                          label="Thời gian từ"
                          value={selectedDateFrom}
                          onChange={handleDateFromChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change time',
                          }}
                        />
                        <KeyboardDatePicker
                          margin="normal"
                          id="date-picker-dialog"
                          label="Đơn hàng đặt trước ngày"
                          format="dd/MM/yyyy"
                          value={selectedDateTo}
                          onChange={handleDateToChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                        <KeyboardTimePicker
                          margin="normal"
                          label="Thời gian đến"
                          value={selectedDateTo}
                          onChange={handleDateToChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change time',
                          }}
                        />
                      </Grid>
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item xs={12} sm={2} lg={1} style={{ textAlign: "right" }}>
                    <a style={{color: "gray"}} href={`${api_url}/order/order_export.xlsx${query_string}`} target="_blank" rel="noreferrer" tooltip="Export Here">
                      <i className="material-icons">save_alt</i>
                    </a>
                    <PermissionToggle>
                      <Tooltip title="Display Edit" aria-label="display edit">
                        <IconButton style={{color: "gray", cursor: "pointer"}} onClick={setControlEdit}>
                          <EditIcon />
                        </IconButton>
                      </Tooltip>
                    </PermissionToggle>
                  </Grid>
                </Grid>
              </div>
            </div>
          ),
        }}
      />
    </div>
  )
}