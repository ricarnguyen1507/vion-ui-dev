import React, {useContext, useState, useEffect} from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import MaterialTable from 'material-table'
import { makeStyles } from "@material-ui/core/styles"
import { MuiPickersUtilsProvider, KeyboardDateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import viLocale from "date-fns/locale/vi"
import { Permission } from "context/UserContext";
// import sendTracking from "../../tracking";


import api from 'services/api_cms'
const useStyles = makeStyles(() => ({
  root: {
    minWidth: "100%"
  },
  timePicker: {
    width: "100%",
    minWidth: 240
  },
}))

export default function ({ open, handleClose, dataDetail, setDataDetail, calcPay, status, shippingMethod, reason }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const statusLookup = {}
  const shippingLookup = {}
  const permission = useContext(Permission)
  status.map(s => statusLookup[s.status_value] = s.status_name)

  const reasonLookup = {}
  reason.map(s => reasonLookup[s.reason_value] = s.reason_name)
  useEffect(() => {
    if (dataDetail && dataDetail.sub_orders) {
      dataDetail.sub_orders.map(sub => {
        sub.order_items && sub.order_items.map(item => {
          sub['sub_order_total'] = 0
          sub['sub_order_total'] = calcPay(item)
          sub['cost_price'] = 0
          sub['cost_price'] = item.cost_price
          sub['sell_price'] = 0
          sub['sell_price'] = item.sell_price
        })
      })
    }

  }, [dataDetail])

  const column =
    [
      // { title: "Ghi chú hàng hoá khi giao", field: "notes", editable: "onUpdate" },
      { title: "Mã đơn hàng con", field: "order_id", editable: "never" },
      {
        title: "Tên Sản Phẩm",
        field: "product_name",
        render: rowData => childItem(rowData.order_items, 'product_name'),
        editable: "never"
      },
      {
        title: "Mã SKUs",
        field: "sku_id",
        render: rowData => childItem(rowData.order_items, 'product', 'sku_id'),
        editable: "never"
      },
      {
        title: "Số Lượng",
        field: "quantity",
        render: rowData => childItem(rowData.order_items, 'quantity'),
        editable: "never"
      },
      {
        title: "Đơn vị tính",
        field: "quantity",
        render: rowData => childItem(rowData.order_items, 'product', 'unit'),
        editable: "never"
      },
      {
        title: "Chính sách đổi trả",
        field: "return_terms",
        render: rowData => childItem(rowData.order_items, 'product', 'return_terms'),
        editable: "never"
      },
      {
        title: "Mô tả  CTKM",
        field: "promotion_detail",
        render: rowData => childItem(rowData.order_items, 'promotion_detail'),
        editable: "never"
      },
      {
        title: "CTKM",
        field: "promotion_desc",
        render: rowData => childItem(rowData.order_items, 'promotion_desc'),
        editable: "never"
      },
      {
        title: "Giá vốn (Có VAT)",
        type: "currency",
        cellStyle: {
          textAlign: "left"
        },
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        field: "cost_price",
        // render: rowData => childItem(rowData.order_items, 'cost_price'),
        editable: "never"
      },
      {
        title: "Giá bán (Có VAT)",
        type: "currency",
        cellStyle: {
          textAlign: "left"
        },
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        field: "sell_price",
        // render: rowData => childItem(rowData.order_items, 'sell_price'),
        editable: "never"
      },
      {
        title: "Thành tiền",
        type: "currency",
        cellStyle: {
          textAlign: "left"
        },
        currencySetting: {
          locale: "vi-VI",
          currencyCode: "VND",
          maximumFractionDigits: 3,
          minimumFractionDigits: 0
        },
        field: "sub_order_total",
        // render: rowData => childItem(rowData.order_items, 'sub_order_total'),
        editable: "never"
      },
      // {
      //   title: "Tổng tiền đơn hàng con",
      //   type: "currency",
      //   cellStyle: {
      //     textAlign: "left"
      //   },
      //   currencySetting: {
      //     locale: "vi-VI",
      //     currencyCode: "VND",
      //     maximumFractionDigits: 3,
      //     minimumFractionDigits: 0
      //   },
      //   field: "sub_order_total",
      //   editable: "never"
      // },
      {
        title: "Trạng thái đơn hàng con",
        field: "order_status",
        editable: "onUpdate",
        lookup: statusLookup,
        o: 'eq'
      },
      {
        title: "Khu vực",
        field: "areas",
        render: rowData => areaItem(rowData.order_items)
      },
      { title: "Lý do", field: "reason", editable: "onUpdate", lookup: reasonLookup },
      { title: "Mã vận đơn", field: "shipping_code", editable: "onUpdate" },
      {
        title: "NCC",
        field: "partner_name",
        render: rowData => rowData?.['order.partner']?.partner_name || "",
        editable: "never"
      },
      {
        title: "PTGH",
        field: "shipping_method",
        render: rowData => rowData?.['order.partner'] ? shippingMethod[rowData?.['order.partner']?.shipping_method] : "",
        editable: "never"
      },
      {
        title: "ĐVVC",
        field: "shipping_partner_value",
        lookup: shippingLookup
      },
      { title: "Ghi chú của CS", field: "notes", editable: "onUpdate" },
      {
        title: "Ngày giao hàng thành công",
        field: "date_delivery_success",
        render: rowData => rowData?.date_delivery_success ? <p>{ new Date(rowData.date_delivery_success).toLocaleString("vi-VN")}</p> : "",
        editable: "onUpdate",
        editComponent: (props) => (
          <>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={viLocale}>
              <KeyboardDateTimePicker
                className={classes.timePicker}
                variant="inline"
                ampm={false}
                name="date_delivery_success"
                value={props.rowData?.date_delivery_success}
                onChange={date =>
                  props.onRowDataChange({
                    ...props.rowData,
                    date_delivery_success: Date.parse(date)
                  })
                }
                animateYearScrolling={true}
                inputVariant="outlined"
                format="yyyy-MM-dd HH:mm:ss"
                disableFuture={true}
              />
            </MuiPickersUtilsProvider>
          </>
        )
      },
    ]

  let childItem = function (items, field = 'uid', secondF = null) {
    const html =
            <table>
              <tbody>
                {items.map((item) =>
                  <tr key={item.uid}>
                    {!secondF ? <td>{ item[field] }</td> : <td>{ item[field] && item[field][secondF] }</td>}
                  </tr>
                )}
              </tbody>
            </table>
    return html
  }
  let areaItem = function (items) {
    const html =
            <table>
              <tbody>
                {items.map((item) =>
                  <tr key={item.uid}>
                    <td>{ item?.product?.['product.areas']?.map(a => a.name).join(',') }</td>
                  </tr>
                )}
              </tbody>
            </table>
    return html
  }

  async function onRowUpdate (newData, oldData) {
    const { uid, order_status, notes, shipping_code, reason, shipping_partner_value, date_delivery_success } = newData
    let newSubOrder = []
    if (dataDetail.sub_orders) {
      newSubOrder = dataDetail.sub_orders
      newSubOrder.map((no, k) => {
        if (no?.['order_items']?.[0]?.uid === oldData?.['order_items']?.[0]?.uid) {
          return newSubOrder[k] = newData
        }
      })
      setDataDetail({...dataDetail, sub_orders: newSubOrder})
    } else {
      newSubOrder = newData
      setDataDetail({...newSubOrder})
    }
    // sendTracking({pages: 'order_sub', dataOrigin: oldData, dataEdit: null, actionType: 'edit', userData})
    await api.post('/order', { uid, order_status, notes, shipping_code, reason, shipping_partner_value, date_delivery_success, is_sub_order: true }).catch(err => {
      console.log(err)
    })
    window.location.reload()
  }
  return (
    <div>
      <Dialog fullWidth open={open} onClose={handleClose} maxWidth="xl" className={classes.root}>
        <MaterialTable
          columns={column}
          data={dataDetail.sub_orders || [dataDetail] || []}
          title={<h1 className={classes.titleTable}>Order Item</h1>}
          onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
          options={{
            headerStyle: {
              fontWeight: 600,
              background: "#f3f5ff",
              color: "#6e6e6e",
            },
            sorting: false,
            exportButton: true,
            rowStyle: rowData => ({
              backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
            }),

          }}
          editable={{
            isEditable: rowData => rowData.order_id,
            onRowUpdate: (permission === 0 || permission === 3) && onRowUpdate
          }}
        />
        <DialogActions>
          <Button onClick={handleClose} color="secondary" variant="contained" size="small">
                        Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}