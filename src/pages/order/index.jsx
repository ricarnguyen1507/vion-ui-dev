import React, { useState, useEffect } from 'react';
import api from 'services/api_cms';
import TableOrder from './table'
import Loading from 'components/Loading';
const shippingMethod = ['FORWARDING', 'STOCKING', 'STOCK_AT_SUP']
const requestDeliveryTime = {'work_day': "Từ 8 đến 17 giờ các ngày làm việc hành chính", 'all_day': "Tất cả các ngày trong tuần"}
const payGetway = {
  'momo': "Momo",
  'quickpay': "Quickpay",
  'vnpay': "VNPay",
  'moca': "Moca",
  'cod': "COD"
}
function parseDate (unixDate) {
  const d = new Date(unixDate).toLocaleString()
  return d//`${String(d.getDate()).padStart(2,'0')}/${String(d.getMonth()+1).padStart(2,'0')}/${d.getFullYear()}`
}
function calcPay ({sell_price = 0, discount = 0, quantity = 0}) {
  return (1 - discount / 100) * sell_price * quantity
}
function parseOrderData (orders) {
  for(let r of orders) {
    r.customer_account_name = r['order.customer'] ? r['order.customer'].customer_name : ""
    r.customer_account_phone = r['order.customer'] ? r['order.customer'].phone_number : ""
    r.created_at = parseDate(r.created_at)
  }
  return orders
}

function getFilterStr ({filters}) {
  const str = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      if(o === 'regexp') {
        return `regexp(${field}, /${value}/)`
      } else {
        return `${o || 'alloftext'}(${field},"${value}")`
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      // console.log(value)
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
  }).filter(v => v !== "")
  return str.join(' AND ')
}

function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  // const [dataTable, setDataTable] = useState(null)
  const [status, setStatus] = useState(null)
  const [shippingPartner, setShippingPartner] = useState(null)
  const [reason, setReason] = useState(null)
  const [mode, setMode] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))
  /* const changeMode = (m = "table") => {
    setMode(m)
  } */
  // Load data
  useEffect(() => {
    const fetchs = [
      "/list/status",
      "/list/reason",
    ].map(url => fetchData(url).then(res => res.data.result))
    Promise.all(fetchs).then(([
      status,
      reasons
    ]) => {
      setStatus(status)
      setReason(reasons)
      setMode("table")
    })
  }, []);

  if (mode === "table") {
    return <TableOrder
      controlEditTable={controlEditTable}
      setControlEditTable={setControlEditTable}
      // setData={setDataTable}
      status={status}
      calcPay={calcPay}
      parseOrderData={parseOrderData}
      // changeMode={changeMode}
      stateQuery={stateQuery}
      setStateQuery={setStateQuery}
      getFilterStr={getFilterStr}
      shippingMethod={shippingMethod}
      reason={reason}
      requestDeliveryTime={requestDeliveryTime}
      payGetway={payGetway}
    />
  } else {
    return <Loading />
  }

}