import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import api from 'services/api_cms'

import getFilterStr from 'services/tableFilterString'

/**GraphMutation */
import { context } from 'context/Shared'
import {
  useHistory,
  useLocation
} from "react-router-dom"
/**GraphMutation */

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

export default function TableBrandShopGroup ({ controlEditTable, setControlEditTable, displayStatus, stateQuery, setStateQuery }) {

  const history = useHistory()
  const ctx = useContext(context)
  const { pathname } = useLocation()
  const classes = useStyles()

  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const { strFilter } = getFilterStr(query)
    return api.post("/list/specs-type", {params: {
      number: query.pageSize,
      page: query.page,
      ...(strFilter && {filter: ' AND ' + strFilter}),
    }})
      .then(response => {
        const { summary: [{totalCount}], result } = response.data
        return {
          data: result,
          page: query.page,
          pageSize: query.pageSize,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [selectedRow, setSelectedRow] = useState(null)
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq',
        sorting: false
      },
      {
        title: "Specs Type Name",
        field: "specs_type_name",
        editable: "onUpdate",
        o: 'regexp',
        cellStyle: {
          width: 20,
          maxWidth: 20
        },
        headerStyle: {
          width: 20,
          maxWidth: 20
        }
      },
      {
        title: "Số loại",
        field: "specs_type.values",
        editable: "never",
        render: (rowData) => <div>{rowData['specs_type.values']?.length || 0}</div>,
        sorting: false,
        filtering: false
      }
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })
  // Render
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Customer</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: _props => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Specs Type</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: stateQuery.pageSize,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),

        }}
        actions={[
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}