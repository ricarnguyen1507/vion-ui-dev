import React, { useState } from 'react'
import Edit from './edit'
import TableBrandShopGroup from './table'
import {
  Route,
  useRouteMatch
} from "react-router-dom"
import { Shared } from 'context/Shared'

const genders = ['Male', 'Female', 'Other']

const display_status = ['PENDING', 'REJECTED', 'APPROVED']

export default function () {
  const [sharedData] = useState(() => new Map())
  const { path } = useRouteMatch()
  const [controlEditTable, setControlEditTable] = useState(false)
  const [errorPassword, setErrorPassword] = useState(false);
  const [specsValues, setSpecsValues] = useState([])
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0
  }))
  const handleClose = () => {
    setErrorPassword(false)
  }

  return (<Shared value={sharedData}>
    <Route path={`${path}/:actionType`}>
      <Edit
        errorPassword={errorPassword}
        genders={genders}
        handleClose={handleClose}
        displayStatus={display_status}
        specsValues={specsValues}
        setSpecsValues={setSpecsValues}
      />
    </Route>
    <Route exact path={path}>
      <TableBrandShopGroup
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        genders={genders}
        displayStatus={display_status}
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
      />

    </Route>
  </Shared>
  )
}
