import React, { useState, useMemo, useContext } from 'react'
import {
  useHistory
} from "react-router-dom"
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import RoleButton from 'components/Role/Button';
import Button from "@material-ui/core/Button"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import api from 'services/api_cms'
import SelectMultiSpecsValue from "modules/SelectMulti/specs_values/list_manage"
/**Graph Mutation Apply */
import { InputEdge, Node } from 'components/GraphMutation'
import { context } from 'context/Shared'
import { ObjectsToForm } from "utils"

export default function () {
  const classge = useGeneral()
  const history = useHistory()
  const ctx = useContext(context)
  const [dataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_specs_type",
    "dgraph.type": "SpecsType",
    "display_status": 2
  })
  delete dataEdit?.['specs_type.values|display_order']

  const specsValues = useMemo(() => {
    if (dataEdit?.['specs_type.values']?.length > 0) {
      return dataEdit['specs_type.values']
    } else {
      return []
    }
  }, [dataEdit])

  const [files] = useState({})
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))

  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/specs-type", formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/specs_type')
  }


  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  function invalidCallback (result) {
    setMultiSelectInvalid(result)
  }

  let validateForm = {}
  validateForm = useMemo(() =>
    ({})
    // if (!dataEdit.brand_shop) {
    //   return {
    //     ...validateForm,
    //     brandShop: {
    //       error: true,
    //       helperText: "This field is required"
    //     }
    //   }
    // } else {
    //   return {}
    // }
  , [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // View
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Specs Type" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"specs_type_name"}
                    fullWidth
                    label="Specs Type Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required']}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <SelectMultiSpecsValue
                    isArray={false}
                    listOption={specsValues}
                    currentList={dataEdit['specs_type.values']}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    maxItem="-1"
                    minItem="0"
                    listTitle="Specs Values"
                    facetPrefix="specs_type.values"
                    invalidCallback={invalidCallback}
                  />
                </Grid>
              </Grid>
            </div>

          </Grid>

        </Grid>

        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={goBack}
            disabled={Object.keys(validateForm).length !== 0 || multiSelectInvalid}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
