import React, {useState, useContext} from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip } from "@material-ui/core";
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"
import getFilterStr from 'services/tableFilterString'
const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
const collectionTagCate = {
  'assess': 'Assess',
  'promotion': 'Promotion',
  'shipping': 'Shipping'
}
const collectionTagType = {
  'text': 'Text',
  'percentage': 'Phần trăm',
  'extra_gift': 'Tặng kèm',
  'fee': 'Giá tiền',
  'ship': 'Chỉ giao',
  'freeship': 'FreeShip',
  'ship_freeship': 'Freeship và chỉ giao'
}
export default function TableTag ({stateQuery, setStateQuery, setDataTable, controlEditTable, setControlEditTable, displayStatus }) {
  const classes = useStyles()
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  const [selectedRow, setSelectedRow] = useState(null)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get("/list/tag", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...{filter: (strFilter ? ' AND ' + strFilter : '')},
    }})
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái",
        field: "display_status",
        editable: "onUpdate",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq',
      },
      { title: "Tên tag", field: "tag_title", o: 'eq', editable: "onUpdate" },
      {
        title: "Loại tag",
        field: "tag_type",
        editable: "onUpdate",
        lookup: collectionTagType,
        filtering: false
      },
      {
        title: "Loại hiển thị",
        field: "tag_category",
        editable: "onUpdate",
        lookup: collectionTagCate,
        filtering: false
      },
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Nhà Sản Xuất</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: _props => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Product Tag</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank" tooltip="Add Supplier">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }} tooltip="Display Edit">
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          // {
          //     icon: AddBox,
          //     tooltip: 'Add Supplier',
          //     isFreeAction: true,
          //     onClick: () => {
          //         functionEditData('Add')
          //     }
          // },
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Supplier",
            onClick: (event, { tableData, ...rowData }) => {
              rowData.promotion_value_1 = (rowData.promotion_value === "percentage") ? rowData.percentage : rowData.fee;
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}