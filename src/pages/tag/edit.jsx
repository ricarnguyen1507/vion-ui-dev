import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AddBox from '@material-ui/icons/AddBox'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from "components/Media/index"
import { genCdnUrl } from 'components/CdnImage'
import useStyles from './edit_style'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { InputEdge, Node } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"

const tagCategory = [
  {
    code: 'assess',
    label: 'Assess'
  },
  {
    code: 'promotion',
    label: 'Promotion'
  },
  {
    code: 'shipping',
    label: 'Shipping'
  },
  {
    code: 'name_tag',
    label: 'Name Tag'
  }
];
const tag_type = [
  [
    {
      label: 'Text',
      code: 'text'
    }
  ],
  [
    {
      label: 'Phần trăm',
      code: 'percentage'
    },
    {
      label: 'Tặng kèm',
      code: 'extra_gift'
    },
    {
      label: 'Giá tiền',
      code: 'fee'
    }
  ],
  [
    {
      label: 'Chỉ giao',
      code: 'ship'
    },
    {
      label: 'FreeShip',
      code: 'freeship',
    },
    {
      label: 'Freeship và chỉ giao',
      code: 'ship_freeship'
    }
  ]
]

export default function (props) {
  const {functionBack, displayStatus } = props
  const classes = useStyles()
  const classge = useStylesGeneral()
  const [files] = useState({})
  const history = useHistory()
  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_tag",
    "dgraph.type": "Tag"
  })
  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/tag')
  }
  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/tag", formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [tagTypeList, setTagTypeList] = useState(() => (dataEdit != undefined) ? dataEdit.tag_category == 'assess' ? tag_type[0] : dataEdit.tag_category == 'promotion' ? tag_type[1] : dataEdit.tag_category == 'shipping' ? tag_type[2] : [] : [])
  const [tagTypeSelected, setTagTypeSelected] = useState(() => tagTypeList?.find(l => l?.code === dataEdit?.tag_type))

  const [tagCatSelected] = useState(() => tagCategory?.find(c => c?.code === dataEdit?.tag_category))

  const [updated] = useState({
    values: {
      'display_status': dataEdit?.display_status,
      'reference_type': 0
    },
    images: {}
  })
  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }

  // HANDLE PROMOTION VALUE
  const handleChangePromotion = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setDataEdit({...dataEdit, [name]: value})
    if(dataEdit.tag_type == "percentage") {
      let promotion = `<promotion_value> "percentage" (percentage=${value}) .`
      node.setState('promotion_value', "percentage")
      node.setState('promotion_value|percentage', promotion, true, true)
    } else if(dataEdit.tag_type == "fee") {
      let fee = `<promotion_value> "fee" (fee=${value}) .`
      node.setState('promotion_value', "fee")
      node.setState('promotion_value|fee', fee, true, true)
    }
  }

  //
  const handleChangeTagCategory = (e, item) => {
    e.preventDefault()
    if (item.code === 'assess') {
      setDataEdit({ ...dataEdit, tag_category: item.code})
      setTagTypeList(tag_type[0])
      setTagTypeSelected({})
      node.setState('tag_category', item.code)
      node.setState('promotion_value', "", true)
      node.setState('name_tag_value_1', "", true)
      node.setState('name_tag_value_2', "", true)
    } else if (item.code === 'promotion') {
      setDataEdit({ ...dataEdit, tag_category: item.code})
      setTagTypeList(tag_type[1])
      setTagTypeSelected({})
      node.setState('tag_category', item.code)
      node.setState('name_tag_value_1', "", true)
      node.setState('name_tag_value_2', "", true)
    } else if (item.code === 'name_tag') {
      setDataEdit({ ...dataEdit, tag_category: item.code})
      node.setState('tag_category', item.code)
      node.setState('tag_type', "", true)
      node.setState('promotion_value', "", true)
    } else {
      setDataEdit({ ...dataEdit, tag_category: item.code})
      setTagTypeList(tag_type[2])
      setTagTypeSelected({})
      node.setState('tag_category', item.code)
      node.setState('promotion_value', "", true)
      node.setState('name_tag_value_1', "", true)
      node.setState('name_tag_value_2', "", true)
    }
  }

  const handleChangeTagType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, tag_type: item.code })
    node.setState('tag_type', item.code)
  }


  // Lưu lại những image thay đổi
  const updateImage = (field, file) => {
    node.setState('image_promotion', `images/tag/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Tag" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
      >
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"tag_title"}
                    fullWidth
                    label="Tên Tag (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} className={classge.formpd} >
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    options={tagCategory}
                    getOptionLabel={option => option.label}
                    defaultValue={tagCatSelected}
                    style={{ width: "100%" }}
                    onChange={handleChangeTagCategory}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn loại hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
                {dataEdit?.['tag_category'] === 'name_tag' &&
                  <>
                    <Grid item xs={12} sm={12} lg={12}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"name_tag_value_1"}
                        fullWidth
                        label="Giá trị 1"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'maxStringLength:50'
                        ]}
                        errorMessages={[
                          'Max length is 50'
                        ]}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} lg={12}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"name_tag_value_2"}
                        fullWidth
                        label="Giá trị 2"
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'maxStringLength:50'
                        ]}
                        errorMessages={[
                          'Max length is 50'
                        ]}
                      />
                    </Grid>
                  </>
                }
                {dataEdit?.['tag_category'] !== 'name_tag' &&
                  <Grid item xs={12} sm={12} lg={12}>
                    <Autocomplete
                      options={tagTypeList}
                      getOptionLabel={option => option.label ?? ""}
                      value={tagTypeSelected}
                      style={{ width: "100%" }}
                      onChange={handleChangeTagType}
                      renderInput={params => (
                        <TextField {...params}
                          label="Chọn loại tag"
                          variant="outlined"
                          fullWidth
                          margin="dense"
                        />
                      )}
                    />
                  </Grid>
                }
                {dataEdit?.['tag_category'] == 'promotion' ?
                  <Grid container className={classes.rootListCate} spacing={2}>
                    {(dataEdit?.tag_type == 'percentage' || dataEdit?.tag_type == 'fee') ?
                      <Grid item xs={12} sm={6} lg={6} className={classes.listCate}>
                        <TextValidator
                          fullWidth
                          id="promotion_value_1"
                          type="text"
                          label={dataEdit.tag_type == 'percentage' ? '%' : '$'}
                          variant="outlined"
                          margin="dense"
                          name="promotion_value_1"
                          onChange={handleChangePromotion}
                          defaultValue={dataEdit?.promotion_value_1 || ''}
                          validators={[
                            // 'required',
                            'matchRegexp:^[0-9]$'
                          ]}
                          errorMessages={[
                            'This field is required',
                            'Character is not accept ']}
                        />
                      </Grid> : ''}

                    <Grid item xs={12} sm={6} lg={6} className={classes.listCate}>
                      <Card>
                        <CardMedia children={<Media src={genCdnUrl(dataEdit?.image_promotion, "image_cover.png")} type="image" style={{ width: 400, height: 225 }} fileHandle={updateImage} field="image" accept="image/jpeg, image/png" />} />
                      </Card>
                    </Grid>
                  </Grid> :
                  <Grid item xs={12} sm={12} lg={12} className={classes.listCate}>
                    <Card>
                      <CardMedia children={<Media src={genCdnUrl(dataEdit?.image_promotion, "image_cover.png")} type="image" style={{ width: 400, height: 225 }} fileHandle={updateImage} field="image" accept="image/jpeg, image/png" />} />
                    </Card>
                  </Grid>
                }
              </Grid>
              <Grid container spacing={2} className={classge.formpd} style={{paddingTop: 20}}>
                <Grid item xs={12} sm={12} lg={12}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus?.[dataEdit?.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
