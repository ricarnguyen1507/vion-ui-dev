import React from "react";
import useStyles from 'pages/style_general'
import ReactExport from "react-data-export";
import GetAppIcon from "@material-ui/icons/GetApp";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { titleCase } from "services/titleCase";
import { clone } from "services/diff";
import Tooltip from "@material-ui/core/Tooltip";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ExportExcel ({ data, isEditTable }) {
  const classge = useStyles();

  const dataFields = [
    "id",
    "display_status",
    "tag_title",
    "tag_type",
    "tag_category"

  ]

  const handleData = (arrayData) => {
    if (arrayData !== null) {
      const cloneArr = clone(arrayData);
      // cloneArr.forEach((item) => arrResult.push(handlePlainData(item)));
      return cloneArr;
    }
  };

  const excelData = data && handleData(data);

  return (
    <ExcelFile
      element={
        isEditTable ?
          <Button
            variant="contained"
            aria-label="back"
            className={classge.btnExport}
          >
            <GetAppIcon className={classge.expIcon} />
          Export
          </Button>
          :
          <Tooltip title="Export data" aria-label="export data">
            <IconButton variant="contained">
              <GetAppIcon className={classge.expIcon} />
            </IconButton>
          </Tooltip>

      }
    >
      <ExcelSheet data={excelData} name="Product_Tag">
        {excelData && excelData.length
          ? dataFields.map((item, idx) => (
            <ExcelColumn
              key={idx}
              label={`${titleCase(item)} <${item}>`}
              value={item}
            />
          ))
          : {}}
      </ExcelSheet>
    </ExcelFile>
  );
}
