import React, { useState, useEffect } from 'react';
import api from 'services/api_cms';
import TableTag from './table'
import Edit from './edit';
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
const display_status = ['PENDING', 'REJECTED', 'APPROVED']
function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [provinces, setProvinces] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  // Load data
  useEffect(() => {
    const fetchs = [
      '/list/areas',
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([_tag, areas]) => {
      setProvinces(areas)
    })
  }, []);

  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  const handleDelete = (row) => api.post('/tag/' + row.uid, { del: (row.shipping_value != undefined) ? { uid: row.uid, shipping_value: { uid: row.shipping_value.uid } } : { uid: row.uid } })
    .then(res => {
      if (res.status === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      }
    }).catch(error => console.log(error))

  return(
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          functionBack={functionBack}
          displayStatus={display_status}
          listProvinces={provinces}
        />
      </Route>
      <Route exact path={path}>
        <TableTag
          data={dataTable}
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setControlEditTable={setControlEditTable}
          setDataTable={setDataTable}
          handleDelete={handleDelete}
          displayStatus={display_status}
          listProvinces={provinces}
        />
      </Route>
    </Shared>
  )
}