import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'

export default function TableType ({ data, controlEditTable, setControlEditTable, functionEditData }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  const column = [
    {
      title: "Type Name",
      field: "type_name",
      editable: "onUpdate",
      cellStyle: {
        paddingLeft: 30
      },
      headerStyle: {
        paddingLeft: 30
      }
    },
    { title: "Type Value", field: "type_value", editable: "onUpdate" },
  ]

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={data}
        title={<h1 className={classes.titleTable}>Type</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),

        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Type',
            isFreeAction: true,
            onClick: () => {
              functionEditData('Add')
            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Type",
            onClick: (event, { tableData, ...rowData }) => {
              functionEditData('Edit', rowData, tableData.id)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}