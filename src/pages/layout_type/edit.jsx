import React, {useState} from 'react';
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import PageTitle from "components/PageTitle"
import useGeneral from 'pages/style_general'
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import {clone, diff} from 'services/diff'

export default function ({originData, actionType, functionBack, handleSubmitData }) {

  const [dataEdit, setDataEdit] = useState(() => {
    if(actionType === 'Add') {
      return { uid: '_:new_layout_type' }
    }
    if(actionType === 'Edit') {
      return clone(originData)
    }
  })

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setDataEdit({ ...dataEdit, [name]: value })
  }

  const handleSubmit = () => {
    const submitData = actionType === 'Edit' ? diff(originData, dataEdit, dataEdit.uid) : { set: dataEdit }
    handleSubmitData(actionType, submitData)
  }

  // View
  const classge = useGeneral();
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Type" />
      <ValidatorForm className={classge.root} onSubmit={handleSubmit}
      >
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={6} lg={8}>
                  <TextValidator
                    id="nameType"
                    name="type_name"
                    label="Name type"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={handleChange}
                    value={dataEdit.type_name || ""}
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:500',
                      // 'matchRegexp: ^[A-Za-z0-9 ]$'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 500',
                      // 'Character is not accept '
                    ]}
                  />
                </Grid>

                <Grid item xs={12} sm={6} lg={4}>
                  <TextValidator
                    className={classge.valueType}
                    fullWidth
                    id="valueType"
                    type="number"
                    label="Value type"
                    variant="outlined"
                    margin="dense"
                    name="type_value"
                    onChange={handleChange}
                    value={dataEdit.type_value || ""}
                    validators={[
                      'required',
                      'minNumber:1',
                      'maxNumber:100',
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min number is 1',
                      'Max number is 100',
                      'Character is not accept ']}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
