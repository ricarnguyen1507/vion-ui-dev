import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableType from './table'
import Loading from "components/Loading"

export default function () {
  const [dataTable, setDataTable] = useState(null)
  const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(true)

  useEffect(() => {
    api.get('/list/layout_type').then(res => {
      setDataTable(res.data.result);
    }).catch(error => console.log(error));
  }, []);

  // Handle function
  const functionBack = () => {
    setMode("table")
    setControlEditTable(false)
  }
  const functionEditData = (actionType, row = {}, index = -1) => {
    setActionType(actionType)
    setIndexEdit(index)
    setDataEdit(row)
    setMode("edit")
  }
  const handleDelete = (dataDel) => api.post('/layout_type', { del: { uid: dataDel.uid } })
    .then(_ => {
      const idx = dataTable.findIndex(d => d.uid === dataDel.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  const handleSubmitData = (actionType, data) => {
    if(Object.keys(data).length > 0) {
      api.post('/layout_type', data)
        .then(response => {
          const { set: newCatType } = data
          if(actionType === 'Add') {
            newCatType.uid = response.data.uids['new_layout_type']
            setDataTable([newCatType, ...dataTable])
          } else if(actionType === 'Edit') {
            Object.assign(dataTable[indexEdit], newCatType)
            setDataTable([...dataTable])
          }
          setMode("table")
          setDataEdit(null)
        })
        .catch(error => console.log(error))
    } else {
      setMode("table")
    }
  }

  // Display view
  if (mode === "table") {
    return dataTable === null ? <Loading /> : (
      <TableType
        data={dataTable}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
      />
    )
  }
  if (mode === "edit") {
    return dataEdit === null ? <Loading /> : (
      <Edit
        actionType={actionType}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
      />
    )
  }
}
