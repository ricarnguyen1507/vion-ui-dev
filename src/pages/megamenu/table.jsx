import React, { useRef, useState, forwardRef } from 'react';
import tblQuery from "services/tblQuery";
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general';
import SettingsIcon from '@material-ui/icons/Settings';

export default function TableLiveStream ({ ctx }) {
  const classes = useStyles()
  const table = useRef(null)
  const [editable, setEditable] = useState(true)

  const [tblProps] = useState(() => {
    if (ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      icons: {
        Edit: forwardRef((props, ref) => <SettingsIcon {...props} ref={ref} />)
      },
      data: tblQuery("/megamenu/list"),
      columns: [
        { title: "UID", field: "uid", editable: 'never', filtering: false },
        { title: "Tên megamenu", field: "megamenu_name", editable: 'never', operator: 'regexp' },
        { title: "Số ngày kiểm tra đơn hàng", field: "date_total_order", editable: 'never', filtering: false }
      ],
      title: <h1 className={classes.titleTable}>Megamenu</h1>,
      onRowClick (e) {
        if (selectedRow != e.currentTarget) {
          if (selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          e.currentTarget.style.backgroundColor = "#EEE"
          selectedRow = e.currentTarget
        }
      },
      onRowsPerPageChange (v) { this.options.pageSize = v },
      onPageChange (v) { this.options.page = v },
      options: {
        page: 0,
        pageSize: 10,
        pageSizeOptions: [10, 20, 50],
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        search: false,
        filtering: true,
        sorting: true,
        exportButton: true,
        grouping: false,
        actionsColumnIndex: -1
      }
    }
  })

  // Return UI
  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tblProps}
        actions={[
          {
            icon: AddBox,
            tooltip: "Add Megamenu",
            isFreeAction: true,
            onClick () {
              ctx.editData("Add");
            },
          },
          {
            icon: "edit",
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick () {
              setEditable(!editable);
            }
          },
          editable ? {
            icon: "edit",
            tooltip: "Edit Megamenu",
            onClick (e, { tableData, ...rowData }) {
              ctx.editData("Edit", rowData);
            },
          } : null
        ]}
      />
    </div>
  )
}