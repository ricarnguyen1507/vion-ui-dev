import React, { useState } from 'react';
import { useHistory, Route, useRouteMatch } from "react-router-dom";
import Table from './table';
import Edit from './edit';

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (action, row = null) {
      this.data = row
      history.push(`${path}/${action}`)
    }
  }))

  return <>
    <Route path="/app/megamenu" exact={true} >
      <Table ctx={ctx} />
    </Route>

    <Route path={`${path}/:actionType`}>
      <Edit ctx={ctx} />
    </Route>
  </>
}