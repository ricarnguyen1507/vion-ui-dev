import React, { useState, useMemo } from 'react'
import api from 'services/api_cms'
import RoleButton from 'components/Role/Button'
import ReturnIcon from '@material-ui/icons/KeyboardReturn'
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import { Node, InputEdge, CheckBoxEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"

export default function ({ ctx }) {
  const classge = useGeneral()

  const [dataEdit, setDataEdit] = useState(() => ctx.data ?? {
    'uid': '_:new_megamenu',
    "dgraph.type": "MegaMenu",
    'is_display': false,
    'megamenu_name': '',
    'date_total_order': 0
  })
  const [node] = useState(() => new Node(dataEdit))

  /** ============  Chọn Collection Child ============ **/
  const [listCollection, setListCollection] = useState([dataEdit?.['megamenu.collection']] || [])
  const value_collection = useMemo(() => {
    const uid = dataEdit?.['megamenu.collection']?.['uid'] || null
    if (uid)
      return listCollection.find(item => item?.uid === uid ?? null)
    return null;
  }, [dataEdit, listCollection])

  const handleChangeCollectionChild = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'megamenu.collection': item ? { uid: item.uid, collection_name: item.collection_name } : null })
    node.setState('megamenu.collection', item ? { uid: item.uid } : null)
  }

  const onInputCollectionChild = (e, val) => {
    if (typeof val === 'string' && val) {
      let text = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
      api.post(`/collection_child/search`, text).then(res => {
        const newOption = listCollection || []
        const temp = res.data.result.filter(r => (!newOption.find(no => no?.uid === r?.uid)))
        setListCollection([...newOption, ...temp])
      })
    }
  }

  const handleSubmit = () => {
    const formData = ObjectsToForm(node.getMutationObj())
    api.post('/megamenu/edit', formData).then(ctx.goBack).catch(error => {
      console.log(error)
    })
  }

  /** ============  Return UI ============ **/
  const onError = (e) => {
    console.log("Lỗi", e.name);
  }
  const ActionIcon = <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Megamenu" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" > Thông tin </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <FormControlLabel
                    label="Hiển thị ( Cài đặt xuất hiện hiển thị tại UI Client )"
                    control={
                      <CheckBoxEdge
                        Component={Checkbox}
                        node={node}
                        pred={"is_display"}
                        variant="outlined"
                        margin="dense"
                        color="primary"
                      />
                    }
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"megamenu_name"}
                    fullWidth
                    label="Tên hiển thị (*)"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3'
                    ]}
                  />
                </Grid>
                <Grid item xs={6} sm={6} lg={6}>
                  <Autocomplete
                    onChange={handleChangeCollectionChild}
                    options={listCollection}
                    value={value_collection}
                    getOptionLabel={option => option?.['collection_name'] || ""}
                    onInputChange={onInputCollectionChild}
                    renderInput={params => (
                      <TextField {...params}
                        label="Chọn ngành hàng con"
                        fullWidth
                        variant="outlined"
                        margin="dense"
                        required
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={6} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"date_total_order"}
                    fullWidth
                    label="Số ngày tính đơn hàng"
                    variant="outlined"
                    margin="dense"
                    type="number"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required'
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>

        <Grid className={classge.rootSubmit}>
          <Button
            style={{ marginRight: '10px' }}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} /> Return
          </Button>
          <RoleButton actionType="Save" />
        </Grid>
      </ValidatorForm>
    </>
  )
}
