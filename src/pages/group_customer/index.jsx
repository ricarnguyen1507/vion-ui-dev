import React, { useState } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableGroupCustomer from './table'
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(true)
  const [customers, setCustomers] = useState([])
  const [stateQuery, setStateQuery] = useState(() => ({
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null, index) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
      if(index != null) {
        setIndexEdit(index)
      }
    }
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row = {}, index = -1) => {
    setActionType(actionType)
    setIndexEdit(index)
    setDataEdit(row)
    // setMode("edit")
  }
  const handleDelete = (dataDel) => api.post('/group_customer', { del: { uid: dataDel.uid } })
    .then(() => {
      const idx = dataTable.findIndex(d => d.uid === dataDel.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  const handleSubmitData = (actionType, data) => {
    if(Object.keys(data).length > 0) {
      api.post('/group_customer', data)
        .then(response => {
          // const { set } = data
          if(actionType === 'Add') {
            const updateItem = response?.data?.['new_group_customer']?.['result']?.[0]
            setDataTable([updateItem, ...dataTable])
          } else if(actionType === 'Edit') {
            // Object.assign(dataTable[indexEdit], newCatType)
            const updateItem = response?.data?.['new_group_customer']?.['result']?.[0]
            dataTable[indexEdit] = { ...dataTable[indexEdit], ...updateItem }
            setDataTable([...dataTable])
          }
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      ctx.goBack()
    }
  }

  // Display view
  return (<Shared value={sharedData}>
    <Route path={`${path}/:actionType`}>
      <Edit
        actionType={actionType}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
        customers={customers}
        setCustomers={setCustomers}
      />
    </Route>
    <Route exact path={path}>
      <TableGroupCustomer
        data={dataTable}
        controlEditTable={controlEditTable}
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
        setControlEditTable={setControlEditTable}
        setDataTable={setDataTable}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
      />
    </Route>
  </Shared>)
}
