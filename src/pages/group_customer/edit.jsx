import React, {useState, useContext} from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import PageTitle from "components/PageTitle"
import useGeneral from 'pages/style_general'
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import ListManage from 'modules/SelectMulti/select_multi/list_manage'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"
import { InputEdge, Node } from 'components/GraphMutation'

export default function ({functionBack, customers, setCustomers }) {

  const history = useHistory()
  const ctx = useContext(context)

  const [timer, setTimer] = useState(null)
  const [dataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_group_customer",
    "dgraph.type": "GroupCustomer",
  })
  delete dataEdit['group_customer.customers|display_order']
  if(dataEdit?.['group_customer.customers']?.length > 0) {
    setCustomers(dataEdit['group_customer.customers'])
    let number = 0
    for(let obj of dataEdit?.['group_customer.customers']) {
      obj['group_customer.customers|display_order'] = `<group_customer.customers> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }

  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/group-customer')
  }

  const handleSubmit = () => {
    const formData = ObjectsToForm(node.getMutationObj())
    api.post("/group_customer", formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [groupCustomers] = useState({
    set: {},
    del: {}
  })
  const updateListCustomer = ({set, del}) => {
    groupCustomers.set = set
    groupCustomers.del = del

    let listCustomer = dataEdit?.['group_customer.customers'] ?? []
    const removeItem = listCustomer?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('group_customer.customers', mergeArr, false)
      let highlight_col = node.getState('group_customer.customers') ?? []
      highlight_col.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('group_customer.customers', set, true)
    }
  }

  const phone_number_valid = /^\d+$/
  const onInputSelectCustomerChange = (e, val) => {
    let queries = ''
    if (phone_number_valid.test(val)) {
      queries = {phone_number: val.replace(/["\\]/g, '\\$&').trim()}
    } else if (val !== '') {
      queries = {customer_name: val.replace(/["\\]/g, '\\$&').trim()}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      if (queries !== "") {
        api.post(`/list/customer-options`, queries)
          .then(res => {
            const newOption = customers
            res.data.result.forEach(r => {
              if (!newOption.find(no => no.uid === r.uid)) {
                newOption.push(r)
              }
            })
            setCustomers([...newOption])
          })
      }
    }, 550))
  }

  // View
  const classge = useGeneral();
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Group Customer" />
      <ValidatorForm className={classge.root} onSubmit={handleSubmit}
      >
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"group_customer_name"}
                    fullWidth
                    label="Tên nhóm"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:500',
                      // 'matchRegexp: ^[A-Za-z0-9 ]$'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 500',
                      // 'Character is not accept '
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <ListManage
                    isShow={dataEdit?.target_type !== 0}
                    listOption={customers}
                    currentList={dataEdit?.['group_customer.customers'] || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListCustomer}
                    maxItem="-1"
                    listTitle="Thêm khách hàng"
                    facetPrefix="group_customer.customers"
                    inputChange={onInputSelectCustomerChange}
                  />
                </Grid>


              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
