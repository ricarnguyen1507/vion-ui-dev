import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"
import getFilterStr from 'services/tableFilterString'

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

export default function TableCollection ({ stateQuery, setStateQuery, controlEditTable, setControlEditTable, displayStatus, setDataTable }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get("/list/collection?t=0&is_temp=false&is_parent=false", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...{filter: (strFilter ? ' AND ' + strFilter : '')},
    }})
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        // Data Collection
        // collectionData = setOrderList(data?.filter(r => r.is_deleted !== true));
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq',
        sorting: false
      },
      {
        title: "UID",
        field: "uid",
        sorting: false,
        filtering: false
      },
      {
        title: "Tên ngành hàng",
        field: "collection_name",
        render: rowData => rowData?.collection_name,
        sorting: false,
        o: 'alloftext'

      },
      {
        title: "Ngành hàng cha",
        field: "collection_name",
        render: rowData => rowData?.parent?.collection_name,
        sorting: false,
        filtering: false
      },
      {
        title: "Số sản phẩm active",
        field: "products",
        render: rowData => rowData?.products?.length,
        sorting: false
      },
      {
        title: "Collection Image",
        field: "collection_image",
        render: rowData => <ImgCdn src={rowData.collection_image} style={{ width: 128, height: 72 }} />,
        sorting: false,
        filtering: false
      },
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  // const DrageState = {
  //   parent_row: -1,
  //   parent_dropIndex: -1,
  //   child_row: -1,
  //   child_dropIndex: -1,
  // }

  // const reodering = (list, dragObject, isParent = false) => {
  //   const result = Array.from(list);
  //   var startIndex = -1, endIndex = -1;
  //   if (isParent) {
  //     startIndex = result.indexOf(result.find(item => item.parent_order === dragObject.index));
  //     endIndex = result.indexOf(result.find(item => item.parent_order === dragObject.dropIndex));
  //   }
  //   else {
  //     startIndex = result.indexOf(result.find(item => {
  //       if (item.parent && item.parent.child_order >= 0) {
  //         return item.parent.child_order === dragObject.index
  //       }
  //     }));
  //     endIndex = result.indexOf(result.find(item => {
  //       if (item.parent && item.parent.child_order) {
  //         return item.parent.child_order === dragObject.dropIndex
  //       }
  //     }));
  //   }
  //   if (startIndex === -1 || endIndex === -1) {
  //     return;
  //   }
  //   const [removed] = result.splice(startIndex, 1);
  //   result.splice(endIndex, 0, removed);
  //   setDataTable(result)
  // };

  // console.log('data', collectionData)
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // parentChildData={(row, rows) => rows.find(r => row.parent && (r.uid === row.parent.uid))}
        // title={<h1 className={classes.titleTable}>Collection</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Ngành hàng con</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { data &&
                                        <ExportExcel data={data} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )
        }}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Collection",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}
