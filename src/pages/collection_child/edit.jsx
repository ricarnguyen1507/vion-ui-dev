import React, { useState, useContext, useMemo } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import { genCdnUrl } from 'components/CdnImage'
// import ExportExcel from "./export_excel";
import ListManage from 'modules/SelectMulti/select_multi/list_manage'
import { InputEdge, Node, OptionEdge } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"

export default function ({ dataOption, functionBack, displayStatus }) {
  const classge = useGeneral()
  const history = useHistory()

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    "dgraph.type": "Collection",
    "collection_type": 0,
    "is_temporary": false,
    "is_parent": false
  })
  for (let obj of dataOption) {
    if (obj?.['children|display_order']) {
      delete obj?.['children|display_order']
    }
  }
  delete dataEdit?.['products|display_order']
  delete dataEdit?.['highlight.products']
  delete dataEdit?.['highlight.products|display_order']

  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  const [updated] = useState({
    values: {
      'display_status': dataEdit?.display_status
    }
  })

  const tCollections = useMemo(() => {
    const filter = actionType === 'Add' ? (c) => !c.parent : (c) => !c.parent && dataEdit?.uid !== c?.uid
    return dataOption?.filter(filter)
  }, [dataEdit, actionType, dataOption])
  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/collection_child')
  }
  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files, { childProduct: childProduct })
    api.post("/collectionNewForm/" + dataEdit?.uid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }
  const [childProduct] = useState({
    set: {},
    del: {}
  })

  const updateListProduct = ({ set, del }) => {
    childProduct.set = set
    childProduct.del = del
  }

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  // const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  // function invalidCallback (result) {
  //   setMultiSelectInvalid(result)
  // }

  // let validateForm = {}
  // validateForm = useMemo(() => {
  //     if(!dataEdit.collection_image || dataEdit.collection_image === ""){
  //         return {
  //             ...validateForm,
  //             icon: {
  //                 error: true,
  //                 helperText: "This field is required"
  //             }
  //         }
  //     } else {
  //         return {}
  //     }
  // }, [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */


  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Collection" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={8} lg={8}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"collection_name"}
                    fullWidth
                    label="Collection Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                </Grid>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                      Danh sách Sản Phẩm
                    </Typography>
                  </div>
                  <Divider />
                  <ListManage
                    isArray={false}
                    listOption={dataEdit?.products || []}
                    currentList={dataEdit?.products || []}
                    actionType={actionType}
                    dataEdit={dataEdit}
                    updateListManage={updateListProduct}
                    maxItem="0"
                    listTitle="Sản phẩm"
                    facetPrefix="product.collection"
                  />
                </div>
              </Grid>
            </div>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                  Xét duyệt Ngành hàng
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus[dataEdit?.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>

          <Grid item xs={12} sm={4} lg={4}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Option
                </Typography>
              </div>
              <Divider />
              <Grid item xs={12} sm={12} lg={12}>
                <OptionEdge
                  node={node}
                  pred={"parent"}
                  options={tCollections || []}
                  getOptionLabel={option => option?.collection_name || ""}
                  style={{ width: "100%" }}
                  renderInput={params => (
                    <TextField {...params}
                      label="Select parent collection"
                      fullWidth
                      variant="outlined"
                      margin="dense"
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <InputEdge
                  Component={TextValidator}
                  node={node}
                  pred={"brand_shop_id"}
                  fullWidth
                  label="Mã Brand của FRT"
                  variant="outlined"
                  margin="dense"
                  validators={[
                    'maxStringLength:50'
                  ]}
                  errorMessages={[
                    'Max length is 50']}
                />
              </Grid>
            </div>
          </Grid>

        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <h5> Collection Icon (2000x1000)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit?.collection_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="collection_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{ marginRight: '10px' }}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>
          <RoleButton actionType={actionType} />
          {/* {actionType === "Edit" &&
            <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
