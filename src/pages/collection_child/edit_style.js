import { makeStyles } from "@material-ui/core/styles"

export default makeStyles(theme => ({
  root: {

  },
  title: {
    fontFamily: " 'Roboto', 'Helvetica', 'Arial' ",
    display: 'inline',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  },
  rootBasic: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: 5,
    display: 'flex',
    position: 'relative',
    marginTop: 30,
    flexDirection: 'column',
    padding: theme.spacing(2),
  },
  titleProduct: {
    margin: "10px 28px",
    position: 'relative',
    padding: 0
  },
  colorPicker: {
    display: "inline-block",
    float: 'right',
  },
  listCate: {
    div: {
      display: 'inline-block'
    }
  },
  listIcon: {
    minWidth: 20
  },
  rootDisplayCate: {

  },
  titleListChoose: {
    marginBottom: 15,
  },
  displayCate: {
    padding: 20,
  },
  listItemCate: {
    maxWidth: "100%",
    display: 'flex',
    flexDirection: 'column',
    overflowY: 'auto',
    border: "1px solid #ddd",
    maxHeight: "150px",
    flexWrap: "wrap"
  },
  listItem: {
    width: "auto"
  },
  listItemText: {
    whiteSpace: "nowrap"
  },
  containIcon: {
    float: "left",
    marginRight: 15,
    padding: 15,
    marginTop: -40,
    borderRadius: 3,
    backgroundColor: '#999',
    backgroundImage: 'linear-gradient(60deg, #50BC98, #2E9B93)',
    boxShadow: ' 0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(37, 66, 162,.4) '
  },
  iconProduct: {
    textAlign: 'center',
    color: '#fff',
    fontWeight: 'bold',
    margin: "5px 4px 0px",
    width: 24,
    height: 24
  },

  rootForm: {
    textAlign: "center",
    padding: 10,
    minWidth: 750,
  },

  formpd: {
    paddingTop: 10,
    textAlign: "center"
  },
  gridCombobox: {
    padding: 8,
    marginBottom: 10,
    marginTop: 10,
  },
  btnAdd: {
    margin: 20,
  },
  btnPreview: {
    margin: '0 15px',
    backgroundColor: theme.palette.primary.main,
    "&:hover": {
      backgroundColor: "#172964"
    }
  },
  formControl: {
    maxWidth: "80%",
  },
  imgView: {
    maxWidth: 150,
    maxHeight: 150,
    backgroundSize: '80% 80%',
    backgroundRepeat: "no-repeat"
  },
  imgPanel: {
    padding: 15,
    width: "100%",
    textAlign: "center"
  },
  formMultiPreviews: {
    margin: theme.spacing(1),
    width: "100%",
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  btnData: {
    margin: 20,
    padding: ".5rem 1rem",
    borderRadius: ".2rem"
  },
  imgEditView: {
    display: "block",
    maxWidth: 250,
    maxHeight: 250,
    paddingTop: 20,
    margin: "auto"
  },
  titleColor: {
    float: "left",
    alignContent: 'center',
    padding: '10px 0px 0px 50px'
  },
  rootColor: {

  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  rootMedia: {

  },
  inputFile: {
    display: "none"
  },
  fileUpload: {
    paddingBottom: 20,
  },
  btnUpload: {
    borderRadius: 20,

  },
  viewMedia: {
    maxHeight: 210,
    maxWidth: 210,
    display: "flex"
  },
  viewMediaAfter: {
    maxWidth: 300,
    display: "inline-block",
    padding: 10,
    margin: 10
  },
  media: {
    height: 140,
  },
  mediaProduct: {

  },
  titleRoot: {
    marginBottom: 10
  },
  btnSubmit: {
    textAlign: "center"
  }
}));
