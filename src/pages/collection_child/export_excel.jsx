import React from "react";
import useStyles from 'pages/style_general'
import ReactExport from "react-data-export";
import GetAppIcon from "@material-ui/icons/GetApp";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import { titleCase } from "services/titleCase";
import { clone } from "services/diff";
import Tooltip from "@material-ui/core/Tooltip";
import {changePropName} from "services/changePropName";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ExportExcel ({ data, isEditTable }) {
  const classge = useStyles();

  const dataFields = [
    "collection_image",
    "collection_name",
    "collection_type",
    "display_order",
    "display_status",
    "highlight__products",
    "highlight_name",
    "reference_type",
    "sort_order",
    "uid",
    "~highlight__collections"
  ]

  const handlePlainData = (obj, result = {}) => {
    for (let key in obj) {
      if (typeof obj[key] === "object") {
        const newKey = key.replace(".", "__");
        changePropName(obj, newKey, key);
        const linkedFields = combineElement(obj[newKey], newKey);
        delete obj[newKey];
        delete result[key];
        result = { ...obj, ...result, ...linkedFields };
      }
    }
    return result;
  };

  const combineElement = (obj, keyName, objResult = {}) => {
    if (Array.isArray(obj)) {
      const uidList = obj.map((elm) => elm.uid);
      objResult[keyName] = uidList.toString();
    } else {
      for (let key in obj) {
        const dependKey = keyName.concat("_") + key;
        const newObj = changePropName(obj, dependKey, key);
        objResult = { ...newObj };
      }
    }
    return objResult;
  };

  const handleData = (arrayData, arrResult = []) => {
    if (arrayData !== null) {
      const cloneArr = clone(arrayData);
      cloneArr.forEach((item) => arrResult.push(handlePlainData(item)));
      return arrResult;
    }
  };

  const excelData = data && handleData(data);

  return (
    <ExcelFile
      element={
        isEditTable ?
          <Button
            variant="contained"
            aria-label="back"
            className={classge.btnExport}
          >
            <GetAppIcon className={classge.expIcon} />
          Export
          </Button>
          :
          <Tooltip title="Export data" aria-label="export data">
            <IconButton variant="contained">
              <GetAppIcon className={classge.expIcon} />
            </IconButton>
          </Tooltip>

      }
    >
      <ExcelSheet data={excelData} name="Collection">
        {excelData && excelData.length
          ? dataFields.map((item, idx) => (
            <ExcelColumn
              key={idx}
              label={`${titleCase(item)} <${item}>`}
              value={item}
            />
          ))
          : {}}
      </ExcelSheet>
    </ExcelFile>
  );
}
