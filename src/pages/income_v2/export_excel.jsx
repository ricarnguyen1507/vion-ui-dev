import React from "react";
import GetAppIcon from "@material-ui/icons/GetApp";
const { api_url } = window.appConfigs
export default function ExportExcel ({...props}) {
  return (
    <a style={{color: "gray"}} href={`${api_url}/income/income_export.xlsx${props?.query_string?? ''}`} target="_blank" tooltip="Export Here" rel="noreferrer">
      <GetAppIcon />
    </a>
  )
}
