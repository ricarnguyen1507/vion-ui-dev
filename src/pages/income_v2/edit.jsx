import React, {
  useState
} from 'react'
import {
  useParams
} from "react-router-dom"
import {
  ValidatorForm
} from 'react-material-ui-form-validator'
import RoleButton from 'components/Role/Button'
import ReturnIcon from '@material-ui/icons/KeyboardReturn'
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Typography from '@material-ui/core/Typography'
import AddBox from '@material-ui/icons/AddBox'
import Autocomplete from '@material-ui/lab/Autocomplete'

import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'

import {ObjectsToForm,genUid} from "utils"
import SelectMultiIncome from 'modules/SelectMulti/income/list_manage'
import api from 'services/api_cms'
import { useUserState, Permission } from "context/UserContext";
import { updateMultiSelectLayout } from 'services/updateMultiSelect'
import {generateOrderId} from 'services/income-id-generator'
import {
  InputEdge,
  Node
} from 'components/GraphMutation'

function parseCurrentDate () {
  const date = new Date()
  const year = String(date.getFullYear()).slice(-2)
  const month = String(date.getMonth() + 1).padStart(2, "0")
  const day = String(date.getDate()).padStart(2, "0")
  return `${year}${month}${day}`
}
export default function ({ ctx, products, setProduct, incTypes}) {
  const classge = useStylesGeneral()
  var { uid } = useUserState();
  const { actionType } = useParams()
  const [dataEdit, setDataEdit] = useState(() => ctx.data ?? {
    uid: '_:new_income',
    income_type: 0,
    quantity: 1,
    "dgraph.type": "Income"
  })

  const [node] = useState(() => new Node(dataEdit))
  const [pricingNode] = useState(() => node.getEdge("income.pricing", { uid: "_:new_pricing", "dgraph.type": "Pricing" }).state)

  const handleIncomeTypeChange = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'income_type': incTypes.indexOf(item) })
    node.setState('income_type', incTypes.indexOf(item))
  }
  const [timer, setTimer] = useState(null)
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = {fulltext_search: val.replace(/["\\]/g, '\\$&')}
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProduct([...newOption])
        })
    }
  }
  function handleSubmit () {
    let date_created = new Date().getTime()
    node.setState('created_at',date_created)
    node.setState('income.user', {uid: uid})
    const today = parseCurrentDate()
    node.setState('income_name', `S-${today}`)
    const formData = ObjectsToForm(node.getMutationObj())
    api.post('/income', formData)
      .then(ctx.goBack)
      .catch(err => {
        console.log(err)
      })
  }
  const updateListIncome = async ({ set }) => {
    let listIncomeSecOld = dataEdit?.['income.section'] ?? []
    if(set.length && products.length){

      let total = 0
      for(let obj of set){
        for(let prod of products){
          if(obj['section.product'] == prod.uid){
            total += obj.quantity * prod.cost_price
          }
        }
      }
      pricingNode.setState('cost_price_with_vat',total)
      setDataEdit({ ...dataEdit, 'cost_price_with_vat': total})
    }
    await updateMultiSelectLayout(set, listIncomeSecOld, node, 'income.section')
  }
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Thu Chi" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
        onError={errors => console.log(errors)}
      >
        <Grid container spacing={2} >
          <Grid item xs={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid style={{paddingTop: 20 }} container spacing={2}>

                <Grid item xs={12}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <SelectMultiIncome
                        listOption={products}
                        currentList={dataEdit?.['income.section'] || []}
                        actionType={actionType}
                        dataEdit={dataEdit}
                        updateListManage={updateListIncome}
                        maxItem="-1"
                        minItem="1"
                        listTitle="Thêm Section"
                        facetPrefix="income.section"
                        inputChange={onInputSelectProductChange}
                        disabled={dataEdit?.['income.section'] ? true : false}
                      />
                    </Grid>
                    <Grid item xs={12}>
                    <TextField
                        fullWidth
                        id="cost_price_with_vat"
                        label="Tổng tiền số tiền"
                        variant="outlined"
                        margin="dense"
                        name="cost_price_with_vat"
                        value={dataEdit?.cost_price_with_vat || dataEdit?.['income.pricing']?.cost_price_with_vat}
                        required
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <InputEdge
                        Component={TextField}
                        node={node}
                        pred={"notes"}
                        fullWidth
                        className={classge.inputAddressType}
                        type="text"
                        label="Ghi chú"
                        variant="outlined"
                        margin="dense"
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Autocomplete
                        options={incTypes}
                        value={incTypes?.[dataEdit?.income_type] || "Thu"}
                        onChange={handleIncomeTypeChange}
                        style={{ width: "100%" }}
                        renderInput={params => (
                          <TextField {...params}
                            label="Loại chi phí"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                          />
                        )}
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton page="income" actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}