import React, {
  useRef,
  useState,
  useMemo
} from 'react'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from '@material-ui/pickers';
import { Grid,IconButton, Tooltip} from "@material-ui/core";
import DateFnsUtils from '@date-io/date-fns';
import useStyles from 'pages/style_general'
import api from 'services/api_cms'
import tblQuery from "services/tblQuery"
import {createLookup} from "utils"
// const { api_url } = window.appConfigs
import ExportExcel from "./export_excel"

export default function TableAds ({ctx, cmsUser,incTypes,setProduct}) {
  const classes = useStyles()
  const table = useRef(null)
  const [lookupUser] = useState(() => createLookup(cmsUser, "uid", "user_name"), [cmsUser])

  const [selectedDateFrom, setSelectedDateFrom] = useState(() => {
    let from = new Date()
    from.setHours(0, 0, 0, 1)
    return from
  });

  const handleDateFromChange = (date) => {
    setSelectedDateFrom(date)
  };
  const [selectedDateTo, setSelectedDateTo] = useState(() => {
    let to = new Date()
    to.setHours(23, 59, 59, 999)
    return to
  });
  const handleDateToChange = (date) => {
    setSelectedDateTo(date)
  }

  const query_string = useMemo(() => {
    let query = {}
    query.selectedDateFrom = (new Date(selectedDateFrom).getTime())
    query.selectedDateTo = (new Date(selectedDateTo).getTime())
    if (!Array.isArray(query)) {
      let k
      let queryStr = ""
      for(k in query) {
        queryStr === "" ? queryStr += `?${k}=${query[k] || ""}` : queryStr += `&${k}=${query[k] || ""}`
      }
      return queryStr
    } else {
      return ""
    }
  }, [selectedDateFrom, selectedDateTo])
  function onRowDelete ({tableData, ...rowData}) {
    return api.delete(`/ads/${rowData.uid}`).then(() => {
      table.current?.onQueryChange()
    }).catch(err => {
      alert('Xóa không thành công')
      console.log(err)
    })
  }

  const [tableProps] = useState(() => {
    if(ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      data: tblQuery("/list/incomes_v2"),
      columns: [
        { title: "Mã đơn hàng", field: "income_name", editable: "never", operator: "regexp"
        },
        {
          title: "Người tạo", field: "income.user", editable: "never", operator: "uid_in",
          lookup: lookupUser,
          render (rowData) {
            const uid = rowData["income.user"]?.uid
            return uid ? lookupUser[uid] : ""
          }
        },
        {
          title: "Tổng tiền", field: "cost_price_with_vat", editable: "never", operator: "uid_in",
          render: rowData => <span>{rowData?.['income.pricing']?.cost_price_with_vat ?? ''}</span>
        },
        {
          title: "Trạng thái hiển thị",
          field: "income_type",
          editable: "never",
          render: (rowData) => <div>{incTypes[rowData.income_type]}</div>,
          lookup: incTypes,
        },
        { title: "Ngày tạo", field: "created_at", editable: "never", operator: "regexp",
          render (rowData) {
            const date = new Date(rowData.created_at).toString()
            return date || ""
          }
        },
      ],
      title: <h1 className={classes.titleTable}>Income</h1>,
      onRowClick ({currentTarget: target}) {
        if(selectedRow != target) {
          if(selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          selectedRow = target
          selectedRow.style.backgroundColor = "#EEE"
        }
      },
      options: {
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        filtering: true,
        sorting: true,
        exportButton: true,
        grouping: true,
        pageSize: 10,
        pageSizeOptions: [10, 25, 50],
        actionsColumnIndex: -1
      }
    }
  })

  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tableProps}
        actions={[
          // {
          //   icon: AddBox,
          //   tooltip: 'Add Income ',
          //   isFreeAction: true,
          //   onClick () {
          //     ctx.editData();
          //   }
          // },
          {
            icon: "edit",
            tooltip: "Edit Income ",
            onClick (e, { tableData, ...rowData }) {
              if(rowData?.['income.product']){
                setProduct([rowData?.['income.product']])
              }
              ctx.editData(rowData, tableData.id);
            }
          }
        ]}
        editable={{onRowDelete}}
        components={{
          Toolbar: () => (
            <div>
              {/* <MTableToolbar {...props} /> */}
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container spacing={2} className={classes.formpd}>
                  <Grid item xs={2} sm={2} lg={2} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Đơn Hàng</h1>
                  </Grid>
                  <Grid item xs={6} sm={6} lg={6}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Grid container justifyContent="space-around">
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Đơn hàng đặt sau ngày"
                          value={selectedDateFrom}
                          onChange={handleDateFromChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                        <KeyboardDatePicker
                          margin="normal"
                          id="date-picker-dialog"
                          label="Đơn hàng đặt trước ngày"
                          format="dd/MM/yyyy"
                          value={selectedDateTo}
                          onChange={handleDateToChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        />
                      </Grid>
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item xs={4} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{ color: "gray", cursor: "pointer" }} onClick={() => { ctx.editData() }} target="_blank" tooltip="Add Income">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Export" aria-label="export">
                      <IconButton style={{color: "gray", cursor: "pointer"}} tooltip="Display Export">
                        <ExportExcel query_string={query_string}/>
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </div>
          ),
        }}
      />
    </div>
  )
}