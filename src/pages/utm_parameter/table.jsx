import React, {
  useContext,
  useState,
  useEffect
} from 'react'

import {
  useHistory,
  useLocation
} from "react-router-dom"

import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import api from 'services/api_cms'

import { context } from 'context/Shared'

export default function TableAds () {
  const classes = useStyles()

  const history = useHistory()
  const { pathname } = useLocation()

  const ctx = useContext(context)

  const [selectedRow, setSelectedRow] = useState(null)

  const [data, setData] = useState([])

  useEffect(() => {
    api.get('parameterUtm').then(response => {
      setData(response.data.result)
    })
  }, [])

  const column = [
    { title: "Tên param", field: "tracking_code", filtering: true, editable: "never" },
    { title: "Diễn giải", field: "short_desc", editable: "never" }
  ]

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={data}
        title={<h1 className={classes.titleTable}>Tracking Utm</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Ads ',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              history.push(`${pathname}/edit`)
            }
          },
          {
            icon: "edit",
            tooltip: "Edit Ads ",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/edit`)
            }
          }
        ]}
        editable={{
          onRowDelete: ({tableData, ...rowData}) => api.delete(`/parameterUtm/${rowData.uid}`).then(() => {
            data.splice(tableData.id, 1)
            setData([...data])
          }).catch(err => {
            alert('Xóa không thành công')
            console.log(err)
          })
        }}
      />
    </div>
  )
}