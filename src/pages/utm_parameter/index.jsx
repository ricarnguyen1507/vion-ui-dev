import React, { useState } from 'react'
import {
  Route,
  useRouteMatch
} from "react-router-dom"

import Table from './table'
import Edit from './edit'

import { Shared } from 'context/Shared'

export default function () {
  const { path } = useRouteMatch()
  const [sharedData] = useState(() => new Map())
  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit />
      </Route>
      <Route exact path={path}>
        <Table />
      </Route>
    </Shared>
  )
}