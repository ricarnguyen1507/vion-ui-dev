import React, {
  useCallback,
  useState
} from 'react'

import { withStyles } from '@material-ui/core'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import MuiTableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import TextField from '@material-ui/core/TextField'
import Button from "@material-ui/core/Button"
import DeleteIcon from '@material-ui/icons/Delete'
import AddBox from '@material-ui/icons/AddBox'

import { InputEdge, NodeTypes } from 'components/GraphMutation'

const TableCell = withStyles({
  root: {
    borderBottom: "none"
  }
})(MuiTableCell)

function TrackingValue ({item, updateList}) {
  const [isDeleted, setIsDeleted] = useState(() => item.isDeleted)
  const ToogleDelete = useCallback(() => {
    setIsDeleted(item.isDeleted = !item.isDeleted)
    updateList()
  }, [item, updateList])
  return (
    <TableRow hover={true}>
      <TableCell>
        <InputEdge Component={TextField}
          node={item.state}
          pred={"val_name"}
          fullWidth
          size="small"
          margin="none"
        />
      </TableCell>
      <TableCell>
        <InputEdge Component={TextField}
          node={item.state}
          pred={"val_code"}
          fullWidth
          size="small"
          margin="none"
        />
      </TableCell>
      <TableCell style={{width: 150, textAlign: 'center'}}>
        <Button
          startIcon={<DeleteIcon />}
          onClick={ToogleDelete}
        >
          { isDeleted ? 'Undelete' : 'Delete' }
        </Button>
      </TableCell>
    </TableRow>
  )
}

export default function ({ addItem, updateList, items }) {
  function addTrackingValue () {
    addItem({
      uid: `_:trackingValue_${new Date().getTime()}`,
      "dgraph.type": "TrackingValue"
    }, NodeTypes.ORPHAN)
  }
  return <>
    <Button
      variant="contained"
      startIcon={<AddBox />}
      style={{ marginTop: 35, marginBottom: 15 }}
      onClick={addTrackingValue}
    >
      Thêm giá trị
    </Button>
    <Table size="small">
      <TableHead>
        <TableRow selected={true}>
          <TableCell>Tên Giá Trị</TableCell>
          <TableCell>Mã Giá Trị</TableCell>
          <TableCell style={{textAlign: 'center'}}>actions</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {items.map(item => <TrackingValue key={item.state.uid} item={item} updateList={updateList} />)}
      </TableBody>
    </Table>
  </>
}