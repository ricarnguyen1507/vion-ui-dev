import React, { useState, useEffect } from 'react'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

import Edit from './edit'
import TableUser from './table'

import api from 'services/api_cms'

export default function () {
  const history = useHistory()
  const {path} = useRouteMatch()
  const [roles, setRoles] = useState([])

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
    }
  }))

  useEffect(function () {
    api.get('/list/role').then(rsp => {
      setRoles(rsp.data.result)
    }).catch(error => console.log(error))
  }, []);

  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        ctx={ctx}
        roles={roles}
      />
    </Route>
    <Route exact path={path}>
      <TableUser ctx={ctx} />
    </Route>
  </>
}
