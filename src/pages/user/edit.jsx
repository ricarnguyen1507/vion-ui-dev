import React, { useState } from 'react';
import { useParams } from "react-router-dom"

import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField';
import api from 'services/api_cms'
import useGeneral from 'pages/style_general'

import {
  Node,
  InputEdge,
  OptionEdge
} from 'components/GraphMutation'

const passValidator = [
  'minStringLength:6',
  'maxStringLength:50'
]

export default function ({ctx, roles}) {
  const classge = useGeneral()

  const {actionType} = useParams()

  const [dataEdit] = useState(() => ctx.data ?? {
    uid: '_:new_user',
    "dgraph.type": "User"
  })

  const [node] = useState(() => new Node(dataEdit))

  function handleSubmit () {
    api.post('/user', node.getMutationForm())
      .then(ctx.goBack)
      .catch(err => {
        console.log(err)
      })
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />

  return (
    <>
      <PageTitle title="User" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={8} lg={8}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node} pred={"user_name"}
                    fullWidth
                    label="Input name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node} pred={"phone_number"}
                    label="Input phone number"
                    variant="outlined"
                    type="string"
                    margin="dense"
                    fullWidth
                    validators={[
                      'maxStringLength:11',
                      'matchRegexp:[0-9]'
                    ]}
                    errorMessages={[
                      'Max phone number is 11',
                      'Number only'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node} pred={"email"}
                    fullWidth
                    label="Input email"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'maxStringLength:100'
                    ]}
                    errorMessages={[
                      'Max length is 100'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <InputEdge
                    Component={TextValidator}
                    node={node} pred={"password"}
                    type="password"
                    label="Input password"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    validators={(actionType === 'Add' || dataEdit.password) ? passValidator : []}
                    errorMessages={[
                      'Min length is 6',
                      'Max length is 32'
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid item xs={12} sm={4} lg={4}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Option
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={12} lg={12}>
                  <OptionEdge
                    node={node}
                    pred={"role"}
                    options={roles}
                    getOptionLabel={o => o['role_name'] ?? ""}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Select role"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>

        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} />
              Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
