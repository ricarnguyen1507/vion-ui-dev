import React, { useRef, useState } from "react";
import MaterialTable from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import useStyles from "pages/style_general";
import api from 'services/api_cms';
import tblQuery from "services/tblQuery"

export default function TableUser ({ ctx }) {
  const classes = useStyles();
  const table = useRef(null)

  const [editable, setEditable] = useState(false)

  function onRowDelete ({tableData, ...rowData}) {
    return api.delete(`/user/${rowData.uid}`).then(() => {
      table.current?.onQueryChange()
    }).catch(err => {
      alert('Xóa không thành công')
      console.log(err)
    })
  }

  const [tblProps] = useState(() => {
    if(ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      data: tblQuery("/list/user"),
      columns: [
        { title: "User Name", field: "user_name", operator: 'regexp', editable: "never" },
        { title: "Phone Number", field: "phone_number", operator: 'regexp', editable: "never" },
        { title: "Email", field: "email", operator: 'regexp', editable: "never" },
        {
          title: "Role",
          field: "role_name",
          editable: "never",
          filtering: false,
          render: ({role}) => (
            <div>{role?.role_name ?? ""}</div>
          ),
        }
      ],
      title: <h1 className={classes.titleTable}>User</h1>,
      onRowClick (e) {
        if(selectedRow != e.currentTarget) {
          if(selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          e.currentTarget.style.backgroundColor = "#EEE"
          selectedRow = e.currentTarget
        }
      },
      onRowsPerPageChange (v) { this.options.pageSize = v },
      onPageChange (v) { this.options.page = v },
      options: {
        page: 0,
        pageSize: 10,
        pageSizeOptions: [10, 25, 50],
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        search: false,
        filtering: true,
        sorting: true,
        exportButton: false,
        grouping: false,
        actionsColumnIndex: -1
      }
    }
  })

  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tblProps}
        actions={[
          {
            icon: AddBox,
            tooltip: "Add User",
            isFreeAction: true,
            onClick () {
              ctx.editData();
            },
          },
          {
            icon: "edit",
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick () {
              setEditable(!editable);
            }
          },
          editable ? {
            icon: "edit",
            tooltip: "Edit User",
            onClick (e, { tableData, ...rowData }) {
              ctx.editData(rowData, tableData.id);
            },
          } : null
        ]}
        editable={editable ? {
          onRowDelete
        } : null}
      />
    </div>
  );
}
