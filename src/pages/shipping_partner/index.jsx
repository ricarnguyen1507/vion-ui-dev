import React, { useState } from 'react';
import api from 'services/api_cms';
import TableShippingPartner from './table'
import Edit from './edit';
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [actionType, setActionType] = useState('')
  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  const [stateQuery, setStateQuery] = useState(() => ({
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }

  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setDataEdit(row)
    setIndexEdit(index)
  }
  const handleDelete = (row) => api.post('/shipping-partner', { del: { uid: row.uid } })
    .then(res => {
      if(res.status === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  // Submit data ( Mode : Edit || Add )
  const handleSubmitData = (actionType, data) => {
    if(Object.keys(data).length > 0) {
      api.post('/shipping-partner', data)
        .then(response => {
          const { set } = data
          if(actionType === 'Add') {
            set.uid = response.data.uids['new_brand']
            setDataTable([set, ...dataTable])
          } else if(actionType === 'Edit') {
            dataTable[indexEdit] = {...dataTable[indexEdit], ...set}
            setDataTable([...dataTable])
          }
          setDataEdit(null)
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      ctx.goBack()
    }
  }
  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        actionType={actionType}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
      />
    </Route>
    <Route exact path={path}>
      <TableShippingPartner
        ctx={ctx}
        data={dataTable}
        controlEditTable={controlEditTable}
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
        setDataTable={setDataTable}
        setControlEditTable={setControlEditTable}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
      />
    </Route>
  </>
}