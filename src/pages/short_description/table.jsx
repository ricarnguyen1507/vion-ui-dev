import React, { useState } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox';
import useStyles from 'pages/style_general'
import Loading from 'components/Loading';
import { useHistory } from "react-router-dom";

/* const STATUS_LIST = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
} */
const RELOAD_CODE = 0;
const parseDate = (unixDate) => {
  if(!unixDate)
    return ''
  const d = new Date(unixDate)
  return `${String(d.getHours()).padStart(2, '0')}:${String(d.getMinutes()).padStart(2, '0')} ${String(d.getDate()).padStart(2, '0')}/${String(d.getMonth() + 1).padStart(2, '0')}/${d.getFullYear()}`
}

export default function TableMusic ({ data, functionEditData, getData, stateQuery = {}, loadingText = '', loadingCode }) {
  const history = useHistory();
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const [column] = useState(() => {
    const column = [
      { title: "Uid", field: "uid", editable: "onUpdate" },
      { title: "Tên", field: "display_name", editable: "onUpdate" },
      { title: "Nội dung", field: "short_des", editable: "onUpdate", filtering: false, },
      {
        title: "Ngày tạo", field: "created_at", editable: 'onUpdate', filtering: false,
        render: rowData => <span>{parseDate(rowData?.['created_at'] ?? null)}</span>,
      },
      {
        title: "Cập nhật", field: "updated_at", editable: 'onUpdate', filtering: false,
        render: rowData => <span>{parseDate(rowData?.['updated_at'] ?? null)}</span>,
      },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  });

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={data}
        title={<h1 className={classes.titleTable}>Mô tả ngắn</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        onFilterChange={e => getData({filters: e})}
        onPageChange={page => getData({page})}
        onRowsPerPageChange={pageSize => getData({pageSize})}
        onSearchChange={e => getData({search: e})}
        isLoading={loadingText !== null && loadingText.length > 0}
        page={stateQuery?.page ?? 0}
        totalCount={stateQuery?.totalCount ?? data?.length ?? 0}
        components={{
          OverlayLoading: _props => (
            <Loading statusCode={loadingCode} statusText={loadingText} style={{backgroundColor: 'rgba(255, 255, 255, 0.7)'}}>
              {loadingCode === RELOAD_CODE && <button style={{position: 'absolute', top: '60%', left: '50%', transform: 'translate(-50%,-50%)'}} onClick={() => history.go(0)}>Tải lại</button>}
            </Loading>)
        }}
        options={{
          loadingType: 'overlay',
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: stateQuery?.pageSize ?? 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Thêm mới',
            isFreeAction: true,
            onClick: () => {
              functionEditData('Add')
            }
          },
          rowData => ({
            icon: 'edit',
            tooltip: "Sửa",
            onClick: () => {
              functionEditData('Edit', rowData)
            },
          })
        ]}
      />
    </div>
  )
}