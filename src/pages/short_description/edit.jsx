import React, { useState, useMemo } from 'react';
import {
  useParams
} from "react-router-dom";
import useStyles from './edit_style'
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import useGeneral from 'pages/style_general'
import { diff } from 'services/diff'
import ColorPicker from 'pages/product/edit/color_product'

/* const STATUS_LIST = [
  {
    id: 0,
    name: 'PENDING',
  },
  {
    id: 1,
    name: 'REJECTED',
  },
  {
    id: 2,
    name: 'APPROVED',
  }
] */

export default function ({ data = [], functionBack, handleSubmitData }) {
  const { id } = useParams();
  const classes = useStyles()
  const [dataEdit, setDataEdit] = useState({
    'display_name': '',
    'short_des': '',
    'short_des_1': '',
    'short_des_2': '',
    'short_des_3': '',
    'short_des_4': '',
    'display_status': 2,
    'is_default': false,
    short_des_1_color: "#000",
    short_des_2_color: "#000",
    short_des_3_color: "#000",
    short_des_4_color: "#000",
    short_des_color: "#000"
  });
  const actionType = id === 'new' ? 'Add' : 'Edit';
  const originData = useMemo(() => {
    const temp = data?.find(item => id && id !== 'new' && item.uid === id) ?? {};
    setDataEdit({...dataEdit, ...temp})
    return temp;
  }, [data, id])
  const classge = useGeneral()

  const handleSubmit = () => {
    const { set, del } = diff(originData, dataEdit, dataEdit.uid)
    const submitData = {
      set: set ?? {},
      del: del ?? {},
    }
    handleSubmitData(submitData)
  };

  const onError = (e) => {
    console.log("lỗi", e.name);
  }
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setDataEdit({ ...dataEdit, [name]: value })
  }
  const handleChangeCheckBox = ({target: {name, checked}}) => {
    setDataEdit({ ...dataEdit, [name]: checked })
  }
  /* const handleChangeStatus = function (e, item) {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': item ? item.id : STATUS_LIST[0].id })
  } */

  // const display_status = useMemo(() => STATUS_LIST.find(item => item.id === (dataEdit?.['display_status'] ?? '')) ?? null, [dataEdit])

  const ActionIcon = <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Mô tả ngắn" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root} onError={onError}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin
                </Typography>
                <FormControlLabel
                  control={
                    <Checkbox
                      className={classes.titleProduct}
                      id="is_default"
                      name="is_default"
                      color="primary"
                      checked={dataEdit['is_default']}
                      onChange={handleChangeCheckBox}/>
                  }
                  label="Mặc định"
                />
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    className={classge.inputData}
                    id="display_name"
                    label="Tên"
                    variant="outlined"
                    margin="dense"
                    name="display_name"
                    onChange={handleChange}
                    value={dataEdit['display_name']}
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:200',
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 200',
                      'Character is not accept ']}
                  />

                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <Grid container spacing={2} className={classes.formpd}>
                    <Grid item xs={10} sm={10} lg={10}>
                      <TextValidator
                        className={classes.priceText}
                        id="short_des"
                        name="short_des"
                        label="Nội dung"
                        type="text"
                        size="small"
                        variant="outlined"
                        fullWidth
                        onChange={handleChange}
                        value={dataEdit.short_des}
                        validators={[
                          'required',
                          'minStringLength:3',
                          'maxStringLength:60',
                          // 'matchRegexp: [a-zA-Z0-9 ]'
                        ]}
                        errorMessages={[
                          'This field is required',
                          'Min length is 3',
                          'Max length is 60',
                          'Character is not accept ']}
                      />
                    </Grid>
                    <Grid item xs={2} sm={2} lg={2}>
                      <ColorPicker
                        color={dataEdit.short_des_color || "#000"}
                        handleColor={(c) => { setDataEdit({...dataEdit, short_des_color: c.hex}) }}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} className={classes.formpd}>
                    <Grid item xs={10} sm={10} lg={10}>
                      <TextValidator
                        className={classes.priceText}
                        disabled
                        id="short_des_1"
                        name="short_des_1"
                        label="Dòng 1"
                        type="text"
                        size="small"
                        variant="outlined"
                        fullWidth
                        onChange={handleChange}
                        value={dataEdit.short_des_1}
                      />
                    </Grid>
                    <Grid item xs={2} sm={2} lg={2}>
                      <ColorPicker
                        disabled
                        color={dataEdit.short_des_1_color || "#000"}
                        handleColor={(c) => { setDataEdit({...dataEdit, short_des_1_color: c.hex}) }}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} className={classes.formpd}>
                    <Grid item xs={10} sm={10} lg={10}>
                      <TextValidator
                        className={classes.priceText}
                        disabled
                        id="short_des_2"
                        name="short_des_2"
                        label="Dòng 2"
                        type="text"
                        size="small"
                        variant="outlined"
                        fullWidth
                        onChange={handleChange}
                        value={dataEdit.short_des_2}
                      />
                    </Grid>
                    <Grid item xs={2} sm={2} lg={2}>
                      <ColorPicker
                        disabled
                        color={dataEdit.short_des_2_color || "#000"}
                        handleColor={(c) => { setDataEdit({...dataEdit, short_des_2_color: c.hex}) }}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} className={classes.formpd}>
                    <Grid item xs={10} sm={10} lg={10}>
                      <TextValidator
                        className={classes.priceText}
                        x disabled
                        id="short_des_3"
                        name="short_des_3"
                        label="Dòng 3"
                        type="text"
                        size="small"
                        variant="outlined"
                        fullWidth
                        onChange={handleChange}
                        value={dataEdit.short_des_3}
                      />
                    </Grid>
                    <Grid item xs={2} sm={2} lg={2}>
                      <ColorPicker
                        disabled
                        color={dataEdit.short_des_3_color || "#000"}
                        handleColor={(c) => { setDataEdit({...dataEdit, short_des_3_color: c.hex}) }}
                      />
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} className={classes.formpd}>
                    <Grid item xs={10} sm={10} lg={10}>
                      <TextValidator
                        className={classes.priceText}
                        disabled
                        id="short_des_4"
                        name="short_des_4"
                        label="Dòng 4"
                        type="text"
                        size="small"
                        variant="outlined"
                        fullWidth
                        onChange={handleChange}
                        value={dataEdit.short_des_4}
                      />
                    </Grid>
                    <Grid item xs={2} sm={2} lg={2}>
                      <ColorPicker
                        disabled
                        color={dataEdit.short_des_4_color || "#000"}
                        handleColor={(c) => { setDataEdit({...dataEdit, short_des_4_color: c.hex}) }}
                      />
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={6} sm={6} lg={6}>
                  <p><strong>{`{price}`}</strong>: chênh lệch giá bán và giá niêm yết</p>
                  <p><strong>{`{percent}`}</strong>: tỉ lệ chênh lệch giá bán và giá niêm yết</p>
                  <p><strong>{`{origin}`}</strong>: xuất xứ sản phẩm</p>
                  <p><strong>{`{brand}`}</strong>: Brand sản phâm</p>
                </Grid>
                <Grid item xs={6} sm={6} lg={6}>
                  <p><strong>{`{supplier}`}</strong>: Nhà cung cấp</p>
                  <p><strong>{`{brandshopname}`}</strong>: Tên hiển thị trong sản phẩm</p>
                  <p><strong>{`{warranty}`}</strong>: Chính sách bảo hành sản phẩm</p>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
