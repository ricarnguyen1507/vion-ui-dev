import React, { useState, useEffect } from 'react';
import {
  useHistory,
  Route,
  Switch
} from "react-router-dom";
import api from 'services/api_cms';
import Loading from 'components/Loading';
import TableMusic from './table'
import Edit from './edit';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
function Alert (props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const RELOAD_CODE = 0;

export default function () {
  const [dataTable, setDataTable] = useState([])
  const [actionType] = useState('')
  const [openMessage, setOpenMessage] = React.useState(false);
  const [snackbarMessage, setMessage] = React.useState('');
  const history = useHistory()

  const [loadingText, setLoadingText] = useState('');
  const [loadingCode, setLoadingCode] = useState();

  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))

  function getFilterStr ({filters}) {
    let strFunc = filters.map(({value, column: {o}}) => {
      if(typeof value === 'string') {
        value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
        if(value) {
          if (!o) {
            return value
          }
        }
      }
    })

    const strFilter = filters.map(({value, column: {o, field}}) => {
      if(typeof value === 'string') {
        value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
        if(value) {
          if (o) {
            return `${o}(${field},"${value}")`
          }
        }
      } else if(typeof value === 'number' || typeof value === 'boolean') {
        return `${o ?? 'eq'}(${field},${value})`
      } else if(Array.isArray(value)) {
        return value.map(v => `${o}(${field},${v})`).join(' OR ')
      }
      return ""
    }).filter(v => v.trim() !== "")

    strFunc && strFunc.length > 0 && strFunc.join('') !== "" ? strFunc = `func: alloftext(display_name, "${strFunc.join(' ')}")` : strFunc = "";
    return {strFunc, strFilter: strFilter.join(' AND ')}
  }

  function getData (query = {}) {
    setLoadingText("Đang lấy dữ liệu")
    const queryData = {
      ...stateQuery,
      ...query
    }
    setStateQuery(queryData)
    const {strFunc, strFilter} = getFilterStr(queryData)
    try{
      api.post(`/short-description-list`, {
        number: query?.pageSize ?? 10,
        page: query?.page ?? 0,
        ...(strFilter && {filter: strFilter}),
        ...(strFunc && {func: strFunc})
      }).then(res => {
        const { summary: {totalCount, page, offset}, data } = res.data
        setDataTable(data)
        setStateQuery({
          ...stateQuery,
          totalCount,
          page,
          offset
        })
        setLoadingText("")
        console.log(stateQuery)
      }).catch(err => {
        //    setDataTable([])
        //    setStateQuery({
        //         ...stateQuery,
        //         totalCount: 0,
        //         page : 0,
        //         offset: 0
        //     })
        setLoadingText("Lỗi: " + err.message)
        setLoadingCode(RELOAD_CODE)

      })
    } catch (err) {
      // setDataTable([])
      // setStateQuery({
      //     ...stateQuery,
      //     totalCount: 0,
      //     page : 0,
      //     offset: 0
      // })
      setLoadingText("Lỗi: " + err.message)
      setLoadingCode(RELOAD_CODE)
    }
  }

  const handleOpenMessage = (message) => {
    setMessage(message)
    setOpenMessage(true);
  };

  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenMessage(false);
  };
    // Load data
  useEffect(() => {
    getData({})
  }, []);

  // Function handle
  const functionBack = () => {
    history.goBack()
  }
  const functionEditData = (actionType, row) => {
    if (row && row.uid) {
      history.push(`/app/short-description/${row.uid}`)
    } else {
      history.push(`/app/short-description/new`)
    }
  }
  const handleDelete = (row) => api.delete(`/delete/${row.uid}`)
    .then(() => {
      const idx = dataTable.findIndex(d => d.uid === row.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  // Submit data ( Mode : Edit || Add )
  const handleSubmitData = (data) => {
    if (Object.keys(data).length > 0) {
      const formData = new FormData()
      formData.set('set', JSON.stringify(data.set))
      formData.set('del', JSON.stringify(data.del))
      api.post('/short-description', formData, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data"
        }
      }).then(() => {
        history.goBack();
        getData({})
      }).catch(error => {
        handleOpenMessage(error.response.data.message)
      })
    }
  }

  return dataTable === null ? <Loading /> : (
    <Switch>
      <Route
        path="/app/short-description"
        exact={true}
      >
        <TableMusic
          data={dataTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          stateQuery={stateQuery}
          getData={getData}
          loadingCode={loadingCode}
          loadingText={loadingText}
        />
      </Route>
      <Route
        path="/app/short-description/:id"
        exact={true}
      >
        <>
          <Snackbar open={openMessage} autoHideDuration={6000} onClose={handleCloseMessage}>
            <Alert onClose={handleCloseMessage} severity="error">
              {snackbarMessage}
            </Alert>
          </Snackbar>
          <Edit
            actionType={actionType}
            data={dataTable}
            functionBack={functionBack}
            handleSubmitData={handleSubmitData}
          />
        </>
      </Route>
    </Switch>
  )
}