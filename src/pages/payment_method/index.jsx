import React, { useState } from 'react';
import TablePayMethod from './table'
import Edit from './edit';
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const display_status = ['PENDING', 'REJECTED', 'APPROVED']

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [provinces] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  // Function handle
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))

  const functionBack = () => {
    history.push(`${path}`)
  }
  /* const handleDelete = (row) => api.post('/payment_method/' + row.uid)
    .then(res => {
      if (res.status === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error)) */


  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          functionBack={functionBack}
          displayStatus={display_status}
        />
      </Route>
      <Route exact path={path}>
        <TablePayMethod
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setControlEditTable={setControlEditTable}
          displayStatus={display_status}
          listProvinces={provinces}
        />
      </Route>
    </Shared>
  )
}