import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip } from "@material-ui/core";
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"
const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
function getFilterStr ({filters}) {
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  return {strFilter: strFilter.join(' AND ')}
}
export default function TablePayMethod ({ stateQuery, setStateQuery, setDataTable, controlEditTable, setControlEditTable, displayStatus }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get("/list/payment_method", {
      params: {
        number: query.pageSize || 10,
        page: query.page || 0,
        ...{filter: (strFilter ? strFilter : '')},
      }
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái",
        field: "display_status",
        editable: "onUpdate",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq'
      },
      { title: "Phương thức thanh toán", field: "payment_name", editable: "onUpdate", o: 'eq' }
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Nhà Sản Xuất</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{ padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{ textAlign: "left" }}>
                    <h1 className={classes.titleTable}>Payment Method</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{ color: "gray", cursor: "pointer" }} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank" tooltip="Add Payment Method">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{ color: "gray", cursor: "pointer" }} onClick={() => { setControlEditTable(!controlEditTable) }} tooltip="Display Edit">
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )
        }}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Supplier",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}