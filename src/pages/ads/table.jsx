import React, {
  useRef,
  useState,
  useEffect
} from 'react'
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
import api from 'services/api_cms'
import tblQuery from "services/tblQuery"
import {createLookup} from "utils"

const activeBox = { width: "40px", height: "40px", border: "1px solid cyan", borderRadius: "25px" }

function ActiveStatus ({rowData, onChanged}) {
  const elm = useRef(null)
  function toggle (active) {
    elm.current.style.backgroundColor = active ? "orange" : "#FAFAFA"
  }
  function clickHandle () {
    toggle(rowData.is_active = !rowData.is_active)
    onChanged(rowData)
  }
  useEffect(() => { toggle(rowData.is_active) }, [rowData])
  return <div ref={elm} onClick={clickHandle} style={activeBox}>&nbsp;</div>
}

export default function TableAds ({ctx, customerGroup}) {
  const classes = useStyles()
  const table = useRef(null)
  const [lookupCusGroup] = useState(() => createLookup(customerGroup, "uid", "group_customer_name"), [customerGroup])

  function handleChangeCheckBox ({uid, is_active}) {
    api.put("/ads/active", { uid, is_active }).catch(err => {
      console.log(err)
      alert(`Active ads ${uid} failed`)
    })
  }

  function onRowDelete ({tableData, ...rowData}) {
    return api.delete(`/ads/${rowData.uid}`).then(() => {
      table.current?.onQueryChange()
    }).catch(err => {
      alert('Xóa không thành công')
      console.log(err)
    })
  }

  const [tableProps] = useState(() => {
    if(ctx.tblProps) {
      return ctx.tblProps
    }
    let selectedRow
    return ctx.tblProps = {
      data: tblQuery("/list/ads"),
      columns: [
        { title: "ads type", field: "ads_type", editable: "never", operator: "regexp" },
        {
          title: "customer group", field: "ads.group_customer", editable: "never", operator: "uid_in",
          lookup: lookupCusGroup,
          render (rowData) {
            const uid = rowData["ads.group_customer"]?.uid
            return uid ? lookupCusGroup[uid] : ""
          }
        },
        { title: "product uid", field: "click", editable: "never", operator: "regexp"},
        { title: "notes", field: "notes", editable: "never", operator: "regexp"},
        {
          title: "Active", field: "is_active", editable: "never", type: "boolean", operator: "eq",
          render: rowData => <ActiveStatus rowData={rowData} onChanged={handleChangeCheckBox} />
        },
        {
          title: "Image", field: "link", editable: "never", filtering: false,
          render: rowData => <ImgCdn src={rowData.link} style={{ width: 128, height: 72 }} />
        }
      ],
      title: <h1 className={classes.titleTable}>Ads</h1>,
      onRowClick ({currentTarget: target}) {
        if(selectedRow != target) {
          if(selectedRow) {
            selectedRow.style.backgroundColor = "#FFF"
          }
          selectedRow = target
          selectedRow.style.backgroundColor = "#EEE"
        }
      },
      options: {
        headerStyle: {
          fontWeight: 600,
          background: "#f3f5ff",
          color: "#6e6e6e",
        },
        filtering: true,
        sorting: true,
        exportButton: true,
        grouping: true,
        pageSize: 10,
        pageSizeOptions: [10, 25, 50],
        actionsColumnIndex: -1
      }
    }
  })

  return (
    <div className="fade-in-table">
      <MaterialTable tableRef={table} {...tableProps}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Ads ',
            isFreeAction: true,
            onClick () {
              ctx.editData();
            }
          },
          {
            icon: "edit",
            tooltip: "Edit Ads ",
            onClick (e, { tableData, ...rowData }) {
              ctx.editData(rowData, tableData.id);
            }
          }
        ]}
        editable={{onRowDelete}}
      />
    </div>
  )
}