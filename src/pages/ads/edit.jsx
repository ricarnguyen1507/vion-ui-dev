import React, {
  useState
} from 'react'
import {
  useParams
} from "react-router-dom"
import {
  ValidatorForm,
  TextValidator
} from 'react-material-ui-form-validator'
import RoleButton from 'components/Role/Button'
import ReturnIcon from '@material-ui/icons/KeyboardReturn'
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Typography from '@material-ui/core/Typography'
import AddBox from '@material-ui/icons/AddBox'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import Media from 'components/Media'

import {ObjectsToForm} from "utils"

import api from 'services/api_cms'

import {
  OptionEdge,
  InputEdge,
  Node
} from 'components/GraphMutation'

import { genCdnUrl } from 'components/CdnImage'

const adsTypeLabel = {
  product: "Product UID",
  collection: "Collection UID",
  collection_temp: "BST UID",
  brandShop: "Brandshop UID",
  landing_page: "LandingPage UID"
}

export default function ({ ctx, adsTypes, trackingUtm, customerGroup }) {
  const classge = useStylesGeneral()

  const { actionType } = useParams()

  const [dataEdit] = useState(() => ctx.data ?? {
    uid: '_:new_ads',
    ads_type: adsTypes[0],
    "dgraph.type": "Ads"
  })

  const [node] = useState(() => new Node(dataEdit))
  const [files] = useState(() => ({}))

  const [uidAdsTypeLable, setUidAdsTypeLabel] = useState(() => adsTypeLabel[dataEdit.ads_type] || "")

  function handleAdsTypeChange (e, item) {
    setUidAdsTypeLabel(adsTypeLabel[item])
  }
  function handleFileChange (field, file) {
    node.setState('mime_type', file.type)
    node.setState(field, `images/ads_images/${file.md5}.${file.type.split('/')[1]}`)
    files[file.md5] = file
  }

  function handleSubmit () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post('/ads', formData)
      .then(ctx.goBack)
      .catch(err => {
        console.log(err)
      })
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Ads" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
        onError={errors => console.log(errors)}
      >
        <Grid container spacing={2} >
          <Grid item xs={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                  Ads
                </Typography>
              </div>
              <Divider />
              <Grid style={{paddingTop: 20 }} container spacing={2}>
                <Grid item>
                  <Media src={genCdnUrl(dataEdit.link)} type="image" style={{ width: 512, height: 256 }} fileHandle={handleFileChange} field="link" accept="image/png, image/jpeg" />
                  <h4>Ads image (1600x900)</h4>
                </Grid>
                <Grid item xs={6}>
                  <Grid container spacing={2}>
                    <Grid item xs={12}>
                      <OptionEdge
                        node={node}
                        pred={"ads_type"}
                        onChange={handleAdsTypeChange}
                        options={adsTypes}
                        renderInput={params => (
                          <TextField {...params}
                            label="Ads Type"
                            fullWidth
                            variant="outlined"
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <OptionEdge
                        node={node}
                        pred={"tracking_utm"}
                        options={trackingUtm || []}
                        getOptionLabel={option => option?.tracking_name ?? ""}
                        val={trackingUtm.find(g => g.uid === dataEdit["tracking_utm"]?.uid)}
                        renderInput={params => (
                          <TextField {...params}
                            label="Tracking Utm"
                            fullWidth
                            variant="outlined"
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <OptionEdge
                        node={node}
                        pred={"ads.group_customer"}
                        options={customerGroup || []}
                        getOptionLabel={option => option?.group_customer_name ?? ""}
                        val={customerGroup.find(g => g.uid === dataEdit["ads.group_customer"]?.uid)}
                        renderInput={params => (
                          <TextField {...params}
                            label="Customer Group"
                            fullWidth
                            variant="outlined"
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <InputEdge
                        Component={TextValidator}
                        node={node}
                        pred={"click"}
                        fullWidth
                        className={classge.inputAddressType}
                        type="text"
                        label={uidAdsTypeLable}
                        variant="outlined"
                        margin="dense"
                        validators={[
                          'required',
                          'minStringLength:3',
                          'maxStringLength:16'
                        ]}
                        errorMessages={[
                          'This field is required',
                          'Min length is 3',
                          'Max length is 16'
                        ]}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <InputEdge
                        Component={TextField}
                        node={node}
                        pred={"notes"}
                        fullWidth
                        className={classge.inputAddressType}
                        type="text"
                        label="Notes"
                        variant="outlined"
                        margin="dense"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={ctx.goBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton page="ads" actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}