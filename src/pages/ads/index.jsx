import React, { useState, useEffect } from 'react'

import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

import api from 'services/api_cms'
import tblQuery from "services/tblQuery"
import Table from './table'
import Edit from './edit'

const adsTypes = ['product', 'collection', 'collection_temp', 'brandShop', 'landing_page']

export default function () {
  const history = useHistory()
  const {path} = useRouteMatch()

  const [ready, setReady] = useState(false)
  const [customerGroup, setCustomerGroup] = useState(null)
  const [trackingUtm, setTrackingUtm] = useState(null)

  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
    },
    tblData: tblQuery("/list/ads")
  }))


  // Load data
  useEffect(() => {
    const requests = [
      '/trackingUtm/get-option',
      '/group_customer/get-option'
    ].map(uri => api.get(uri))

    Promise.all(requests)
      .then(responses => responses.map(r => r.data.result))
      .then(([trackingUtm, customerGroup]) => {
        setTrackingUtm(trackingUtm)
        setCustomerGroup(customerGroup)
        setReady(true)
      })
      .catch(error => {
        console.log(error)
      })
  }, [])

  if(ready)
    return <>
      <Route path={`${path}/:actionType`}>
        <Edit
          ctx={ctx}
          adsTypes={adsTypes}
          trackingUtm={trackingUtm}
          customerGroup={customerGroup}
        />
      </Route>
      <Route exact path={path}>
        <Table
          ctx={ctx}
          customerGroup={customerGroup}
          adsTypes
        />
      </Route>
    </>
  return "loading..."
}