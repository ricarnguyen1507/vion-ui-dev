import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import {LoadingContext} from "context/LoadingContext";
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"
function fetchData (url) {
  return api.get(url, {
    responseType: "json"
  })
    .catch(err => {
      console.log(err)
    })
}

/**
 * Export
 */

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const { setIsLoading } = React.useContext(LoadingContext);
  const [dataTable, setDataTable] = useState(null)
  const [insteadTempCollections, setInsteadTempCollections] = useState(null)

  const [controlEditTable, setControlEditTable] = useState(false)
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  // const [mapTypes, setMapTypes] = useState(null)

  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [products, setProducts] = useState([])
  const reference_type = ['Products', 'Collection']
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
      if (row && row['highlight.products']) {
        setProducts(row['highlight.products'])
      }
    }
  }))
  const functionBack = () => {
    history.push(`${path}`)
  }
  useEffect(() => {
    const fetchs = [
      "/list/collection?t=200&option_fields=true",
      "/list/collection?t=0&option_fields=true"
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([
      hotdeals,
      instead_temp_collecitons
    ]) => {
      hotdeals?.forEach(h => {
        dataTable?.unshift(h)
      })
      setInsteadTempCollections(instead_temp_collecitons)
    })
  }, []);

  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setIndexEdit(index)
    // setMode("edit")
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang
  const handleSubmitData = async (actionType, { set, del, files }) => {
    // Add / Update
    if (Object.keys(set).length > 0) {
      const formData = new FormData()
      formData.set('set', JSON.stringify(set))
      formData.set('del', JSON.stringify(del))
      for(let field in files) {
        formData.set(field, files[field])
      }
      api.post('/collection', formData, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data"
        }
      })
        .then(response => {
          if (actionType === 'Add') {
            set.uid = response?.data?.['new_collection']?.[0]?.uid
            setDataTable([set, ...dataTable])
          } else if (actionType === 'Edit') {
            const updateItem = response?.data?.['new_collection']?.['result']?.[0]
            dataTable[indexEdit] = { ...dataTable[indexEdit], ...updateItem }
            setDataTable([...dataTable])
          }
          setIsLoading(false)
          // setMode("table")
          setDataEdit(null)
          functionBack()
        })
        .catch(error => console.log(error))
    } else {
      // setMode("table")
      functionBack()
    }
  }

  const handleDelete = (row) => api.delete(`/collection/${row.uid}`)
    .then(res => {
      if (res.status === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if(r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  /* const validDataTable = (data) => {
    if (data === null) {
      // setIsLoading(true);
      return true;
    }
    // setIsLoading(false);
    return false;
  } */
  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          actionType={actionType}
          originData={dataEdit}
          functionBack={functionBack}
          handleSubmitData={handleSubmitData}
          ReferenceType={reference_type}
          products={products}
          setProducts={setProducts}
          collections={insteadTempCollections}
        />
      </Route>
      <Route exact path={path}>
        <TableCollection
          ctx={ctx}
          data={dataTable}
          setProducts={setProducts}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setDataTable={setDataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          ReferenceType={reference_type}
        />
      </Route>
    </Shared>
  )
}
