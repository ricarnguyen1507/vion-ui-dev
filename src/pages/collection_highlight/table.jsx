import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
// import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

function getFilterStr ({filters}) {
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  return {strFilter: strFilter.join(' AND ')}
}
export default function TableCollection ({stateQuery, setStateQuery, controlEditTable, setControlEditTable, setProducts, ReferenceType, setDataTable }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get("/list/collection?t=0&is_temp=false", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...{filter: (strFilter ? ' AND ' + strFilter : '')}
    }})
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        // Data Collection
        // collectionData = setOrderList(data?.filter(r => r.is_deleted !== true));
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Loại Tham chiếu",
        field: "reference_type",
        render: (rowData) => <div>{ReferenceType[rowData.reference_type] || "Products"}</div>,
        filtering: false
      },
      {
        title: "Tên Ngành Hàng",
        field: "collection_name",
        render: rowData => !rowData.parent ? rowData.collection_name : <p>&nbsp;~&nbsp;&nbsp;{rowData.collection_name}</p>,
        o: 'eq'

      },
      {
        title: "Tên hiển thị",
        field: "highlight_name",
        o: 'eq'
      },
      {
        title: "Ảnh 16:9 của Highlight",
        field: "image_highlight",
        render: rowData => <ImgCdn src={rowData.image_highlight} style={{ width: 128, height: 72 }} />,
        filtering: false
      },
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })


  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        parentChildData={(row, rows) => rows.find(r => row.parent && (r.uid === row.parent.uid))}
        // title={<h1 className={classes.titleTable}>Ngành hàng</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Ngành hàng nổi bật</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Highlight",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              setProducts(rowData['highlight.products'] || [])
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}
