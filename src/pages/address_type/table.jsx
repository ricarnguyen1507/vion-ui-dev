import React, {useState} from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import api from 'services/api_cms'

export default function TableAddressType ({ctx, controlEditTable, setControlEditTable, setDataTable}) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null)
  function getFilterStr (query) {
    const { filters } = query
    var arrfilter = []
    filters?.map(({value, column: {field}}) => {
      let reSearch = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      return arrfilter.push(`regexp(${field},/${reSearch}/)`)
    })
    return { strFilter: arrfilter.join(' AND ')}
  }
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0
  }))
  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const { strFilter } = getFilterStr(query)
    return api.get("/list/addresstype", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...(strFilter && {filter: ' AND ' + strFilter}),
    }})
      .then(response => {
        const { summary: [{totalCount}], result } = response.data
        setDataTable(result)
        return {
          data: result,
          page: query.page,
          pageSize: query.pageSize,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const column = [
    { title: "Address Type Name", field: "type_name", editable: "onUpdate" },
    { title: "Address Type Value", field: "type_value", editable: "onUpdate" },
  ]
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>Address Type</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Address Type ',
            isFreeAction: true,
            onClick: () => {
              ctx.editData()
            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Address Type ",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.editData(rowData)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}