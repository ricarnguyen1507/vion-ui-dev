import React, {useState} from 'react';
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import AddBox from '@material-ui/icons/AddBox'
import PageTitle from "components/PageTitle"
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import { diff, clone } from 'services/diff'

export default function (props) {
  const { originData, actionType, functionBack, handleSubmitData } = props
  const classge = useStylesGeneral()
  console.log(actionType)
  const [dataEdit, setDataEdit] = useState(() => {
    if(actionType === 'Add') {
      return { uid: '_:new_address_type' }
    }
    if(actionType === 'Edit') {
      return clone(originData)
    }
  })
  // Handle function
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setDataEdit({...dataEdit, [name]: value})
  }
  const handleSubmit = function () {
    const submitData = actionType === 'Edit' ? diff(originData, dataEdit, dataEdit.uid) : { set: dataEdit }
    handleSubmitData(actionType, submitData)
  }

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Address Type" />
      <ValidatorForm
        className={classge.root}
        onSubmit={handleSubmit}
        onError={errors => console.log(errors)}
      >
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={8} lg={8}>
                  <TextValidator
                    fullWidth
                    className={classge.inputAddressType}
                    id="nameAddressType"
                    type="text"
                    label="Address Type Name "
                    variant="outlined"
                    margin="dense"
                    name="type_name"
                    onChange={handleChange}
                    value={dataEdit?.type_name || ''}
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50',
                      'Character is not accept ']}
                  />
                </Grid>

                <Grid item xs={12} sm={4} lg={4}>
                  <TextValidator
                    fullWidth
                    id="typeAddressType"
                    type="number"
                    label="Address Type Value "
                    variant="outlined"
                    margin="dense"
                    name="type_value"
                    onChange={handleChange}
                    value={dataEdit?.type_value || ''}
                    validators={[
                      'required',
                      'minNumber: 0',
                      'maxNumber: 100',
                      // 'matchRegexp: [a-zA-Z0-9 ]'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min number is 0',
                      'Max number is 100',
                      // 'Character is not accept '
                    ]}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
