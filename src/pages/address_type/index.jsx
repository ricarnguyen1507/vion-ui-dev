import React, { useState } from 'react';
import api from 'services/api_cms';
import TableAddressType from './table'
import Edit from './edit';
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')

  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(true)
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))
  // Load data
  // useEffect(() => {
  //   api.get(`/list/addresstype`)
  //     .then(res => {
  //       setDataTable(res?.data?.result);
  //     })
  //     .catch(error => console.log(error));
  // }, []);

  // Handle function
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setIndexEdit(index)
    // setMode("edit")
  }
  const handleDelete = (dataDel) => api.post(`/addresstype`, { del: { uid: dataDel.uid } })
    .then(() => {
      const idx = dataTable.findIndex(d => d.uid === dataDel.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  // Submit data ( Mode : Edit || Add )
  const handleSubmitData = (actionType, data) => {
    if(Object.keys(data).length > 0) {
      api.post('/addresstype', data)
        .then(response => {
          const { set } = data
          if(actionType === 'Add') {
            set.uid = response.data.uids
            setDataTable([set, ...dataTable])
          } else if(actionType === 'Edit') {
            dataTable[indexEdit] = {...dataTable[indexEdit], ...set}
            setDataTable([...dataTable])
          }
          setDataEdit(null)
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      ctx.goBack()
    }
  }
  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        actionType={actionType}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
      />
    </Route>
    <Route exact path={path}>
      <TableAddressType
        ctx={ctx}
        data={dataTable}
        controlEditTable={controlEditTable}
        setControlEditTable={setControlEditTable}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
        setDataTable={setDataTable}
      />
    </Route>
  </>
}