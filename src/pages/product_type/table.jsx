import React, { useState, useContext } from "react";
import MaterialTable from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import useStyles from 'pages/style_general'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"
import { context } from 'context/Shared'

export default function TableProductType ({
  stateQuery,
  setStateQuery,
  controlEditTable,
  setControlEditTable,
}) {
  const classes = useStyles();
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  const [selectedRow, setSelectedRow] = useState(null);
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    return api.post("/list/product_type", {
      number: query.pageSize || 10,
      page: query.page || 0
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const column = [
    { title: "Product Type Name", field: "type_name", editable: "onUpdate" },
  ];
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        // title={<h1 className={classes.titleTable}>Product Type</h1>}
        onRowClick={(evt, selectRow) => setSelectedRow(selectRow)}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Loại sản phẩm</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank" tooltip="Add Supplier">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }} tooltip="Display Edit">
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { data &&
                              <ExportExcel data={data} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: (rowData) => ({
            backgroundColor:
              selectedRow && selectedRow.tableData.id === rowData.tableData.id
                ? "#EEE"
                : "#FFF",
          }),
        }}
        actions={[
          // {
          //   icon: AddBox,
          //   tooltip: "Add Product Type",
          //   isFreeAction: true,
          //   onClick: () => {
          //     functionEditData("Add");
          //   },
          // },
          // {
          //   icon: "edit",
          //   tooltip: "Display Edit",
          //   isFreeAction: true,
          //   onClick: (rowData) => {
          //     setControlEditTable(!controlEditTable);
          //   },
          // },
          controlEditTable === false
            ? {
              icon: "edit",
              tooltip: "Edit Product Type",
              onClick: (event, { tableData, ...rowData }) => {
                ctx.set('dataEdit', rowData)
                history.push(`${pathname}/Edit`)
              }
            }
            : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null,
        }}
      />
    </div>
  );
}
