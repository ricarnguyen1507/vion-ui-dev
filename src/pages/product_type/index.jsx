import React, { useState } from 'react';
import api from 'services/api_cms';
import TableProductType from './table'
import Edit from './edit';
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [actionType, setActionType] = useState('')
  const [dataEdit, setDataEdit] = useState(null)
  const [controlEditTable, setControlEditTable] = useState(false)
  // Load data
  const [stateQuery, setStateQuery] = useState(() => ({
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row) => {
    setActionType(actionType)
    setDataEdit(row)
  }
  const handleDelete = (row) => api.post('/product_type', { del: { uid: row.uid } })
    .then(res => {
      if(res.status === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      }
    }).catch(error => console.log(error))

  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          actionType={actionType}
          originData={dataEdit}
          functionBack={functionBack}
        />
      </Route>
      <Route exact path={path}>
        <TableProductType
          ctx={ctx}
          data={dataTable}
          controlEditTable={controlEditTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
        />
      </Route>
    </Shared>
  )
}