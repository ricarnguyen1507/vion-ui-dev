import React from "react";
import useStyles from 'pages/style_general'
import ReactExport from "react-data-export";
import GetAppIcon from "@material-ui/icons/GetApp";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import { titleCase } from "services/titleCase";
import { changePropName } from "services/changePropName";
import { clone } from "services/diff";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export default function ExportExcel ({ data, isEditTable }) {
  const classge = useStyles();

  const dataFields = [
    "areas",
    "barcode",
    "color",
    "cost_price_with_vat",
    "cost_price_without_vat",
    "created_at",
    "discount_tag",
    "display_name",
    "display_name_detail",
    "final_price",
    "instock",
    "free_shipping_tag",
    "from_date",
    "fulltext_search",
    "haravan_id",
    "haravan_variant_id",
    "height",
    "keywords",
    "listed_price_with_vat",
    "listed_price_without_vat",
    "long",
    "original",
    "packaging_unit",
    "packaging_unit_quantity",
    "parent_sku_id",
    "payment_terms",
    "price_with_vat",
    "price_without_vat",
    "product__brand_brand_name",
    "product__brand_id",
    "product__brand_uid",
    "product__collection",
    "product__partner_account_name",
    "product__partner_account_number",
    "product__partner_address_str",
    "product__partner_bank_branch_name",
    "product__partner_bank_name",
    "product__partner_contact_person",
    "product__partner_contract_number",
    "product__partner_contract_type",
    "product__partner_email",
    "product__partner_id",
    "product__partner_partner_name",
    "product__partner_phone_number",
    "product__partner_return_terms",
    "product__partner_shipping_method",
    "product__partner_tax_id",
    "product__partner_uid",
    "product__pricing_cost_price_with_vat",
    "product__pricing_cost_price_without_vat",
    "product__pricing_front_margin",
    "product__pricing_listed_price_with_vat",
    "product__pricing_listed_price_without_vat",
    "product__pricing_price_with_vat",
    "product__pricing_price_without_vat",
    "product__pricing_uid",
    "product__platform",
    "product__manufacturer_id",
    "product__manufacturer_manufacturer_name",
    "product__manufacturer_uid",
    "product__uom_UOM_name",
    "product__uom_uid",
    "product_name",
    "promotion_detail",
    "return_terms",
    "ribbon",
    "short_desc",
    "sku_id",
    "sub_unit",
    "sub_unit_quantity",
    "to_date",
    "uid",
    "unit",
    "width",
    "show_detail",
    "detail_content",
    "detail_title",
    "detail_uid",
    "tableData_id",
    "uom_quantity",
    "updated_at",
  ]

  const handlePlainData = (obj, result = {}) => {
    for (let key in obj) {
      if (typeof obj[key] === "object") {
        if (key.indexOf(".") > 0) {
          const newKey = key.replace(".", "__");
          changePropName(obj, newKey, key);
          const linkedFields = combineElement(obj[newKey], newKey);
          // delete obj[newKey];
          // delete result[key];
          result = { ...obj, ...result, ...linkedFields };
        }
        else {
          const linkedFields = combineElement(obj[key], key);
          result = { ...obj, ...result, ...linkedFields };
        }
      }
    }
    return result;
  };

  const combineElement = (obj, keyName, objResult = {}) => {
    if (Array.isArray(obj)) {
      const uidList = obj.map((elm) => elm.uid);
      objResult[keyName] = uidList.toString();
    } else {
      for (let key in obj) {
        const dependKey = keyName.concat("_") + key;
        const newObj = changePropName(obj, dependKey, key);
        objResult = { ...newObj };
      }
    }
    return objResult;
  };

  const handleData = (arrayData, arrResult = []) => {
    if (arrayData !== null) {
      const cloneArr = clone(arrayData);
      cloneArr.forEach((item) => arrResult.push(handlePlainData(item)));
      return arrResult;
    }
  };

  const excelData = data && handleData(data);

  return (
    <ExcelFile
      element={
        isEditTable ?
          <Button
            variant="contained"
            aria-label="back"
            className={classge.btnExport}
          >
            <GetAppIcon className={classge.expIcon} />
          Export
          </Button>
          :
          <Tooltip title="Export data" aria-label="export data">
            <IconButton variant="contained" >
              <GetAppIcon className={classge.expIcon} />
            </IconButton>
          </Tooltip>
      }
    >
      <ExcelSheet data={excelData} name="Product">
        {excelData && excelData.length
          ? dataFields.map((item, idx) => (
            <ExcelColumn
              key={idx}
              label={`${titleCase(item)} <${item}>`}
              value={item}
            />
          ))
          : {}}
      </ExcelSheet>
    </ExcelFile>
  );
}
