import React, { useState } from "react";
// import { OutTable, ExcelRenderer } from "react-excel-renderer";
import MaterialTable from "material-table";
import useStyles from 'pages/style_general'

export default function ImportExcel () {
  const classge = useStyles();
  const [data, setData] = useState([]);
  const [uploadExcel] = useState({
    rows: [],
    cols: [],
  });

  /* const fileHandler = (event) => {
    let fileObj = event.target.files[0];

    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if (err) {
        console.log(err);
      } else {
        setUploadExcel({
          cols: resp.cols,
          rows: resp.rows,
        });
        const fieldArr = resp.rows[0].map(col => {
          const nameIdx = col.indexOf("<");
          return  col.substring(nameIdx + 1, col.length - 1);
        });
        setData(parseFileUpload(resp.rows, fieldArr))
      }
    });
  }; */

  // const submitData = () => {
  //   console.log("data submit")
  // }

  const columns = uploadExcel.rows.length > 0 ? uploadExcel.rows[0].map((col) => {
    const nameIdx = col.indexOf("<");
    return {
      title: col.substring(0, nameIdx - 1),
      field: col.substring(nameIdx + 1, col.length - 1),
    };
  })
    : [];

  console.log("data", data);

  // VIEW
  return (
    <div >
      {/* <div className="upload-btn-wrapper">
        <Button className="upload-btn" onChange={fileHandler} >
          <UploadIcon style={{marginRight: 5}}/>
          Upload file
        </Button>
        <input type="file" onChange={fileHandler} style={{ padding: "10px" }} />

        <Button onClick={submitData}>
          Submit Data
        </Button>
      </div> */}
      {/* <OutTable data={dataExcel.rows} columns={dataExcel.rows} /> */}

      <MaterialTable
        title={<h2 className={classge.titleTable}>Import Product</h2>}
        columns={columns}
        data={data}
        editable={{
          // onRowAdd: newData =>
          // new Promise((resolve, reject) => {
          //     setTimeout(() => {
          //         /* setData([...data, newData]); */

          //         resolve();
          //     }, 1000);
          // }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                const dataUpdate = [...data];
                const index = oldData.tableData.id;
                dataUpdate[index] = newData;
                setData([...dataUpdate]);
                resolve();
              }, 1000)
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setData([...dataDelete]);
                resolve();
              }, 1000);
            }),
        }}
      />
    </div>
  );
}
