import React, { useState } from 'react'
import MaterialTable from 'material-table'
import { Grid, Tooltip, IconButton } from '@material-ui/core'
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from "@material-ui/icons/Edit"
import AttachFileIcon from '@material-ui/icons/AttachFile';
import SearchIcon from '@material-ui/icons/Search'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
import api from 'services/api_cms'
// import ExportExcel from "./export_excel";

const productStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}

function calFinalPrice ({ sell_price = 0, discount = 0 }) {
  return discount > 0 ? sell_price * (1 - discount / 100) : sell_price
}

function prepareData (products) {
  products.forEach(r => {
    r.final_price = calFinalPrice(r)
    if (r['product.pricing']) {
      r.cost_price_without_vat = r['product.pricing'].cost_price_without_vat || 0
      r.cost_price_with_vat = r['product.pricing'].cost_price_with_vat || 0
      r.listed_price_without_vat = r['product.pricing'].listed_price_without_vat || 0
      r.listed_price_with_vat = r['product.pricing'].listed_price_with_vat || 0
      r.price_without_vat = r['product.pricing'].price_without_vat || 0
      r.price_with_vat = r['product.pricing'].price_with_vat || 0
      r.from_date = r['product.pricing'].from_date || ""
      r.to_date = r['product.pricing'].to_date | ""
    }

  })
}

function getFilterStr ({filters}) {
  let strFunc = filters.map(({value, column: {o}}) => {
    if(typeof value === 'string') {
      value = value.replace(/["\\]/g, '\\$&').trim()
      if(value) {
        if (!o) {
          return value
        }
      }
    }
  })

  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/["\\]/g, '\\$&').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  strFunc && strFunc.length > 0 && strFunc.join('') != "" ? strFunc = `func: alloftext(fulltext_search, "${strFunc.join(' ')}")` : strFunc = ""
  return {strFunc, strFilter: strFilter.join(' AND ')}
}


export default function TableProduct ({ stateQuery, setStateQuery, functionEditData, displayStatus, setMode }) {
  const classes = useStyles()
  const [controlEditTable, setControlEditTable] = useState(false)
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  // const { isLogin, setIsLogin } = React.useContext(LoginContext)

  function getData (query) {
    if(!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFunc, strFilter} = getFilterStr(query)
    ///${query.pageSize}/${query.page}/${query.search || "all"}`
    return api.post("/list/product", {
      number: query.pageSize,
      page: query.page,
      ...(strFilter && {filter: ' AND ' + strFilter}),
      ...(strFunc && {func: strFunc}),
      is_temporary: true
    })
      .then(response => {
        const { summary: [{totalCount}], result } = response.data
        prepareData(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  /* function onSearchChange() {
        getData(stateQuery)
    } */
  /* const closeSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
  } */
  // const snackbarTransition = (props) => <Slide {...props} direction="left" />

  const createSearchData = () => {
    api.get('/product/create-search-data')
      .then(res => console.log(res))
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: productStatus,
        o: 'eq'
      },
      { title: "UID ", field: "uid", editable: "never", filtering: false},
      { title: "SKU ", field: "sku_id", editable: "never" },
      { title: "Tên Hiển thị", field: "display_name", editable: "never" },
    ]
    for(let {value, column: {field}} of stateQuery.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })

  return (
    <div className="fade-in-table">
      <MaterialTable
        data={getData}
        columns={column}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Landing Voucher</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { functionEditData("Add") }} target="_blank" tooltip="Add Supplier">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }} tooltip="Display Edit">
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { dataTable &&
                                        <ExportExcel data={dataTable} isEditTable={false} />
                    } */}
                    <Tooltip title="Import Excel" aria-label="import excel">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setMode("import") }}>
                        <AttachFileIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Create Search Data" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { createSearchData() }} target="_blank" tooltip="Add Supplier">
                        <SearchIcon />
                      </IconButton>
                    </Tooltip>

                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          search: false,
          // searchText: stateQuery.search,
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          debounceInterval: 500,
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          })
        }}
        // title={<h1 className={classes.titleTable}>Product</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        detailPanel={rowData => (
          <>
            <Grid container className={classes.rootDetailTable} spacing={2}>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_cover} style={{ width: "50%" }} />
              </Grid>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_banner} style={{ width: "80%" }} />
              </Grid>
              <Grid item xs={12} sm={4} lg={4} className={classes.formpd}>
                <ImgCdn src={rowData.image_promotion} style={{ width: "80%" }} />
              </Grid>
            </Grid>
          </>
        )}
        actions={[
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Product",
            onClick: (event, { tableData, ...rowData }) => {
              functionEditData('Edit', rowData, tableData.id)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
    // </>
  )
}