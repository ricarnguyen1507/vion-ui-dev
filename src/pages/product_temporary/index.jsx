import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import TableProduct from './table'
import Edit from './edit'
import sortAlphabet from 'services/sortAlphabet'
import Loading from "components/Loading"
// import ImportExcel from "./import_excel";
import { LoadingContext } from "context/LoadingContext";

function fetchData (url) {
  if(url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }
}
const display_status = ['PENDING', 'REJECTED', 'APPROVED']

export default function () {
  const { isLoading, setIsLoading } = React.useContext(LoadingContext);
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    orderBy: undefined,
    orderDirection: "",
    page: 0,
    pageSize: 10,
    search: "",
    totalCount: 0,
  }))

  const [dataTable, setDataTable] = useState([]);
  const [dataEdit, setDataEdit] = useState(null)
  const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  // const [indexEdit, setIndexEdit] = useState(null)

  const [brands, setBrands] = useState(null)
  const [companies, setCompanies] = useState(null)
  // const [collections, setCollections] = useState(null)
  const [subCollections, setSubCollections] = useState(null)
  const [types, setTypes] = useState(null)
  const [platforms, setPlatforms] = useState(null)
  const [icons, setIcons] = useState(null)
  const [producttypes, setProductTypes] = useState(null)
  const [partners, setPartners] = useState(null)
  const [uoms, setUoms] = useState(null)
  const [manufacturer, setManufacturer] = useState(null)
  const [areas, setAreas] = useState(null)

  useEffect(() => {
    const fetchs = [
      {url: "/list/collection", method: "get"},
      {url: "/list/brand", method: "get"},
      {url: "/list/distributor", method: "get"},
      {url: "/list/icon", method: "get"},
      {url: "/list/product_type", method: "get"},
      {url: "/list/partner", method: "get"},
      {url: "/list/uom", method: "get"},
      {url: "/list/manufacturer", method: "get"},
      {url: "/list/areas", method: "get"}
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([
      collections,
      brands,
      companies,
      icons,
      producttypes,
      partners,
      uoms,
      manufacturer,
      areas
    ]) => {
      const groupedCollections = {
        0: [],
        100: [],
        200: []
      }
      collections.forEach(c => {
        if(groupedCollections[c.collection_type]) {
          groupedCollections[c.collection_type].push(c)
        }
      })
      for(let k in groupedCollections) {
        groupedCollections[k] = sortAlphabet(groupedCollections[k], 'collection_name')
      }

      // setCollections(collections)
      setSubCollections(groupedCollections[0])
      setTypes(groupedCollections[200])
      setPlatforms(groupedCollections[100])

      setBrands(brands)
      setCompanies(companies)
      setIcons(icons)
      setProductTypes(producttypes)
      setPartners(partners)
      setUoms(uoms)
      setManufacturer(manufacturer)
      setAreas(areas)
      setIsLoading(false)
    }).catch(err => {
      console.log("can not connect api server", err)
    })
  }, [setIsLoading]);

  const functionBack = () => {
    setMode("table")
  }
  const functionEditData = (actionType, row) => {
    setActionType(actionType)
    setDataEdit(row)
    // setIndexEdit(index)
    setMode("edit")
  }
  const handleDelete = (row) =>
    api.delete(`/product/${row.uid}`)
  /* .then(res => {
                const idx = products.indexOf(row)
                products.splice(idx, 1)
                setProducts([...products])
            })
            .catch(error => console.log(error)) */


  async function handleSubmitData (submitData) {
    const formData = new FormData()
    formData.set("delEdges", JSON.stringify(submitData.delEdges))
    formData.set("delRecords", JSON.stringify(submitData.delRecords))
    formData.set("data", JSON.stringify(submitData.data))
    formData.set("details", JSON.stringify(submitData.details))

    for(let field in submitData.images) {
      formData.set(field, submitData.images[field])
    }
    const isNew = submitData.uid.startsWith('_:')

    await api({
      method: 'post',
      url: '/product/' + (isNew ? 'new' : submitData.uid),
      data: formData,
      headers: {
        "Accept": "application/json",
        "Content-Type": "multipart/form-data"
      }
    })

    functionBack()
    setIsLoading(false)
  }

  if(isLoading) {
    return <Loading />
  }
  // Display view
  if (mode === "table") {
    return <TableProduct
      stateQuery={stateQuery}
      setStateQuery={setStateQuery}
      handleDelete={handleDelete}
      functionEditData={functionEditData}
      displayStatus={display_status}
      setMode={setMode}
      dataTable={dataTable}
      setDataTable={setDataTable}
    />
  }
  if (mode === "edit") {
    return <Edit
      actionType={actionType}
      originData={dataEdit}

      subCollections={subCollections}
      setSubCollections={setSubCollections}
      platforms={platforms}
      setPlatforms={setPlatforms}
      types={types}

      companies={companies}
      brands={brands}
      icons={icons}
      setIcons={setIcons}
      producttypes={producttypes}
      partners={partners}
      uoms={uoms}
      manufacturer={manufacturer}
      displayStatus={display_status}
      areas={areas}

      functionBack={functionBack}
      handleSubmitData={handleSubmitData}
    />
  }

  // if (mode === "import") {
  //     return <ImportExcel
  //         functionBack={functionBack}
  //         setDataTable={setDataTable}
  //         dataTable={dataTable}
  //         functionEditData={functionEditData}
  //         handleSubmitData={handleSubmitData}
  //     />
  // }
}