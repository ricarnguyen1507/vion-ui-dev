import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import RichTextEditor from 'react-rte';
import IconButton from '@material-ui/core/IconButton'
import ClearIcon from '@material-ui/icons/Clear'

function TabPanel ({ children, value, index }) {
  return (
    <div hidden={value !== index} id={`tabpanel-${index}`}>
      {children}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps (index) {
  return {
    id: `tab-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  }
}));

const toolbarConfig = {
  display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
  INLINE_STYLE_BUTTONS: [
    { label: 'Bold', style: 'BOLD', className: 'custom-css-class' },
    { label: 'Italic', style: 'ITALIC' },
    { label: 'Underline', style: 'UNDERLINE' }
  ],
  BLOCK_TYPE_DROPDOWN: [
    { label: 'Normal', style: 'unstyled' },
    { label: 'Heading Large', style: 'header-one' },
    { label: 'Heading Medium', style: 'header-two' },
    { label: 'Heading Small', style: 'header-three' }
  ],
  BLOCK_TYPE_BUTTONS: [
    { label: 'UL', style: 'unordered-list-item' },
    { label: 'OL', style: 'ordered-list-item' }
  ]
}

export default function ({details, idx, handleChangeDetails, onDelete }) {
  const classes = useStyles();
  const [tabIdx, setTabIdx] = React.useState(0);

  const swichTab = (event, nextTabIdx) => {
    setTabIdx(nextTabIdx);
  };

  const TabSwitches = details.map((v, i) => <Tab key={i} label={v.title} {...a11yProps(i)} />)

  // Xu ly noi dung ben trong moi tabpanel
  const TabContents = details.map((v, i) => {
    const [content, setContent] = React.useState(RichTextEditor.createValueFromString(v.content, 'html'))
    const handleChange = (value) => {
      try{
        setContent(value)
        handleChangeDetails({ ...v, content: content.toString('html')})
      }
      catch(err)
      {
        console.log(err)
      }

    };
    return (
      <TabPanel key={i} value={tabIdx} index={i}>
        <RichTextEditor
          toolbarConfig={toolbarConfig}
          value={content}
          onChange={handleChange} />
      </TabPanel>
    )
  })

  // Render => all details
  return (
    <div className={classes.root}>
      <Tabs value={tabIdx} onChange={swichTab} style={{backgroundColor: '#F5F5F5'}}>
        {TabSwitches}
        <IconButton
          onClick={() => onDelete(details.uid, idx)}
        >
          <ClearIcon/>
        </IconButton>
      </Tabs>
      {TabContents}
    </div>
  );
}