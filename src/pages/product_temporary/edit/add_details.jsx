import React, {useState} from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Divider from '@material-ui/core/Divider'
import DialogTitle from '@material-ui/core/DialogTitle';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import Button from "@material-ui/core/Button"
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  dialogBtn: {
    marginTop: 10,
  }
}))

export default function ({open, handleClose, addDetail}) {
  const classes = useStyles()
  const [titleDetail, setTitleDetail] = useState('')

  const handleChangeTitleDetail = (e) => {
    setTitleDetail(e.target.value)
  }

  const handleSubmit = () => {
    try{
      addDetail(titleDetail, '')
      setTitleDetail('')
    }
    catch(err)
    {
      console.log(err)
    }

  }
  return (
    <>
      <ValidatorForm onSubmit={handleSubmit} >
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth="lg">
          <DialogTitle id="form-dialog-title">
                        Details Name
          </DialogTitle>
          <Divider/>
          <DialogContent>
            <TextValidator
              autoFocus
              margin="dense"
              id="titledetail"
              label=""
              type="text"
              value={titleDetail}
              onChange={handleChangeTitleDetail}
              fullWidth
              validators={[
                'required',
                'minStringLength:5',
                'maxStringLength:500',
                // 'matchRegexp: ^[A-Za-z0-9 ]$'
              ]}
              errorMessages={[
                'This field is required',
                'Min length is 5',
                'Max length is 500',
                // 'Character is not accept '
              ]}
            />
          </DialogContent>
          <DialogActions className={classes.dialogBtn}>
            <Button
              type="submit"
              size="small"
              variant="contained"
              onClick={handleClose}
              color="secondary">
                            Cancel
            </Button>
            <Button
              color="primary"
              size="small"
              type="submit"
              variant="contained"
              aria-label="add"
              onClick={handleSubmit}
            >
                            Apply
            </Button>
          </DialogActions>
        </Dialog>
      </ValidatorForm>
    </>
  )
}