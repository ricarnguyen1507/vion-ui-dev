import React from 'react'
import Grid from "@material-ui/core/Grid"
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Paper from '@material-ui/core/Paper'
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Media from "components/Media/index"
import {genCdnUrl} from 'components/CdnImage'

export default function Preview ({idx, uid, preview, fileChange, onDelete}) {
  const fileHandle = (field, file) => {
    fileChange(`previews|${field}|${preview.uid}|${preview[field] || ""}`, file)
  }
  return (
    <Grid item>
      <Paper style={{margin: 5, padding: 5, textAlign: 'center'}}>
        <Card style={{marginBottom: 5}}>
          <CardMedia
            image={"1920x960.png"}
            children={
              <Media
                src={genCdnUrl(uid && preview.source, "")}
                mediaType={preview.media_type}
                fileHandle={fileHandle} style={{ width: 100, height: 100}}
                field={`source`} accept="image/jpeg, image/png, video/*"
              />
            }
          />
        </Card>
        <Card>
          <CardMedia
            image={"400x400.png"}
            children={
              <Media
                src={genCdnUrl(uid && preview.thumb, "")}
                mediaType={"image"}
                fileHandle={fileHandle} style={{ width: 100, height: 100}}
                field={`thumb`} accept="image/jpeg, image/png"
              />
            }
          />
        </Card>
        <IconButton onClick={() => onDelete(preview.uid, idx)}><DeleteIcon /></IconButton>
      </Paper>
    </Grid>
  )
}