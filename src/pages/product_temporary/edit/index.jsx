import React, { useState, useMemo } from 'react'
import useStyles from './edit_style'
import useStylesGeneral from 'pages/style_general'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import RoleButton from 'components/Role/Button'
import ReturnIcon from "@material-ui/icons/KeyboardReturn"
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import Autocomplete from '@material-ui/lab/Autocomplete'
import Details from './content_tabs'
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Divider from '@material-ui/core/Divider'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from "components/Media/index"
import Preview from "./preview_media"
import CollectionList from './collection_list'
import { genUid } from "utils"
import AddDetail from './add_details'
import { clone, conpare_diff_arr_obj } from 'services/diff'
import ColorPicker from './color_product'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { genCdnUrl } from 'components/CdnImage'
// import ExportExcel from "../export_excel"
import { LoadingContext } from "context/LoadingContext";

function compareChecked (states) {
  const del = [], set = []
  Object.entries(states).forEach(([uid, [o, n]]) => {
    if(o !== n) {
      (n ? set : del).push({ uid })
    }
  })
  return [del, set]
}

function setDel (o, n, delEdges, ...propNames) {
  propNames.forEach(propName => {
    if(o[propName] && (n[propName] === null || (n[propName] && o[propName].uid !== n[propName].uid))) {
      delEdges[propName] = { "uid": o[propName].uid }
    }
  })
}

export default function ({ originData, actionType, platforms, types, icons, setIcons, functionBack, handleSubmitData, displayStatus }) {
  const { setIsLoading } = React.useContext(LoadingContext);
  const classes = useStyles()
  const classge = useStylesGeneral();

  const [dataEdit, setDataEdit] = useState(() => {
    if (actionType === 'Add') {
      return { uid: '_:new_product', cost_price: 0, sell_price: 0, discount: 0, instock: true, ribbon: "", discount_tag: "", is_temporary: true }
    }
    if (actionType === 'Edit') {
      return clone(originData)
    }
  })

  const checkedState = useMemo(() => {
    const sc = {};
    const pl = {};
    (dataEdit["product.collection"] || []).forEach(i => { sc[i.uid] = [true, true] });
    (dataEdit["product.platform"] || platforms).forEach(i => { pl[i.uid] = [false, true] });
    return { sc, pl};
  })

  const [color, setColor] = useState(dataEdit.color || "#f48024")

  const handleColor = (color) => {
    setColor(color.hex)
  }
  // Tạo mới 1 details
  const [details, setDetails] = useState(dataEdit.detail ? [dataEdit.detail] : [])
  const [previews, setPreviews] = useState(dataEdit.previews || [])
  const [updated] = useState({
    delEdges: {
      "details": [],
      "previews": [],
      "product.collection": [],
      "product.platform": [],
      "product.pricing": null,
      'areas': []
    },
    delRecords: [],
    values: {
      'display_status': dataEdit.display_status,
      'areas': [],
      'instock': dataEdit.instock,
      'is_temporary': dataEdit.is_temporary
    },
    previews: {},
    details: {},
    images: {},
    pricing: dataEdit['product.pricing'] || {}
  })

  const addPreview = () => {
    setPreviews([...previews, { uid: genUid('preview') }])
  }
  // Lưu lại các preview thay đổi
  const previewChange = (field, file) => {
    updated.previews[field] = file
  }
  const deletePreview = (uid, idx) => {
    if (!uid.startsWith("_:")) {
      updated.delEdges["previews"].push({ uid })
      updated.delRecords.push({ uid })
    }
    Object.keys(updated.previews).forEach(p => {
      if (p.indexOf(uid) !== -1) {
        delete updated.previews[p]
      }
    })
    previews.splice(idx, 1)
    setPreviews([...previews])
    // console.log("delete", uid)
  }
  // Lưu lại những image thay đổi
  const updateImage = (field, file) => {
    updated.images[field] = file
  }

  // On-off dialog ( detail + icon)
  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  // =========================================================================
  // Handle Details
  // =========================================================================
  // Tạo + cập nhật details
  const addDetail = (title) => {
    if (details.length >= 1) {
      console.log("Chúng ta chỉ dùng 1 cấp mô tả chi tiết")
    } else {
      try {
        const newDetail = { uid: genUid('detail'), title, content: '' }
        setDetails([newDetail, ...details])
        updated.details[newDetail.uid] = newDetail
        handleClose()
      }
      catch (error) {
        console.log(error)
      }
    }

  }
  // Update nội dung cơ bản - ko chọn icon
  const updateDetail = function (uid, field, value) {
    if (updated.details[uid]) {
      updated.details[uid][field] = value
    } else {
      updated.details[uid] = { uid, [field]: value }
    }
  }
  // Delete details
  const deleteDetail = (uid, idx) => {
    if (!uid.startsWith("_:")) {
      updated.delEdges["details"].push({ uid })
      updated.delRecords.push({ uid })
    }
    if (updated.details[uid]) {
      delete updated.details[uid]
    }
    details.splice(idx, 1)
    setDetails([...details])
  }
  // =========================================================================
  // Handle detail icon
  // =========================================================================

  // function handleSubmit() {}
  function change_alias (str) {
    return !str ? '' : str.normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd').replace(/Đ/g, 'D');
  }
  // Handle submit form
  const handleSubmit = function () {
    setIsLoading(true);
    const [delCats, setCats] = compareChecked(checkedState.sc)
    const [delPlats, setPlats] = compareChecked(checkedState.pl)
    if (delCats.length) {
      updated.delEdges["product.collection"] = delCats
    }
    if (delPlats.length) {
      updated.delEdges["product.platform"] = delPlats
    }
    if (setCats.length) {
      updated.values["product.collection"] = setCats
    }
    if (setPlats.length) {
      updated.values["product.platform"] = setPlats
    }
    let setDelAreas = conpare_diff_arr_obj(originData && originData.areas || [], dataEdit && dataEdit.areas || [])
    updated.delEdges["areas"] = setDelAreas.del || []
    updated.values["areas"] = setDelAreas.set || []

    details.forEach(d => {
      if (updated.details[d.uid] && d["detail.icon"] && updated.details[d.uid]["detail.icon"]) {
        if (d["detail.icon"].uid !== updated.details[d.uid]["detail.icon"].uid) {
          updated.delRecords.push({
            uid: d.uid,
            "detail.icon": {
              uid: d["detail.icon"].uid
            }
          })
        }
      }
    })

    setDel(
      dataEdit, updated.values, updated.delEdges,
      "product.distributor",
      "product.brand",
      "product.type",
      "product.partner",
      "product.uom",
      "product.manufacturer",
      "product.pricing",
      "areas"
    )


    const fullTextSearch = [
      dataEdit.product_name, change_alias(dataEdit.product_name),
      dataEdit.display_name, change_alias(dataEdit.display_name),
      dataEdit.display_name_detail, change_alias(dataEdit.display_name_detail),
      dataEdit.short_desc, change_alias(dataEdit.short_desc),
      dataEdit.keywords, change_alias(dataEdit.keywords),
      dataEdit.sku_id, change_alias(dataEdit.sku_id),
      dataEdit.uid, change_alias(dataEdit.uid),
    ]

    // xây cấu trúc gửi lên API
    const submitData = {
      uid: dataEdit.uid,
      delEdges: { ...updated.delEdges },
      delRecords: updated.delRecords,
      data: {
        uid: dataEdit.uid,
        color,
        ...updated.values,
        'product.pricing': {...updated.pricing, "dgraph.type": "Pricing"},
        'fulltext_search': fullTextSearch.join(' '),
      },
      details: Object.values(updated.details),
      images: {
        ...updated.images,
        ...updated.previews
      }
    }

    handleSubmitData(submitData)
  }

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    updated.values[name] = value
    setDataEdit({ ...dataEdit, [name]: value })
  }

  const handleChangeCheckBox = ({target: {name, checked}}) => {
    updated.values[name] = checked
  }

  /* const handleChangeManufacturerOption = (e, item) => {
    e.preventDefault()
    updated.values['product.manufacturer'] = item ? { uid: item.uid } : null
  }

  const handleChangeBrandOption = (e, item) => {
    e.preventDefault()
    updated.values["product.brand"] = item ? { uid: item.uid } : null
  }

  const handleChangeProductTypeOption = (e, item) => {
    e.preventDefault()
    updated.values["product.type"] = item ? { uid: item.uid } : null
  }
  const handleChangePartnerOption = (e, item) => {
    e.preventDefault()
    updated.values["product.partner"] = item ? { uid: item.uid } : null
  }

  const handleChangeUomOption = (e, item) => {
    e.preventDefault()
    updated.values["product.uom"] = item ? { uid: item.uid } : null
  }

  const handleChangePricing = (e) => {
    const mutate_time = (new Date()).getTime()
    e.preventDefault()
    const { name, value } = e.target
    updated.pricing[name] = value
    if (!updated.pricing.uid) {
      updated.pricing.create_at = mutate_time
    } else {
      updated.pricing.updated_at = mutate_time
    }
    setDataEdit({ ...dataEdit, 'product.pricing': updated.pricing })
  } */


  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
  }

  /* const handleChangeAreas = (e, item) => {
    e.preventDefault()
    setDataEdit({...dataEdit, areas: item.length > 0 ? item.map(i => ({uid: i.uid})) : [] })
  } */

  const ActionIcon = actionType === "Add" ? <AddBox className={classes.iconProduct} /> : <EditIcon className={classes.iconProduct} />
  return <>
    <PageTitle title="Landing Voucher" />
    <AddDetail
      open={open}
      handleClose={handleClose}
      addDetail={addDetail}
    />
    <ValidatorForm onSubmit={handleSubmit} className={classes.root}>
      <Grid container spacing={2} >
        <Grid item xs={12} sm={8} lg={8}>
          <div className={classes.rootBasic}>
            <div className={classes.titleProduct}>
              <div className={classes.containIcon}>
                {ActionIcon}
              </div>
              <Typography className={classes.title} variant="h3" >
                                    Thông tin cơ bản
              </Typography>
              <div className={classes.colorPicker}>
                <ColorPicker
                  color={color}
                  handleColor={handleColor}
                />
              </div>
            </div>
            <Divider />
            <Grid container spacing={2} className={classes.formpd}>
              <Grid item xs={12} sm={12} lg={12}>
                <TextValidator
                  // required
                  className={classes.priceText}
                  id="sku_id"
                  name="sku_id"
                  label="(* SKUID)"
                  type="text"
                  variant="outlined"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.sku_id || ''}
                  validators={[
                    'required',
                    'minStringLength:3',
                    'maxStringLength:50',
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min length is 3 character ',
                    'Max length is 50 character',
                    // 'Character is not accept ! '
                  ]}
                />
              </Grid>

              <Grid item xs={12} sm={12} lg={12}>
                <TextValidator
                  id="nameProduct"
                  name="product_name"
                  label="Tên Sản Phẩm (*)"
                  variant="outlined"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.product_name || ""}
                  validators={[
                    'required',
                    'minStringLength:3',
                    'maxStringLength:500',
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min length is 3',
                    'Max length is 500',
                    // 'Character is not accept '
                  ]}
                />

              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <TextValidator
                  id="display_name"
                  name="display_name"
                  label="Tên hiển thị (display name)"
                  variant="outlined"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.display_name || ""}
                  validators={[
                    'minStringLength:3',
                    'maxStringLength:500',
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    'Min length is 3',
                    'Max length is 500',
                    // 'Character is not accept '
                  ]}
                />

              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <TextValidator
                  id="display_name_detail"
                  name="display_name_detail"
                  label="Tên hiển thị chi tiết (display name detail)"
                  variant="outlined"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.display_name_detail || ""}
                  validators={[
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    // 'Character is not accept '
                  ]}
                />

              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <TextValidator
                  id="short_desc"
                  name="short_desc"
                  label="Mô tả ngắn (hiển thị TV)"
                  variant="outlined"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.short_desc || ""}
                  validators={[
                    'minStringLength:3',
                    'maxStringLength:200',
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    'Min length is 3',
                    'Max length is 200',
                    // 'Character is not accept '
                  ]}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <TextValidator
                  id="keywords"
                  name="keywords"
                  label="Keywords (*)"
                  variant="outlined"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.keywords || ""}
                  validators={[
                    'required',
                    'minStringLength:3',
                    'maxStringLength:500',
                    // 'matchRegexp: ^[A-Za-z0-9 ]$'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min length is 3',
                    'Max length is 500',
                    // 'Character is not accept '
                  ]}
                />
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>


      {/* CONTENT TAB : HIGHLINE - TECHSPECS - DESCRIPTION  */}
      <div className={classes.rootBasic}>
        <div className={classes.titleProduct}>
          <Grid container spacing={2} >
            <Grid item xs={6} sm={4} lg={4} className={classes.listCate}>
              <Typography className={classes.title} variant="h3" >
                                    Details
                {details.length < 1 ?
                  <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    className={classes.btnPreview}
                    onClick={handleClickOpen}>
                    <AddBox></AddBox>
                  </Button> : ""
                }
              </Typography>
            </Grid>
            <Grid item xs={6} sm={8} lg={8} className={classes.listCate}>
              <FormControlLabel
                control={
                  <Checkbox
                    className={classes.titleProduct}
                    id="show_detail"
                    name="show_detail"
                    color="primary"
                    defaultChecked={dataEdit['show_detail']}
                    onChange={handleChangeCheckBox}/>
                }
                label="Hiển thị nội dung chi tiết"
              />
            </Grid>
          </Grid>
        </div>
        <Divider />
        <Grid container spcing={2} className={classes.formpd}>
          <Grid item xs={12} sm={12} lg={12}>
            {details.map((detail, idx) => <Details icons={icons} setIcons={setIcons} key={detail.uid} idx={idx} detail={detail} updateDetail={updateDetail} onDelete={deleteDetail} />)}
          </Grid>
        </Grid>
      </div>
      <div className={classes.rootBasic}>
        <div className={classes.titleProduct}>
          <Typography className={classes.title} variant="h3" >
                            Preview
            <Button
              variant="contained"
              color="secondary"
              className={classes.btnPreview}
              onClick={addPreview}>
              <AddBox></AddBox>
            </Button>
          </Typography>

        </div>
        <Divider />
        <Grid container>
          {previews.map((p, i) => <Preview key={p.uid} idx={i} onDelete={deletePreview} fileChange={previewChange} uid={dataEdit.uid} preview={p} />)}
        </Grid>
      </div>

      {/* IMAGE PRIMARY : PROMOTION - BANNER - COVER  */}
      <div className={classes.rootBasic}>
        <div className={classes.titleProduct}>
          <Typography className={classes.title} variant="h3" >
                            Image Primary
          </Typography>
        </div>
        <Divider />
        <Grid container spacing={2} className={classes.formpd}>
          <Grid item>
            <h5>Image Banner (1600x900)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit.image_banner, "image_banner.png")} type="image" style={{ width: 400, height: 225 }} fileHandle={updateImage} field="image_banner" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
          <Grid item>
            <h5>Image Highlight (1600x900)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit.image_highlight, "image_highlight.png")} type="image" style={{ width: 400, height: 225 }} fileHandle={updateImage} field="image_highlight" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
      </div>
      <div className={classes.rootBasic}>
        <div className={classes.titleProduct}>
          <Typography className={classes.title} variant="h3">
                            Type
          </Typography>
        </div>
        <Grid container className={classes.rootListCate} spacing={2}>
          <Grid item xs={12} sm={12} lg={12} className={classes.listCate}>
            <CollectionList
              items={types}
              checkedState={checkedState.sc}
            />
          </Grid>
        </Grid>
      </div>
      <div className={classes.rootBasic}>
        <div className={classes.titleProduct}>
          <Typography className={classes.title} variant="h3">
                            Xét duyệt sản phẩm
          </Typography>
        </div>
        <Grid container className={classes.rootListCate} spacing={2}>
          <Grid item xs={12} sm={12} lg={12} className={classes.listCate}>
            <Autocomplete
              options={displayStatus}
              value={displayStatus[dataEdit.display_status] || "PENDING"}
              onChange={handleChangeDisplayStatus}
              style={{ width: "100%" }}
              renderInput={params => (
                <TextField {...params}
                  label="Trạng thái hiển thị"
                  variant="outlined"
                  fullWidth
                  margin="dense"
                />
              )}
            />
          </Grid>
        </Grid>
      </div>

      {/* Button submit */}
      <Grid className={classes.btnSubmit}>
        <Button
          color="secondary"
          aria-label="add"
          variant="contained"
          className={classes.btnData}
          onClick={functionBack}
        >
          <ReturnIcon className={classge.iconback}/>
                        Return
        </Button>
        <RoleButton actionType={actionType} />
        {/* { actionType === "Edit" &&
                        <ExportExcel data={[dataEdit]} isEditTable={true} />
        } */}
      </Grid>
    </ValidatorForm>
  </>
}
