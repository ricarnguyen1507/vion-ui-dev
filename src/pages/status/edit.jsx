import React, {useState} from 'react';
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import useStylesGeneral from 'pages/style_general'
import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import {clone, diff} from 'services/diff'

export default function (props) {
  const classge = useStylesGeneral()
  const { originData, actionType, functionBack, handleSubmitData } = props

  const [dataEdit, setDataEdit] = useState(() => {
    if(actionType === 'Add') {
      return { uid: '_:new_status' }
    }
    if(actionType === 'Edit') {
      return clone(originData)
    }
  })
  // Handle submit
  const handleChange = (e) => {
    setDataEdit({ ...dataEdit, [e.target.name]: e.target.value })
  }
  const handleSubmit = function () {
    const submitData = actionType === 'Edit' ? diff(originData, dataEdit, dataEdit.uid) : { set: dataEdit }
    handleSubmitData(actionType, submitData)
  }

  // View

  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  return (
    <>
      <PageTitle title="Status" />
      <ValidatorForm className={classge.root} onSubmit={handleSubmit}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Information
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2} className={classge.formpd}>
                <Grid item xs={12} sm={6} lg={8}>
                  <TextValidator
                    fullWidth
                    id="nameStatus"
                    type="text"
                    label="Name Status"
                    variant="outlined"
                    margin="dense"
                    name="status_name"
                    onChange={handleChange}
                    value={dataEdit?.status_name || ''}
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50'
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50'
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={4}>
                  <TextValidator
                    fullWidth
                    id="valueStatus"
                    type="number"
                    label="Value Status"
                    variant="outlined"
                    margin="dense"
                    name="status_value"
                    onChange={handleChange}
                    value={dataEdit?.status_value || ""}
                    validators={[
                    ]}
                    errorMessages={[
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    id="codeStatus"
                    type="text"
                    label="Status Code"
                    variant="outlined"
                    margin="dense"
                    name="status_code"
                    onChange={handleChange}
                    value={dataEdit?.status_code || ''}
                    validators={[
                    ]}
                    errorMessages={[
                    ]}
                  />
                </Grid>

              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid className={classge.rootSubmit}>
          <Button
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} />
        </Grid>
      </ValidatorForm>
    </>
  )
}
