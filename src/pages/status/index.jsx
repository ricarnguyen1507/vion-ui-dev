import React, { useState } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableStatus from './table'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

export default function () {
  const history = useHistory()
  const { path } = useRouteMatch()
  const [dataTable, setDataTable] = useState(null)
  const [actionType, setActionType] = useState('')

  const [controlEditTable, setControlEditTable] = useState(false)
  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState(null)
  const [stateQuery, setStateQuery] = useState(() => ({
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null, index) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
      if(index != null) {
        setIndexEdit(index)
      }
    }
  }))

  // Function handle
  const functionBack = () => {
    history.push(`${path}`)
  }
  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setDataEdit(row)
    setIndexEdit(index)
  }
  const handleDelete = (row) => api.post('/status', { del: { uid: row.uid } })
    .then(_ => {
      const idx = dataTable.findIndex(d => d.uid === row.uid)
      dataTable.splice(idx, 1)
      setDataTable([...dataTable])
    })
    .catch(error => console.log(error))

  const handleSubmitData = (actionType, data) => {
    if(Object.keys(data).length > 0) {
      api.post('/status', data)
        .then(response => {
          const { set: newStatus } = data
          if(actionType === 'Add') {
            newStatus.uid = response.data.uids['new_status']
            setDataTable([newStatus, ...dataTable])
          } else if(actionType === 'Edit') {
            Object.assign(dataTable[indexEdit], newStatus)
            setDataTable([...dataTable])
          }
          setDataEdit(null)
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      ctx.goBack()
    }
  }

  // Display view
  return <>
    <Route path={`${path}/:actionType`}>
      <Edit
        actionType={actionType}
        originData={dataEdit}
        functionBack={functionBack}
        handleSubmitData={handleSubmitData}
      />
    </Route>
    <Route exact path={path}>
      <TableStatus
        ctx={ctx}
        data={dataTable}
        controlEditTable={controlEditTable}
        stateQuery={stateQuery}
        setStateQuery={setStateQuery}
        setDataTable={setDataTable}
        setControlEditTable={setControlEditTable}
        handleDelete={handleDelete}
        functionEditData={functionEditData}
      />
    </Route>
  </>
}
