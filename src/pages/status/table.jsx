import React, { useState } from "react";
import MaterialTable from "material-table";
import AddBox from "@material-ui/icons/AddBox";
import useStyles from 'pages/style_general'
import api from 'services/api_cms'
export default function TableStatus (props) {

  const classes = useStyles();
  const {
    ctx, stateQuery, setStateQuery, setDataTable,
    controlEditTable,
    setControlEditTable
  } = props;
  const [selectedRow, setSelectedRow] = useState(null); // Selected row
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    return api.get("/list/status", {params: {
      number: query.pageSize || 10,
      page: query.page || 0
    }})
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const column = [
    {
      title: "Status Name",
      field: "status_name",
      editable: "onUpdate",
      cellStyle: {
        paddingLeft: 30,
      },
      headerStyle: {
        paddingLeft: 30,
      },
    },
    { title: "Status Code", field: "status_code", editable: "onUpdate" },
    { title: "Status Value", field: "status_value", editable: "onUpdate" },
  ];

  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>Status</h1>}
        onRowClick={(evt, selectRow) => setSelectedRow(selectRow)}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: (rowData) => ({
            backgroundColor:
              selectedRow && selectedRow.tableData.id === rowData.tableData.id
                ? "#EEE"
                : "#FFF",
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: "Add Status",
            isFreeAction: true,
            onClick: () => {
              ctx.editData()
            },
          },
          {
            icon: "edit",
            tooltip: "Edit Status",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable);
            },
          },
          controlEditTable === false
            ? {
              icon: "edit",
              tooltip: "Edit Type",
              onClick: (event, { tableData, ...rowData }) => {
                ctx.editData(rowData, tableData.id)
              },
            }
            : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null,
        }}
      />
    </div>
  );
}
