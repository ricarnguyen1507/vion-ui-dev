import React, { useState, useEffect } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableCollection from './table'
import { LoadingContext } from "context/LoadingContext";
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory
} from "react-router-dom"

const display_status = ['PENDING', 'REJECTED', 'APPROVED']
const reference_type = ["Sản phẩm"]
const layout_type = [
  [
    {
      code: 0,
      label: 'SingleProduct'
    },
    {
      code: 1,
      label: 'MultiProduct'
    },
    {
      code: 2,
      label: 'Top10'
    }
  ]
]
const config_type = [
  {
    code: 0,
    label: 'Chọn sản phẩm'
  },
  {
    code: 1,
    label: 'Theo điều kiện'
  }
]

/**
 * Export
 */
/* function fetchDataBrandShop (url) {
  return api.get(url, {
    responseType: "json"
  }).catch(err => {
    console.log(err)
  })
} */

function fetchData (url) {
  if (url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }

}

export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const { setIsLoading } = React.useContext(LoadingContext);
  const [dataTable, setDataTable] = useState(null)

  const [controlEditTable, setControlEditTable] = useState(false)
  // const [mode, setMode] = useState("table")
  const [actionType, setActionType] = useState('')
  // const [mapTypes, setMapTypes] = useState(null)

  const [indexEdit, setIndexEdit] = useState(null)
  const [dataEdit, setDataEdit] = useState({})
  const [parentCollections, setParentCollections] = useState(null)
  const [childCollections, setChildCollections] = useState(null)
  const [brandShops, setBrandShops] = useState(null)
  const [partners, setPartners] = useState(null)
  const [brands, setBrands] = useState(null)
  const [negativeProducts, setNegativeProducts] = useState([])

  useEffect(() => {
    const fetchCollections = [
      { url: "/list/collection?t=0&is_parent=true&option_fields=true&is_temp=false", method: "get" },
      { url: "/list/collection?t=0&is_parent=false&option_fields=true&is_temp=false", method: "get" },
      { url: "/brand-shop", method: "get" },
      { url: "/list/brand", method: "get" },
      { url: "/list/partner", method: "get" }
    ].map((url) => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchCollections).then(([
      pCollections,
      cCollections,
      brand_shops,
      brands,
      partners
    ]) => {
      setParentCollections(pCollections)
      setChildCollections(cCollections)
      setBrandShops(brand_shops)
      setBrands(brands)
      setPartners(partners)
    })
  }, []);

  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))
  const [ctx] = useState(() => ({
    goBack () {
      window.history.back()
    },
    editData (row = null) {
      this.data = row
      history.push(`${path}/${row ? 'Edit' : 'Add'}`)
      setActionType(row ? 'Edit' : 'Add')
      if (row) {
        setDataEdit(row)
      }
    }
  }))
  const functionBack = () => {
    history.push(`${path}`)
  }
  const [products, setProducts] = useState([])
  const functionEditData = (actionType, row, index) => {
    setActionType(actionType)
    setIndexEdit(index)
    // setMode("edit")
  }
  // Ham xu ly thay doi data khi ma gui tu add - edit sang
  const handleSubmitData = async (actionType, { set, del, files }) => {
    // Add / Update
    if (Object.keys(set).length > 0) {
      const formData = new FormData()
      formData.set('set', JSON.stringify(set))
      formData.set('del', JSON.stringify(del))
      for (let field in files) {
        formData.set(field, files[field])
      }
      api.post('/collection', formData, {
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data"
        }
      })
        .then(response => {
          if (actionType === 'Add') {
            set.uid = response.data['new_collection'] && response.data['new_collection'][0] && response.data['new_collection'][0].uid
            setDataTable([set, ...dataTable])
          } else if (actionType === 'Edit') {
            const updateItem = response?.data?.['new_collection']?.['result']?.[0]
            dataTable[indexEdit] = { ...dataTable[indexEdit], ...updateItem }
            setDataTable([...dataTable])
          }
          setIsLoading(false)
          ctx.goBack()
        })
        .catch(error => console.log(error))
    } else {
      // setMode("table")
      ctx.goBack()
    }
  }

  const handleDelete = (row) => api.delete(`/collection/${row.uid}`)
    .then(res => {
      if (res.status === 200) {
        row.is_deleted = true
        dataTable.forEach((r) => {
          if (r.parent && r.parent.uid === row.uid) {
            r.is_deleted = true
          }
        })
        setDataTable([...dataTable])
      }
    })
    .catch(error => console.log(error))

  /* const validDataTable = (data) => {
    if (data === null) {
      // setIsLoading(true);
      return true;
    }
    // setIsLoading(false);
    return false;
  } */
  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          brandShops={brandShops}
          parentCollections={parentCollections}
          childCollections={childCollections}
          partners={partners}
          brands={brands}
          actionType={actionType}
          originData={dataEdit}
          functionBack={functionBack}
          handleSubmitData={handleSubmitData}
          displayStatus={display_status}
          referenceType={reference_type}
          layoutType={layout_type}
          configType={config_type}
          fetchData={fetchData}
          products={products}
          setProducts={setProducts}
          // setMode={setMode}
          negativeProducts={negativeProducts}
          setNegativeProducts={setNegativeProducts}
        />
      </Route>
      <Route exact path={path}>
        <TableCollection
          ctx={ctx}
          data={dataTable}
          stateQuery={stateQuery}
          setStateQuery={setStateQuery}
          setDataTable={setDataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          handleDelete={handleDelete}
          functionEditData={functionEditData}
          displayStatus={display_status}
          referenceType={reference_type}
          layoutType={layout_type}
          setProducts={setProducts}
          setNegativeProducts={setNegativeProducts}
        />
      </Route>
    </Shared>
  )
}
