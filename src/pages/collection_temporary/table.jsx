import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import ImgCdn from 'components/CdnImage'
// import ExportExcel from "./export_excel";
import EditIcon from "@material-ui/icons/Edit";
import { Grid, IconButton, Tooltip} from "@material-ui/core";
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

const collectionStatus = {
  '0': 'PENDING',
  '1': 'REJECTED',
  '2': 'APPROVED'
}
function getFilterStr ({filters}) {
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  return {strFilter: strFilter.join(' AND ')}
}
export default function TableCollection ({ setNegativeProducts, setProducts, stateQuery, setStateQuery, controlEditTable, setControlEditTable, displayStatus, setDataTable }) {
  const classes = useStyles()
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)
  const [selectedRow, setSelectedRow] = useState(null)
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.get("/list/collection?t=0&is_temp=true", {params: {
      number: query.pageSize || 10,
      page: query.page || 0,
      ...{filter: (strFilter ? ' AND ' + strFilter : '')},
    }})
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        // prepareData(data);
        setDataTable(result);
        // Data Collection
        // collectionData = setOrderList(data?.filter(r => r.is_deleted !== true));
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: collectionStatus,
        o: 'eq'
      },
      {
        title: "Tên Ngành hàng",
        field: "collection_name",
        render: rowData => !rowData.parent ? rowData.collection_name : <p>&nbsp;~&nbsp;&nbsp;{rowData.collection_name}</p>,
        o: 'eq'
      },
      {
        title: "Image",
        field: "collection_image",
        render: rowData => <ImgCdn src={rowData.collection_image} style={{ width: 128, height: 72 }} />
      },
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })


  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        parentChildData={(row, rows) => rows.find(r => row['~highlight.collections'] && row['~highlight.collections'][0] && (r.uid === row['~highlight.collections'][0].uid))}
        // title={<h1 className={classes.titleTable}>Deal</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        components={{
          Toolbar: () => (
            <>
              <div style={{padding: '0px 20px', textAlign: "right", }}>
                <Grid container className={classes.formpd}>
                  <Grid item xs={6} sm={8} lg={8} style={{textAlign: "left"}}>
                    <h1 className={classes.titleTable}>Ngành hàng tạm</h1>
                  </Grid>
                  <Grid item xs={6} sm={4} lg={4} style={{ textAlign: "right" }}>
                    <Tooltip title="Add" aria-label="add">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { ctx.set('dataEdit', null)
                        history.push(`${pathname}/Add`) }} target="_blank">
                        <AddBox />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Display Edit" aria-label="display edit">
                      <IconButton style={{color: "gray", cursor: "pointer"}} onClick={() => { setControlEditTable(!controlEditTable) }}>
                        <EditIcon />
                      </IconButton>
                    </Tooltip>
                    {/* { data &&
                                        <ExportExcel data={data} isEditTable={false} />
                    } */}
                  </Grid>
                </Grid>
              </div>
            </>
          )}}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          // exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          // {
          //     icon: AddBox,
          //     tooltip: 'Add Deal',
          //     isFreeAction: true,
          //     onClick: () => {
          //         functionEditData('Add')
          //     }
          // },
          // {
          //     icon: 'edit',
          //     tooltip: "Display Edit",
          //     isFreeAction: true,
          //     onClick: (rowData) => {
          //         setControlEditTable(!controlEditTable)
          //     }
          // },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Collection",
            onClick: (event, { tableData, ...rowData }) => {
              ctx.set('dataEdit', rowData)
              if (rowData && rowData['highlight.products']) {
                setProducts(rowData['highlight.products'] || [])
              }
              if (rowData && rowData['collection.neg_prod_cond']) {
                setNegativeProducts(rowData['collection.neg_prod_cond'])
              }
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null //controlEditTable === false ? handleDelete : null
        }}
      />
    </div>
  )
}
