import React, { useState, useContext } from 'react';
import {
  useHistory
} from "react-router-dom"
import useStyles from './edit_style'
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import CachedIcon from '@material-ui/icons/Cached';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import { genCdnUrl } from 'components/CdnImage'
import SelectMultiProduct from 'modules/SelectMulti/products/list_manage'
import SelectMultiCollectionAndCond from 'modules/SelectMulti/collection_and_cond/list_manage'
import SelectMultiCollectionOrCond from 'modules/SelectMulti/collection_or_cond/list_manage'
// import ExportExcel from "./export_excel";
import ColorPicker from './color_text'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import { InputEdge, Node, CheckBoxEdge } from 'components/GraphMutation'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { ObjectsToForm } from "utils"
let formData = new FormData();
const isuid = /^0[xX][0-9A-F]+$/i


const CollCondSectionOpt = [
  {
    section_value: "belong_brandshop",
    section_name: "Thuộc về brandshop",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 1
  },
  {
    section_value: "belong_nh1",
    section_name: "Thuộc về Ngành hàng cha",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 2
  },
  {
    section_value: "belong_nh2",
    section_name: "Thuộc về Ngành hàng con",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 3
  },
  {
    section_value: "has_price_with_vat",
    section_name: "Có giá bán",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 4
  },
  {
    section_value: "has_product_tags",
    section_name: "Có product tag",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 5
  },
  {
    section_value: "belong_supplier",
    section_name: "Thuộc nhà cung cấp",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 6
  },
  {
    section_value: "belong_brand",
    section_name: "Thuộc thương hiệu",
    section_type: "item",
    section_ref: "",
    section_ref_value: "",
    display_order: 7
  },
]
const PriceConds = [
  {
    uid: "0xLE(value)",
    section_name: "Tối đa"
  },
  {
    uid: "0xGE(value)",
    section_name: "Tối thiểu"
  },
  {
    uid: "0xEQ(value)",
    section_name: "Bằng"
  },
]
const TagConds = [
  {
    uid: "0xNameTag",
    code: "name_tag",
    section_name: "Name Tag"
  },
  {
    uid: "0xAssess",
    code: "assess",
    section_name: "Assess"
  },
  {
    uid: "0xPromotion",
    code: "promotion",
    section_name: "Promotion"
  },
  {
    uid: "0xShipping",
    code: "shipping",
    section_name: "Shipping"
  },
]

export default function ({ originData = {}, brandShops, parentCollections, childCollections, partners, brands, functionBack, displayStatus, layoutType, configType, products, setProducts, negativeProducts, setNegativeProducts }) {
  const history = useHistory()
  const classge = useGeneral();
  const classes = useStyles()

  const ctx = useContext(context)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_collection",
    "dgraph.type": "Collection",
    "is_temporary": true,
    "reference_type": 0,
    "layout_type": 1,
    "config_type": 0,
    "collection_type": 0
  })
  delete dataEdit?.['highlight.products|display_order']
  if(dataEdit?.['highlight.products']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['highlight.products']) {
      obj['highlight.products|display_order'] = `<highlight.products> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }

  delete dataEdit?.['collection.and_cond|display_order']
  if(dataEdit?.['collection.and_cond']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['collection.and_cond']) {
      obj['collection.and_cond|display_order'] = `<collection.and_cond> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  delete dataEdit?.['collection.or_cond|display_order']
  if(dataEdit?.['collection.or_cond']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['collection.or_cond']) {
      obj['collection.or_cond|display_order'] = `<collection.or_cond> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }
  delete dataEdit?.['collection.neg_prod_cond|display_order']
  if(dataEdit?.['collection.neg_prod_cond']?.length > 0) {
    let number = 0
    for(let obj of dataEdit?.['collection.neg_prod_cond']) {
      obj['collection.neg_prod_cond|display_order'] = `<collection.neg_prod_cond> <${obj.uid}> (display_order=${number}) .`
      number++
    }
  }

  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  const [node] = useState(() => new Node(dataEdit))
  const [layoutTypeList] = useState(() => (originData != undefined) ? originData.reference_type == 0 ? layoutType[0] : originData.reference_type == 1 ? layoutType[1] : originData.tag_category == 2 ? layoutType[2] : layoutType[0] : layoutType[0])
  const [listProductUids, setListProductUids] = useState(() => dataEdit['highlight.products']?.map(p => p.uid).join(','))
  const [updated] = useState({
    values: {
      'display_status': dataEdit?.display_status || 0,
      // 'list_product_highlight':'',
      'reference_type': 0
    }
  })

  const [files] = useState({})

  const updateImage = (field, file) => {
    node.setState(field, `images/collection/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }

  const handleProductHighLight = async (e) => {
    e.preventDefault()
    const { value = "" } = e.target
    var lastChar = value.substr(value.length - 1);
    var lastWord = value.split(',');
    let checkUid = isuid.test(lastWord[lastWord.length - 1])
    if (lastChar != ',' && checkUid == true) {
      formData.set("uid", value)
      const res = await api.post('/get/productList', formData)
      const responseProd = []
      value.split(',').map((v, idx) => {
        let find = res.data?.result?.find(r => r.uid == v) ?? {}
        if (Object.keys(find).length > 0) {
          responseProd.push({
            uid: find.uid,
            'highlight.products|display_order': `<highlight.products> <${find.uid}> (display_order=${idx}) .`
          })
        }
      })
      let listHighligtOld = dataEdit?.['highlight.products'] ?? []
      setListProductUids(responseProd.map(s => s.uid).join(','))

      // let del = listHighligtOld?.map(r => ({uid: r.uid}))
      if(responseProd.length) {
        await updateMultiSelectProduct(responseProd, listHighligtOld, node, 'highlight.products')
      }
    } else if (value == null || value == '') {
      setDataEdit({
        ...dataEdit,
        // [name]: value,
        'highlight.products': [],
      })
    } else {
      setDataEdit({
        ...dataEdit,
        // [name]: value
      })
    }

  }
  // console.log('setDataEdit',dataEdit.list_product_highlight)

  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/collection_temporary')
  }
  const handleSubmit = function () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/collectionNewForm/" + dataEdit?.uid, formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    updated.values['display_status'] = displayStatus.indexOf(item)
    node.setState('display_status', displayStatus.indexOf(item))
  }

  const handleChangeLayoutType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'layout_type': item.code })
    node.setState('layout_type', item.code)
  }
  const handleChangeConfigType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'config_type': item.code })
    node.setState('config_type', item.code)
  }
  const [layoutTypeSelected] = useState(() => layoutTypeList.find(l => l.code === dataEdit?.layout_type))


  const updateListProduct = async ({ set }) => {
    setListProductUids(set.map(s => s.uid).join(','))
    let listHighligtOld = dataEdit?.['highlight.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'highlight.products')
  }
  /**
     * START: collection conds
     * Lấy các conds đã set trước đó để tạo query lần đầu
     */
  const [listConditions, setListConditions] = useState(() => ({
    and: dataEdit?.['collection.and_cond'] || [],
    or: dataEdit?.['collection.or_cond'] || [],
  }))

  const updateListCollectionAndCond = ({ set, del }) => {
    let and_cond = node.getState('collection.and_cond') ?? []
    if(set.length >= and_cond.length) {
      node.setState("collection.and_cond", set, true)
      and_cond = node.getState('collection.and_cond') ?? []
    } else if(set.length < del.length) {
      if(and_cond.length) {
        const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
        and_cond.map(ac => removeItem.find(ri => ac.state.uid == ri.uid ? ac.isDeleted = true : ac.isDeleted = false))
      }
    }
    setListConditions({ ...listConditions, and: [...set] })
  }

  const updateListCollectionOrCond = ({ set, del }) => {
    let or_cond = node.getState('collection.or_cond') ?? []
    if(set.length >= or_cond.length) {
      node.setState("collection.or_cond", set, true)
      or_cond = node.getState('collection.or_cond') ?? []
    } else if(set.length < del.length) {
      if(or_cond.length) {
        const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
        or_cond.map(rc => removeItem.find(ri => rc.state.uid == ri.uid ? rc.isDeleted = true : rc.isDeleted = false))
      }
    }
    setListConditions({ ...listConditions, or: [...set] })
  }

  const [conditionNegProduct] = useState({
    set: {},
    del: {}
  })
  const updateListNegProduct = ({ set }) => {
    conditionNegProduct.set = set
    let listNegOld = dataEdit?.['collection.neg_prod_cond'] ?? []
    const removeItem = listNegOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('collection.neg_prod_cond', mergeArr, false)
      let negative_products = node.getState('collection.neg_prod_cond') ?? []
      negative_products.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('collection.neg_prod_cond', set, true)
    }
  }
  const onInputSelectNegProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchNegProdOption(queries)
    }, 550))
  }
  function fetchNegProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          res.data.result.forEach(r => {
            if (!negativeProducts.find(no => no.uid === r.uid)) {
              setNegativeProducts([...negativeProducts, r])
            }
          })
        })
    }
  }
  function updateListProdCond ({set, del}) {
    const removeItem = del?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem?.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('highlight.products', mergeArr, false)
      let highlight = node.getState('highlight.products') ?? []
      highlight.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('highlight.products', set, true)
    }
  }
  /** END collection conds */

  const [color, setColor] = useState(() => ({
    'color_title': dataEdit?.color_title || "#f48024",
    'color_price': dataEdit?.color_price || "#f48024",
    'color_discount': dataEdit?.color_discount || "#f48024",
  }))

  const handleColor = (color_picked, fieldName) => {
    setColor({ ...color, [fieldName]: color_picked.hex })
    setDataEdit({ ...dataEdit, [fieldName]: color_picked.hex })
    node.setState(fieldName, color_picked.hex)
  }

  const [timer, setTimer] = useState(null)
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = dataEdit?.['hilight_products'] ? dataEdit?.['hilight_products'] : products
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }


  const getProdFromConditions = () => {
    if (dataEdit.config_type === 1) {
      api.post('/config-condition/list/products', {
        and_cond: listConditions.and,
        or_cond: listConditions.or,
        neg_prod_cond: conditionNegProduct.set?.length > 0 ? conditionNegProduct.set.map(cd => cd.uid) : dataEdit?.['collection.neg_prod_cond']?.map(cd => cd.uid)
      }, {
        headers: {
          "Accept": "application/json"
        }
      }).then(res => {
        if (res.status === 200) {
          const { uids, total } = res.data
          let set = uids.map((n, idx) => ({ uid: n, display_order: idx, 'highlight.products|display_order': `<highlight.products> <${n}> (display_order=${idx}) .` }))
          let del = dataEdit?.['highlight.products']?.map(n => ({ uid: n.uid }))
          updateListProdCond({set, del})
          alert(`Có tất cả ${total} sản phẩm thoả điều kiện đã tạo. Danh sách uids: ${uids?.join(',') || ''}`)
        }
      })
    }
  }

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  const [multiSelectInvalid, setMultiSelectInvalid] = useState(true)
  function invalidCallback (result) {
    setMultiSelectInvalid(result)
  }

  let validateForm = {}
  // validateForm = useMemo(() => {
  //     if(!dataEdit.collection_image || dataEdit.collection_image === ""){
  //         return {
  //             ...validateForm,
  //             icon: {
  //                 error: true,
  //                 helperText: "This field is required"
  //             }
  //         }
  //     } else {
  //         return {}
  //     }
  // }, [dataEdit])

  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />

  // const [clonedData] = useState(() => [clone(dataEdit)]);
  return (
    <>
      <PageTitle title="Ngành Hàng Tạm" />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                      Information
                    </Typography>
                  </div>
                  <Divider />
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <CheckBoxEdge
                            Component={Checkbox}
                            node={node}
                            pred={"type_highlight"}
                            fullWidth
                            variant="outlined"
                            margin="dense"
                            color="primary"
                            style={{margin: "25px 10px"}}
                          />
                        }
                        label="Highlight (hiện ảnh 16:9)"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <div className={classes.colorPicker} style={{ margin: "15px 0" }}>
                            <ColorPicker
                              color={color.color_title}
                              fieldName="color_title"
                              handleColor={handleColor}
                            />
                          </div>
                        }
                        label="&nbsp;&nbsp;Màu (Tiêu đề)"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <div className={classes.colorPicker} style={{ margin: "15px 0" }}>
                            <ColorPicker
                              color={color.color_price}
                              fieldName="color_price"
                              handleColor={handleColor}
                            />
                          </div>
                        }
                        label="&nbsp;&nbsp;Màu (Giá bán)"
                      />
                    </Grid>
                    <Grid item xs={12} sm={6} lg={3}>
                      <FormControlLabel
                        control={
                          <div className={classes.colorPicker} style={{ margin: "15px 0" }}>
                            <ColorPicker
                              color={color.color_discount}
                              fieldName="color_discount"
                              handleColor={handleColor}
                            />
                          </div>
                        }
                        label="&nbsp;&nbsp;Màu (Giá gốc)"
                      />
                    </Grid>
                  </Grid>
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"collection_name"}
                    fullWidth
                    label="Collection Name"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                  <InputEdge
                    Component={TextValidator}
                    node={node}
                    pred={"display_name"}
                    fullWidth
                    label="Tên trên CMS"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required',
                      'minStringLength:3',
                      'maxStringLength:50',
                    ]}
                    errorMessages={[
                      'This field is required',
                      'Min length is 3',
                      'Max length is 50']}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} lg={12}>
                <div className={classge.rootPanel}>
                  <div className={classge.titlePanel}>
                    <div className={classge.iconTitle}>
                      {ActionIcon}
                    </div>
                    <Typography className={classge.contentTitle} variant="h5" >
                      Sản phẩm
                    </Typography>
                  </div>
                  <Divider />
                  <Grid container spacing={2}>
                    <Grid item xs={6} sm={6} lg={6}>
                      <Autocomplete
                        options={layoutTypeList}
                        getOptionLabel={option => option.label ?? ""}
                        defaultValue={layoutTypeSelected}
                        onChange={handleChangeLayoutType}
                        style={{ width: "100%" }}
                        renderInput={params => (
                          <TextField {...params}
                            label="Kiểu hiện thị"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                          />
                        )}
                      />
                    </Grid>
                    <Grid item xs={6} sm={6} lg={6}>
                      <Autocomplete
                        options={configType}
                        getOptionLabel={option => option?.label ?? ""}
                        defaultValue={dataEdit?.config_type ? configType?.find(cf => cf.code === dataEdit?.config_type) : configType[0]}
                        onChange={handleChangeConfigType}
                        style={{ width: "100%" }}
                        renderInput={params => (
                          <TextField {...params}
                            label="Phương thức chọn sản phẩm"
                            variant="outlined"
                            fullWidth
                            margin="dense"
                          />
                        )}
                      />
                    </Grid>
                  </Grid>

                  {!dataEdit?.config_type ?
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={12} lg={12}>
                        <TextField
                          id="list_product_highlight"
                          name="list_product_highlight"
                          label="Danh sách UID sản phẩm"
                          type="text"
                          size="small"
                          variant="outlined"
                          fullWidth
                          value={listProductUids}
                          onChange={handleProductHighLight}
                        />
                        <SelectMultiProduct
                          isShow={dataEdit?.reference_type === 0}
                          listOption={products}
                          currentList={dataEdit?.['highlight.products'] || []}
                          actionType={actionType}
                          dataEdit={dataEdit}
                          updateListManage={updateListProduct}
                          maxItem="-1"
                          inputChange={onInputSelectProductChange}
                          listTitle="Thêm sản phẩm"
                          facetPrefix="highlight.products"
                          invalidCallback={invalidCallback}
                        />
                      </Grid>
                    </Grid> :
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={12} lg={12}>
                        <SelectMultiCollectionAndCond
                          isShow={dataEdit?.config_type}
                          CollCondSectionOpt={CollCondSectionOpt}
                          brandShops={brandShops || []}
                          parentCollections={parentCollections}
                          childCollections={childCollections}
                          PriceConds={PriceConds}
                          TagConds={TagConds}
                          partners={partners || []}
                          brands={brands || []}
                          currentList={dataEdit?.['collection.and_cond'] || []}
                          actionType={actionType}
                          dataEdit={dataEdit}
                          updateListManage={updateListCollectionAndCond}
                          maxItem="-1"
                          minItem="1"
                          listTitle="Các điều kiện bắt buộc"
                          facetPrefix="collection.and_cond"
                          invalidCallback={invalidCallback}
                        />
                      </Grid>
                      <Grid item xs={12} sm={12} lg={12}>
                        <SelectMultiCollectionOrCond
                          isShow={dataEdit?.config_type}
                          CollCondSectionOpt={CollCondSectionOpt}
                          brandShops={brandShops}
                          parentCollections={parentCollections}
                          childCollections={childCollections}
                          PriceConds={PriceConds}
                          TagConds={TagConds}
                          partners={partners}
                          brands={brands}
                          currentList={dataEdit?.['collection.or_cond'] || []}
                          actionType={actionType}
                          dataEdit={dataEdit}
                          updateListManage={updateListCollectionOrCond}
                          maxItem="-1"
                          minItem="0"
                          listTitle="Các điều kiện thay thế cho điều kiện bắt buộc"
                          facetPrefix="collection.or_cond"
                          invalidCallback={invalidCallback}
                        />
                      </Grid>
                    </Grid>
                  }
                </div>
              </Grid>
              {(dataEdit?.config_type == 1) ?
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <div className={classge.titlePanel}>
                      <div className={classge.iconTitle}>
                        {ActionIcon}
                      </div>
                      <Typography className={classge.contentTitle} variant="h5" >
                            Sản phẩm KHÔNG áp dụng
                      </Typography>
                    </div>
                    <Divider />
                    <SelectMultiProduct
                      listOption={negativeProducts}
                      currentList={dataEdit?.['collection.neg_prod_cond'] || []}
                      actionType={actionType}
                      dataEdit={dataEdit}
                      updateListManage={updateListNegProduct}
                      maxItem="-1"
                      listTitle="Thêm sản phẩm KHÔNG áp dụng"
                      facetPrefix="collection.neg_prod_cond"
                      inputChange={onInputSelectNegProductChange}
                    />
                  </div>
                </Grid> : ''
              }
            </Grid>
            <div className={classge.rootBasic}>
              <div className={classge.titleProduct}>
                <Typography className={classge.title} variant="h3">
                  Xét duyệt
                </Typography>
              </div>
              <Grid container className={classge.rootListCate} spacing={2}>
                <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                  <Autocomplete
                    options={displayStatus}
                    value={displayStatus[dataEdit?.display_status] || "PENDING"}
                    onChange={handleChangeDisplayStatus}
                    style={{ width: "100%" }}
                    renderInput={params => (
                      <TextField {...params}
                        label="Trạng thái hiển thị"
                        variant="outlined"
                        fullWidth
                        margin="dense"
                      />
                    )}
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item>
            <h5> Image (1600x900)</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit?.collection_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="collection_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
          <Grid item>
            <h5> Background</h5>
            <Card>
              <CardMedia children={<Media src={genCdnUrl(dataEdit?.background_image, "collection_image.png")} type="image" style={{ width: 512, height: 256 }} fileHandle={updateImage} field="background_image" accept="image/jpeg, image/png" />} />
            </Card>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{ marginRight: '10px' }}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={functionBack}
          >
            <ReturnIcon className={classge.iconback} />
            Return
          </Button>

          <Button
            style={{ marginRight: '10px' }}
            color="primary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={getProdFromConditions}
          >
            <CachedIcon className={classge.iconback} />
            Tải lại Danh Sách SP
          </Button>

          <RoleButton actionType={actionType} disabled={Object.keys(validateForm).length !== 0 || multiSelectInvalid} />
          {/* {actionType === "Edit" &&
            <ExportExcel data={[dataEdit]} isEditTable={true} />
          } */}
        </Grid>
      </ValidatorForm>
    </>
  )
}
