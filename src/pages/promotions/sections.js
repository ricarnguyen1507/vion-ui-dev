const buttonDirectSection = [
  {
    link_name: "Sản phẩm",
    link_direct: "product",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "LiveStream",
    link_direct: "livestream",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "Bộ sưu tập",
    link_direct: "collection_temp",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "Ngành hàng cha",
    link_direct: "collection",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "Brand shop",
    link_direct: "brand_shop",
    link_value: "",
    link_action: ""
  }
]
const buttonDirectBrandShopSection = [
  {
    link_name: "Sản phẩm",
    link_direct: "product",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "LiveStream brandShop",
    link_direct: "livestream",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "Bộ sưu tập brandShop",
    link_direct: "collection_temp",
    link_value: "",
    link_action: ""
  },
  {
    link_name: "Ngành hàng brandShop",
    link_direct: "collection",
    link_value: "",
    link_action: ""
  }
]

module.exports = {buttonDirectSection, buttonDirectBrandShopSection}