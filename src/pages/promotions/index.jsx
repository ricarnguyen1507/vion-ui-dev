import React, { useState, useEffect, useMemo } from 'react'
import api from 'services/api_cms'
import Edit from './edit'
import TableLayout from './table'
import { Shared } from 'context/Shared'
import {
  Route,
  useRouteMatch,
  useHistory,
  useLocation
} from "react-router-dom"
// import {
//   Route,
//   useRouteMatch,
//   useHistory
// } from "react-router-dom"
import BrandShopSelections from 'components/BrandShop_Promotion'


function fetchData (url) {
  if(url.method == "post") {
    return api.post(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  } else {
    return api.get(url.url, {
      responseType: "json"
    }).catch(err => {
      console.log(err)
    })
  }
}

const display_status = ['PENDING', 'REJECTED', 'APPROVED']
const config_type = [
  {
    code: 0,
    label: 'Chọn sản phẩm'
  },
  {
    code: 1,
    label: 'Tất cả sản phẩm'
  },
  {
    code: 2,
    label: 'Collection'
  },
  {
    code: 3,
    label: 'Nghành hàng cha'
  },
  {
    code: 4,
    label: 'Nghành hàng con'
  },
  {
    code: 5,
    label: 'Nghành hàng brandshop'
  }
]
/**
 * Export
 */
export default function () {
  const [sharedData] = useState(() => new Map())
  const history = useHistory()
  const { path } = useRouteMatch()
  const { pathname } = useLocation()
  const [dataTable, setDataTable] = useState(null)
  const [collections, setCollections] = useState(null)
  const [liveStream, setLiveStream] = useState(null)
  const [collectionTemp, setCollectionTemp] = useState(null)
  const [negativeProducts, setNegativeProducts] = useState([])
  const [controlEditTable, setControlEditTable] = useState(true)

  const [customers, setCustomers] = useState([])
  const [brandShop, setBrandShop] = useState([])
  const [products, setProducts] = useState([])
  const [stateQuery, setStateQuery] = useState(() => ({
    filters: [],
    page: 0,
    pageSize: 10,
    totalCount: 0,
  }))

  const functionBack = () => {
    history.push(`${path}`)
  }

  useEffect(() => {
    const fetchs = [
      {url: '/brand-shop', method: "get"},
    ].map(url => fetchData(url).then(res => res?.data?.result))
    Promise.all(fetchs).then(([brand_shops]) => {
      brand_shops.unshift({
        uid: null,
        brand_shop_name: "Không"
      })
      setBrandShop(brand_shops)
    })
  }, []);

  // const functionBack = () => {
  //   setMode("table")
  // }

  /**
   * START Dialog BrandShopSelections
   */
  const [open, setOpen] = useState(false);
  const [selectedBrandShop, setSelectedBrandShop] = useState(() => brandShop[0], [brandShop])
  const [selectedPromotion, setSelectedPromotion] = useState(null)

  const handleClickOpen = () => {
    setOpen(true)
  };

  const handleClose = () => {
    setOpen(false)
  };
  const handleNext = () => {
    setOpen(false)
    history.push(`${pathname}/Add`)

    setProducts([])
    /**
     * check suboptions
     */
  };

  const {buttonDirectSection, buttonDirectBrandShopSection} = require('./sections')
  const promotionSectionOpt = useMemo(() => {
    if (!selectedBrandShop?.uid || selectedBrandShop?.uid === null) {
      return buttonDirectSection
    } else {
      return buttonDirectBrandShopSection
    }
  }, [selectedBrandShop])

  const configSection = useMemo(() => {
    if (!selectedBrandShop?.uid || selectedBrandShop?.uid === null) {
      delete config_type[5]
      return config_type
    } else {
      return config_type
    }
  }, [selectedBrandShop])

  const [timer, setTimer] = useState(null)
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products || []
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    if (selectedBrandShop?.uid && selectedBrandShop?.uid !== null) {
      queries = {...queries, brand_shop_uid: selectedBrandShop?.uid}
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }


  useEffect(() => {
    // Load data sub options
    let video_stream = {url: '/video-stream-options', method: "get"}
    let collection = {url: '/list/collection?t=0&is_temp=false&is_parent=true&option_fields=true', method: "get"}
    let collection_temp = {url: '/list/collection?t=0&is_temp=true&option_fields=true', method: "get"}
    if(!selectedBrandShop || selectedBrandShop?.uid == null) {
      const fetchs = [
        {...video_stream},
        {...collection},
        {...collection_temp},
      ].map(url => fetchData(url).then(res => res?.data?.result))
      Promise.all(fetchs).then(([live_streams, collection, collection_temp]) => {
        setLiveStream(live_streams)
        setCollections(collection)
        setCollectionTemp(collection_temp)
      })
    }
  }, [selectedBrandShop]);
  /**
   * END Dialog BrandShopSelections
   */

  /* const functionEditData = (actionType, row) => {
    // Khi edit mà nếu ko phải hybrid của brandshop thì lấy Home
    setSelectedBrandShop(row?.['hybrid_layout.brand_shop'] || brandShop[0])
    setActionType(actionType)
    setDataEdit(row)
    if (row && row['layout.customers']) {
      setCustomers(row['layout.customers'])
    }
    if (actionType === "Add") {
      handleClickOpen()
    }
  } */

  const handleDelete = (row) => api.delete(`/hybrid_layout/${row.uid}`)
    .then(res => {
      if (res.data.statusCode === 200) {
        const idx = dataTable.findIndex(d => d.uid === row.uid)
        dataTable.splice(idx, 1)
        setDataTable([...dataTable])
      } else {
        alert(res.data.message)
      }
    })
    .catch(error => console.log(error))

  // Display view
  return (
    <Shared value={sharedData}>
      <Route path={`${path}/:actionType`}>
        <Edit
          collections={collections}
          liveStream={liveStream}
          collectionTemp={collectionTemp}
          brandShop={brandShop}
          products={products}
          setProducts={setProducts}
          functionBack={functionBack}
          promotionSectionOpt={promotionSectionOpt}
          customers={customers}
          setCustomers={setCustomers}
          displayStatus={display_status}
          selectedBrandShop={selectedBrandShop}
          selectedPromotion={selectedPromotion}
          onInputSelectProductChange={onInputSelectProductChange}
          stateQuery={stateQuery}
          configType={configSection}
          negativeProducts={negativeProducts}
          setNegativeProducts={setNegativeProducts}
          setLiveStream = {setLiveStream}
          setCollections = {setCollections}
          setCollectionTemp = {setCollectionTemp}
        />
      </Route>
      <Route exact path={path}>
        <TableLayout
          // setDataTable={setDataTable}
          controlEditTable={controlEditTable}
          setControlEditTable={setControlEditTable}
          collections={collections}
          handleDelete={handleDelete}
          setCustomers={setCustomers}
          displayStatus={display_status}
          setStateQuery={setStateQuery}
          stateQuery={stateQuery}
          handleClickOpen={handleClickOpen}
          setSelectedBrandShop={setSelectedBrandShop}
          setNegativeProducts={setNegativeProducts}
        />
        <BrandShopSelections
          selectedPromotion = {selectedPromotion}
          setSelectedPromotion={setSelectedPromotion}
          selectedBrandShop={selectedBrandShop}
          setSelectedBrandShop={setSelectedBrandShop}
          open={open}
          onClose={handleClose}
          onNext={handleNext}
          brandShop={brandShop}
          defaultHomeUid="home"
        />
      </Route>
    </Shared>
  )
}
