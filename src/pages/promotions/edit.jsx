import React, { useState, useMemo, useContext, useEffect } from 'react';
import {
  useHistory
} from "react-router-dom"
import RoleButton from 'components/Role/Button';
import ReturnIcon from '@material-ui/icons/KeyboardReturn';
import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import PageTitle from "components/PageTitle"
import AddBox from '@material-ui/icons/AddBox'
import EditIcon from '@material-ui/icons/Edit'
import Divider from '@material-ui/core/Divider'
import useGeneral from 'pages/style_general'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import { updateMultiSelectProduct } from 'services/updateMultiSelect'
import TextEditor from "components/Editor/text-editor"
import SelectMultiProduct from 'modules/SelectMulti/products/list_manage'
import SelectMultiButtonDirect from 'modules/SelectMulti/buttons/list_manage'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia'
import Media from 'components/Media'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import {
  KeyboardDateTimePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import Loading from 'components/Loading';
import DateFnsUtils from '@date-io/date-fns';
import Checkbox from '@material-ui/core/Checkbox'
import { genUid } from "utils"
import { context } from 'context/Shared'
import api from 'services/api_cms'
import { InputEdge, Node, CheckBoxEdge } from 'components/GraphMutation'
import { ObjectsToForm } from "utils"
import { genCdnUrl } from 'components/CdnImage'

let formData = new FormData();
const isuid = /^0[xX][0-9A-F]+$/i
// Add style
function isValidDate (d) {
  return d instanceof Date && !isNaN(d);
}
export default function ({ setLiveStream, setCollections, setCollectionTemp, collections, liveStream, collectionTemp, brandShop, configType, products, setProducts, setNegativeProducts, negativeProducts, promotionSectionOpt, displayStatus, onInputSelectProductChange, selectedBrandShop, selectedPromotion }) {

  const classge = useGeneral()
  const history = useHistory()
  const ctx = useContext(context)
  const [files] = useState({})
  const [isLoading, setLoading] = useState(false)
  const [dataEdit, setDataEdit] = useState(() => ctx.get('dataEdit') || {
    "uid": "_:new_promotion",
    "dgraph.type": "Promotions",
    'display_status': 0,
    'from_date': new Date().getTime(),
    'to_date': new Date().getTime(),
    'brand_shop_name': selectedBrandShop?.uid != null ? selectedBrandShop?.brand_shop_name : 'Không',
    'promotion_name': selectedPromotion ? selectedPromotion?.promotion_name : '',
    'config_type': selectedPromotion?.promotion_type == 'freeship' ? 5 : 0
  })
  // const [buttonSections, setButtonSections] = useState(() => dataEdit?.['promotion.button_direct'] || [])

  const actionType = dataEdit.uid.startsWith('_:') ? 'Add' : 'Edit'
  // const checkDecimal = /^[-+]?[0-9]+\.[0-9]+$/
  // delete dataEdit['layout.layout_section|display_order']
  const [node] = useState(() => new Node(dataEdit))
  const [promotionValueNode] = useState(() => node.getEdge("promotion.promotion_value", { uid: "_:new_promotion_value", "dgraph.type": "PromotionValue", ...selectedPromotion}).state)
  /* const [brandShopNode] = useState(() => {
    if(dataEdit['promotion.brand_shop'] || selectedBrandShop.uid != null) {
      return node.getEdge("promotion.brand_shop", actionType == 'Edit' ? dataEdit['promotion.brand_shop'] : { uid: selectedBrandShop.uid, brand_shop_name: selectedBrandShop.brand_shop_name }).state
    }
  }) */
  const [timer, setTimer] = useState(null)
  const [listProductUids, setListProductUids] = useState(() => dataEdit['promotion.products']?.map(p => p.uid).join(','))
  function goBack () {
    ctx.set('dataEdit', null)
    history.replace('/app/promotions')
  }
  async function handleSubmit () {
    const formData = ObjectsToForm(node.getMutationObj(), files)
    api.post("/promotion", formData)
      .then(() => {
        goBack()
      })
      .catch(err => {
        console.log(err)
      })
  }
  const [options] = useState(() => promotionSectionOpt?.map(ls => ({
    uid: genUid("button_direct"),
    ...ls
  })))

  const handleProductHighLight = async (e) => {
    e.preventDefault()
    const { value = "" } = e.target
    var lastChar = value.substr(value.length - 1);
    var lastWord = value.split(',');
    let checkUid = isuid.test(lastWord[lastWord.length - 1])
    if (lastChar != ',' && checkUid == true) {
      formData.set("uid", value)
      const res = await api.post('/get/productList', formData)
      const responseProd = []
      value.split(',').map((v, idx) => {
        let find = res.data?.result?.find(r => r.uid == v) ?? {}
        if (Object.keys(find).length > 0) {
          responseProd.push({
            uid: find.uid,
            'promotion.products|display_order': `<promotion.products> <${find.uid}> (display_order=${idx}) .`
          })
        }
      })
      let listHighligtOld = dataEdit?.['promotion.products'] ?? []
      setListProductUids(responseProd.map(s => s.uid).join(','))

      // let del = listHighligtOld?.map(r => ({uid: r.uid}))
      if(responseProd.length) {
        await updateMultiSelectProduct(responseProd, listHighligtOld, node, 'promotion.products')
      }
    } else if (value == null || value == '') {
      setDataEdit({
        ...dataEdit,
        // [name]: value,
        'promotion.products': [],
      })
    } else {
      setDataEdit({
        ...dataEdit,
        // [name]: value
      })
    }

  }
  const dateStartPromotion = useMemo(() => {
    if(dataEdit['from_date']) {
      return new Date(dataEdit['from_date']);
    }
    return null;
  }, [dataEdit])
  const handleChangeDateStart = (date) => {
    if(isValidDate(date)) {
      setDataEdit({ ...dataEdit,
        'from_date': new Date(date).getTime(),
      })
    }
    node.setState('from_date', new Date(date).getTime())
  };
  const dateEndPromotion = useMemo(() => {
    if(dataEdit['to_date']) {
      return new Date(dataEdit['to_date']);
    }
    return null;
  }, [dataEdit])
  const handleChangeDateEnd = (date) => {
    if(isValidDate(date)) {
      setDataEdit({ ...dataEdit,
        'to_date': new Date(date).getTime(),
      })
    }
    node.setState('to_date', new Date(date).getTime())
  };

  /* const updateListPromotionValue = async ({set, del}) => {
    let listLaySecOld = dataEdit?.['promotion.promotion_value'] ?? []
    await updateMultiSelectProduct(set, listLaySecOld, node, 'promotion.promotion_value')
    // setButtonSections(set)
  } */
  const handleChangeConfigType = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'config_type': item.code })
    node.setState('config_type', item.code)
  }
  const handleChangeDisplayStatus = (e, item) => {
    e.preventDefault()
    setDataEdit({ ...dataEdit, 'display_status': displayStatus.indexOf(item) })
    node.setState('display_status', displayStatus.indexOf(item))
  }
  const updateListProduct = async ({ set }) => {
    setListProductUids(set.map(s => s.uid).join(','))
    let listHighligtOld = dataEdit?.['promotion.products'] ?? []
    await updateMultiSelectProduct(set, listHighligtOld, node, 'promotion.products')
  }
  const [conditionNegProduct] = useState({
    set: {},
    del: {}
  })
  const updateListNegProduct = ({ set }) => {
    conditionNegProduct.set = set
    let listNegOld = dataEdit?.['promotion.neg_prod_cond'] ?? []
    const removeItem = listNegOld?.filter(({ uid: uid1 }) => !set?.some(({ uid: uid2 }) => uid1 === uid2));
    if(removeItem.length > 0) {
      let mergeArr = set.concat(removeItem)
      node.setState('promotion.neg_prod_cond', mergeArr, false)
      let negative_products = node.getState('promotion.neg_prod_cond') ?? []
      negative_products.map(hl => removeItem.find(ri => hl.state.uid == ri.uid ? hl.isDeleted = true : hl.isDeleted = false))
    } else {
      node.setState('promotion.neg_prod_cond', set, true)
    }
  }
  const updateListButton = async ({set}) => {
    node.setState('promotion.button_direct', set[0], true)
    // setButtonSections(set)
  }

  const onInputSelectNegProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchNegProdOption(queries)
    }, 550))
  }
  function fetchNegProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          res.data.result.forEach(r => {
            if (!negativeProducts.find(no => no.uid === r.uid)) {
              setNegativeProducts([...negativeProducts, r])
            }
          })
        })
    }
  }
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */
  // const [multiSelectInvalid, setMultiSelectInvalid] = useState(false)

  // function invalidCallback (result) {
  //   setMultiSelectInvalid(result)
  // }

  function handleFileChange (field, file) {
    // setDataEdit({ ...dataEdit, [field]: file})
    node.setState(field, `images/promotion/${file.md5}.${file.type.split('/')[1]}`, true)
    files[file.md5] = file
  }
  useEffect(() => {
    if (selectedBrandShop?.uid && selectedBrandShop?.uid !== null) {
      setLoading(true)
      const fetchs = [
        `/list/collection?t=400&is_temp=false&is_parent=false&option_fields=true&brand_shop_uid=${selectedBrandShop.uid}`,
        `/list/collection?t=0&is_temp=true&option_fields=true&brand_shop_uid=${selectedBrandShop.uid}`,
        `/video-stream-options?brand_shop_uid=${selectedBrandShop?.uid}`
      ].map(url => api.get(url).then(res => res.data.result))
      Promise.all(fetchs).then(([collection, collection_temp, live_streams]) => {
        setLiveStream(live_streams)
        setCollections(collection)
        setCollectionTemp(collection_temp)
        setLoading(false)
      }).catch(() => {
      })
    }
  }, [selectedBrandShop]);

  let validateForm = {}
  validateForm = useMemo(() => {
    if (dataEdit?.to_date < dataEdit?.from_date) {
      return {
        ...validateForm,
        dateEnd: {
          error: true,
          helperText: `End date invalid`
        }
      }
    } else {
      return {}
    }
  }, [dataEdit])
  /**
     * Giữ đoạn validate này ở cuối cùng nhé, tks
     */

  // Render
  const ActionIcon = actionType === "Add" ? <AddBox className={classge.iconAction} /> : <EditIcon className={classge.iconAction} />
  if(isLoading)
    return <Loading />
  return (
    <>
      <PageTitle title={`Hybrid Layout ${dataEdit?.brand_shop_name || ""}`} />
      <ValidatorForm onSubmit={handleSubmit} className={classge.root}>
        <Grid container spacing={2} >
          <Grid item xs={12} sm={12} lg={12}>
            <div className={classge.rootPanel}>
              <div className={classge.titlePanel}>
                <div className={classge.iconTitle}>
                  {ActionIcon}
                </div>
                <Typography className={classge.contentTitle} variant="h5" >
                                    Thông tin cơ bản
                </Typography>
              </div>
              <Divider />
              <Grid container spacing={2}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6} lg={6}>
                    <InputEdge
                      Component={TextValidator}
                      node={node}
                      pred={"display_name_detail"}
                      fullWidth
                      label="Tên chi tiết"
                      variant="outlined"
                      margin="dense"
                      validators={[
                        'required',
                        'minStringLength:3',
                        'maxStringLength:50'
                      ]}
                      errorMessages={[
                        'This field is required',
                        'Min length is 3',
                        'Max length is 50',
                        'Character is not accept ']}
                    />
                  </Grid>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} style={{ width: '100%' }}>
                    <Grid item xs={12} sm={3} lg={3}>
                      <KeyboardDateTimePicker
                        style={{ width: '100%' }}
                        format="HH:mm dd/MM/yyyy"
                        margin="normal"
                        id="date-picker-dialog-date-streams"
                        label="Thời gian bắt đầu"
                        value={dateStartPromotion}
                        onChange={handleChangeDateStart}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </Grid>
                  </MuiPickersUtilsProvider>
                  <MuiPickersUtilsProvider utils={DateFnsUtils} style={{ width: '100%' }}>
                    <Grid item xs={12} sm={3} lg={3}>
                      <KeyboardDateTimePicker
                        style={{ width: '100%' }}
                        format="HH:mm dd/MM/yyyy"
                        margin="normal"
                        id="date-picker-dialog-date-streams"
                        label="Thời gian kết thúc"
                        value={dateEndPromotion}
                        onChange={handleChangeDateEnd}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                        {...validateForm.dateEnd || {}}
                      />
                    </Grid>
                  </MuiPickersUtilsProvider>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    disabled="true"
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Brand Shop"
                    variant="outlined"
                    margin="dense"
                    value={dataEdit?.brand_shop_name || ''}
                  />
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <TextValidator
                    fullWidth
                    disabled="true"
                    className={classge.inputData}
                    id="outlined-basic"
                    label="Loại khuyến mãi"
                    variant="outlined"
                    margin="dense"
                    value={dataEdit?.promotion_name || ''}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={promotionValueNode}
                    pred={"promotion_values"}
                    fullWidth
                    label="Giá trị đơn hàng tối thiểu"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required',
                    ]}
                  />
                </Grid>
                <Grid item xs={12} sm={6} lg={6}>
                  <InputEdge
                    Component={TextValidator}
                    node={promotionValueNode}
                    pred={"promotion_limit_price"}
                    fullWidth
                    label="Mức giảm tối đa"
                    variant="outlined"
                    margin="dense"
                    validators={[
                      'required'
                    ]}
                    errorMessages={[
                      'This field is required',
                    ]}
                  />
                </Grid>
                {
                  !selectedPromotion?.promotion_type == 'freeship' ?
                    <Grid item xs={12} sm={12} lg={12}>
                      <div className={classge.rootPanel}>
                        <div className={classge.titlePanel}>
                          <div className={classge.iconTitle}>
                            {ActionIcon}
                          </div>
                          <Typography className={classge.contentTitle} variant="h5" >
                      SKUs áp dụng
                          </Typography>
                        </div>
                        <Divider />
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={12} lg={12}>
                            <Autocomplete
                              options={configType}
                              getOptionLabel={option => option?.label ?? ""}
                              defaultValue={dataEdit?.config_type ? configType?.find(cf => cf.code === dataEdit?.config_type) : configType[0]}
                              onChange={handleChangeConfigType}
                              style={{ width: "100%" }}
                              renderInput={params => (
                                <TextField {...params}
                                  label="Trạng thái hiển thị"
                                  variant="outlined"
                                  fullWidth
                                  margin="dense"
                                />
                              )}
                            />
                          </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                          <Grid item xs={12} sm={12} lg={12}>
                            <TextField
                              id="list_product_highlight"
                              name="list_product_highlight"
                              label="Danh sách UID sản phẩm"
                              type="text"
                              size="small"
                              variant="outlined"
                              fullWidth
                              value={listProductUids}
                              onChange={handleProductHighLight}
                            />
                            <SelectMultiProduct
                              listOption={products}
                              currentList={dataEdit?.['promotion.products'] || []}
                              actionType={actionType}
                              dataEdit={dataEdit}
                              updateListManage={updateListProduct}
                              maxItem="-1"
                              inputChange={onInputSelectProductChange}
                              listTitle="Thêm sản phẩm"
                              facetPrefix="promotion.products"
                              // invalidCallback={invalidCallback}
                            />
                          </Grid>
                        </Grid>
                      </div>
                    </Grid>
                    : ''
                }

                {(dataEdit?.config_type && dataEdit?.config_type == 0) ?
                  <Grid item xs={12} sm={12} lg={12}>
                    <div className={classge.rootPanel}>
                      <div className={classge.titlePanel}>
                        <div className={classge.iconTitle}>
                          {ActionIcon}
                        </div>
                        <Typography className={classge.contentTitle} variant="h5" >
                            Sản phẩm KHÔNG áp dụng
                        </Typography>
                      </div>
                      <Divider />
                      <SelectMultiProduct
                        listOption={negativeProducts || []}
                        currentList={dataEdit?.['promotion.product_exclude'] || []}
                        actionType={actionType}
                        dataEdit={dataEdit}
                        updateListManage={updateListNegProduct}
                        maxItem="-1"
                        listTitle="Thêm sản phẩm KHÔNG áp dụng"
                        facetPrefix="promotion.product_exclude"
                        inputChange={onInputSelectNegProductChange}
                      />
                    </div>
                  </Grid> : ''
                }
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <div className={classge.titlePanel}>
                      <div className={classge.iconTitle}>
                        {ActionIcon}
                      </div>
                      <Typography className={classge.contentTitle} variant="h5" >
                  Thông tin mô tả
                      </Typography>
                    </div>
                    <Divider />
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={12} lg={12}>
                        <InputEdge
                          Component={TextValidator}
                          node={node}
                          pred={"display_name"}
                          fullWidth
                          label="Tên hiển thị"
                          variant="outlined"
                          margin="dense"
                          validators={[
                            'required',
                            'minStringLength:3',
                            'maxStringLength:50'
                          ]}
                          errorMessages={[
                            'This field is required',
                            'Min length is 3',
                            'Max length is 50',
                            'Character is not accept ']}
                        />
                      </Grid>
                      <Grid item xs={12} sm={12} lg={12}>
                        <Card style={{ display: 'flex', flexDirection: 'row' }}>
                          <CardMedia children={<>
                            <CardContent style={{ padding: '10px 0 0 10px' }}>
                              <Typography gutterBottom variant="h5" component="h2">
                          Image thumbnail
                              </Typography>
                            </CardContent>
                            <Media validateForm={validateForm['image_logo'] || {}} src={genCdnUrl(dataEdit['image_cover'], "image_banner.png")} type="image" style={{ width: 220, height: 220, margin: '10px' }} fileHandle={handleFileChange} field="image_cover" accept="image/*" />
                          </>} />
                          <CardMedia children={<>
                            <CardContent style={{ padding: '10px 0 0 10px' }}>
                              <Typography gutterBottom variant="h5" component="h2">
                          Banner chi tiết
                              </Typography>
                            </CardContent>
                            <Media validateForm={validateForm['image_banner'] || {}} src={genCdnUrl(dataEdit['image_banner'], "image_banner.png")} type="image" style={{ width: 440, height: 220, margin: '10px' }} fileHandle={handleFileChange} field="image_banner" accept="image/*" />
                          </>} />
                        </Card>
                      </Grid>
                      <Grid item xs={12} sm={12} lg={12}>
                        <TextEditor
                          imageName={'promotion'}
                          files={files}
                          node={node}
                          pred={"description_html"}
                        />
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootPanel}>
                    <div className={classge.titlePanel}>
                      <div className={classge.iconTitle}>
                        {ActionIcon}
                      </div>
                      <Typography className={classge.contentTitle} variant="h5" >
                        Button direct
                      </Typography>
                    </div>
                    <Divider />
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={6} lg={6}>
                        <FormControlLabel
                          control={
                            <CheckBoxEdge
                              Component={Checkbox}
                              node={node}
                              pred={"is_display"}
                              fullWidth
                              variant="outlined"
                              margin="dense"
                              color="primary"
                            />
                          }
                          label="Hiển thị"
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                      <div className={classge.rootPanel}>
                        <SelectMultiButtonDirect
                          listOption={options}
                          liveStream={liveStream}
                          collectionTemp={collectionTemp}
                          collections={collections || []}
                          brandShop={brandShop || []}
                          products={products}
                          setProducts={setProducts}
                          currentList={dataEdit?.['promotion.button_direct'] || []}
                          actionType={actionType}
                          dataEdit={dataEdit}
                          updateListManage={updateListButton}
                          files={files}
                          maxItem="-1"
                          minItem="0"
                          facetPrefix="promotion.button_direct"
                          // invalidCallback={invalidCallback}
                          onInputSelectProductChange={onInputSelectProductChange}
                        />
                      </div>
                    </Grid>
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} lg={12}>
                  <div className={classge.rootBasic}>
                    <div className={classge.titleProduct}>
                      <Typography className={classge.title} variant="h3">
                                    Xét duyệt Ngành hàng
                      </Typography>
                    </div>
                    <Grid container className={classge.rootListCate} spacing={2}>
                      <Grid item xs={12} sm={12} lg={12} className={classge.listCate}>
                        <Autocomplete
                          options={displayStatus}
                          value={displayStatus?.[dataEdit?.display_status] || "PENDING"}
                          onChange={handleChangeDisplayStatus}
                          style={{ width: "100%" }}
                          renderInput={params => (
                            <TextField {...params}
                              label="Trạng thái hiển thị"
                              variant="outlined"
                              fullWidth
                              margin="dense"
                            />
                          )}
                        />
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
        {/* Button submit */}
        <Grid className={classge.rootSubmit}>
          <Button
            style={{marginRight: '10px'}}
            color="secondary"
            variant="contained"
            aria-label="back"
            className={classge.btnControlGeneral}
            onClick={goBack}
          >
            <ReturnIcon className={classge.iconback} />
                        Return
          </Button>
          <RoleButton actionType={actionType} disabled={Object.keys(validateForm).length !== 0}/>
        </Grid>
      </ValidatorForm>
    </>
  )
}
