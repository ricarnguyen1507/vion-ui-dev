import React, { useState, useContext } from 'react';
import MaterialTable from 'material-table'
import AddBox from '@material-ui/icons/AddBox'
import useStyles from 'pages/style_general'
import { context } from 'context/Shared'
import api from 'services/api_cms'
import {
  useHistory,
  useLocation
} from "react-router-dom"

function prepareData (promotions) {
  promotions.forEach(r => {

    r.time = new Date(r.from_date).toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }) + " - " + new Date(r.to_date).toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' })
    if(r?.['promotion.promotion_value']) {
      r.promotion_name = r['promotion.promotion_value'].promotion_name ?? ''
    }
  })
}
function getFilterStr ({filters}) {
  const strFilter = filters.map(({value, column: {o, field}}) => {
    if(typeof value === 'string') {
      value = value.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/]/g, '').trim()
      if(value) {
        if (o) {
          return `${o}(${field},"${value}")`
        }
      }
    } else if(typeof value === 'number' || typeof value === 'boolean') {
      return `${o || 'eq'}(${field},${value})`
    } else if(Array.isArray(value)) {
      return value.map(v => `${o}(${field},${v})`).join(' OR ')
    }
    return ""
  }).filter(v => v.trim() !== "")

  return {strFilter: strFilter.join(' AND ')}
}

export default function TableLayout ({ controlEditTable, setStateQuery, stateQuery, setControlEditTable, handleClickOpen, displayStatus, setSelectedBrandShop }) {
  const classes = useStyles()
  const [selectedRow, setSelectedRow] = useState(null) // Selected row
  const { pathname } = useLocation()
  const history = useHistory()
  const ctx = useContext(context)

  // Set column for table
  function getData (query) {
    if (!query.inited) {
      query.inited = true
      Object.assign(query, stateQuery)
    }
    setStateQuery(query)
    const {strFilter} = getFilterStr(query)
    return api.post("/list/promotion", {
      params: {
        number: query.pageSize || 10,
        page: query.page || 0,
        ...{filter: (strFilter ? ' AND ' + strFilter : '')},
      }
    })
      .then(response => {
        const { summary: [{ totalCount }], result } = response.data
        result.map(lp => {
          if (lp['promotion.brand_shop']) {
            lp.brand_shop_name = lp['promotion.brand_shop'].brand_shop_name
          } else {
            lp.brand_shop_name = "Trang chủ"
          }
        })
        prepareData(result)
        // setDataTable(result);
        return {
          data: result,
          page: query.page,
          totalCount
        }
      })
      .catch(err => {
        console.log(err)
      })
  }
  // Set column for table

  const [column] = useState(() => {
    const column = [
      {
        title: "Trạng thái hiển thị",
        field: "display_status",
        editable: "never",
        render: (rowData) => <div>{displayStatus[rowData.display_status]}</div>,
        lookup: displayStatus,
        o: 'eq',
        sorting: false
      },
      {
        title: "Thời gian",
        field: "time",
        o: 'eq'
      },
      { title: "Tên chi tiết khuyến mãi", field: "display_name_detail", o: 'eq' },
      { title: "Loại khuyến mãi", field: "promotion_name" },
      { title: "Brand Shop", field: "brand_shop_name", render: rowData => <span>{rowData['promotion.brand_shop']?.brand_shop_name ?? ''}</span> }
    ]
    for(let {value, column: {field}} of stateQuery?.filters) {
      column.find(c => c.field === field)['defaultFilter'] = value
    }
    return column
  })


  /**
     * View
     */
  return (
    <div className="fade-in-table">
      <MaterialTable
        columns={column}
        data={getData}
        title={<h1 className={classes.titleTable}>Khuyến mãi</h1>}
        onRowClick={((evt, selectRow) => setSelectedRow(selectRow))}
        options={{
          headerStyle: {
            fontWeight: 600,
            background: "#f3f5ff",
            color: "#6e6e6e",
          },
          filtering: true,
          sorting: true,
          exportButton: true,
          grouping: true,
          pageSize: 10,
          pageSizeOptions: [10, 25, 50],
          actionsColumnIndex: -1,
          rowStyle: rowData => ({
            backgroundColor: (selectedRow && selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
          }),
        }}
        actions={[
          {
            icon: AddBox,
            tooltip: 'Add Layout',
            isFreeAction: true,
            onClick: () => {
              ctx.set('dataEdit', null)
              handleClickOpen()

            }
          },
          {
            icon: 'edit',
            tooltip: "Display Edit",
            isFreeAction: true,
            onClick: () => {
              setControlEditTable(!controlEditTable)
            }
          },
          controlEditTable === false ? {
            icon: "edit",
            tooltip: "Edit Layout",
            onClick: (event, { tableData, ...rowData }) => {
              if(rowData && rowData['promotion.brand_shop']) {
                rowData.brand_shop_name = rowData['promotion.brand_shop'].brand_shop_name
                setSelectedBrandShop(rowData['promotion.brand_shop'])
              }else {
                rowData.brand_shop_name = "Không"
                setSelectedBrandShop({
                  uid: null,
                  brand_shop_name: "Không"
                })
              }
              ctx.set('dataEdit', rowData)
              history.push(`${pathname}/Edit`)
            }
          } : null,
        ]}
        editable={{
          onRowDelete: null
        }}
      />
    </div>
  )
}