/*
import axios from 'axios';
const {url, key} = appConfigs.tracking

const tracking = axios.create({
  baseURL: url,
  withCredentials: false,
  headers: {
    "x-api-key": key
  }
})

const sendTracking = ({pages, dataOrigin = null, dataEdit = null, actionType, userData}) => {
  console.log({pages, dataOrigin, dataEdit, actionType, userData})
  const params = {
    category: null,
    data: []
  }
  if(pages === 'product') {
    params.category = 'product';
    const changeData = {dataOrigin, dataEdit}
    params.data = [{
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      action: actionType.toString().toUpperCase(),
      product_uid: dataOrigin?.uid ?? null,
      product_sku: dataOrigin?.sku_id ?? null,
      product_name: dataOrigin?.product_name ?? null,
      changes: JSON.stringify(changeData)
    }]

  }
  if(pages === 'order') {
    params.category = 'order';
    const changeData = {dataOrigin, dataEdit}
    params.data = [{
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      action: actionType.toString().toUpperCase(),
      primary_order_id: dataOrigin?.uid ?? null,
      sub_order_id: null,
      changes: JSON.stringify(changeData)
    }]
  }

  if(pages === 'order_sub') {
    params.category = 'order';
    const changeData = {dataOrigin, dataEdit}
    params.data = [{
      account_uid: userData?.uid ?? null,
      account_name: userData?.user_name ?? '',
      action: actionType.toString().toUpperCase(),
      primary_order_id: null,
      sub_order_id: dataOrigin?.uid ?? null,
      changes: JSON.stringify(changeData)
    }]
  }
  if(params.category) {
    tracking.post(`/log/cms/${params.category}`, params.data).then(({data}) => {
      console.log('Gui log thanh cong')
    }).catch(err => {
      console.log(err)
    })
  }
}
export default sendTracking;
*/