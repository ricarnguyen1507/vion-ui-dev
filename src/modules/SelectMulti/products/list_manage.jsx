import React, { useState, useMemo } from 'react'
import Grid from "@material-ui/core/Grid"
import Item from './item'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import AddBox from '@material-ui/icons/AddBox'
import DeleteIcon from '@material-ui/icons/Delete'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
// import { makeStyles } from '@material-ui/core/styles'
import { clone } from 'services/diff'
import Typography from '@material-ui/core/Typography'
import RootRef from "@material-ui/core/RootRef";
import useGeneral from 'pages/style_general'
import ImportExportIcon from '@material-ui/icons/ImportExport';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { genUid } from "utils"
/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
/* const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
}) */

export default function ({listOption, currentList = [], updateListManage, dataEdit, isArray = true, listTitle = "", label = "Vị trí số: ", keyName = false, maxItem = -1, isShow = true, inputChange, facetPrefix, delAll = false, minItem = 0, invalidCallback }) {
  // const classes = useStyles()
  const classge = useGeneral()
  let [dataMap, setDataMap] = useState(() => {
    if (currentList.length > 0) {
      return currentList.map((cur, index) => ({number: index, uid: cur.uid || null, product_name: cur.product_name}))
    } else {
      return [
        {number: 0, uid: null}
      ]
    }
  }, [currentList])

  const [newList, setNewList] = useState(() => {
    if (dataMap?.length === 1 && dataMap?.[0]?.uid === null) {
      return [
        {number: 0, uid: genUid('drag_item')}
      ]
    } else {
      return clone(dataMap)
    }
  }, [dataMap])


  const updateItem = (uid, originItem) => {
    newList.forEach(n => {
      if (n.number === originItem.number) {
        uid ? n.uid = uid : n.uid = genUid('drag_item')
      }
    })
    setNewList([...newList])
    callUpdateList(newList)
  }

  const onAdd = () => {
    if (maxItem == -1 || dataMap.length < maxItem) {
      setDataMap([...dataMap, {number: dataMap.length, uid: null}])
      setNewList([...newList, {number: dataMap.length, uid: genUid('drag_item')}])
    }
  }
  const onDelete = () => {
    setNewList([{number: 0, uid: genUid('drag_item')}])
    callUpdateList([{number: 0, uid: genUid('drag_item')}])
  }

  // HANDLE DRAG AND DROP IN TABLE
  // Arrange order of data list
  function reorderList (list, from, to) {
    const result = Array.from(list);
    const [removed] = result.splice(from, 1);
    result.splice(to, 0, removed);
    for (let i = 0; i < result.length; i++) {
      result[i].number = i;
    }
    setNewList(result);
    callUpdateList(result);
  }

  const onDragEnd = result => {
    // dropped outside the list
    const { source, destination} = result;
    if (!result && !destination) {
      return;
    }
    if (!source || !destination) {
      return;
    }
    reorderList(newList, source.index, destination.index)
  }

  function callUpdateList (orderedList) {
    const facetDisplayOrder = `${facetPrefix}|display_order`
    let set = orderedList.filter(li => li.uid && !li.uid.startsWith("_:")).map(n => ({uid: n.uid, display_order: n.number, [facetDisplayOrder]: `<${facetPrefix}> <${n.uid}> (display_order=${n.number}) .`}))
    let del = dataMap.filter(li => li.uid).map(n => ({ uid: n.uid }))

    updateListManage({set, del})
  }

  function handleMoveItemToTop (idx) {
    reorderList(newList, idx, 0);
  }

  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */
  let validateForm = useMemo(() => {
    if (isShow) {
      if (minItem && newList?.filter(nl => !nl.uid.startsWith("_:drag_item"))?.length < minItem) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: true,
          helperText: `This field is requred, min section is ${minItem}`
        }
      } else {
        if (typeof invalidCallback === "function") { invalidCallback(false) }
        return {}
      }
    }
  }, [newList, isShow])

  const filterOptionList = (originArr, listArr) => {
    const result = [];
    const uidList = listArr.map(elm => elm.uid);
    for (let i = 0; i < originArr.length; i++) {
      if (!uidList.includes(originArr[i].uid)) {
        result.push(originArr[i]);
      }
    }
    return result;
  }
  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */

  // Render
  return (
    <div>
      { isShow ?
        <>
          <div className={classge.titlePanel} style={{margin: "20px 15px 0 0"}}>

            <Grid container spacing={2}>
              <Grid item xs={4} sm={4} lg={4}>
                <Typography className={classge.contentTitle} variant="h5" >
                  {listTitle} {maxItem === "-1" ? " (unlimited)" : maxItem === "0" ? "" : ` (tối đa: ${maxItem})`}
                  {isArray ?
                    <IconButton className={classge.btnDelete} onClick={onAdd}>
                      <AddBox />
                    </IconButton>
                    :
                    ""}
                </Typography>
              </Grid>
              {delAll === true ?
                <Grid item xs={4} sm={4} lg={4}>
                  <Typography className={classge.contentTitle} variant="h5" >
                    <IconButton className={classge.btnDelete} onClick={onDelete}>
                      <DeleteIcon />
                    </IconButton>
                                Xoá tất cả
                  </Typography>
                </Grid>
                : ""
              }
            </Grid>
          </div>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided) => (
                <RootRef rootRef={provided.innerRef}>
                  <List >
                    {/* <Grid container spacing={2}> */}
                    {newList.map((value, idx) => (
                      <Draggable key={value?.uid + "_" + idx} draggableId={ value?.uid + "_" + idx } index={idx}>
                        {(provided) => (
                          // <Grid item xs={12} sm={(12/cols)} lg={(12/cols)} >
                          <ListItem
                            classes={{
                              secondaryAction: classge.secondaryActionRoot,
                            }}
                            style={{display: "flex", alignItems: "center", padding: "5px", cursor: "grab"}}
                            ContainerComponent="li"
                            ContainerProps={{ ref: provided.innerRef }}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Item
                              key={keyName ? keyName : dataEdit?.uid + "-dentify-item-" + value?.uid}
                              listOption={filterOptionList(listOption, newList)}
                              fullList={listOption}
                              originItem={value}
                              dataEdit={dataEdit}
                              label={label + (value.number + 1)}
                              updateItem={updateItem}
                              inputChange={inputChange}
                              validateForm={validateForm}

                            />
                            <ImportExportIcon style={{marginLeft: "6px"}}/>
                            {idx !== 0 ?
                              <Button
                                variant="contained"
                                color="primary"
                                style={{marginLeft: "6px"}} onClick={() => handleMoveItemToTop(idx)}>
                                                                    Top
                              </Button>
                              :
                              <Button
                                disabled
                                style={{display: "hidden"}}>
                              </Button>
                            }
                            <ListItemSecondaryAction >
                            </ListItemSecondaryAction>
                          </ListItem>
                          // </Grid>
                        )}
                      </Draggable>
                    ))}
                    {/* </Grid> */}
                    {provided.placeholder}
                  </List>
                </RootRef>
              )}
            </Droppable>
          </DragDropContext>
        </>
        : ""}
    </div>
  )
}
