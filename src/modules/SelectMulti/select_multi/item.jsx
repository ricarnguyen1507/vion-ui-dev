import React, { useState } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = [], listOption = [], updateItem, label, inputChange, validateForm = {} }) {

  const [selectedItem, setSelectedItem] = useState(() => originItem)
  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item ? item.uid : null, originItem)
  }

  function optionLabel (option) {
    if (option.product_name) {
      if (option.sku_id) {
        return `${option.product_name} - ${option.sku_id}`
      } else {
        return option.product_name
      }
    } else if (option.collection_name) {
      return option.collection_name
    } else if (option.phone_number) {
      if (option.customer_name) {
        return `${option.phone_number} - ${option.customer_name} - ${option.uid}`
      } else {
        return `${option.phone_number} - ${option.uid}`
      }
    } else if (option.group_customer_name) {
      return option.group_customer_name
    }
  }

  // Render
  return (
    originItem ?
      <Autocomplete
        options={listOption}
        getOptionLabel={option => optionLabel(option) || ''}
        value={selectedItem && selectedItem.uid && (listOption.find(c => c.uid === selectedItem.uid) || null)}
        style={{ width: "100%" }}
        onChange={handleChangeItemOptions}
        onInputChange={inputChange}
        renderInput={params => (
          <TextField {...params}
            {...validateForm || {}}
            label={ label }
            variant="outlined"
            fullWidth
            margin="dense"
          />
        )}
      /> : ""
  )
}
