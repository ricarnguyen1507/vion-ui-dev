import React, { useState, useMemo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"
import CardMedia from '@material-ui/core/CardMedia'
import Card from '@material-ui/core/Card'
import Media from "components/Media/index"
import { genCdnUrl } from 'components/CdnImage'

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collections, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ updated, files, originItem = {}, listOption, liveStream, collectionTemp, collections, brandShop, products, promotions, landingPage, updateItem, label, inputChange, validateForm = {}, onInputSelectProductChange}) {

  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  // const [timer, setTimer] = useState(null)
  // function fetchProdOption (queries) {
  //   if (queries !== "") {
  //     api.post(`/list/product-option`, queries)
  //       .then(res => {
  //         const newOption = products || []
  //         res.data.result.forEach(r => {
  //           if (!newOption.find(no => no.uid === r.uid)) {
  //             newOption.push(r)
  //           }
  //         })
  //         setProducts([...newOption])
  //       })
  //   }
  // }
  // const onInputSelectProductChange = (e, val) => {
  //   let queries = ''
  //   if (val !== '') {
  //     queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
  //   } else {
  //     queries = ''
  //   }
  //   clearTimeout(timer)
  //   setTimer(setTimeout(() => {
  //     fetchProdOption(queries)
  //   }, 550))
  // }

  const gridColl = useMemo(() => {
    if (selectedItem?.section_value !== 'home' && selectedItem?.section_value !== 'brandshop') {
      return 4
    } else {
      return 10
    }
  }, [selectedItem])
  const listSubOption = useMemo(() => {
    if (selectedItem?.section_value === 'livestream') {
      return liveStream
    } else if (selectedItem?.section_value === 'collection_temp') {
      return collectionTemp
    } else if (selectedItem?.section_value === 'collection') {
      return collections
    } else if (selectedItem?.section_value === 'brand_shop') {
      return brandShop
    } else if (selectedItem?.section_value === 'product') {
      if (selectedItem?.section_ref) {
        onInputSelectProductChange(null, selectedItem?.section_ref)
      }
      return products
    } else if (selectedItem?.section_value === 'landing_page') {
      return landingPage
    } else if (selectedItem?.section_value === 'promotion') {
      return promotions
    }else {
      return []
    }
  }, [selectedItem])

  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item?.uid || null, originItem)
  }

  const handleChangeSubItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem({...selectedItem, section_ref: item?.uid || null})
    updateItem(selectedItem?.uid || null, originItem, item?.uid || null)
  }

  const updateImage = (field, file) => {
    originItem.image = `images/layout_section/${file.md5}.${file.type.split('/')[1]}`
    originItem.renew = true
    originItem['dgraph.type'] = "LayoutSection"
    updated.layout_section.push(originItem)
    files[file.md5] = file
  }

  function optionLabel (option) {
    if (option.product_name) {
      if (option.sku_id) {
        return `${option.product_name} - ${option.sku_id}`
      } else {
        return option.product_name
      }
    } else if (option.collection_name) {
      return option.collection_name
    } else if (option['stream.name']) {
      return option['stream.name']
    } else if (option.section_name) {
      return option.section_name
    } else if (option.brand_shop_name) {
      return option.brand_shop_name
    } else if (option.landing_name) {
      return option.landing_name
    } else if(option.display_name_detail) {
      return option.display_name_detail
    }
  }
  // Render
  return (
    originItem ?
      <Grid container spacing={2}>
        <Grid item xs={12} sm={gridColl} lg={gridColl}>
          <Autocomplete
            options={listOption}
            getOptionLabel={option => optionLabel(option) || ''}
            value={selectedItem && selectedItem.section_value && (listOption.find(c => c.section_value === selectedItem.section_value) || null)}
            style={{ width: "100%" }}
            onChange={handleChangeItemOptions}
            onInputChange={inputChange}
            renderInput={params => (
              <TextField {...params}
                {...validateForm}
                label={ label }
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        {selectedItem?.section_value !== 'home' && selectedItem?.section_value !== 'brandshop' && selectedItem?.section_value !== 'product' ?
          <>
            <Grid item xs={8} sm={6} lg={6}>
              <Autocomplete
                options={listSubOption || []}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem && selectedItem?.section_ref && (listSubOption?.find(c => c?.uid === selectedItem?.section_ref) || null)}
                style={{ width: "100%" }}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    error={!(selectedItem && selectedItem.section_ref && listSubOption?.find(c => c.uid === selectedItem.section_ref))}
                  />
                )}
              />
            </Grid>
          </>
          : selectedItem?.section_value === "product" ?
            <Grid item xs={8} sm={6} lg={6}>
              <Autocomplete
                options={products}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem && selectedItem.section_ref && (listSubOption?.find(c => c.uid === selectedItem.section_ref) || null)}
                style={{ width: "100%" }}
                onInputChange={onInputSelectProductChange}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    label="Chọn sản phẩm"
                    variant="outlined"
                    fullWidth
                    margin="dense"
                  />
                )}
              />
            </Grid>
            : ""}
        <Grid item xs={4} sm={2} lg={2}>
          <Card
            style={{maxWidth: 180}}
          >
            <CardMedia children={<Media src={genCdnUrl(originItem.image_source || originItem.image, "image_cover.png")} type="image" style={{ width: 180, height: 90 }} fileHandle={updateImage} field="image_cover" accept="image/jpeg, image/png" />} />
          </Card>
        </Grid>
      </Grid>
      : ""
  )
}
