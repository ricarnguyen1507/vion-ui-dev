import React, { useState, useMemo } from 'react'
import Grid from "@material-ui/core/Grid"
import Item from './item'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import AddBox from '@material-ui/icons/AddBox'
import DeleteIcon from '@material-ui/icons/Delete'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
// import { makeStyles } from '@material-ui/core/styles'
import { clone } from 'services/diff'
import Typography from '@material-ui/core/Typography'
import RootRef from "@material-ui/core/RootRef";
import useGeneral from 'pages/style_general'
import ImportExportIcon from '@material-ui/icons/ImportExport';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { genUid } from "utils"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collections, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
/* const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
}) */

export default function ({updated, files, listOption, currentList = [], liveStream, collectionTemp, promotions, collections, brandShop, products, setProducts, landingPage, updateListManage, dataEdit, isArray = true, listTitle = "", label = "Vị trí số: ", keyName = false, maxItem = -1, isShow = true, inputChange, facetPrefix, delAll = false, minItem = 0, invalidCallback, onInputSelectProductChange}) {
  // const classes = useStyles()
  const classge = useGeneral()
  const [dataMap, setDataMap] = useState(() => {
    if (currentList.length > 0) {
      return currentList.map((cur, index) => ({number: index, ...cur}))
    } else {
      return [
        {
          number: 0,
          uid: null,
          section_value: null,
          section_name: null,
          section_type: null,
          section_ref: null
        }
      ]
    }
  })

  const [newList, setNewList] = useState(() => {
    if (dataMap?.length === 1 && dataMap?.[0]?.uid === null) {
      return [
        {
          ...dataMap[0],
          uid: genUid('drag_item')
        }
      ]
    } else {
      return clone(dataMap)
    }

  })
  const updateItem = (uid, originItem, subItemUid = null, section_limit = 20, textValue = {}, image = '') => {
    for(let n of newList) {
      if (!n.section_limit) {
        n.section_limit = section_limit
      }
      if (n.number === originItem.number) {
        if (!subItemUid) {
          const reNew = (uid !== null && uid.startsWith("_:")) ? genUid('layout_item') : uid
          n['dgraph.type'] = "LayoutSection"
          n.uid = reNew || genUid('drag_item')
          const selected = listOption.find(l => l.uid === uid)

          if(selected) {
            n.section_value = selected.section_value
            n.section_name = selected.section_name
            n.section_type = selected.section_type
            n.section_ref = selected.section_ref || null
          }

          if (image != '') {
            n.image = image
          }
        } else {
          const selected = listOption.find(l => l.section_value === originItem.section_value)
          n.section_ref = subItemUid || selected?.section_ref || null
          n.renew = true
          Object.keys(textValue).forEach(value => {
            n[value] = textValue[value]
          })
        }
      }
    }
    setNewList([...newList])
    callUpdateList(newList)
  }


  const onAdd = () => {
    if (maxItem == -1 || dataMap.length < maxItem) {
      setDataMap([...dataMap, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
      setNewList([...newList, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
    }
  }
  const onDelete = () => {
    if (maxItem == -1 || dataMap.length < maxItem) {
      setDataMap([...dataMap, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
      setNewList([...newList, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
    }
  }

  // HANDLE DRAG AND DROP IN TABLE
  // Arrange order of data list
  function reorderList (list, from, to) {
    const result = Array.from(list);
    const [removed] = result.splice(from, 1);
    result.splice(to, 0, removed);
    for (let i = 0; i < result.length; i++) {
      result[i].number = i;
    }
    setNewList(result);
    callUpdateList(result);
  }

  const onDragEnd = result => {
    // dropped outside the list
    const { source, destination} = result;

    if (!result && !destination) {
      return;
    }
    if (!source || !destination) {
      return;
    }
    reorderList(newList, source.index, destination.index)
  }

  function callUpdateList (orderedList) {
    const facetDisplayOrder = `${facetPrefix}|display_order`
    let set = orderedList.filter(li => !li.uid.startsWith("_:drag_item") && li.uid).map(n => ({uid: n.uid, display_order: n.number, [facetDisplayOrder]: `<${facetPrefix}> <${n.uid}> (display_order=${n.number}) .`, ...n, 'dgraph.type': "LayoutSection"}))
    let del = dataMap.filter(li => li.uid && !li.uid.startsWith("_:")).map(n => ({uid: n.uid}))
    updateListManage({set, del})
  }

  function handleMoveItemToTop (idx) {
    reorderList(newList, idx, 0);
  }

  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */
  let validateForm = useMemo(() => {
    if (isShow) {
      let selectLiveStream = true
      let selectCollectionTemp = true
      let selectCollection = true
      let selectProduct = true
      let selectBrandShop = true
      let selectLandingPage = true
      newList.forEach(n => {
        /**LiveStream */
        switch(n?.section_value) {
          case "livestream":
            if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
              selectLiveStream = false
            } else if (!n?.section_ref) {
              selectLiveStream = false
            } else if (!n.image && !n.image_source) {
              selectLiveStream = false
            } else {
              selectLiveStream = true
            }
            break;
          case "collection_temp":
            if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
              selectCollectionTemp = false
            } else if (!n?.section_ref) {
              selectCollectionTemp = false
            } else if (!n.image && !n.image_source) {
              selectCollectionTemp = false
            } else {
              selectCollectionTemp = true
            }
            break;
          case "collection":
            if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
              selectCollection = false
            } else if (!n?.section_ref) {
              selectCollection = false
            } else if (!n.image && !n.image_source) {
              selectCollection = false
            } else {
              selectCollection = true
            }
            break;
          case "product":
            if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
              selectProduct = false
            } else if (!n?.section_ref) {
              selectProduct = false
            } else if (!n.image && !n.image_source) {
              selectProduct = false
            } else {
              selectProduct = true
            }
            break;
          case "brand_shop":
            if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
              selectBrandShop = false
            } else if (!n?.section_ref) {
              selectBrandShop = false
            } else if (!n.image && !n.image_source) {
              selectBrandShop = false
            } else {
              selectBrandShop = true
            }
            break;
          case "landing_page":
            if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
              selectLandingPage = false
            } else if (!n?.section_ref) {
              selectLandingPage = false
            } else if (!n.image && !n.image_source) {
              selectLandingPage = false
            } else {
              selectLandingPage = true
            }
            break;
        }
      })
      if (!selectLiveStream || !selectCollectionTemp || !selectCollection || !selectProduct || !selectBrandShop || !selectLandingPage) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: false,
          helperText: `Please select sub Option`
        }
      } else if (minItem && newList?.filter(nl => !nl.uid.startsWith("_:drag_item"))?.length < minItem) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: true,
          helperText: `This field is requred, min section is ${minItem}`
        }
      } else {
        if (typeof invalidCallback === "function") { invalidCallback(false) }
        return {}
      }
    }
  }, [newList, isShow])
  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */

  // Render
  return (
    <div>
      { isShow ?
        <>
          <div className={classge.titlePanel} style={{margin: "20px 15px 0 0"}}>
            <Grid container spacing={2}>
              <Grid item xs={4} sm={4} lg={4}>
                <Typography className={classge.contentTitle} variant="h5" >
                  {listTitle} {maxItem === "-1" ? " (unlimited)" : maxItem === "0" ? "" : ` (tối đa: ${maxItem})`}
                  {isArray ?
                    <IconButton className={classge.btnDelete} onClick={onAdd}>
                      <AddBox />
                    </IconButton>
                    :
                    ""}
                </Typography>
              </Grid>
              {delAll === true ?
                <Grid item xs={4} sm={4} lg={4}>
                  <Typography className={classge.contentTitle} variant="h5" >
                    <IconButton className={classge.btnDelete} onClick={onDelete}>
                      <DeleteIcon />
                    </IconButton>
                                Xoá tất cả
                  </Typography>
                </Grid>
                : ""
              }
            </Grid>

          </div>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided) => (
                <RootRef rootRef={provided.innerRef}>
                  <List >
                    {/* <Grid container spacing={2}> */}
                    {newList?.map((value, idx) => (
                      <Draggable key={value?.uid + "_" + idx} draggableId={ value?.uid + "_" + idx } index={idx}>
                        {(provided) => (
                          // <Grid item xs={12} sm={(12/cols)} lg={(12/cols)} >
                          <ListItem
                            classes={{
                              secondaryAction: classge.secondaryActionRoot,
                            }}
                            style={{display: "flex", alignItems: "center", padding: "5px", cursor: "grab"}}
                            ContainerComponent="li"
                            ContainerProps={{ ref: provided.innerRef }}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Item
                              key={keyName ? keyName : dataEdit?.uid + "-dentify-item-" + value?.uid}
                              listOption={listOption}
                              liveStream={liveStream}
                              collectionTemp={collectionTemp}
                              collections={collections}
                              brandShop={brandShop}
                              products={products}
                              setProducts={setProducts}
                              landingPage={landingPage}
                              promotions={promotions}
                              originItem={value}
                              dataEdit={dataEdit}
                              files={files}
                              updated={updated}
                              label={label + (value.number + 1)}
                              updateItem={updateItem}
                              inputChange={inputChange}
                              validateForm={validateForm}
                              onInputSelectProductChange={onInputSelectProductChange}
                            />
                            <ImportExportIcon style={{marginLeft: "6px"}}/>
                            {idx !== 0 ?
                              <Button
                                variant="contained"
                                color="primary"
                                style={{marginLeft: "6px"}} onClick={() => handleMoveItemToTop(idx)}>
                                                                    Top
                              </Button>
                              :
                              <Button
                                disabled
                                style={{display: "hidden"}}>
                              </Button>
                            }
                            <ListItemSecondaryAction >
                            </ListItemSecondaryAction>
                          </ListItem>
                          // </Grid>
                        )}
                      </Draggable>
                    ))}
                    {/* </Grid> */}
                    {provided.placeholder}
                  </List>
                </RootRef>
              )}
            </Droppable>
          </DragDropContext>
        </>
        : ""}
    </div>
  )
}
