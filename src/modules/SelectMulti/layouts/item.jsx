import React, { useState, useMemo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = {}, listOption, collectionTemp, brandshopGroups, promotions, livestreamGroups, updateItem, label, inputChange, validateForm = {}}) {

  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  const gridColl = useMemo(() => {
    if (selectedItem?.section_value === 'collection_temp' ||
        selectedItem?.section_value === 'brand_shop' ||
        selectedItem?.section_value === 'viewed_prod' ||
        selectedItem?.section_value === 'viewed_history' ||
        selectedItem?.section_value === 'collection' ||
        selectedItem?.section_value === 'promotions' ||
        selectedItem?.section_value === 'brandshop_collection' ||
        selectedItem?.section_value === 'livestream' ||
        selectedItem?.section_value === 'end_stream' ||
        selectedItem?.section_value === 'favourite') {
      return 4
    } else {
      return 12
    }
  }, [selectedItem])
  const listSubOption = useMemo(() => {
    if (selectedItem?.section_value === 'collection_temp') {
      return collectionTemp
    } else if (selectedItem?.section_value === 'brand_shop') {
      return brandshopGroups
    } else if (selectedItem?.section_value === 'livestream') {
      return livestreamGroups
    } else if(selectedItem?.section_value === 'promotions') {
      return promotions
    }else {
      return []
    }
  }, [selectedItem])


  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item?.uid || null, originItem)
  }

  const handleChangeSubItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem({...selectedItem, section_ref: item?.uid || null})
    updateItem(selectedItem?.uid || null, originItem, item?.uid || null)
  }

  const handleChangeText = e => {
    e.preventDefault()
    const { name, value } = e.target
    setSelectedItem({...selectedItem, [name]: value || null})
    updateItem(selectedItem?.uid || null, originItem, selectedItem.section_ref || null, null, {[name]: value})
  }

  const handleChangeLimitValue = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setSelectedItem({...selectedItem, [name]: value || null})
    updateItem(selectedItem?.uid || null, originItem, selectedItem.section_ref || null, value)
  }
  function optionLabel (option) {
    if (option.product_name) {
      if (option.sku_id) {
        return `${option.product_name} - ${option.sku_id}`
      } else {
        return option.product_name
      }
    } else if (option.collection_name) {
      return option.collection_name
    } else if (option.brandshop_group_name) {
      return option.brandshop_group_name
    } else if (option.phone_number) {
      if (option.customer_name) {
        return `${option.phone_number} - ${option.customer_name} - ${option.uid}`
      } else {
        return `${option.phone_number} - ${option.uid}`
      }
    } else if (option.section_name) {
      return option.section_name
    } else if (option.livestream_group_name) {
      return option.livestream_group_name
    } else if (option.display_name_detail) {
      return option.display_name_detail
    }
  }
  // Render
  return (
    originItem ?
      <Grid container spacing={2}>
        <Grid item xs={12} sm={gridColl} lg={gridColl}>
          <Autocomplete
            options={listOption}
            getOptionLabel={option => optionLabel(option) || ''}
            value={selectedItem && selectedItem.section_value && (listOption.find(c => c.section_value === selectedItem.section_value) || null)}
            style={{ width: "100%" }}
            onChange={handleChangeItemOptions}
            onInputChange={inputChange}
            renderInput={params => (
              <TextField {...params}
                {...validateForm}
                label={ label }
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        {selectedItem?.section_value === 'collection_temp' ?
          <>
            <Grid item xs={8} sm={6} lg={6}>
              <Autocomplete
                options={listSubOption}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem && selectedItem.section_ref && (listSubOption.find(c => c.uid === selectedItem.section_ref) || null)}
                style={{ width: "100%" }}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    error={!(selectedItem && selectedItem.section_ref && listSubOption.find(c => c.uid === selectedItem.section_ref))}
                    required={true}
                  />
                )}
              />
            </Grid>
            <Grid item xs={4} sm={2} lg={2}>
              <TextField
                fullWidth
                id="section_limit"
                label="Giới hạn item"
                variant="outlined"
                margin="dense"
                name="section_limit"
                onChange={handleChangeLimitValue}
                defaultValue={selectedItem.section_limit || ""}
              />
            </Grid>
          </>
          : ""}
        {selectedItem?.section_value === 'brand_shop' ||
        selectedItem?.section_value === 'livestream' ?
          <>
            <Grid item xs={12} sm={3} lg={3}>
              <TextField
                fullWidth
                id="section_name"
                label="Hiển thị"
                variant="outlined"
                margin="dense"
                name="section_name"
                onChange={handleChangeText}
                defaultValue={selectedItem.section_name || ""}
              />
            </Grid>
            <Grid item xs={8} sm={5} lg={5}>
              <Autocomplete
                options={listSubOption || []}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem && selectedItem.section_ref && (listSubOption?.find(c => c.uid === selectedItem.section_ref) || null)}
                style={{ width: "100%" }}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    error={!(selectedItem && selectedItem.section_ref && listSubOption?.find(c => c.uid === selectedItem.section_ref))}
                    required={true}
                  />
                )}
              />
            </Grid>
          </>
          : ""}
        {selectedItem?.section_value === 'viewed_prod' ||
        selectedItem?.section_value === 'viewed_history' ||
        selectedItem?.section_value === 'collection' ||
        selectedItem?.section_value === 'promotions' ||
        selectedItem?.section_value === 'brandshop_collection' ||
        selectedItem?.section_value === 'end_stream' ||
        selectedItem?.section_value === 'favourite' ?
          <>
            <Grid item xs={12} sm={8} lg={8}>
              <TextField
                fullWidth
                id="section_name"
                label="Hiển thị"
                variant="outlined"
                margin="dense"
                name="section_name"
                onChange={handleChangeText}
                defaultValue={selectedItem.section_name || ""}
              />
            </Grid>
          </>
          : ""}
      </Grid>
      : ""
  )
}
