import React, { useState, useMemo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import { TextValidator } from 'react-material-ui-form-validator'
import Grid from "@material-ui/core/Grid"
import useGeneral from 'pages/style_general'
/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collections, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = {}, listOption, liveStream, collectionTemp, collections, brandShop, products, updateItem, label, inputChange, validateForm = {}, onInputSelectProductChange}) {
  const classge = useGeneral()
  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  // const gridColl = useMemo(() => {
  //   if (selectedItem?.section_value !== 'home' && selectedItem?.section_value !== 'brandshop') {
  //     return 4
  //   } else {
  //     return 10
  //   }
  // }, [selectedItem])

  const listSubOption = useMemo(() => {
    if (selectedItem?.link_direct === 'livestream') {
      return liveStream
    } else if (selectedItem?.link_direct === 'collection_temp') {
      return collectionTemp
    } else if (selectedItem?.link_direct === 'collection') {
      return collections
    } else if (selectedItem?.link_direct === 'brand_shop') {
      return brandShop
    } else if (selectedItem?.link_direct === 'product') {
      if (selectedItem?.link_value) {
        onInputSelectProductChange(null, selectedItem?.link_value)
      }
      return products
    } else {
      return []
    }
  }, [selectedItem])
  console.log('select item', selectedItem)
  console.log('listSubOption', listSubOption)
  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item?.uid || null, originItem)
  }

  const handleChangeSubItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem({...selectedItem, link_value: item?.uid || null})
    updateItem(selectedItem?.uid || null, originItem, item?.uid || null)
  }
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setSelectedItem({...selectedItem, [name]: value})
    updateItem(selectedItem?.uid || {}, originItem, null, value || "")
  }
  function optionLabel (option) {
    if (option.product_name) {
      if (option.sku_id) {
        return `${option.product_name} - ${option.sku_id}`
      } else {
        return option.product_name
      }
    } else if (option.collection_name) {
      return option.collection_name
    } else if (option['stream.name']) {
      return option['stream.name']
    } else if (option.brand_shop_name) {
      return option.brand_shop_name
    }
  }
  // Render
  return (
    originItem ?
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} lg={6}>
          <Autocomplete
            options={listOption}
            getOptionLabel={option => option.link_name || ''}
            value={selectedItem && selectedItem.link_direct && (listOption.find(c => c.link_direct === selectedItem.link_direct) || null)}
            style={{ width: "100%" }}
            onChange={handleChangeItemOptions}
            onInputChange={inputChange}
            renderInput={params => (
              <TextField {...params}
                {...validateForm}
                label={ label }
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        {selectedItem?.link_direct !== 'brandshop' && selectedItem?.link_direct !== 'product' ?
          <>
            <Grid item xs={8} sm={6} lg={6}>
              <Autocomplete
                options={listSubOption || []}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem && selectedItem?.link_value && (listSubOption?.find(c => c?.uid === selectedItem?.link_value) || null)}
                style={{ width: "100%" }}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    label= {selectedItem?.link_name ?? ''}
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    error={!(selectedItem && selectedItem.link_value && listSubOption?.find(c => c.uid === selectedItem.link_value))}
                  />
                )}
              />
            </Grid>
          </>
          : selectedItem?.link_direct === "product" ?
            <Grid item xs={8} sm={6} lg={6}>
              <Autocomplete
                options={products}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem && selectedItem.link_value && (listSubOption?.find(c => c.uid === selectedItem.link_value) || null)}
                style={{ width: "100%" }}
                onInputChange={onInputSelectProductChange}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    label="Chọn sản phẩm"
                    variant="outlined"
                    fullWidth
                    margin="dense"
                  />
                )}
              />
            </Grid>
            : ""}
        <Grid item xs={12} sm={12} lg={12}>
          <TextValidator
            fullWidth
            // {...validateForm['display_name'] || {}}
            className={classge.inputData}
            id="outlined-basic"
            label="Call to action"
            variant="outlined"
            margin="dense"
            name="link_action"
            onChange={handleChange}
            value={selectedItem?.link_action || ""}
            validators={[
              'maxStringLength:50'
            ]}
            errorMessages={[
              'Max length is 50',
              'Character is not accept ']}
          />
        </Grid>
      </Grid>
      : ""
  )
}
