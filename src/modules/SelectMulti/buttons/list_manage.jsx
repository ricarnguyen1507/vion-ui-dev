import React, { useState } from 'react'
import Item from './item'
// import useGeneral from 'pages/style_general'
import { genUid } from "utils"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collections, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
/* const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
}) */

export default function ({updated, files, listOption, currentList = null, liveStream, collectionTemp, collections, brandShop, products, setProducts, updateListManage, dataEdit, keyName = false, isShow = true, inputChange, onInputSelectProductChange}) {
  // const classes = useStyles()
  // const classge = useGeneral()
  const [dataMap] = useState(() => {
    if (Object.keys(currentList).length > 0) {
      return [currentList]
    } else {
      return [
        {
          uid: genUid("button_direct"),
          link_direct: null,
          link_name: null,
          link_value: null,
          link_action: null
        }
      ]
    }
  })

  // const [newList, setNewList] = useState(() => {
  //   if (dataMap?.length === 1 && dataMap?.[0]?.uid === null) {
  //     return [
  //       {
  //         ...dataMap[0],
  //         uid: genUid('drag_item')
  //       }
  //     ]
  //   } else {
  //     return clone(dataMap)
  //   }

  // })
  const updateItem = (uid, originItem, subItemUid = null, link_action = "") => {
    for(let n of dataMap) {
      if (!subItemUid) {
        n['dgraph.type'] = "ButtonDirect"
        n.uid = originItem?.uid.startsWith('_:') ? uid : n?.uid
        n.link_action = link_action ?? ''
        const selected = listOption.find(l => l.uid === uid)
        if(selected) {
          n.link_direct = selected.link_direct
          n.link_name = selected.link_name
          // n.link_value = selected.link_value || null
        }
        if(!n.uid.startsWith('_:')) {
          n.renew = true
        }
      } else {
        const selected = listOption.find(l => l.link_value === originItem.link_value)
        n.link_value = subItemUid || selected?.link_value || null
        n.renew = true
      }
    }
    callUpdateList(dataMap)
  }


  // HANDLE DRAG AND DROP IN TABLE
  // Arrange order of data list
  /* function reorderList (list, from, to) {
    const result = Array.from(list);
    const [removed] = result.splice(from, 1);
    result.splice(to, 0, removed);
    for (let i = 0; i < result.length; i++) {
      result[i].number = i;
    }
    callUpdateList(result);
  } */

  function callUpdateList (orderedList) {
    let set = orderedList.filter(li => !li.uid.startsWith("_:drag_item") && li.uid).map(n => ({uid: n.uid, ...n, 'dgraph.type': "ButtonDirect"}))
    let del = dataMap.filter(li => li.uid && !li.uid.startsWith("_:")).map(n => ({uid: n.uid}))
    updateListManage({set, del})
  }

  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */
  // let validateForm = useMemo(() => {
  //   if (isShow) {
  //     let selectLiveStream = true
  //     let selectCollectionTemp = true
  //     let selectCollection = true
  //     let selectProduct = true
  //     let selectBrandShop = true
  //     let selectLandingPage = true
  //     newList.forEach(n => {
  //       /**LiveStream */
  //       switch(n?.section_value) {
  //         case "livestream":
  //           if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
  //             selectLiveStream = false
  //           } else if (!n?.section_ref) {
  //             selectLiveStream = false
  //           } else if (!n.image && !n.image_source) {
  //             selectLiveStream = false
  //           } else {
  //             selectLiveStream = true
  //           }
  //           break;
  //         case "collection_temp":
  //           if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
  //             selectCollectionTemp = false
  //           } else if (!n?.section_ref) {
  //             selectCollectionTemp = false
  //           } else if (!n.image && !n.image_source) {
  //             selectCollectionTemp = false
  //           } else {
  //             selectCollectionTemp = true
  //           }
  //           break;
  //         case "collection":
  //           if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
  //             selectCollection = false
  //           } else if (!n?.section_ref) {
  //             selectCollection = false
  //           } else if (!n.image && !n.image_source) {
  //             selectCollection = false
  //           } else {
  //             selectCollection = true
  //           }
  //           break;
  //         case "product":
  //           if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
  //             selectProduct = false
  //           } else if (!n?.section_ref) {
  //             selectProduct = false
  //           } else if (!n.image && !n.image_source) {
  //             selectProduct = false
  //           } else {
  //             selectProduct = true
  //           }
  //           break;
  //         case "brand_shop":
  //           if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
  //             selectBrandShop = false
  //           } else if (!n?.section_ref) {
  //             selectBrandShop = false
  //           } else if (!n.image && !n.image_source) {
  //             selectBrandShop = false
  //           } else {
  //             selectBrandShop = true
  //           }
  //           break;
  //         case "landing_page":
  //           if (n?.uid?.startsWith("_:layout_item") && !n?.section_value) {
  //             selectLandingPage = false
  //           } else if (!n?.section_ref) {
  //             selectLandingPage = false
  //           } else if (!n.image && !n.image_source) {
  //             selectLandingPage = false
  //           } else {
  //             selectLandingPage = true
  //           }
  //           break;
  //       }
  //     })
  //     if (!selectLiveStream || !selectCollectionTemp || !selectCollection || !selectProduct || !selectBrandShop || !selectLandingPage) {
  //       if (typeof invalidCallback === "function") { invalidCallback(true) }
  //       return {
  //         error: false,
  //         helperText: `Please select sub Option`
  //       }
  //     } else if (minItem && newList?.filter(nl => !nl.uid.startsWith("_:drag_item"))?.length < minItem) {
  //       if (typeof invalidCallback === "function") { invalidCallback(true) }
  //       return {
  //         error: true,
  //         helperText: `This field is requred, min section is ${minItem}`
  //       }
  //     } else {
  //       if (typeof invalidCallback === "function") { invalidCallback(false) }
  //       return {}
  //     }
  //   }
  // }, [newList, isShow])
  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */

  // Render
  return (
    <div>
      { isShow ?
        <>
          {dataMap?.map(value => (
            <Item
              key={keyName ? keyName : dataEdit?.uid + "-dentify-item-" + value?.uid}
              listOption={listOption}
              liveStream={liveStream}
              collectionTemp={collectionTemp}
              collections={collections}
              brandShop={brandShop}
              products={products}
              setProducts={setProducts}
              originItem={value}
              dataEdit={dataEdit}
              files={files}
              updated={updated}
              updateItem={updateItem}
              inputChange={inputChange}
              // validateForm={validateForm}
              onInputSelectProductChange={onInputSelectProductChange}
            />
          ))}
        </>
        : ""}
    </div>
  )
}
