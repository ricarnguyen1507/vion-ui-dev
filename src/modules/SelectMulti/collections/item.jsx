import React, { useState } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = [], listOption = [], fullList = [], updateItem, label, inputChange, validateForm = {} }) {
  const [selectedItem, setSelectedItem] = useState(() => originItem)
  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item ? item.uid : null, originItem)
  }

  function optionLabel (option) {
    return option?.collection_name || ""
  }

  // Render
  return (
    originItem ?
      <Autocomplete
        options={listOption}
        getOptionLabel={option => optionLabel(option) || ''}
        getOptionSelected={() => fullList.find(c => c.uid === selectedItem.uid)}
        value={selectedItem && selectedItem.uid && (fullList.find(c => c.uid === selectedItem.uid) || null)}
        style={{ width: "100%" }}
        onChange={handleChangeItemOptions}
        onInputChange={inputChange}
        renderInput={params => (
          <TextField {...params}
            {...validateForm || {}}
            label={ label }
            variant="outlined"
            fullWidth
            margin="dense"
          />
        )}
      /> : ""
  )
}
