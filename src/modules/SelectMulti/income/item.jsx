import React, { useState,useMemo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"
/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = [], disabled, fullList = [], updateItem, label, inputChange, validateForm = {} }) {

  const [selectedItem, setSelectedItem] = useState(() => originItem)
  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item ? item.uid : null, originItem)
  }
  useMemo(() => {
    if (selectedItem?.['section.product']) {
      inputChange(null, selectedItem?.['section.product'])
      return fullList
    } else {
      return []
    }
  }, [selectedItem])
  const handleChangeText = e => {
    e.preventDefault()
    const { name, value } = e.target
    setSelectedItem({...selectedItem, [name]: value || null})
    updateItem(selectedItem?.uid || null, originItem,value)
  }
  // Render
  return (
    originItem ?
    <Grid container spacing={2}>
       <Grid item xs={12} sm={6} lg={6}>
        <Autocomplete
          options={fullList}
          getOptionLabel={option => option.product_name || ''}
          getOptionSelected={() => fullList.find(c => c.uid === selectedItem.uid)}
          value={selectedItem && selectedItem['section.product'] && (fullList.find(c => c.uid === selectedItem['section.product']) || null)}
          style={{ width: "100%" }}
          onChange={handleChangeItemOptions}
          onInputChange={inputChange}
          disabled={disabled}
          renderInput={params => (
            <TextField {...params}
              {...validateForm || {}}
              label={ label }
              variant="outlined"
              fullWidth
              margin="dense"
            />
          )}
        />
       </Grid>
        <Grid item xs={12} sm={6} lg={6}>
          <TextField
            fullWidth
            id="quantity"
            label="Số lượng"
            variant="outlined"
            margin="dense"
            name="quantity"
            onChange={handleChangeText}
            value={selectedItem && selectedItem?.quantity ? selectedItem.quantity : selectedItem?.['section.product'] ? 1 : ''}
            required
            disabled={disabled}
          />
        </Grid>
    </Grid>
      : ""
  )
}
