import React, { useState, useMemo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"
import { TextValidator } from 'react-material-ui-form-validator'
import Card from '@material-ui/core/Card'
import Media from "components/Media"
import { genCdnUrl } from 'components/CdnImage'
import useGeneral from '../../../pages/style_general'
import api from 'services/api_cms'

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ files, updated, originItem = {}, listOption, validateForm, collectionTemp, products, promotions, landing_page, trackings, setProduct, brandShop, setBrandShop, livestream, collections, updateItem, label, inputChange}) {
  const classge = useGeneral()
  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  if (selectedItem != null) {
    if(JSON.stringify(originItem.listSubValueEdit) !== JSON.stringify(selectedItem.listSubValueEdit)) {
      selectedItem.listSubValueEdit = originItem.listSubValueEdit
    }
  }
  const listSubOption = useMemo(() => {
    if (selectedItem?.type_name === 'collection_temp') {
      return collectionTemp
    } else if (selectedItem?.type_name === 'collection') {
      return collections
    } else if (selectedItem?.type_name === 'livestream') {
      return livestream
    } else if (selectedItem?.type_name === 'landing_page') {
      return landing_page
    } else if(selectedItem?.type_name === 'promotions') {
      return promotions
    }
  }, [selectedItem])
  console.log('listSubOption', listSubOption)
  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item || {}, originItem)
    // updateItem(selectedItem || {}, originItem, { 'listSubValue': item?.uid} || null)
    //  updateItem(selectedItem || {}, originItem, null, null, null, null,item)
  }
  const [timer, setTimer] = useState(null)
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products || []
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProduct([...newOption])
        })
    }
  }
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }
  const handleChangeSubItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem({...selectedItem, listSubValue: item?.uid || null})
    updateItem(selectedItem || {}, originItem, { 'listSubValue': item?.uid} || null)
  }
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    setSelectedItem({...selectedItem, [name]: value})
    updateItem(selectedItem || {}, originItem, null, {'display_name': value} || null)
  }
  const onInputSelectBrandShopChange = (e, val) => {
    let queries = ''
    val.replace(/["\\]/g, '\\$&').trim()
    if (val !== '') {
      queries = { brand_shop_name: val.replace(/["\\]/g, '\\$&').trim() }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchBrandShopOption(queries)
    }, 550))
  }
  function fetchBrandShopOption (queries) {
    if (queries !== "") {
      api.post(`/list/brandShop-option`, queries)
        .then(res => {
          const newOption = brandShop
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setBrandShop([...newOption])
        })
    }
  }
  const handleChangeTrackingUtm = (e, item) => {
    e.preventDefault()
    let uid = {
      uid: item?.uid
    }
    setSelectedItem({ ...selectedItem, tracking_utm: uid || null })
    updateItem(selectedItem || {}, originItem, null, null, {uid: item?.uid} || null)
  }

  const updateImage = (field, file) => {
    originItem.image = `images/launcher/${file.md5}.${file.type.split('/')[1]}`
    originItem.renew = true
    originItem['dgraph.type'] = "TypeValues"
    updated.type_values.push(originItem)
    files[file.md5] = file
  }

  function optionLabel (option) {
    if (selectedItem?.type_name === 'collection_temp') {
      return option.collection_name
    } else if(selectedItem?.type_name === 'collection') {
      return option.collection_name
    } else if(selectedItem?.type_name === 'brand') {
      return option.brand_name
    } else if(selectedItem?.type_name === 'product') {
      if (option.sku_id) {
        return `${option.product_name} - ${option.sku_id}`
      } else {
        return option.product_name
      }
    } else if(selectedItem?.type_name === 'livestream') {
      return option['stream.name']
    } else if(selectedItem?.type_name === 'landing_page') {
      return option.landing_name
    } else if(selectedItem?.type_name === 'promotions') {
      return option.display_name_detail
    }
  }

  // function optionLabel (option) {
  //   if (option.product_name) {
  //     if (option.sku_id) {
  //       return `${option.product_name} - ${option.sku_id}`
  //     } else {
  //       return option.product_name
  //     }
  //   }
  // }

  // Render
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={ selectedItem?.type_name == 'home' ? 5 : 2} lg={ selectedItem?.type_name == 'home' ? 5 : 2}>
        <Autocomplete
          options={listOption}
          getOptionLabel={option => option.launcher_type_value || ''}
          value={selectedItem && selectedItem.type_name && (listOption.find(c => c.type_name === selectedItem.type_name) || null)}
          style={{ width: "100%" }}
          onChange={handleChangeItemOptions}
          onInputChange={inputChange}
          renderInput={params => (
            <TextField {...params}
              label={ label }
              variant="outlined"
              fullWidth
              margin="dense"
            />
          )}
        />
      </Grid>
      <>
        {selectedItem?.type_name == 'home' ? '' :
          <Grid item xs={8} sm={3} lg={3}>
            {selectedItem?.type_name === 'product' ?
              <Autocomplete
                options={products}
                getOptionLabel={option => optionLabel(option) || ''}
                value={selectedItem ? products.length > 0 ? selectedItem.listSubValueEdit == undefined ? products.find(c => c.uid === selectedItem.listSubValue) : products.find(c => c.uid === selectedItem.listSubValueEdit.uid) : null : null }
                style={{ width: "100%" }}
                onInputChange={onInputSelectProductChange}
                onChange={handleChangeSubItemOptions}
                renderInput={params => (
                  <TextField {...params}
                    label="Chọn sản phẩm"
                    variant="outlined"
                    fullWidth
                    margin="dense"
                  />
                )}
              /> : selectedItem?.type_name === 'brandShop' ?
                <Autocomplete
                  options={brandShop}
                  getOptionLabel={option => option.brand_shop_name || ''}
                  value={selectedItem ? brandShop.length > 0 ? selectedItem.listSubValueEdit == undefined ? brandShop.find(c => c.uid === selectedItem.listSubValue) : brandShop.find(c => c.uid === selectedItem.listSubValueEdit.uid) : null : null }
                  style={{ width: "100%" }}
                  onInputChange={onInputSelectBrandShopChange}
                  onChange={handleChangeSubItemOptions}
                  renderInput={params => (
                    <TextField {...params}
                      label="Chọn brand shop"
                      variant="outlined"
                      fullWidth
                      margin="dense"
                    />
                  )}
                />
                :
                <Autocomplete
                  options={listSubOption}
                  getOptionLabel={option => optionLabel(option) || ''}
                  value={selectedItem ? listSubOption ? selectedItem.listSubValueEdit == undefined ? listSubOption.find(c => c.uid === selectedItem.listSubValue) : listSubOption.find(c => c.uid === selectedItem.listSubValueEdit.uid) : null : null }
                  style={{ width: "100%" }}
                  disabled={!(selectedItem && selectedItem.type_name != '')}
                  onChange={handleChangeSubItemOptions}
                  renderInput={params => (
                    <TextField {...params}
                      variant="outlined"
                      fullWidth
                      margin="dense"
                    />
                  )}
                />
            }
          </Grid>
        }
        <Grid item xs={4} sm={3} lg={3}>
          <TextValidator
            fullWidth
            // {...validateForm['display_name'] || {}}
            className={classge.inputData}
            id="outlined-basic"
            label="Tên hiển thị"
            variant="outlined"
            margin="dense"
            name="display_name"
            onChange={handleChange}
            value={selectedItem?.display_name || ""}
            validators={[
              'maxStringLength:50'
            ]}
            errorMessages={[
              'Max length is 50',
              'Character is not accept ']}
          />
        </Grid>
        <Grid item xs={4} sm={2} lg={2}>
          <Autocomplete
            options={trackings}
            getOptionLabel={option => option.tracking_name || ''}
            value={selectedItem && selectedItem.tracking_utm != undefined && trackings.find(t => t.uid === selectedItem.tracking_utm?.uid) || null}
            onChange={handleChangeTrackingUtm}
            style={{ width: "100%" }}
            disabled={!(selectedItem && selectedItem.type_name != '')}
            renderInput={params => (
              <TextField {...params}
                // {...selfValidateForm || {}}
                label="Tracking utm"
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        <Grid item xs={4} sm={2} lg={2}>
          <Card
            style={{maxWidth: 180}}
            // disabled={(selectedItem?.listSubValue != null || originItem.image != undefined) ? false : selectedItem?.type_name != 'home'}
            {...(validateForm['image'] || {})}>
            <Media src={genCdnUrl(originItem.image)} type="image" style={{ width: 180, height: 90 }} fileHandle={updateImage} field="image_cover" accept="image/png, image/jpeg" />
          </Card>
        </Grid>
      </>
    </Grid>
  )
}
