import React, { useState, useMemo } from 'react'
import Grid from "@material-ui/core/Grid"
import Item from './item'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import AddBox from '@material-ui/icons/AddBox'
import DeleteIcon from '@material-ui/icons/Delete'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
// import { makeStyles } from '@material-ui/core/styles'
import { clone } from 'services/diff'
import Typography from '@material-ui/core/Typography'
import RootRef from "@material-ui/core/RootRef";
import useGeneral from 'pages/style_general'
import ImportExportIcon from '@material-ui/icons/ImportExport';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { genUid } from "utils"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
/* const useStyles = makeStyles({
  rootDetail: {
    backgroundColor: "#f6f7ff"
  },
  btnDelete: {
    float: "right",
  },
  rootImgIcon: {
    display: "flex"
  },
  titleImgIcon: {
    float: "left",
    paddingTop: 10,
    marginLeft: 20
  },
}) */

export default function ({CollCondSectionOpt, currentList = [], brandShops, parentCollections, childCollections, PriceConds, TagConds, partners, brands, updateListManage, dataEdit, isArray = true, listTitle = "", label = "Vị trí số: ", keyName = false, maxItem = -1, isShow = true, inputChange, facetPrefix, delAll = false, minItem = 0, invalidCallback }) {
  const classge = useGeneral()
  const [listOption] = useState(() => CollCondSectionOpt.map(ls => ({
    uid: genUid("cond_section"),
    ...ls
  })))
  const [dataMap, setDataMap] = useState(() => {
    if (currentList.length > 0) {
      return currentList.map((cur, index) => ({number: index, ...cur}))
    } else {
      return [
        {
          number: 0,
          uid: null,
          section_value: null,
          section_name: null,
          section_type: null,
          section_ref: null
        }
      ]
    }
  })

  const [newList, setNewList] = useState(() => {
    if (dataMap?.length === 1 && dataMap?.[0]?.uid === null) {
      return [
        {
          ...dataMap[0],
          uid: genUid('drag_item')
        }
      ]
    } else {
      return clone(dataMap)
    }

  })
  const updateItem = (uid, originItem, subItemUid = null, section_ref_value = 0, refValue = {}) => {
    const temp = newList.map(item => {
      const n = item;
      if (uid == null && n.number === originItem.number) {
        n.uid = genUid('drag_item')
      } else if (n.number === originItem.number) {
        if (!subItemUid) {
          const reNew = (uid !== null && uid.startsWith("_:") && n.uid.startsWith("_:")) ? genUid('condition_item') : originItem.uid.startsWith('_:') ? uid : originItem.uid
          const selected = listOption.find(l => l.uid === uid)
          n.uid = reNew || genUid('drag_item')
          n.section_value = selected?.section_value
          n.section_name = selected?.section_name
          n.section_type = selected?.section_type
          n.section_ref = selected?.section_ref || null
          n.section_ref_value = section_ref_value !== 0 ? String(section_ref_value) : ""
          n.renew = !originItem.uid.startsWith('_:')
          n['dgraph.type'] = "ConditionSection"
        } else {
          const selected = listOption.find(l => l.section_value === originItem.section_value)
          n.section_ref = subItemUid || selected?.section_ref || null
          n.renew = !originItem.uid.startsWith('_:')
          Object.keys(refValue).forEach(value => {
            n[value] = refValue[value]
          })
        }

        if (section_ref_value !== 0) {
          // const selected = listOption.find(l => l.section_value === originItem.section_value)
          n.section_ref_value = section_ref_value
          n.renew = !originItem.uid.startsWith('_:')
        }
      }
      return n;
    })
    setNewList(temp)
    callUpdateList(newList)
  }


  const onAdd = () => {
    if (maxItem == -1 || dataMap.length < maxItem) {
      setDataMap([...dataMap, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
      setNewList([...newList, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
    }
  }
  const onDelete = () => {
    if (maxItem == -1 || dataMap.length < maxItem) {
      setDataMap([...dataMap, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
      setNewList([...newList, {number: dataMap.length, uid: genUid('drag_item'), section_ref: null}])
    }
  }

  // HANDLE DRAG AND DROP IN TABLE
  // Arrange order of data list
  function reorderList (list, from, to) {
    const result = Array.from(list);
    const [removed] = result.splice(from, 1);
    result.splice(to, 0, removed);
    for (let i = 0; i < result.length; i++) {
      result[i].number = i;
    }
    setNewList(result);
    callUpdateList(result);
  }


  const onDragEnd = result => {
    // dropped outside the list
    const { source, destination} = result;

    if (!result && !destination) {
      return;
    }
    if (!source || !destination) {
      return;
    }
    reorderList(newList, source.index, destination.index)
  }

  function callUpdateList (orderedList) {
    const facetDisplayOrder = `${facetPrefix}|display_order`
    let set = orderedList.filter(li => !li.uid.startsWith("_:drag_item") && li.uid).map(n => ({uid: n.uid, [facetDisplayOrder]: `<${facetPrefix}> <${n.uid}> (display_order=${n.number}) .`, ...n, 'dgraph.type': "ConditionSection"}))
    let del = dataMap.filter(li => li.uid && !li.uid.startsWith("_:")).map(n => ({uid: n.uid}))
    updateListManage({set, del})
  }

  function handleMoveItemToTop (idx) {
    reorderList(newList, idx, 0);
  }
  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */
  let validateForm = useMemo(() => {
    if (isShow) {
      let selectBrandShop = true
      let selectPColl = true
      let selectCColl = true
      let selectPriceVat = true
      let selectTag = true
      newList.forEach(n => {
        if (n?.section_value === "belong_brandshop" && !n?.section_ref) {
          selectBrandShop = false
        } else {
          selectBrandShop = true
        }

        if (n?.section_value === "belong_nh1" && !n?.section_ref) {
          selectPColl = false
        } else {
          selectPColl = true
        }

        if (n?.section_value === "belong_nh2" && !n?.section_ref) {
          selectCColl = false
        } else {
          selectCColl = true
        }

        if (n?.section_value === "has_price_with_vat" && (!n?.section_ref || !n?.section_ref_value || n?.section_ref_value === "")) {
          selectPriceVat = false
        } else {
          selectPriceVat = true
        }

        if (n?.section_value === "has_product_tags" && (!n?.section_ref || !n?.section_ref_value)) {
          selectTag = false
        } else {
          selectTag = true
        }
      })

      /**
             * Chuẩn bị check cho validateForm
             */
      if (minItem && newList?.filter(nl => !nl.uid.startsWith("_:drag_item"))?.length < minItem) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: true,
          helperText: `This field is requred, min section is ${minItem}`
        }
      } else if (!selectBrandShop) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: false,
          helperText: `Please select BrandShop`
        }
      } else if (!selectPColl) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: false,
          helperText: `Please select Parent Collection`
        }
      } else if (!selectCColl) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: false,
          helperText: `Please select Child Collection`
        }
      } else if (!selectPriceVat) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: false,
          helperText: `Please select Price condition and Enter it's value`
        }
      } else if (!selectTag) {
        if (typeof invalidCallback === "function") { invalidCallback(true) }
        return {
          error: false,
          helperText: `Please select Tag Category and it's value`
        }
      } else {
        if (typeof invalidCallback === "function") { invalidCallback(false) }
        return {}
      }
    }
  }, [newList, isShow])
  /**
     * Vui lòng để đoạn code này ở sau cùng...
     */

  // Render
  return (
    <div>
      { isShow ?
        <>
          <div className={classge.titlePanel} style={{margin: "20px 15px 0 0"}}>
            <Grid container spacing={2}>
              <Grid item xs={4} sm={4} lg={4}>
                <Typography className={classge.contentTitle} variant="h5" >
                  {listTitle} {maxItem === "-1" ? " (unlimited)" : maxItem === "0" ? "" : ` (tối đa: ${maxItem})`}
                  {isArray ?
                    <IconButton className={classge.btnDelete} onClick={onAdd}>
                      <AddBox />
                    </IconButton>
                    :
                    ""}
                </Typography>
              </Grid>
              {delAll === true ?
                <Grid item xs={4} sm={4} lg={4}>
                  <Typography className={classge.contentTitle} variant="h5" >
                    <IconButton className={classge.btnDelete} onClick={onDelete}>
                      <DeleteIcon />
                    </IconButton>
                                Xoá tất cả
                  </Typography>
                </Grid>
                : ""
              }
            </Grid>

          </div>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided) => (
                <RootRef rootRef={provided.innerRef}>
                  <List >
                    {/* <Grid container spacing={2}> */}
                    {newList.map((value, idx) => (
                      <Draggable key={value?.uid + "_" + idx} draggableId={value?.uid + "_" + idx} index={idx}>
                        {(provided) => (
                          // <Grid item xs={12} sm={(12/cols)} lg={(12/cols)} >
                          <ListItem
                            classes={{
                              secondaryAction: classge.secondaryActionRoot,
                            }}
                            style={{display: "flex", alignItems: "center", padding: "5px", cursor: "grab"}}
                            ContainerComponent="li"
                            ContainerProps={{ ref: provided.innerRef }}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Item
                              key={keyName ? keyName : dataEdit?.uid + "-dentify-item-" + value?.uid}
                              listOption={listOption}
                              brandShops={brandShops}
                              parentCollections={parentCollections}
                              childCollections={childCollections}
                              PriceConds={PriceConds}
                              TagConds={TagConds}
                              partners={partners}
                              brands={brands}
                              originItem={value}
                              dataEdit={dataEdit}
                              label={label + (value.number + 1)}
                              updateItem={updateItem}
                              inputChange={inputChange}
                              validateForm={validateForm}

                            />
                            <ImportExportIcon style={{marginLeft: "6px"}}/>
                            {idx !== 0 ?
                              <Button
                                variant="contained"
                                color="primary"
                                style={{marginLeft: "6px"}} onClick={() => handleMoveItemToTop(idx)}>
                                                                    Top
                              </Button>
                              :
                              <Button
                                disabled
                                style={{display: "hidden"}}>
                              </Button>
                            }
                            <ListItemSecondaryAction >
                            </ListItemSecondaryAction>
                          </ListItem>
                          // </Grid>
                        )}
                      </Draggable>
                    ))}
                    {/* </Grid> */}
                    {provided.placeholder}
                  </List>
                </RootRef>
              )}
            </Droppable>
          </DragDropContext>
        </>
        : ""}
    </div>
  )
}
