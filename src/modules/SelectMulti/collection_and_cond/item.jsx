import React, { useState, useMemo, useEffect } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"
import api from 'services/api_cms'
/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = {}, listOption, brandShops, parentCollections, childCollections, PriceConds, TagConds, partners, brands, updateItem, label, inputChange, validateForm = {} }) {

  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  const { listSubOption, gridColl, sugGridColl } = useMemo(() => {
    if (selectedItem?.section_value === 'belong_brandshop') {
      return {
        listSubOption: brandShops,
        gridColl: 4,
        sugGridColl: 8
      }
    } else if (selectedItem?.section_value === 'belong_nh1') {
      return {
        listSubOption: parentCollections,
        gridColl: 4,
        sugGridColl: 8
      }
    } else if (selectedItem?.section_value === 'belong_nh2') {
      return {
        listSubOption: childCollections,
        gridColl: 4,
        sugGridColl: 8
      }
    } else if (selectedItem?.section_value === 'has_price_with_vat') {
      return {
        listSubOption: PriceConds,
        gridColl: 4,
        sugGridColl: 2
      }
    } else if (selectedItem?.section_value === 'has_product_tags') {
      return {
        listSubOption: TagConds,
        gridColl: 4,
        sugGridColl: 2
      }
    } else if (selectedItem?.section_value === 'belong_supplier') {
      return {
        listSubOption: partners,
        gridColl: 4,
        sugGridColl: 8
      }
    } else if (selectedItem?.section_value === 'belong_brand') {
      return {
        listSubOption: brands,
        gridColl: 4,
        sugGridColl: 8
      }
    } else {
      return {
        listSubOption: [],
        gridColl: 12,
        sugGridColl: 0
      }
    }
  }, [selectedItem])

  const [tagList, setTagList] = useState([])

  useEffect(() => {
    if (originItem?.section_value === "has_product_tags" && originItem?.section_ref) {
      const tagCondSelected = TagConds.find(tc => tc.uid === originItem?.section_ref)
      if (tagCondSelected?.code) {
        api.get(`/list/tag?tag_category=${tagCondSelected.code}`).then(res => {
          setTagList(res.data?.result || [])
        })
      }
    }
  }, [])

  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    updateItem(item?.uid || null, originItem)
  }

  const handleChangeSubItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem({ ...selectedItem, section_ref: item?.uid || null})
    updateItem(selectedItem?.uid || null, originItem, item?.uid || null)

    /**Xử lý kết quả chọn Tag Categories mà load lại danh sách Tag List có thể chọn */
    if (selectedItem.section_value === "has_product_tags" && item?.code) {
      api.get(`/list/tag?tag_category=${item.code}`)
        .then(response => {
          setTagList(response.data?.result)
        })
    }
  }

  const handleChangeRefValue = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setSelectedItem({ ...selectedItem, [name]: value || null })
    updateItem(selectedItem?.uid || null, originItem, selectedItem.section_ref || null, value)
  }
  const handleChangeItemRefValue = (e, item) => {
    e.preventDefault()
    setSelectedItem({ ...selectedItem, section_ref_value: item?.uid || null})
    updateItem(selectedItem?.uid || null, originItem, selectedItem.section_ref || null, item?.uid || null)
  }

  function optionLabel (option) {
    if (option.brand_shop_name) {
      return option.brand_shop_name
    } else if (option.collection_name) {
      return option.collection_name
    } else if (option.phone_number) {
      if (option.customer_name) {
        return `${option.phone_number} - ${option.customer_name} - ${option.uid}`
      } else {
        return `${option.phone_number} - ${option.uid}`
      }
    } else if (option.section_name) {
      return option.section_name
    } else if (option.partner_name) {
      return option.partner_name
    } else if (option.brand_name) {
      return option.brand_name
    } else if (option.brand_shop_name) {
      return option.brand_shop_name
    }
  }
  // Render
  return (
    originItem ?
      <Grid container spacing={2}>
        <Grid item xs={12} sm={gridColl} lg={gridColl}>
          <Autocomplete
            options={listOption ? listOption : []}
            getOptionLabel={option => optionLabel(option) || ''}
            value={selectedItem && selectedItem?.section_value && (listOption?.find(c => c?.section_value === selectedItem?.section_value) || null)}
            style={{ width: "100%" }}
            onChange={handleChangeItemOptions}
            onInputChange={inputChange}
            renderInput={params => (
              <TextField {...params}
                {...validateForm}
                label={label}
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        {selectedItem ?
          <Grid item xs={8} sm={sugGridColl} lg={sugGridColl}>
            <Autocomplete
              options={listSubOption ? listSubOption : []}
              getOptionLabel={option => optionLabel(option) || ''}
              value={selectedItem && selectedItem?.section_ref && (listSubOption?.find(c => c.uid === selectedItem.section_ref) || null)}
              style={{ width: "100%" }}
              onChange={handleChangeSubItemOptions}
              renderInput={params => (
                <TextField {...params}
                  variant="outlined"
                  fullWidth
                  margin="dense"
                  error={!(selectedItem && selectedItem?.section_ref && listSubOption?.find(c => c.uid === selectedItem.section_ref))}
                  required={true}
                />
              )}
            />
          </Grid> : ""}
        {selectedItem?.section_value === 'has_price_with_vat' ?
          <Grid item xs={12 - gridColl - sugGridColl} sm={12 - gridColl - sugGridColl} lg={12 - gridColl - sugGridColl}>
            <TextField
              fullWidth
              id="section_ref_value"
              label="Giá trị"
              variant="outlined"
              margin="dense"
              name="section_ref_value"
              onChange={handleChangeRefValue}
              defaultValue={selectedItem.section_ref_value || ""}
            />
          </Grid> : "" }
        {selectedItem?.section_value === 'has_product_tags' ?
          <Grid item xs={12 - gridColl - sugGridColl} sm={12 - gridColl - sugGridColl} lg={12 - gridColl - sugGridColl}>
            <Autocomplete
              options={tagList ? tagList : []}
              getOptionLabel={option => option.tag_title || ''}
              value={selectedItem && selectedItem.section_ref_value && (tagList.find(c => c.uid === selectedItem.section_ref_value) || {})}
              style={{ width: "100%" }}
              onChange={handleChangeItemRefValue}
              renderInput={params => (
                <TextField {...params}
                  variant="outlined"
                  fullWidth
                  margin="dense"
                />
              )}
            />
          </Grid> : "" }
      </Grid>
      : ""
  )
}
