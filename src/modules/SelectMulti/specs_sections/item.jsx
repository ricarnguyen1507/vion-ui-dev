import React, { useState } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'
import Grid from "@material-ui/core/Grid"

/**
 * @crime C.A
 * 10-04-2020
 * @param listOption (products, collections, province, district or any thing like this)
 * @param originItem (highlight.products, highlight.collection, province, district,... selected)
 * @callback function updateListManage( { set, del } )
 * array uid set new and del old
 */
export default function ({ originItem = {}, listOption, fullList = [], updateItem, label, inputChange, validateForm = {}}) {

  const [selectedItem, setSelectedItem] = useState(() => {
    if (originItem?.uid?.startsWith("_:drag")) {
      return null
    } else {
      return originItem
    }
  })

  // const [refItemOpt, setRefItemOpt] = useState([])

  const handleChangeItemOptions = (e, item) => {
    e.preventDefault()
    setSelectedItem(item)
    // if (item?.['product.specs_sections']?.length > 0) {
    //   setRefItemOpt(item['product.specs_sections'])
    // } else {
    //   setRefItemOpt([])
    // }
    updateItem(item?.uid || null, originItem)
  }

  const handleChangeRefValue = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setSelectedItem({ ...selectedItem, [name]: value || null })
    updateItem(selectedItem?.uid || null, originItem, selectedItem.section_ref || null, value)
  }
  function optionLabel (option) {
    if (option.section_name) {
      return option.section_name
    } else {
      return "không tìm thấy field name"
    }
  }
  // Render
  return (
    originItem ?
      <Grid container spacing={2}>
        <Grid item xs={6} sm={4} lg={4}>
          <Autocomplete
            options={listOption}
            getOptionLabel={option => optionLabel(option) || ''}
            getOptionSelected={() => fullList.find(c => c.uid === selectedItem.uid)}
            value={selectedItem && selectedItem.section_value && (fullList.find(c => c.section_value === selectedItem.section_value) || null)}
            style={{ width: "100%" }}
            onChange={handleChangeItemOptions}
            onInputChange={inputChange}
            renderInput={params => (
              <TextField {...params}
                {...validateForm}
                label={ label }
                variant="outlined"
                fullWidth
                margin="dense"
              />
            )}
          />
        </Grid>
        <Grid item xs={6} sm={8} lg={8}>
          <TextField
            fullWidth
            id="section_ref_value"
            label="Tên phân loại"
            variant="outlined"
            margin="dense"
            name="section_ref_value"
            onChange={handleChangeRefValue}
            defaultValue={selectedItem?.section_ref_value || ""}
          />
        </Grid>
      </Grid>
      : ""
  )
}
