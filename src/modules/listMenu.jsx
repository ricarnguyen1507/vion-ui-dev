/// Import Icon
import {
  BorderAll as TableIcon,
  Tv as TvIcon,
  PinDrop as AddressIcon,
  AccountBox as UserIcon,
  PlaylistAddCheck as OrderIcon,
  // HomeWork as DistributorIcon,
  InsertEmoticon as StatusIcon,
  LocalOffer as BrandIcon,
  Group as CustomerIcon,
  Policy as RoleIcon,
  Toys as ProductTypeIcon,
  NotificationsActive as NotificationsIcon,
  Public as RegionIcon,
  Highlight as HighLightIcon,
  Layers as PlatformIcon,
  ViewComfy as CollectionIcon,
  AttachMoney as AttachMoneyIcon,
  QueueMusic as QueueMusicIcon,
  BrandingWatermark as BrandingWatermarkIcon,
  LiveTv as LiveTvIcon,
  CastConnected as CastConnectedIcon,
  Description as DescriptionIcon
} from "@material-ui/icons"

/// Import Pages
/* CMS */
import Manufacturer from "pages/manufacturer";
import Suppliers from "pages/partner";

import Product from "pages/product";
import Income from "pages/income"
import Income_v2 from "pages/income_v2"


/**
 * @param label Tên hiển thị bên ngoài UI
 * @param function_name Tên hiển thị trong table Role
 * @param link Đường dẫn đến page
 * @param icon Icon hiển thị của pages
 * @param alias Bí danh - dùng kiểm tra với role_function
 * @param com Component hiển thị trong routes
 */
export const structures = [
  [
    { label: "Nhà Sản Xuất", function_name: "Nhà sản xuất", link: "/app/manufacturer", icon: <ProductTypeIcon />, alias: "manufacturer", com: Manufacturer },
    { label: "Nhà Cung Cấp", function_name: "Nhà Cung Cấp", link: "/app/partner", icon: <CustomerIcon />, alias: "partner", com: Suppliers },
    { label: "Sản Phẩm", function_name: "Sản phẩm", link: "/app/product", icon: <TvIcon />, alias: "product", com: Product},
    { label: "Thu chi", function_name: "Thu chi", link: "/app/income", icon: <TvIcon />,alias: "income", com: Income },
    { label: "Thu chi v2", function_name: "Thu chi v2", link: "/app/income_v2", icon: <TvIcon />,alias: "income_v2", com: Income_v2 },
  ]
]


// 1 - Chuyển nhiều mảng thành 1 mảng - áp dụng cho : Layout, Role
export function multiToOne () {
  if (structures && Array.isArray(structures) === true) {
    let arrOne = []
    structures.forEach((item) => {
      if (item && Array.isArray(item) === true) {
        arrOne = arrOne.concat(item)
      }
    })
    return arrOne
  }
}
