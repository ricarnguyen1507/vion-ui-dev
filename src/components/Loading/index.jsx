import React from 'react';

const Loading = ({statusText = 'Loading...', statusCode = 0, children, style = {} }) => (
  <div className="main-container" style={style}>
    <div className="loading"></div>
    <p className="loading-text">{statusText}</p>
    { statusCode && children }
  </div>
)
export default Loading;
