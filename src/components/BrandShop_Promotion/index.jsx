import React, {useState} from 'react'
// import { makeStyles } from '@material-ui/core/styles';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';

// import { blue } from '@material-ui/core/colors';

import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'

let promotionSection = [
  {
    promotion_name: "Free Ship",
    promotion_type: "freeship",
    promotion_values: 0,
    promotion_limit_price: 0,
    condition_value: 0,
    product_extra: []
  },
  {
    promotion_name: "Tặng kèm sản phẩm",
    promotion_type: "extra_gift",
    promotion_values: 0,
    promotion_limit_price: 0,
    condition_value: 0,
    product_extra: []
  },
  {
    promotion_name: "Tặng kèm hoá đơn",
    promotion_type: "gift_by_bill",
    promotion_values: 0,
    promotion_limit_price: 0,
    condition_value: 0,
    product_extra: []
  },
  {
    promotion_name: "Giảm giá trực tiếp",
    promotion_type: "directly",
    promotion_values: 0,
    promotion_limit_price: 0,
    condition_value: 0,
    product_extra: []
  }
]
/* const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
}); */

export default function SimpleDialog (props) {
  // const classes = useStyles();
  const {onClose, onNext, selectedBrandShop, setSelectedBrandShop, selectedPromotion, setSelectedPromotion, open, brandShop, defaultSelectUid = promotionSection[0] } = props;

  const [selectedItem, setSelectedItem] = useState(() => selectedBrandShop)
  const [selectedPromotionItem, setSelectedPromotionItem] = useState(() => selectedPromotion)
  const handleClose = () => {
    onClose(selectedItem)
  };
  const handleNext = () => {
    if(selectedItem && selectedPromotionItem) {
      onNext()
    }
  };

  const onBrandShopChange = (e, item) => {
    setSelectedItem(item)
    setSelectedBrandShop(item)
  }
  const onPromotionChange = (e, item) => {
    setSelectedPromotionItem(item)
    setSelectedPromotion(item)
  }
  return (
    <Dialog onClose={handleClose} open={open} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">Tạo khuyến mãi</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {brandShop &&
            <Autocomplete
              options={brandShop}
              value={brandShop.find(b => b.uid === (selectedItem?.uid || defaultSelectUid))}
              getOptionLabel={option => (option.brand_shop_name || '')}
              style={{ width: "550px" }}
              onChange={onBrandShopChange}
              renderInput={params => (
                <TextField {...params}
                  label="Chọn Brand Shop"
                  fullWidth
                  variant="outlined"
                  margin="dense"
                  error={!selectedItem}
                  required={true}
                />
              )}
            />
          }
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          {brandShop &&
            <Autocomplete
              options={promotionSection}
              value={promotionSection.find(b => b.promotion_type === (selectedPromotionItem?.promotion_type || defaultSelectUid))}
              getOptionLabel={option => (option.promotion_name || '')}
              style={{ width: "550px" }}
              onChange={onPromotionChange}
              renderInput={params => (
                <TextField {...params}
                  label="Chọn loại khuyến mãi"
                  fullWidth
                  variant="outlined"
                  margin="dense"
                  error={!selectedPromotionItem}
                  required={true}
                />
              )}
            />
          }
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
            Cancel
        </Button>
        <Button onClick={handleNext} color="primary" autoFocus>
            Next
        </Button>
      </DialogActions>
    </Dialog>
  );
}