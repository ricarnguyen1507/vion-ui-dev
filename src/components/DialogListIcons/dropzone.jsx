import React, {useCallback} from 'react'
import {useDropzone} from 'react-dropzone'

/*
<input {...getInputProps()} />
  {
    isDragActive ?
      <p>Thả file hình ảnh ở đây ...</p> :
      <p>Kéo thả hoặc click chuột</p>
  }
*/
export default function Dropzone ({children, onFiles, style}) {
  const onDrop = useCallback(onFiles, [])
  // const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})
  const {getRootProps} = useDropzone({onDrop})
  return <>
    <div style={style} {...getRootProps()}>{children}</div>
  </>
}