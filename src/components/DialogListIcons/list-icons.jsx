import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import DropZone from './dropzone'
import Grid from '@material-ui/core/Grid'
import api from 'services/api_cms'
import readFiles from 'services/files-reader'
const { cdn_url } = window.appConfigs


/* function sliceItem(items, setItems, idx) {
    items.splice(idx, 1)
    setItems([...items])
}
function addItems(items, newItems, setItems) {
    items.push(...newItems)
    setItems([...items])
}
function isNew(uid) {
    return uid.startsWith("_:")
} */

const useStyles = makeStyles(() => ({
  selected: {
    outline: "none",
    borderColor: "cyan",
    boxShadow: "0 0 10px cyan"
  }
}))

let count = 0

export default function ({ icons, setIcons, selectIdx, setSelect }) {
  const classes = useStyles()
  function onFiles (files) {
    readFiles(files, filesData => {
      // Đọc file đễ hiển thị liền mà không cần upload
      const newIcons = filesData.map(data => ({
        uid: `icon_${++count}`,
        base64: true,
        source: data
      }))
      icons.push(...newIcons)
      setIcons([...icons])

      // Upload tất cả files bằng form data
      const formData = new FormData()
      files.forEach((file, idx) => {
        formData.set(newIcons[idx].uid, file)
      })
      api({
        method: 'post',
        url: '/set/icon',
        data: formData,
        headers: {
          "Accept": "application/json",
          "Content-Type": "multipart/form-data"
        }
      }).then(({ data }) => {
        // Cập nhật lại uid cho data sau khi upload
        newIcons.forEach(icon => {
          icon.uid = data[icon.uid]
        })
      }).catch(err => {
        console.log(err)
      })
    })
  }

  const delSelect = (idx) => (e) => {
    e.stopPropagation()
    if (icons[idx].uid.startsWith('0x')) {
      api.post(`/del/icon`, icons[idx])
        .catch(err => {
          console.log(err)
        })
    }
    icons.splice(idx, 1)
    setIcons([...icons])
  }
  const focusItem = idx => e => {
    e.stopPropagation()
    setSelect(idx)
  }

  return (
    <Grid container component={DropZone}>
      <Grid item xs={12} component={DropZone} onFiles={onFiles}
        style={{
          minHeight: 300,
          width: "100%",
          border: "dashed",
          cursor: "pointer",
          position: "relative",
          boxSizing: "border-box",
          borderColor: " #C8C8C8",
          backgroundColor: "#F0F0F0"
        }}
      >
        <Grid container>
          {
            icons.map((image, i) =>
              <Grid item xs={2} key={image.uid} onClick={focusItem(i)} className={(selectIdx === i) ? classes.selected : ""} style={{ position: "relative", margin: 10 }}>
                <IconButton onClick={delSelect(i)} size="small" variant="extended" aria-label="delete" style={{ position: "absolute", top: 0, right: 0 }}>
                  <DeleteIcon fontSize="small"/>
                </IconButton>
                <img src={image.base64 ? image.source : `${cdn_url}/${image.source}`} style={{ width: "100%", height: "100%"}} alt="" />
              </Grid>
            )
          }
        </Grid>
      </Grid>
    </Grid>
  )
}