import React, {useState} from 'react'
import Button from '@material-ui/core/Button'
// import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
// import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle'
import Divider from '@material-ui/core/Divider'
import ListIcons from './list-icons'

export default function ({icons, setIcons, open, setOpen, onSelect}) {
  const [selectIdx, setSelect] = useState(-1)
  return (
    <Dialog maxWidth="sm" fullWidth={true} open={open} onClose={() => setOpen(false)} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Icons List</DialogTitle>
      <Divider/>
      <DialogContent>
        <ListIcons {...{icons, setIcons, selectIdx, setSelect}} />
      </DialogContent>
      <Divider/>
      <DialogActions>
        <Button onClick={() => setOpen(false)} size="small" color="secondary" variant="contained">
          Close
        </Button>
        <Button onClick={() => onSelect(icons[selectIdx])} size="small" color="primary" variant="contained">
          Select
        </Button>
      </DialogActions>
    </Dialog>
  );
}