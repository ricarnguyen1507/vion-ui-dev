import React, {useMemo, useState} from 'react'
import IconButton from '@material-ui/core/IconButton'
import DeleteForeverRoundedIcon from '@material-ui/icons/DeleteForeverRounded';
import Grid from "@material-ui/core/Grid"
import Typography from '@material-ui/core/Typography';
import api from 'services/api_cms';
import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'

// import useGeneral from 'pages/style_general'

export default function ({indexProduct, product = {'stream_product.product': {}, time: 0}, handleChangeData = () => {}, handleDeleteProduct = () => {}}) {
  // const classge = useGeneral()
  const [timer, setTimer] = useState(null)
  const convertTime = (t = 0) => `${parseInt(t / 60 / 60)}h:${parseInt((t % (60 * 60)) / 60)}p:${t % 60}s`

  const [products, setProducts] = useState(() => {
    if(product['stream_product.product']) {
      return [product['stream_product.product']]
    }
    return []
  });
  const handleChange = (e) => {
    e.preventDefault()
    const { name, value = "" } = e.target
    handleChangeData({
      ...product,
      [name]: value
    }, indexProduct)
  }

  const productDetail = useMemo(() => {
    if(product && product['stream_product.product'] && product['stream_product.product'].uid && products.length)
      return products.find(item => item.uid === product['stream_product.product'].uid)
    return null
  }, [product, products])

  const handleChangeProducts = (e, item) => {
    e.preventDefault();
    if(item) {
      handleChangeData({
        ...product,
        'stream_product.product': {
          uid: item.uid
        }
      }, indexProduct)
    }

  }
  function fetchProdOption (queries) {
    if (queries !== "") {
      api.post(`/list/product-option`, queries)
        .then(res => {
          const newOption = products || []
          res.data.result.forEach(r => {
            if (!newOption.find(no => no.uid === r.uid)) {
              newOption.push(r)
            }
          })
          setProducts([...newOption])
        })
    }
  }
  const onInputSelectProductChange = (e, val) => {
    let queries = ''
    if (val !== '') {
      queries = { fulltext_search: val.replace(/["\\]/g, '\\$&') }
    } else {
      queries = ''
    }
    clearTimeout(timer)
    setTimer(setTimeout(() => {
      fetchProdOption(queries)
    }, 550))
  }

  const validateText = useMemo(() => {
    if (product.time !== undefined && product.time !== null && product.time < 0) {
      return {
        time: {
          error: true,
          helperText: "Thời gian lớn hơn 0"
        }
      }
    }
    return {}
  }, [product.time])
  return (
    <>
      <div>
        <Typography variant="h6" >
                    Sản phẩm #{indexProduct + 1}
          <IconButton color="primary" onClick={() => { handleDeleteProduct(indexProduct) }}>
            <DeleteForeverRoundedIcon />
          </IconButton>
        </Typography>
      </div>
      <Grid container spacing={2} style={{width: '100%'}}>
        <Grid item xs={6} sm={6} lg={6}>
          <Autocomplete
            onChange={handleChangeProducts}
            options={products}
            value={productDetail}
            getOptionLabel={option => (option.product_name || '')}
            onInputChange={onInputSelectProductChange}
            renderInput={params => (
              <TextField {...params}
                label="Chọn sản phẩm"
                fullWidth
                variant="outlined"
                margin="dense"
              />
            )}
          />
        </Grid>
        <Grid item xs={4} sm={4} lg={4}>
          <TextField
            {...(validateText['time'] || {})}
            id={`${indexProduct}-${product.time}`}
            name="time"
            variant="outlined"
            fullWidth
            onChange={handleChange}
            value={+product.time}
            label={`Hiển thị sản phẩm lúc ${convertTime(product.time)}`}
            placeholder="Thời gian hiển thị sản phẩm (s)"
          />
        </Grid>
      </Grid>
    </>
  )
}