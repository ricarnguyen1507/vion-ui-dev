import React, { useState } from "react";
import {
  Collapse,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Tooltip,
  Grow
} from "@material-ui/core";
import { Inbox as InboxIcon } from "@material-ui/icons";
import { Link } from "react-router-dom";
import classnames from "classnames";

// styles
import { withStyles } from "@material-ui/core/styles"
import useStyles from "./styles";

// components
import Dot from "../Dot";

export default function SidebarLink ({
  link,
  icon,
  label,
  children,
  location,
  isSidebarOpened,
  nested,
  type,
  // sidebarLabels
}) {

  // classes styles
  var classes = useStyles();

  // Tooltip when collapse Sidebar
  const CustomTooltip = withStyles(() =>
    (
      !isSidebarOpened ?
        {
          tooltip: {
            backgroundColor: `#08A495`,
            color: "#FFF",
            fontSize: "1rem",
            marginLeft: 0,
            height: 44,
            padding: "0px 12px",
            boxSizing: "border-box",
            display: "flex",
            alignItems: "center",
            transition: "all .2s"
          }
        }
        :
        {
          tooltip: { display: "none" }
        }
    ))(Tooltip);

  // local
  var [isOpen, setIsOpen] = useState(false);
  var isLinkActive = link && location.pathname === link;
  // (location.pathname === link || location.pathname.indexOf(link) !== -1);
  if (type === "title")
    return (
      <Typography
        className={classnames(classes.linkText, classes.sectionTitle, {
          [classes.linkTextHidden]: !isSidebarOpened,
        })}
      >
        {label}
      </Typography>
    );

  if (type === "divider") return <Divider className={classes.divider} />;

  if (!children)
    return (
      <CustomTooltip
        interactive
        title={label}
        placement="right"
        TransitionComponent={Grow}
        TransitionProps={{ timeout: 600 }}
      >
        <ListItem
          button
          component={link && Link}
          to={link}
          className={classnames(classes.link, {[classes.linkCollapsed]: !isSidebarOpened})}
          classes={{
            root: classnames(classes.linkRoot, {
              [classes.linkActive]: isLinkActive && !nested,
              [classes.linkNested]: nested,
            }),
          }}
          disableRipple
        >
          <ListItemIcon
            className={classnames(
              classes.linkIcon,
              {[classes.linkIconActive]: isLinkActive}
            )}
          >
            {nested ? <Dot color={isLinkActive && "primary"} /> : icon}
          </ListItemIcon>
          <ListItemText
            className={
              classnames(
                classes.linkText, {
                  [classes.linkTextActive]: isLinkActive,
                  [classes.linkTextHidden]: !isSidebarOpened,
                })
            }
          >
            {label}
          </ListItemText>
        </ListItem>
      </CustomTooltip>
    );

  return (
    <>
      <ListItem
        button
        component={link && Link}
        onClick={toggleCollapse}
        className={classes.link}
        to={link}
        disableRipple
      >
        <ListItemIcon
          className={classnames(classes.linkIcon,
            {[classes.linkIconActive]: isLinkActive}
          )}
        >
          {icon ? icon : <InboxIcon />}
        </ListItemIcon>
        <ListItemText
          className={
            classnames(
              classes.linkText, {
                [classes.linkTextActive]: isLinkActive,
                [classes.linkTextHidden]: !isSidebarOpened,
              })
          }
        >
          {label}
        </ListItemText>
      </ListItem>

      {children && (
        <Collapse
          in={isOpen && isSidebarOpened}
          timeout="auto"
          unmountOnExit
          className={classes.nestedList}
        >
          <List component="div" disablePadding >
            {children.map(childrenLink => (
              <SidebarLink
                key={childrenLink && childrenLink.link}
                location={location}
                isSidebarOpened={isSidebarOpened}
                classes={classes}
                nested
                {...childrenLink}
              />
            ))}
          </List>
        </Collapse>
      )}
    </>
  );


  function toggleCollapse (e) {
    if (isSidebarOpened) {
      e.preventDefault();
      setIsOpen(!isOpen);
    }
  }
}
