import React, { useState, useEffect, useMemo } from "react";
import {
  Drawer,
  // IconButton,
  Typography,
  List,
  ListItem,
  // ListItemIcon,
  // ListItemText,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@material-ui/core";
import { useTheme } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import useStyles from "./styles";
import SidebarLink from "./components/SidebarLink/SidebarLink";
import getStructures from "./structures";
// import Dot from "./components/Dot";
import { useUserState } from "context/UserContext";
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar,
} from "context/LayoutContext";

import textLogo from "images/vion_logo.jpg";
import imgLogo from "images/logo.svg";

function Sidebar ({ location }) {
  var classes = useStyles();
  var theme = useTheme();

  const { role = "", permission = [] } = useUserState();
  const structure = useMemo(() => role ? getStructures(role.toLowerCase(), permission) : [], [role, permission]);

  // global
  var layoutDispatch = useLayoutDispatch();
  var layoutState = useLayoutState();
  var { isSidebarOpened } = layoutState;

  // local
  var [isPermanent, setPermanent] = useState(true);
  var [expanded, setExpanded] = useState(false);

  const closeDrawer = () => {
    toggleSidebar(layoutDispatch)
  }

  const handleChangePanel = (panel) => (isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const sidebarLabels = [
    { label: "CMS" }
  ];
  useEffect(function () {
    window.addEventListener("resize", handleWindowWidthChange);
    handleWindowWidthChange();
    return function cleanup () {
      window.removeEventListener("resize", handleWindowWidthChange);
    };
  });

  return (
    <Drawer
      variant={isPermanent ? "permanent" : "temporary"}
      className={classNames(classes.drawer, classes.root, {
        [classes.drawerOpen]: isSidebarOpened,
        [classes.drawerClose]: !isSidebarOpened,
      })}
      classes={{
        paper: classNames({
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        }),
      }}
      open={isSidebarOpened}
      onClose={closeDrawer}
    >
      {/* <div className={classes.toolbar} /> */}

      <div className={classNames(classes.sidebarHeader, {
        [classes.sidebarHeaderCollapsed]: !isSidebarOpened
      })}>
        <div
          className={classNames(classes.logoContainer,
            { [classes.showImgLogo]: !isSidebarOpened },
          )}
        >
        <img
            className={classNames(classes.coverLogo,
            )}
            src={textLogo}
            height="150px"
            alt="Vion Mart"
          />
          {/* {isSidebarOpened ? (
            <img
              className={classNames(classes.coverLogo,
              )}
              src={textLogo}
              height="28px"
              alt="Vion Mart"
            />
          ) : (
            <img
              className={classNames(classes.coverLogo,
                {[classes.hideLogoOnMobile]: !isSidebarOpened},
              )}
              src={imgLogo}
              height="45px"
              alt="Logo Vion Mart"
            />
          )} */}
        </div>

      </div>
      {/* <div className={classes.mobileBackButton}>
        <IconButton onClick={() => toggleSidebar(layoutDispatch)}>
          <ArrowBackIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse),
            }}
          />
        </IconButton>
      </div> */}

      <List className={classes.sidebarList}>
        {structure.map((item, i) => (
          <ListItem key={i} className={classes.listItem}>
            <Accordion
              onChange={handleChangePanel(`panel${i}`)}
              expanded={expanded === `panel${i}`}
              className={classNames(
                { [classes.activePanel]: expanded === `panel${i}` },
                classes.expandPanel,
              )}
            >
              <AccordionSummary
                className={classNames(
                  { [classes.activePanelSum]: expanded === `panel${i}` },
                  classes.expandPanelSum,
                )}
                aria-controls="panel1a-content"
                id="panel1a-header"
                classes={{ content: classes.expandPanelContent }}
              >
                <Typography className={classes.panelTitle}>
                  {sidebarLabels[i].label}
                </Typography>
              </AccordionSummary>
              <AccordionDetails className={classes.expandPanelDetail}>
                {item.map((link, idx) => (
                  <SidebarLink
                    key={idx}
                    location={location}
                    isSidebarOpened={isSidebarOpened}
                    sidebarLabels={sidebarLabels[i]}
                    {...link}
                  />
                ))}
              </AccordionDetails>
            </Accordion>
          </ListItem>
        ))}

        {/* COLLAPSE BUTTON */}
        {/* <ListItem
          button
          className={classNames(
            { [classes.isCollapsed]: !isSidebarOpened },
            classes.collapseBtn,
          )}
          onClick={() => toggleSidebar(layoutDispatch)}
        >
          <ListItemIcon color="primary" className={classes.listItemIcon}>
            <DoubleBackIcon
              className={classNames(classes.collapseIcon, {
                [classes.reverseCollapseIcon]: !isSidebarOpened,
              })}
            />
          </ListItemIcon>
          <ListItemText
            classes={{
              primary: classNames(classes.collapseText, {
                [classes.collapseTextHidden]: !isSidebarOpened,
              }),
            }}
            primary={"Collapse Sidebar"}
          />
        </ListItem> */}
      </List>
    </Drawer>
  );

  function handleWindowWidthChange () {
    var windowWidth = window.innerWidth;
    var breakpointWidth = theme.breakpoints.values.md;
    var isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  }
}

export default withRouter(Sidebar);
