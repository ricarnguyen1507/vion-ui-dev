import { structures } from "modules/listMenu"

function createRoleFilter (role, aliases) {
  if (aliases) {
    return function (item) {
      return typeof aliases[item.alias] !== 'undefined' && Number.isInteger(aliases[item.alias]) && aliases[item.alias] !== 2
    }
  } else if (role) {
    return function (item) {
      return !item.roles || item.roles.includes(role)
    }
  }
}

export default function getStructures (role, permissions) {
  if (role || permissions?.length) {
    const aliases = (permissions.length > 0) && permissions.reduce((r, { function_alias, permission }) => {
      r[function_alias] = permission
      return r
    }, {})
    const roleFilter = createRoleFilter(role, aliases)
    return structures.map(items => items.filter(roleFilter))
  }
  return []
}