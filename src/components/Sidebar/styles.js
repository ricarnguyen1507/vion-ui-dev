import { makeStyles } from "@material-ui/styles";

const drawerWidth = 240;

export default makeStyles(theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
    boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)'
  },
  drawerOpen: {
    width: drawerWidth,
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 40,
    [theme.breakpoints.down("sm")]: {
      width: drawerWidth,
    },
  },
  toolbar: {
    ...theme.mixins.toolbar,
    minHeight: "64px !important",
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
  sidebarList: {
    padding: 0,
    height: "100%",
    marginBottom: 48
  },
  expandPanel: {
    width: "100%",
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    borderRadius: "0px !important",
  },
  listItem: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    padding: 0,
  },
  // activePanel:{
  //   backgroundColor:"#FFF"
  // },
  expandPanelContent: {
    margin: "0px !important",
    height: "64px",
    display: "flex",
    alignItems: "center"
  },
  panelTitle: {
    fontSize: "1rem",
    fontWeight: 600,
    width: "100%",
    padding: "0px 20px",
  },
  activePanelSum: {
    paddingLeft: '5px !important ',
    color: `${theme.palette.primary.main} !important`,
    // "&:hover": {
    //   color: `${theme.palette.primary.main}`,
    //   backgroundColor: `${theme.palette.primary.main}`
    // }
  },
  expandPanelSum: {
    padding: 0,
    margin: 0,
    minHeight: "4rem",
    color: theme.palette.text.primary,
    "&:hover": {
      paddingLeft: 5,
      color: theme.palette.primary.main,
      transition: "all 0.1s"
    },
  },
  expandPanelDetail: {
    flexDirection: "column",
    padding: 0,
  },
  heading: {
    fontSize: 18,
    textTransform: "capitalize"
  },
  mobileBackButton: {
    marginTop: theme.spacing(0.5),
    marginLeft: theme.spacing(3),
    [theme.breakpoints.only("sm")]: {
      marginTop: theme.spacing(0.625),
    },
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
  collapseBtn: {
    position: "fixed",
    bottom: 0,
    backgroundColor: "#FFF",
    textDecoration: "none",
    width: "240px",
    transition: "width 0.2s",
    display: "flex",
    alignItems: "center",
    boxSizing: "border-box",
    height: "48px",
    padding: "16px",
    border: 0,
    borderTop: "1px solid #eee",
    borderRight: "1px solid rgba(0, 0, 0, 0.12)",
    "&:hover, &:focus": {
      backgroundColor: "#eee",
      borderRight: "1px solid #eee"
    },
    justifyContent: "center",
  },
  isCollapsed: {
    width: "96px !important",
    padding: "16px"
  },
  listItemIcon: {
    color: theme.palette.text.primary,
    transition: theme.transitions.create("color"),
    width: 24,
    display: "flex",
    justifyContent: "center",
  },
  collapseText: {
    padding: 0,
    color: theme.palette.text.primary,
    transition: theme.transitions.create(["opacity", "color"]),
    fontSize: 16,
  },
  collapseTextHidden: {
    opacity: 0
  },
  collapseIcon: {
    transform: "rotate(180deg)",
    transition: "all 0.3s"
  },
  reverseCollapseIcon: {
    transform: "rotate(0deg)",
    transition: "all 0.3s"
  },
  sidebarHeader: {
    minHeight: 88,
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    transition: theme.transitions.create(["display", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  sidebarHeaderCollapsed: {
    flexDirection: "column",
    justifyContent: "center",
  },
  logoContainer: {
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 20,
    display: "flex",
    justifyContent: "flex-start",
    width: "100%",
    alignItems: "center",
    transition: theme.transitions.create(["display", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
    // [theme.breakpoints.down("xs")] :{
    //   display: "none",
    // }
  },
  showImgLogo: {
    marginLeft: 0,
    justifyContent: "center",
    transition: theme.transitions.create(["display", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  coverLogo: {
    transition: theme.transitions.create(["display", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  hideLogoOnMobile: {
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  }
}));
