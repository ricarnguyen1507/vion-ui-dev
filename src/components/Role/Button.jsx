import React, {useContext} from 'react';
import AddIcon from '@material-ui/icons/LibraryAdd';
import SaveIcon from '@material-ui/icons/Save';
import Button from "@material-ui/core/Button";
import useGeneral from 'pages/style_general';
import {Permission} from "context/UserContext";

// Button hiển thị : Add | Save
export default function ({actionType, disabled = false}) {
  const classge = useGeneral()
  const permission = useContext(Permission) // 0: Toàn quyền | 1: Xem | 2: Ẩn
  return (
    <Button
      type="submit"
      disabled={(permission !== 0) || disabled}
      startIcon={actionType === "Edit" ? <SaveIcon /> : <AddIcon />}
      color="primary" variant="contained"
      aria-label="add"
      className={classge.btnControlGeneral}
    >
      {actionType === "Edit" ? "Save" : actionType}
    </Button>
  )
}