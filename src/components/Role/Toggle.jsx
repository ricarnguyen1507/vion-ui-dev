import React, { useContext } from 'react'
import { Permission } from 'context/UserContext'

export default function ({children}) {
  const permission = useContext(Permission)
  return <>{(permission === 0 || permission === 3) && children}</>
}