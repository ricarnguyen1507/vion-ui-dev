import React, {useState} from 'react'
// import { makeStyles } from '@material-ui/core/styles';

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';

// import { blue } from '@material-ui/core/colors';

import Autocomplete from '@material-ui/lab/Autocomplete'
import TextField from '@material-ui/core/TextField'

/* const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
}); */

export default function SimpleDialog (props) {
  // const classes = useStyles();
  const {onClose, onNext, selectedBrandShop, setSelectedBrandShop, open, brandShop, defaultSelectUid = null } = props;

  const [selectedItem, setSelectedItem] = useState(() => selectedBrandShop)
  const handleClose = () => {
    onClose(selectedItem)
  };
  const handleNext = () => {
    onNext()
  };

  const onBrandShopChange = (e, item) => {
    setSelectedItem(item)
    setSelectedBrandShop(item)
  }

  return (
    <Dialog onClose={handleClose} open={open} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">Tạo Hybrid Layout</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {brandShop &&
            <Autocomplete
              // onChange={handleChangeBrandShop}
              options={brandShop}
              value={brandShop.find(b => b.uid === (selectedItem?.uid || defaultSelectUid))}
              getOptionLabel={option => (option.brand_shop_name || '')}
              style={{ width: "550px" }}
              onChange={onBrandShopChange}
              renderInput={params => (
                <TextField {...params}
                  label="Chọn Brand Shop"
                  fullWidth
                  variant="outlined"
                  margin="dense"
                />
              )}
            />
          }
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
            Cancel
        </Button>
        <Button onClick={handleNext} color="primary" autoFocus>
            Next
        </Button>
      </DialogActions>
    </Dialog>
  );
}