import React, { useMemo } from "react";
import {
  Route,
  Switch,
  // Redirect,
  withRouter,
} from "react-router-dom";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Header from "../Header";
import Sidebar from "../Sidebar";
// context
import { useLayoutState } from "context/LayoutContext";
import { useUserState, Permission } from "context/UserContext";
import { multiToOne } from "modules/listMenu";

function Layout (props) {
  var classes = useStyles();

  var layoutState = useLayoutState();
  var { role, permission } = useUserState();
  const routes = useMemo(() => {
    const routes = multiToOne()
    const _role = role.toLowerCase();
    const new_routes = []

    // Thêm permission vào routes
    if(role) {
      if(permission.length === 0) { // Support cho những user nào mà chưa có RoleFunction
        routes.map((item) => {
          if (!item.roles || item.roles.includes(_role)) {
            new_routes.push({ permission: 0, ...item})
          }
        })
      }
      else {
        routes.map((item) => {
          let idx = permission.findIndex(i => i.function_alias === item.alias.trim()) // Tìm index trong permission
          if (idx !== -1) {
            let x = permission[idx].permission // Lấy giá trị của permission ứng với page
            if(item.roles) { // Check trường hợp item có yêu cầu roles = "admin"
              if (item.roles.includes(_role) && parseInt(x) !== 2) {
                new_routes.push({ permission: x, ...item })
              }
            }
            else {
              if (parseInt(x) !== 2) {
                new_routes.push({ permission: x, ...item })
              }
            }
          }
        })
      }
    }
    return new_routes
  }, [role, permission]);

  return (
    <div className={classes.root}>
      <div className={classnames(classes.headerContainer, { [classes.headerContainerShift]: layoutState.isSidebarOpened })}>
        <Header history={props.history} />
      </div>
      <Sidebar />
      <div className={classnames(classes.content, { [classes.contentShift]: layoutState.isSidebarOpened })}>
        <div className={classes.fakeToolbar} />
        <Switch>
          {
            routes.map((route, i) => (
              <Route key={i} path={route.link} render={() => (<Permission.Provider value={route.permission}><route.com/></Permission.Provider>)}/>
            ))
          }
        </Switch>
      </div>
    </div>
  )
}

export default withRouter(Layout);
