import { makeStyles } from "@material-ui/styles";

export default makeStyles((theme) => ({
  root: {
    display: "flex",
    maxWidth: "100vw",
    overflowX: "hidden",
    backgroundColor: "#ecf0f1"
  },
  headerContainer: {
    flexGrow: 1,
    position: "fixed",
    zIndex: theme.zIndex.drawer + 1,
    paddingRight: 24,
    paddingLeft: 24,
    right: 0,
    width: "calc(100% - 97px)",
    transition: theme.transitions.create(["width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    }
  },
  headerContainerShift: {
    width: "calc(100% - 241px)",
    transition: theme.transitions.create(["width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    }
  },
  headerLoadingData: {
    width: "100%",
    transition: theme.transitions.create(["width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    paddingTop: 0,
    width: `calc(100% - 240px)`,
    minHeight: "100vh",
    overflowY: "hidden"
  },
  contentShift: {
    width: `calc(100% - ${240 + theme.spacing(6)}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  fakeToolbar: {
    ...theme.mixins.toolbar,
    minHeight: "88px !important"
  },
}));
