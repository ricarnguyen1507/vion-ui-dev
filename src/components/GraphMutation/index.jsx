import React, {
  useState
} from 'react'

import Autocomplete from '@material-ui/lab/Autocomplete'

/**
 * Validate input value
 * invalid: null, undefined, NaN
 * v == 0 => valid: "", false, 0, ...
 *
 * @param {any} v input value
 * @return {boolean} is input value valid
 */
function validValue (v) {
  return !!v || v == 0
}

let dateFormat = /^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$/i

// Components
export function CheckBoxEdge ({ Component, node, pred, ...props }) {
  const [state, setState] = useState(() => node.getState(pred) ?? false)
  const onChange = ({ target: { checked } }) => {
    setState(checked)
    node.setState(pred, checked)
  }
  return <Component {...props} name={pred} value={state} checked={state} onChange={onChange} />
}

export function InputEdge ({ Component, node, pred, onChange, ...props }) {
  const [state, setState] = useState(() => node.getState(pred) ?? "")
  function handleChange (e) {
    const { value } = e.target
    setState(value)
    switch (props?.typeInput) {
      case "date":
        node.setState(pred, dateFormat.test(value) ? new Date(value).getTime() : null)
        break;
      default:
        node.setState(pred, value)
    }
    typeof onChange === "function" && onChange(e)
  }
  // Dùng value thì TextValidator mới chạy, không dùng value
  return <Component {...props} name={pred} value={state} onChange={handleChange} />
}

export function OptionEdge ({ node, pred, val, onChange, options = [], ...props }) {
  const [state] = useState(() => node.getState(pred))
  const [item] = useState(() => {
    if (state) {
      if (props.multiple) {
        return state.map(s => options.find(o => o.uid === s.state.uid))
      } else {
        return state?.uid ? options.find(o => state.uid === o.uid) : state
      }
    }
    return val ?? (props.multiple ? [] : "")
  })

  function handleChange (e, data) {
    if (props.multiple) {
      const items = data.map(i => ({ uid: i.uid }))
      if (state) {
        const newItems = items.filter(i => !state.find(s => s.state.uid === i.uid))
        node.setState(pred, [...state, ...newItems])
        for (let s of state) {
          s.isDeleted = items.findIndex(i => i.uid === s.state.uid) === -1
        }
      } else {
        node.setState(pred, items)
      }
    } else if(data) {
      node.setState(pred, data)
    }
    typeof onChange === "function" && onChange(e)
  }

  return (<Autocomplete
    onChange={handleChange}
    options={options}
    defaultValue={item}
    {...props}
  />)
}

export function ListEdge ({ Component, node, pred, data = [], ...props }) {
  const [edge] = useState(() => node.getEdge(pred, data))
  const [edges, setEdges] = useState(() => edge.state)
  const updateList = () => {
    edge.state = edge.state.filter(e => !(e.state.isNew && e.isDeleted))
    setEdges(edge.state.filter(e => !e.isDeleted))
  }
  const addItem = (itemData, type) => {
    node.addNode(pred, itemData, type)
    updateList()
  }
  return <Component addItem={addItem} items={edges} updateList={updateList} {...props} />
}

// ----------------------------

// Predicate Type Enum
export const PredTypes = {
  VALUE: 0,
  NODE: 1,
  ARRAY: 2
}

export const NodeTypes = {
  NORMAL: 0,
  ORPHAN: 1
}

export class Edge {
  constructor (state, isNew = true) {
    if (Array.isArray(state)) {
      this.type = PredTypes.ARRAY
    } else {
      this.type = (state && typeof state === 'object') ? PredTypes.NODE : PredTypes.VALUE
    }
    this.deleted = false
    this.selected = false
    this.state = transData(state, isNew)
    if (!isNew) {
      this.iState = transData(state, isNew)
    }
  }
  get isDeleted () {
    return this.deleted || this.state === null
  }
  set isDeleted (status) {
    this.deleted = status
  }
  valueChanged () {
    return ((validValue(this.iState) || validValue(this.state)) && (this.state !== this.iState))
  }
  nodeChanged () {
    return this.iState?.uid !== this.state?.uid
  }
}

export class Node {
  constructor (props, type = NodeTypes.NORMAL) {
    if (!(props.uid && (/^(0x\S+|_:\S+)$/).test(props.uid))) {
      throw new Error("Invalid or missing uid")
    }
    this.nodeType = type
    this.isNew = isNewNode(props)
    if (this.isNew && !(props["dgraph.type"] && (/^\S+$/).test(props["dgraph.type"]))) {
      throw new Error("Missing dgraph.type")
    }
    const { uid, ...nodeData } = props
    this.uid = uid
    this.nodeData = transDataToEdges(nodeData, this.isNew)
  }
  addNode (predName, nodeData, type) {
    if (predName in this.nodeData) {
      this.nodeData[predName].state.push(new Edge(new Node(nodeData, type)))
    } else {
      this.getEdge(predName, [nodeData])
    }
  }
  getUid (predName) {
    return this.nodeData?.[predName]?.state?.uid
  }
  getLength (predName) {
    return this.nodeData?.[predName]?.state?.length || 0
  }
  getState (predName) {
    return this.nodeData?.[predName]?.state
  }
  setState (predName, value, isNew = true) {
    if (predName in this.nodeData) {
      this.nodeData[predName].state = transData(value, isNew)
    } else {
      this.nodeData[predName] = new Edge(value, isNew)
    }
  }
  getEdge (predName, data, isNew = true) {
    if (!(predName in this.nodeData)) {
      if (data) {
        this.nodeData[predName] = data instanceof Edge ? data : new Edge(data, isNew)
      } else {
        throw new Error("No init data")
      }
    }
    return this.nodeData[predName]
  }
  getMutationObj () {
    const set = [], del = []
    for (let [pred, obj] of Object.entries(this.nodeData)) {
      buildMutations(this, pred, obj, set, del)
    }
    return { set: set.join("\r\n"), del: del.join("\r\n") }
  }
  getMutationForm () {
    const f = new FormData()
    for (let [k, v] of Object.entries(this.getMutationObj())) {
      f.set(k, v)
    }
    return f
  }
}

// --- Utils ---

function escStrVal (str) {
  return str.replace(/\\([\s\S])|(")/g, "\\$1$2").replace(/(\r\n|\n)/g, "\\n").trim();
}

function transUID (node) {
  return node.isNew && node.uid.startsWith('_:') ? node.uid : `<${node.uid}>`
}

function buildMutations (subject, pred, object, set, del) {
  const uid = transUID(subject)
  const { isDeleted, state, iState } = object
  if (object.type === PredTypes.VALUE) {
    if (isDeleted) {
      del.push(`${uid} <${pred}> * .`)
    } else if (object.valueChanged()) {
      if (pred.includes('|')) {
        set.push(`${uid} ${object.state}`)
      } else {
        set.push(`${uid} <${pred}> "${typeof state !== 'string' ? state : escStrVal(state)}" .`)
      }
    }
  }
  if (object.type === PredTypes.NODE) {
    if (isDeleted && iState) {
      del.push(`${uid} <${pred}> <${iState.uid}> .`)
      if (iState.type === NodeTypes.ORPHAN) {
        del.push(`<${iState.uid}> * * .`)
      }
    }
    if (!isDeleted) {
      if (state.isNew || object.nodeChanged() || object.selected) {
        set.push(`${uid} <${pred}> <${state.uid}> .`)
      }
      for (let [_pred, _object] of Object.entries(state.nodeData)) {
        if (_pred.includes('|')) {
          set.push(`${uid} ${_object.state}`)
        } else {
          buildMutations(state, _pred, _object, set, del)
        }
      }
    }
  }
  if (object.type === PredTypes.ARRAY) {
    if (isDeleted) {
      del.push(`<${uid}> <${pred}> * .`)
      for (let o of object.state) {
        if (o.iState?.type === NodeTypes.ORPHAN) {
          o.isDeleted = true
        }
      }
    }
    for (let _object of object.state) {
      buildMutations(subject, pred, _object, set, del)
    }
  }
}

function isNewNode (props) {
  return !props?.uid || props.uid.startsWith('_:') || props?.renew === true
}

function transDataToEdges (nodeData, isNew = true) {
  for (let [predName, edgeData] of Object.entries(nodeData)) {
    nodeData[predName] = edgeData instanceof Edge ? edgeData : new Edge(edgeData, isNew)
  }
  return nodeData
}

function transData (data, isNew = true) {
  if (Array.isArray(data)) {
    return data.map(v => v instanceof Edge ? v : new Edge(v, isNew))
  } else if (data && typeof data === 'object' && !(data instanceof Node)) {
    return new Node(data)
  }
  return data
}