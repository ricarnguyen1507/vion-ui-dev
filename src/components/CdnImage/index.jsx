import React from 'react'
import { scaleImageUrl, emptyImage } from 'utils'

const { image_url, video_url } = window.appConfigs

export function genCdnUrl (uri, thumb = emptyImage, type = 'image') {
  return uri ? `${type === 'image' ? image_url : video_url}/${uri}` : thumb
}

export default function ({src, thumb = emptyImage, style, ...props}) {
  const scaledSrc = !src ? thumb : genCdnUrl(style ? scaleImageUrl(src, style.width, style.height) : src)
  return <img style={style} src={scaledSrc} alt={src} {...props} />
}