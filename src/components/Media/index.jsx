import React, { useEffect, useState, useRef } from 'react'
import hashMD5 from 'modules/md5'
import { scaleImageUrl, emptyImage, corrupted_img } from "utils"

let count = 0

function scaleImage (src, width, height, callback) {
  if(src instanceof File && src.type.startsWith('image')) {
    const fileReader = new FileReader()
    fileReader.onerror = function () {
      callback(false, 'Không đọc được file' + src.name, fileReader.error)
      console.log(fileReader.error)
    }
    fileReader.onload = function (e) {
      const base64Img = e.target.result
      const img = new Image()
      img.onerror = function () {
        callback(false, "Không load được image")
      }
      img.onload = function ({target}) {
        if(!target.complete || (typeof target.naturalWidth != "undefined" && target.naturalWidth === 0)) {
          callback(false, "File ảnh đã bể")
        } else {
          const canvas = document.createElement('canvas')
          canvas.width = width
          canvas.height = height
          const ctx = canvas.getContext('2d')
          try {
            ctx.drawImage(img, 0, 0, width, height)
            callback({
              base64Img: canvas.toDataURL(src.type)
            })
          } catch(err) {
            console.log(err)
            callback(false, 'drawImage failed')
          }
        }
      }
      img.src = base64Img
    }
    fileReader.readAsDataURL(src)
  } else {
    callback(false, 'Đây không phải file ảnh')
  }
}

function Media ({type, src}) {
  if(src && type === 'video') {
    return <video style={{ width: "100%", height: "100%" }} src={src} />
  }
  return <img src={src} style={{width: "100%", height: "100%" }} alt={"media"} />
}

export default function ({ style, field, src = emptyImage, accept, type, fileHandle, validateForm }) {
  const [media, setMedia] = useState(() => ({type, src}), [])
  const [tmp_id] = useState(`tmp_id_${count++}`)
  const imgCon = useRef(null)
  const fileInput = useRef(null)
  useEffect(() => {
    function keepSilent (e) {
      e.stopPropagation(); e.preventDefault(); return false
    }
    function onDrop (e) {
      let files = [...(e.dataTransfer ?? e.target).files].filter(f => f.type.startsWith('image') || f.type.startsWith('video'))
      if(files.length > 0) {
        onChange({target: {files}})
      }
      return keepSilent(e)
    }
    function onChange (e) {
      const file = e.target.files[0]
      if(file && file.type.startsWith('video')) {
        setMedia({
          type: "video",
          src: window.URL.createObjectURL(file)
        })
        file.arrayBuffer().then(buffer => {
          file.md5 = hashMD5(buffer)
          fileHandle(field, file, src)
          // console.log("image md5", file.md5)
        })
        // fileHandle(field, file, src)
      } else if(file && file.type.startsWith('image')) {
        const {clientWidth, clientHeight} = imgCon.current
        scaleImage(file, clientWidth, clientHeight, (res, msg) => {
          if(res) {
            setMedia({type: "image", src: res.base64Img})
            file.arrayBuffer().then(buffer => {
              file.md5 = hashMD5(buffer)
              fileHandle(field, file, src)
            })
          } else {
            setMedia({type: "image", src: corrupted_img})
            alert(msg)
          }
        })
      }
    }
    const imgElm = imgCon.current
    const fileElm = fileInput.current
    imgElm.addEventListener('dragover', keepSilent, false)
    imgElm.addEventListener('dragleave', keepSilent, false)
    imgElm.addEventListener('drop', onDrop, false)
    fileElm.addEventListener('change', onChange, false)
    return function () {
      imgElm.removeEventListener('dragover', keepSilent)
      imgElm.removeEventListener('dragleave', keepSilent)
      imgElm.removeEventListener('drop', onDrop)
      fileElm.removeEventListener('change', onChange)
    }
  }, [])
  useEffect(() => {
    if(type === 'image') {
      const {clientWidth, clientHeight} = imgCon.current
      setMedia({
        type: "image",
        src: src.startsWith("data:") ? src : scaleImageUrl(src, clientWidth, clientHeight)
      })
    }
  }, [type, src])

  return <>
    <div ref={imgCon} style={{...style, lineHeight: 0}}>
      <input ref={fileInput} id={tmp_id} accept={accept} style={{display: "none"}} type="file" />
      <label htmlFor={tmp_id}>
        <div style={{ width: "100%", height: "100%" }}><Media {...media} /></div>
      </label>
    </div>
    {validateForm && validateForm.helperText ? <label style={{color: "red"}}>{validateForm.helperText}</label> : ""}
  </>
}