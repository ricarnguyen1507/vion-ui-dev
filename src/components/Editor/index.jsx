import React from 'react'
import EditorJs from '@natterstefan/react-editor-js'
import Button from "@material-ui/core/Button";
import SaveIcon from '@material-ui/icons/Save';

export default function ({ data, dataEdit, setDataEdit, fieldName = null, node }) {
  let editor = null

  const onSave = async () => {
    // https://editorjs.io/saving-data
    try {
      const outputData = await editor.save()
      setDataEdit({ ...dataEdit, [fieldName]: JSON.stringify(outputData) })
      node.setState(fieldName, JSON.stringify(outputData), true)
    } catch (e) {
      console.log('Saving failed: ', e)
    }
  }
  return (
    <div>
      {/* docs: https://editorjs.io/configuration */}
      <EditorJs
        data={data && JSON.parse(data)}
        // will be `editorjs` by default
        holder="custom-editor-container"
        editorInstance={editorInstance => {
          // invoked once the editorInstance is ready
          editor = editorInstance
        }}
      >
        <div id="custom-editor-container" />
      </EditorJs>
      <Button
        color="primary"
        variant="contained"
        aria-label="add"
        // className={classge.btnSubmit}
        type="button"
        onClick={onSave}
      >
        <><SaveIcon />Lưu nội dung</>
      </Button>
    </div>
  )
}