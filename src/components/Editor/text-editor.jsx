import React, { useState } from "react";
import { Editor } from "@tinymce/tinymce-react";
import hashMD5 from 'modules/md5';
import {debounce} from 'lodash-es'

export default function ({ node, pred, files, imageName = "" }) {
  const { image_url } = window.appConfigs;

  const [initialValue] = useState(node.getState(pred) || ""); // Hoàng: Lưu đường dẫn tĩnh thì không cần parse nha tục tưng
  /* const [initialValue] = useState(() => {
    const htmlDescVal = node?.getState(pred);
    if (htmlDescVal && htmlDescVal.length) {
      return htmlDescVal.replace(/ src="([^http]\S+)" /gm, `${image_url}/images/${imageName || 'products'}/$1`) ?? ""; // Hoàng: revert code a lại thiếu $1 nhé tục tưng
    }
  }, [node]) */

  const [onEditorChange] = useState(() => debounce(content => {
    // Bảo lúa: truyền cái tham số imageName vào mà lấy image path bằng 3 ngôi
    // Hóa ra cái text-editor này chỉ xài được cho promotion hoặc products thôi àh
    const parsedContent = content.replace(/ src="([^http]\S+)" /gm, " ").replace(/ title="(\S+)" /gm, ` src="${image_url}/images/${imageName || 'products'}/$1" `);
    node.setState(pred, parsedContent);
  }, 500))

  const onImage = (md5, file) => {
    files[md5] = file
  }

  return (
    <Editor
      id={node.uid}
      apiKey="rcoc7voym57z1vb7ux9496epmh0a8dd0gruugzthgwvh2rdw"
      initialValue={initialValue}
      init={{
        height: 200,
        menubar: true,
        plugins: 'image code',
        image_title: true,
        image_id: true,
        toolbar: 'undo redo | link image  | code | formatselect | bold italic backcolor |' +
          'alignleft aligncenter alignright alignjustify |' +
          'bullist numlist outdent indent | removeformat | help',
        automatic_uploads: true,
        file_picker_types: 'file image media',
        file_picker_callback: function (cb) {
          // Tạo input tag để chọn file hình
          var input = document.createElement('input');
          input.setAttribute('type', 'file');
          input.setAttribute('accept', 'image /*');

          input.onchange = function () {
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
              file.arrayBuffer().then(buffer => {
                const md5 = hashMD5(buffer)
                const ext = file.type.split("/")[1]
                cb(e.target.result, { title: `${md5}.${ext}`});
                onImage(md5, file)
              })
            };
            reader.readAsDataURL(file);
          };
          input.click();
        },
      }}
      onEditorChange={onEditorChange}
    />
  );
}
