import React, {useState} from 'react';
import api from 'services/api_cms';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import LockOpen from '@material-ui/icons/LockOpen';
import Lock from '@material-ui/icons/Lock';

import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Grid from '@material-ui/core/Grid'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';

const useStyles = makeStyles(() => ({
  appBar: {
    position: 'relative',
  },
  title: {
    flex: 1,
    color: "#fff"
  },
  error: {
    textAlign: "center"
  },
  // Button
  rootSubmit: {
    textAlign: "center",
  },
  btn: {
    margin: "10px 0px 20px",
  },
  inputTxt: {
    height: 75
  }
}));

export default function ({open, handleClose, uid, signOut }) {
  const classes = useStyles();

  const [dataEdit, setDataEdit] = useState({password_old: "", password_new: "", password_renew: ""})
  const [showPassword, setShowPassword] = useState({password_old: false, password_new: false, password_renew: false})
  // Button submit ( mode + list check )
  const [disableMode, setDisableMode] = useState(true)
  const [listError, setListError] = useState([false, false, false])
  // Error ( UI + content )
  const [display, setDisplay] = useState("none")
  const [contentError, setContentError] = useState("")

  const handleChange = (e) => {
    e.preventDefault()
    const { name, value } = e.target
    setDataEdit({ ...dataEdit, [name]: value })
  }

  const handleSubmit = () => {
    if(dataEdit.password_new === dataEdit.password_renew) {
      setContentError("")
      api.post('/user/password/change', { $password_old: dataEdit.password_old, $password_new: dataEdit.password_new, $uid: uid}).then(res => {
        if(res.data?.statusCode === 200) {
          alert("Đổi password thành công !")
          signOut()
        }
        else if(res.data?.statusCode === 401) {
          setDisplay("inline")
          setContentError("Old password is wrong !")
        }
      }).catch(err => { console.log(err) })
    }
    else{
      setDisplay("inline")
      setContentError("The new password doesn't match !")
    }
  }

  const handleOnClose = () => {
    setContentError("")
    setDataEdit({ password_old: "", password_new: "", password_renew: ""})
    handleClose()
  }

  const handleValidatorListener = (mode, number) => {
    listError[number] = mode
    setListError([...listError])
    if(listError.findIndex(i => i === false) === -1) {
      setDisableMode(false)
    }
    else{
      setDisableMode(true)
    }
  }

  // View ô mật khẩu
  const handleClickShowPasswordOld = () => {
    setShowPassword({...showPassword, password_old: !showPassword.password_old})
  }
  const handleClickShowPasswordNew = () => {
    setShowPassword({...showPassword, password_new: !showPassword.password_new})
  }
  const handleClickShowPasswordReNew = () => {
    setShowPassword({...showPassword, password_renew: !showPassword.password_renew})
  }

  return (
    <>
      <Dialog fullWidth={true} maxWidth="sm" open={open} >
        <ValidatorForm onSubmit={handleSubmit} >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <Typography variant="h6" className={classes.title}> Change Password </Typography>
              <IconButton edge="start" color="inherit" onClick={handleOnClose} aria-label="close"> <CloseIcon /> </IconButton>
            </Toolbar>
          </AppBar>
          <DialogContent>
            <DialogContentText> Enter the old password and re-enter new password ! </DialogContentText>
            <Grid container spacing={2} >
              <Grid item xs={12} sm={12} lg={12} className={classes.inputTxt}>
                <TextValidator
                  id="password_old"
                  name="password_old"
                  label="Old password"
                  variant="outlined"
                  type={showPassword.password_old ? 'text' : 'password'}
                  margin="dense"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.password_old || ""}
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> <Lock/> </InputAdornment>,
                    endAdornment: <InputAdornment position="end">
                      <IconButton edge="end" onClick={handleClickShowPasswordOld} >
                        {showPassword.password_old ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }}
                  validators={[
                    'required',
                    "minStringLength:3",
                    'maxStringLength:20',
                    // 'matchRegexp:[0-9]'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min password character is 3',
                    'Max password character is 20',
                    // 'Number only'
                  ]}
                  validatorListener={isValid => handleValidatorListener(isValid, 0)}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12} className={classes.inputTxt}>
                <TextValidator
                  id="password_new"
                  name="password_new"
                  label="New password"
                  variant="outlined"
                  type={showPassword.password_new ? 'text' : 'password'}
                  margin="dense"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.password_new || ""}
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> <LockOpen/> </InputAdornment>,
                    endAdornment: <InputAdornment position="end">
                      <IconButton edge="end" onClick={handleClickShowPasswordNew} >
                        {showPassword.password_new ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }}
                  validators={[
                    'required',
                    "minStringLength:3",
                    'maxStringLength:20',
                    // 'matchRegexp:[0-9]'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min password character is 3',
                    'Max password character is 20',
                    // 'Number only'
                  ]}
                  validatorListener={isValid => handleValidatorListener(isValid, 1)}
                />
              </Grid>
              <Grid item xs={12} sm={12} lg={12} className={classes.inputTxt}>
                <TextValidator
                  id="outlined-basic"
                  name="password_renew"
                  label="Re-enter a new password"
                  variant="outlined"
                  type={showPassword.password_renew ? 'text' : 'password'}
                  margin="dense"
                  fullWidth
                  onChange={handleChange}
                  value={dataEdit.password_renew || ""}
                  InputProps={{
                    startAdornment: <InputAdornment position="start"> <LockOpen/> </InputAdornment>,
                    endAdornment: <InputAdornment position="end">
                      <IconButton edge="end" onClick={handleClickShowPasswordReNew} >
                        {showPassword.password_renew ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  }}
                  validators={[
                    'required',
                    "minStringLength:3",
                    'maxStringLength:20',
                    // 'matchRegexp:[0-9]'
                  ]}
                  errorMessages={[
                    'This field is required',
                    'Min password character is 3',
                    'Max password character is 20',
                    // 'Number only'
                  ]}
                  validatorListener={isValid => handleValidatorListener(isValid, 2)}
                />
              </Grid>
              {/* Hiển thị error chung */}
              <Grid item xs={12} sm={12} lg={12} >
                <div className={classes.error}> <h4 style={{display: `${display}`, color: "red"}}> {contentError} </h4> </div>
              </Grid>
            </Grid>
          </DialogContent>
          <Grid className={classes.rootSubmit}>
            <Button type="submit" variant="contained" color="primary" className={classes.btn} disabled={disableMode}> Save Change </Button>
          </Grid>
        </ValidatorForm>
      </Dialog>
    </>
  );
}
