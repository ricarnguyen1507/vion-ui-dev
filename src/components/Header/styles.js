import { makeStyles } from "@material-ui/styles";
import { alpha } from "@material-ui/core/styles/colorManipulator";

export default makeStyles(theme => ({
  logotype: {
    color: "white",
    marginLeft: theme.spacing(2.5),
    marginRight: theme.spacing(2.5),
    fontWeight: 500,
    fontSize: 18,
    whiteSpace: "nowrap",
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
  appBar: {
    position: "relative",
    backgroundColor: "#FFF",
    borderRadius: "0 0 .5rem .5rem",
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen * 2,
    }),
  },
  toolbar: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    flexGrow: 1
  },
  hide: {
    display: "none",
  },
  grow: {
    flexGrow: 1,
  },
  gridContainer: {
    alignItems: "center",
    flexDirection: "row"
  },
  leftGridItem: {
    display: "inherit",
    justifyContent: "flex-start",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      justifyContent: "space-around",
    }
  },
  rightGridItem: {
    display: "inherit",
    justifyContent: "flex-end",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      justifyContent: "space-around",
    }
  },
  searchBox: {
    position: "relative",
    borderRadius: 25,
    color: "#FFF",
    paddingLeft: theme.spacing(2.5),
    marginRight: 8,
    width: 36,
    backgroundColor: alpha(theme.palette.common.black, 0),
    transition: theme.transitions.create(["background-color", "width"]),
    "&:hover": {
      cursor: "pointer",
      backgroundColor: "rgba(8,164,149,0.08)",
      transform: "scale(1.1)"
    },
  },
  searchBoxFocused: {
    backgroundColor: "rgb(8,164,149)",
    "&:hover": {
      backgroundColor: "rgb(8,164,149)",
      transform: "none"
    },
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: 250,
    },
  },
  searchField: {
    width: 36,
    right: 0,
    height: "100%",
    position: "absolute",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    transition: theme.transitions.create("right"),
    "&:hover": {
      cursor: "pointer",
    },
  },
  searchFieldOpened: {
    right: theme.spacing(1.25),
  },
  searchIcon: {
    color: theme.palette.primary.main
  },
  searchIconOpened: {
    color: "#FFF !important"
  },
  inputRoot: {
    color: "inherit",
    width: "100%",
  },
  inputInput: {
    height: 36,
    padding: 0,
    paddingRight: 36 + theme.spacing(1.25),
    width: "100%",
  },
  messageContent: {
    display: "flex",
    flexDirection: "column",
  },
  headerMenu: {
    marginTop: theme.spacing(7),
  },
  headerMenuList: {
    display: "flex",
    flexDirection: "column",
  },
  headerMenuItem: {
    "&:hover, &:focus": {
      backgroundColor: theme.palette.background.light,
    },
  },

  headerMenuButton: {
    color: "#FFF",
    padding: "7px",
    margin: "0px 8px",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "rgba(8,164,149,0.08)",
      transform: "scale(1.1)"
    }
  },
  headerMenuButtonCollapse: {
    marginRight: theme.spacing(2),
  },
  menuButton: {
    padding: 7,
    "&:hover": {
      backgroundColor: "rgba(8,164,149,0.08)"
    }
  },
  headerIcon: {
    color: theme.palette.primary.main
  },
  headerIconCollapse: {
    color: "#FFF",
  },
  profileMenu: {
    minWidth: 265,
  },
  profileMenuUser: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing(2),
    "&:hover, &:focus": {
      backgroundColor: theme.palette.background.light,
    },
  },
  profileMenuItem: {
    color: theme.palette.text.primary,
  },
  profileMenuIcon: {
    marginRight: theme.spacing(2),
    color: theme.palette.text.hint,
  },
  profileMenuLink: {
    fontSize: 16,
    textDecoration: "none",
    "&:hover": {
      cursor: "pointer",
    },
  },
  messageNotification: {
    height: "auto",
    display: "flex",
    alignItems: "center",
    "&:hover, &:focus": {
      backgroundColor: theme.palette.background.light,
    },
  },
  messageNotificationSide: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginRight: theme.spacing(2),
  },
  messageNotificationBodySide: {
    alignItems: "flex-start",
    marginRight: 0,
  },
  sendMessageButton: {
    margin: theme.spacing(4),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    textTransform: "none",
  },
  sendButtonIcon: {
    marginLeft: theme.spacing(2),
  },
  profileIcon: {
    color: theme.palette.warning.main,
    marginRight: theme.spacing(2)

  },
  taskIcon: {
    color: theme.palette.success.main,
    marginRight: theme.spacing(2)

  },
  messageIcon: {
    color: theme.palette.secondary.main,
    marginRight: theme.spacing(2)
  },
  signOutBtn: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: theme.spacing(2),
    cursor: "pointer",
    "&:hover, &:focus": {
      backgroundColor: theme.palette.background.light,
    },
  },
  signOutIcon: {
    color: theme.palette.primary.main,
    marginRight: theme.spacing(2)
  },
  signOutText: {
    fontSize: "16px !important",
    color: theme.palette.primary.main,
  },
  divider: {
    margin: "5px 0px"
  },
  avatar: {
    width: 35,
    height: 35
  },
}));
