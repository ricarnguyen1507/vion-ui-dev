import React, { useState } from "react";
import { AppBar, Toolbar, IconButton, Grid, Menu, MenuItem, Fab, Divider, Avatar} from "@material-ui/core";
import {
  FormatListBulleted as TaskIcon,
  Send as SendIcon,
  ExitToApp as SignOutIcon,
  Menu as ExpandMenu,
  // MenuOpen as MenuOpenIcon,
} from "@material-ui/icons";

import classNames from "classnames";
import useStyles from "./styles";
import { Typography } from "../Wrappers/Wrappers";
import Notification from "../Notification/Notification";
import UserAvatar from "../UserAvatar/UserAvatar";
import avatar from "images/admin_avatar.jpg";
import ChangePassword from './Account/ChangePassword';

import { useUserState, useUserDispatch, signOut } from "context/UserContext";
import { toggleSidebar, useLayoutDispatch } from "context/LayoutContext";

const messages = [
  {
    id: 0,
    variant: "warning",
    name: "Jane Hew",
    message: "Hey! How is it going?",
    time: "9:32",
  },
  {
    id: 1,
    variant: "success",
    name: "Lloyd Brown",
    message: "Check out my new Dashboard",
    time: "9:18",
  },
  {
    id: 2,
    variant: "primary",
    name: "Mark Winstein",
    message: "I want rearrange the appointment",
    time: "9:15",
  },
  {
    id: 3,
    variant: "secondary",
    name: "Liana Dutti",
    message: "Good news from sale department",
    time: "9:09",
  },
];

const notifications = [
  { id: 0, color: "warning", message: "Check out this awesome ticket" },
  {
    id: 1,
    color: "success",
    type: "info",
    message: "What is the best way to get ...",
  },
  {
    id: 2,
    color: "secondary",
    type: "notification",
    message: "This is just a simple notification",
  },
  {
    id: 3,
    color: "primary",
    type: "e-commerce",
    message: "12 new orders has arrived today",
  },
];

export default function Header (props) {
  var classes = useStyles();

  var { uid, username } = useUserState();
  var userDispatch = useUserDispatch();
  var layoutDispatch = useLayoutDispatch();

  var [mailMenu, setMailMenu] = useState(null);
  // var [isMailsUnread, setIsMailsUnread] = useState(true);
  var [notificationsMenu, setNotificationsMenu] = useState(null);
  // var [isNotificationsUnread, setIsNotificationsUnread] = useState(true);
  var [profileMenu, setProfileMenu] = useState(null);
  // var [isSearchOpen, setSearchOpen] = useState(false);


  // Xử lý Dialog của password
  const [dialogPassword, setDialogPassword] = useState(false);
  const handleOpen = () => {
    setDialogPassword(true)
  }
  const handleClose = () => {
    setDialogPassword(false);
  };

  return (
    <>
      <ChangePassword open={dialogPassword} handleClose={handleClose} uid={uid} signOut={signOut} />
      <AppBar className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Grid container className={classes.gridContainer}>
            <Grid item xs={2} sm={6} lg={6} className={classes.leftGridItem}>
              <IconButton onClick={() => toggleSidebar(layoutDispatch)} className={classes.menuButton} >
                <ExpandMenu color="primary" />
              </IconButton>
            </Grid>
            <Grid item xs={10} sm={6} lg={6} className={classes.rightGridItem}>
              {/* <div className={classNames(classes.searchBox, { [classes.searchBoxFocused]: isSearchOpen }, "animation-btn")} >
                                <div
                                    className={classNames(classes.searchField, { [classes.searchFieldOpened]: isSearchOpen },)}
                                    onClick={() => setSearchOpen(!isSearchOpen)}
                                >
                                    <SearchIcon className={classNames(classes.searchIcon, { [classes.searchIconOpened]: isSearchOpen })} />
                                </div>
                                <InputBase placeholder="Search…" classes={{ root: classes.inputRoot, input: classes.inputInput, }} />
                            </div>
                            <IconButton
                                color="primary"
                                aria-haspopup="true"
                                aria-controls="mail-menu"
                                onClick={(e) => { setNotificationsMenu(e.currentTarget); setIsNotificationsUnread(false); }}
                                className={`${classes.headerMenuButton} animation-btn`}
                            >
                                <Badge badgeContent={ isNotificationsUnread ? notifications.length : null } color="warning" >
                                    <NotificationsIcon classes={{ root: classes.headerIcon }} />
                                </Badge>
                            </IconButton>
                            <IconButton
                                color="primary"
                                aria-haspopup="true"
                                aria-controls="mail-menu"
                                onClick={(e) => { setMailMenu(e.currentTarget); setIsMailsUnread(false); }}
                                className={`${classes.headerMenuButton} animation-btn`}
                            >
                                <Badge badgeContent={isMailsUnread ? messages.length : null} color="secondary" >
                                    <MailIcon classes={{ root: classes.headerIcon }} />
                                </Badge>
                            </IconButton> */}
              <IconButton
                aria-haspopup="true"
                color="primary"
                className={classes.headerMenuButton}
                aria-controls="profile-menu"
                onClick={(e) => setProfileMenu(e.currentTarget)}
              >
                {/* <AccountIcon classes={{ root: classes.headerIcon }} /> */}
                <Avatar alt="Admin" src={avatar} className={classes.avatar} />
              </IconButton>
              <Menu
                id="mail-menu"
                open={Boolean(mailMenu)}
                anchorEl={mailMenu}
                onClose={() => setMailMenu(null)}
                MenuListProps={{ className: classes.headerMenuList }}
                className={classes.headerMenu}
                classes={{ paper: classes.profileMenu }}
                disableAutoFocusItem
              >
                <div className={classes.profileMenuUser}>
                  <Typography variant="h4" weight="medium"> New Messages </Typography>
                  <Typography className={classes.profileMenuLink} component="a" color="secondary" >
                    {messages.length} New Messages
                  </Typography>
                </div>
                {messages.map((message) => (
                  <MenuItem key={message.id} className={classes.messageNotification} >
                    <div className={classes.messageNotificationSide}>
                      <UserAvatar color={message.variant} name={message.name} />
                      <Typography size="sm" color="text" colorBrightness="secondary" > {message.time} </Typography>
                    </div>
                    <div className={classNames(classes.messageNotificationSide, classes.messageNotificationBodySide)} >
                      <Typography weight="medium" gutterBottom> {message.name} </Typography>
                      <Typography color="text" colorBrightness="secondary"> {message.message} </Typography>
                    </div>
                  </MenuItem>
                ))}
                <Fab variant="extended" color="primary" aria-label="Add" className={classes.sendMessageButton} >
                                    Send New Message <SendIcon className={classes.sendButtonIcon} />
                </Fab>
              </Menu>
              <Menu
                id="notifications-menu"
                open={Boolean(notificationsMenu)}
                anchorEl={notificationsMenu}
                onClose={() => setNotificationsMenu(null)}
                className={classes.headerMenu}
                disableAutoFocusItem
              >
                {notifications.map((notification) => (
                  <MenuItem key={notification.id} onClick={() => setNotificationsMenu(null)} className={classes.headerMenuItem} >
                    <Notification {...notification} typographyVariant="inherit" />
                  </MenuItem>
                ))}
              </Menu>
              <Menu
                id="profile-menu"
                open={Boolean(profileMenu)}
                anchorEl={profileMenu}
                onClose={() => setProfileMenu(null)}
                className={classes.headerMenu}
                classes={{ paper: classes.profileMenu }}
                disableAutoFocusItem
              >
                <div className={classes.profileMenuUser}>
                  <Typography variant="h4" weight="medium"> Hi ! {username} </Typography>
                  {/* <Typography className={classes.profileMenuLink} component="a" color="primary" href="https://flatlogic.com" >
                                        Flalogic.com
                                    </Typography> */}
                </div>
                {/* <MenuItem className={classNames( classes.profileMenuItem, classes.headerMenuItem, )} >
                                    <AccountIcon className={classes.profileIcon} /> Profile
                                </MenuItem> */}
                <MenuItem className={classNames(classes.profileMenuItem, classes.headerMenuItem,)} onClick={handleOpen}>
                  <TaskIcon className={classes.taskIcon} /> Change password
                </MenuItem>
                {/* <MenuItem className={classNames( classes.profileMenuItem, classes.headerMenuItem, )} >
                                    <MessageIcon className={classes.messageIcon} /> Messages
                                </MenuItem> */}
                <Divider className={classes.divider} />
                <div className={classes.signOutBtn} onClick={() => signOut(userDispatch, props.history)} >
                  <SignOutIcon className={classes.signOutIcon} />
                  <Typography className={classes.signOutText} color="primary"> Sign Out </Typography>
                </div>
              </Menu>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </>
  );
}
