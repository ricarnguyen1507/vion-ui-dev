// Localhost Dev
window.appConfigs = {
  api_url: './api',
  video_url: './cdn',
  image_url: ''
}

// Dev Api Proxy
/* window.appConfigs = {
  api_url: './api',
  image_url: 'https://static.shoppingtv.vn/dev',
  video_url: 'https://tvcommerce-st.fptplay.net/dev'
} */

// Production Live
// window.appConfigs = {
//   api_url: 'http://prod-api.localhost/api',
//   video_url: 'https://tvcommerce-st.fptplay.net/prod',
//   image_url: 'https://tvcommerce-st.fptplay.net/prod'
// }